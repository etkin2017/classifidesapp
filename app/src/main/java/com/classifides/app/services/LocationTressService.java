package com.classifides.app.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.classifides.app.BrowseByCategory;
import com.classifides.app.HomeActivity;
import com.classifides.app.HomeScreen;
import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.classifides.app.data.ComplexPreferences;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.model.locations;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class LocationTressService extends Service {
    private static final String TAG = "Classifieds";
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10f;
    private String loc = "";
    String City = "";
    String State = "";
    public static final String name = "myAction";

    Bundle bbb;

    public static int total_ads = 0;

    public LocationTressService() {
    }


    private class LocationListener implements android.location.LocationListener {
        Location mLastLocation;

        public LocationListener(String provider) {
            //Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            //Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);
            loc = location.getLatitude() + "," + location.getLongitude();
            //Log.e("LOC", loc.toString());

            new AsyncTask() {
                JSONObject rest;

                @Override
                protected Object doInBackground(Object[] params) {
                    try {

                        String path = Config.Location + loc;

                        Map<String, String> map = new HashMap<String, String>();
                        map.put("a", "a");
                        JSONObject res = getJsonFromUrl(path, map);
                        JSONArray arr = res.getJSONArray("results").getJSONObject(0).getJSONArray("address_components");

                        // for (int j = 0; j < arr.length(); i++)
                        JSONObject obj = arr.getJSONObject(0);
                        City = obj.getString("short_name");
                           /* for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj = arr.getJSONObject(i);
                            City="\n"+obj.getString("short_name");
                            *//* if (obj.getJSONArray("types").getString(0).equals("administrative_area_level_2")) {
                                City = obj.getString("short_name");
                            }
                            if (obj.getJSONArray("types").getString(0).equals("administrative_area_level_1")) {
                                State = obj.getString("long_name");
                            }*//*

                        }*/
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if ((!City.equals("")) && (!City.equals("null"))) {
                        String filter = "";
                        try {
                            SharedPreferences sh = Classified.getInstance().getSharedPreferences("categories", Context.MODE_PRIVATE);
                            String cats = sh.getString("cat", "");
                            //Log.e("getcats", cats);
                            JSONObject obj = new JSONObject(cats);
                            Iterator<String> key = obj.keys();
                            while (key.hasNext()) {
                                String c = key.next();
                                filter = filter + c + ",";
                            }
                            if (!filter.equals("")) {
                                filter = " and a.category in (" + filter.substring(0, filter.length() - 1) + ")";
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Map<String, String> map = new HashMap<String, String>();
                        map.put("filter", filter);
                        map.put("city", City);
                        rest = DAO.getJsonFromUrl(Config.Server + "advertise.php", map);
                    } else {

                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Object o) {
                    super.onPostExecute(o);
                    if ((!City.equals("")) && (!City.equals("null"))) {
                        addLocation(City);
                        SharedPreferences sh = Classified.getInstance().getSharedPreferences("cities", Context.MODE_PRIVATE);
                        String oldloc = sh.getString("city", "");
                        sh.edit().putString("city", City).commit();

                        try {
                            if (rest.getInt("success") > 0 && !oldloc.equals(City)) {
                                total_ads = rest.getJSONArray("advertis").length();
                                bbb.putString("arr", rest.getJSONArray("advertis").toString());


                                Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                long[] pattern = {0, 1000, 1000};

                                NotificationCompat.Builder mBuilder =
                                        new NotificationCompat.Builder(LocationTressService.this)
                                                .setSmallIcon(R.drawable.notification)
                                                .setContentTitle("CatchMeOn ads in " + City)
                                                .setContentText("There are " + rest.getJSONArray("advertis").length() + " ads in this city.")
                                                .setSound(soundUri)
                                                .setVibrate(pattern)
                                                .setAutoCancel(true);


                                sendBroadcast(new Intent()
                                        .setAction(name)
                                        .addCategory(Intent.CATEGORY_DEFAULT)
                                        .putExtra("data", rest.getJSONArray("advertis").length() + ""));
                                //Log.e("DTAA", rest.getJSONArray("advertis").length() + "");


                         /*   sendBroadcast(new Intent().
                                    setAction(name).
                                    addCategory(Intent.CATEGORY_DEFAULT)
                                    .putExtra("array", rest.getJSONArray("advertis").toString()));*/


                                mBuilder.setAutoCancel(true);
                                Intent resultIntent = new Intent(LocationTressService.this, HomeActivity.class);
                                resultIntent.putExtra("from", "service");

                                PendingIntent resultPendingIntent =
                                        PendingIntent.getActivity(
                                                LocationTressService.this,
                                                0,
                                                resultIntent,
                                                PendingIntent.FLAG_UPDATE_CURRENT
                                        );


                                mBuilder.setContentIntent(resultPendingIntent);
                                int mNotificationId = 001;

                                NotificationManager mNotifyMgr =
                                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                                mNotifyMgr.notify(mNotificationId, mBuilder.build());


                            } else {

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else{

                    }


                    //    Toast.makeText(getApplicationContext(), City, Toast.LENGTH_LONG).show();
                }
            }.execute(null, null, null);


        }

        @Override
        public void onProviderDisabled(String provider) {
            //Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            //Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            //Log.e(TAG, "onStatusChanged: " + provider);
        }
    }


    private void addLocation(String city) {
        try {
            ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(this, "lc", 0);
            locations lt = complexPreferences.getObject("locs", locations.class);
            ArrayList<String> locs = new ArrayList<String>();


            try {
                for (int i = 0; i < lt.getLocs().size(); i++) {
                    //Log.e("ltloc" + i, lt.getLocs().get(i).toString());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            if (lt != null)
                locs = lt.getLocs();

            ArrayList<String> lc = new ArrayList<String>();
            if (locs != null) {
                if (locs.size() > 0) {
                    if (locs.contains(city))
                        locs.remove(city);
                    lc.add(city);
                    for (int i = 0; i < locs.size(); i++)
                        lc.add(locs.get(i));
                } else {
                    lc.add(city);
                }
            }

            locations lll = new locations();
            lll.setLocs(lc);
            complexPreferences.putObject("locs", lll);
            complexPreferences.commit();


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    LocationListener mLocationListeners = new LocationListener(LocationManager.NETWORK_PROVIDER);

    public static String tAds() {
        return total_ads + "";
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        //Log.e(TAG, "onCreate");
        bbb = new Bundle();
        initializeLocationManager();
      /*  try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }*/
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners);
        } catch (java.lang.SecurityException ex) {
            //Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            //Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        //Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {

            try {
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                mLocationManager.removeUpdates(mLocationListeners);
            } catch (Exception ex) {
                //Log.i(TAG, "fail to remove location listners, ignore", ex);
            }

        }
    }

    private void initializeLocationManager() {
        //Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }


    public JSONObject getJsonFromUrl(String Targeturl, Map<String, String> paramdata) {
        URL url;
        HttpURLConnection connection = null;
        JSONObject error = null;
        try {
            error = new JSONObject("{\"success\":-1}");
            String parms = "";
            Set mapset = paramdata.entrySet();
            Iterator mapitrator = mapset.iterator();
            while (mapitrator.hasNext()) {
                Map.Entry ent = (Map.Entry) mapitrator.next();
                parms += ent.getKey().toString() + "=";
                parms += URLEncoder.encode(ent.getValue().toString(), "UTF-8") + "&";
            }
            parms = parms.substring(0, parms.length() - 1);
            url = new URL(Targeturl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length", "" + Integer.toString(parms.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            //Send request
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(parms);
            wr.flush();
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\n');
            }
            rd.close();
            is.close();
            return new JSONObject(response.toString());
            //return response.toString();


        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            connection.disconnect();
        }

        return error;
    }


}
