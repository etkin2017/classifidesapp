package com.classifides.app.Adaptor;

/**
 * Created by bpncool on 2/23/2016.
 */
public class Section {

    private final String name;

    public boolean isExpanded;
    public boolean isChecked;

    public Section(String name,boolean sect_isChecked) {
        this.name = name;
        isExpanded = sect_isChecked;
        isChecked=sect_isChecked;
    }

    public String getName() {
        return name;
    }
}
