package com.classifides.app.Adaptor;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.classifides.app.model.Item;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by bpncool on 2/23/2016.
 */
public class SectionedExpandableLayoutHelper implements SectionStateChangeListener, ItemClickListener {

    //data list
    private LinkedHashMap<Section, ArrayList<Item>> mSectionDataMap = new LinkedHashMap<Section, ArrayList<Item>>();
    private ArrayList<Object> mDataArrayList = new ArrayList<Object>();
    Context ctx;

    //section map
    //TODO : look for a way to avoid this
    private HashMap<String, Section> mSectionMap = new HashMap<String, Section>();

    //adapter
    private SectionedExpandableGridAdapter mSectionedExpandableGridAdapter;

    //recycler view
    RecyclerView mRecyclerView;

    public SectionedExpandableLayoutHelper(Context context, RecyclerView recyclerView, ItemClickListener itemClickListener) {

        //setting the recycler view

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        mSectionedExpandableGridAdapter = new SectionedExpandableGridAdapter(context, mDataArrayList,
                itemClickListener, this);
        recyclerView.setAdapter(mSectionedExpandableGridAdapter);

        mRecyclerView = recyclerView;
        ctx = context;
    }

    public void notifyDataSetChanged() {
        //TODO : handle this condition such that these functions won't be called if the recycler view is on scroll
        generateDataList();
        mSectionedExpandableGridAdapter.notifyDataSetChanged();
    }

    public void addSection(String section, ArrayList<Item> items, boolean sect_isChecked) {
        Section newSection;
        mSectionMap.put(section, (newSection = new Section(section, sect_isChecked)));
        mSectionDataMap.put(newSection, items);
    }

    public void addItem(String section, Item item) {
        mSectionDataMap.get(mSectionMap.get(section)).add(item);
    }

    public void removeItem(String section, Item item) {
        mSectionDataMap.get(mSectionMap.get(section)).remove(item);
    }

    public void removeSection(String section) {
        mSectionDataMap.remove(mSectionMap.get(section));
        mSectionMap.remove(section);
    }

    private void generateDataList() {
        mDataArrayList.clear();
        for (Map.Entry<Section, ArrayList<Item>> entry : mSectionDataMap.entrySet()) {
            Section key;
            mDataArrayList.add((key = entry.getKey()));

            if (key.isExpanded) {
                mDataArrayList.addAll(entry.getValue());
            }

        }
    }

    public void selectAll(boolean isChecked) {
        mDataArrayList.clear();
        for (Map.Entry<Section, ArrayList<Item>> entry : mSectionDataMap.entrySet()) {
            Section key;
            mDataArrayList.add((key = entry.getKey()));
            entry.getKey().isChecked = isChecked;
            entry.getKey().isExpanded = isChecked;
            for (Item it : entry.getValue()) {
                it.setChecked(isChecked);

            }

        }
        notifyDataSetChanged();
    }

    public void save() {
        mDataArrayList.clear();


        try {
            JSONObject obj = new JSONObject();
            JSONObject obj1 = new JSONObject();
            JSONObject obj2 = new JSONObject();


            for (Map.Entry<String, Section> entry1 : mSectionMap.entrySet()) {
                // use "entry.getKey()" and "entry.getValue()"
                if (entry1.getValue().isChecked == true) {
                    obj2.put(entry1.getValue().getName() + "", entry1.getValue().getName());
                    //Log.e("main_cat" + entry1.getValue().getName() + "", entry1.getValue().getName() + "");


                }
            }


            for (Map.Entry<Section, ArrayList<Item>> entry : mSectionDataMap.entrySet()) {


                for (Item it : entry.getValue()) {
                    if (it.isChecked()) {
                        obj.put(it.getValue() + "", it.getValue() + "");
                        obj1.put(it.getName() + "", it.getName() + "");
                    }
                    //Log.e("obj_" + it + "", it.getName() + "");

                }

            }
            SharedPreferences sh = ctx.getSharedPreferences("categories", Context.MODE_PRIVATE);
            //Log.e("cat", obj.toString());
            sh.edit().putString("cat", obj.toString()).commit();
            sh.edit().putString("check_cat", obj1.toString()).commit();
            sh.edit().putString("main_cat", obj2.toString()).commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSectionStateChanged(Section section, boolean isOpen) {
        if (!mRecyclerView.isComputingLayout()) {
            section.isExpanded = isOpen;
            notifyDataSetChanged();
        }
    }

    @Override
    public void onSectionStateCheckChanged(Section section, boolean isOpen) {
        if (!mRecyclerView.isComputingLayout()) {

            for (Item it : mSectionDataMap.get(section)) {
                it.setChecked(isOpen);
            }
            section.isChecked = isOpen;
            section.isExpanded = isOpen;
            notifyDataSetChanged();
        }
    }

    @Override
    public void itemClicked(Item item, boolean isChecked) {


    }

    @Override
    public void itemClicked(Section section, boolean ck) {
        if (!mRecyclerView.isComputingLayout()) {
            section.isChecked = ck;
            Toast.makeText(ctx, "Section 111: " + section.getName() + " clicked", Toast.LENGTH_SHORT).show();
            notifyDataSetChanged();
        }
    }
}
