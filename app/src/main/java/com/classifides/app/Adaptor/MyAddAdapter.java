package com.classifides.app.Adaptor;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.classifides.app.AdapAddDetail;
import com.classifides.app.MyAdd;
import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.classifides.app.view.MTextView;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashvini on 22-07-2016.
 */
public class MyAddAdapter extends BaseAdapter {

    Context ctx;
    List<MyAdd.MyAddPojo> name;
    ArrayList<Integer> chk_p;
    boolean[] itemChecked;


    int i;
    public MyAddAdapter(Context ctx, List<MyAdd.MyAddPojo> name) {
        this.ctx = ctx;
        this.name = name;
        chk_p=new ArrayList<Integer>();
        itemChecked = new boolean[name.size()];


        i=name.size();
    }

    @Override
    public int getCount() {
        return name.size();
    }

    @Override
    public Object getItem(int position) {
        return name.get(position);
        //return null;
    }

    @Override
    public long getItemId(int position) {
         return position;
      //  return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        ViewhcClass hc = null;
        if (view == null) {
            hc = new ViewhcClass();

            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.my_add_singleprod, parent, false);

            hc.title = (MTextView) view.findViewById(R.id.ptitle);
            hc.desc = (MTextView) view.findViewById(R.id.description);
            hc.dated = (MTextView) view.findViewById(R.id.dated);
            hc.cost = (MTextView) view.findViewById(R.id.cost);
            hc.cat = (MTextView) view.findViewById(R.id.cat);
            hc.subcat = (MTextView) view.findViewById(R.id.subcat);
            hc.postimg = (ImageView) view.findViewById(R.id.prodimg);
            hc.chk = (CheckBox) view.findViewById(R.id.check);

            hc.user_id = (TextView) view.findViewById(R.id.user_id);
            hc.add_id = (TextView) view.findViewById(R.id.add_id);
            hc.type = (TextView) view.findViewById(R.id.type);
            hc.v = (TextView) view.findViewById(R.id.v);
            hc.img_cnt = (TextView) view.findViewById(R.id.img_cnt);
            hc.city = (TextView) view.findViewById(R.id.city);
            hc.placeid = (TextView) view.findViewById(R.id.placeid);
            hc.nearby = (TextView) view.findViewById(R.id.nearby);
            hc.cash_discount = (TextView) view.findViewById(R.id.cash_discount);
            hc.mdiscount = (TextView) view.findViewById(R.id.mdiscount);

            hc.img_path = (TextView) view.findViewById(R.id.img_path);
            view.setTag(hc);


        } else {
            hc = (ViewhcClass) view.getTag();
            hc.chk.setOnCheckedChangeListener(null);
        }

        MyAdd.MyAddPojo json_data = name.get(position);

        final int pos = position;

        hc.title.setText(json_data.getTitle());

        hc.desc.setText(json_data.getDesc());
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

      /*  Date d = format.parse(json_data.getString("postTime"));
            android.text.format.DateFormat df = new android.text.format.DateFormat();*/

        hc.dated.setText("Date: " + json_data.getDated());
        hc.cost.setText("Rs " + json_data.getCost());
        hc.cat.setText("Cat: " + json_data.getCat());
        hc.subcat.setText("Subcat: " + json_data.getSubcat());

        hc.user_id.setText(json_data.getUser_id());
        hc.add_id.setText(json_data.getAdd_id());
        hc.type.setText(json_data.getType() + "");

        hc.nearby.setText(json_data.getNearby());

        hc.placeid.setText(json_data.getPlaceid());
        hc.city.setText(json_data.getCity());
        hc.img_cnt.setText(json_data.getImageCount() + "");
        hc.v.setText(json_data.getV());

        hc.cash_discount.setText(json_data.getCash_discount());
        hc.mdiscount.setText(json_data.getDiscount());
        hc.img_path.setText(json_data.getImg_path());
        hc.chk.setTag(name.get(position));

        hc.chk.setChecked(false);

     /*   if(chk_p.contains(position)){

        }else{

            hc.chk.setChecked(false);
        }*/

       // hc.chk.setChecked(false);



      //  hc.chk.setSelected(name.get(pos).getSetChk());


        hc.chk.setFocusable(false);

     /*   final ViewhcClass finalHc = hc;
        hc.chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                MyAdd.MyAddPojo contact = (MyAdd.MyAddPojo) finalHc.chk.getTag();
                contact.setSetChk(compoundButton.isChecked());

                contact.setSetChk(contact.getSetChk());
                name.get(pos).setSetChk(contact.getSetChk());

              //  Chk_Pos(position);



                //finalHc.chk.setChecked(true);
                if (finalHc.chk.isChecked()==true) {

                    MyAdd.user_id.add(finalHc.user_id.getText().toString());
                    MyAdd.add_id.add(finalHc.add_id.getText().toString());
                    MyAdd.type.add(finalHc.type.getText().toString());


                } else {

                    if (MyAdd.add_id.contains(finalHc.user_id.getText().toString())) {
                        MyAdd.user_id.remove(finalHc.user_id.getText().toString());
                    }

                    if (MyAdd.add_id.contains(finalHc.add_id.getText().toString())) {
                        MyAdd.add_id.remove(finalHc.add_id.getText().toString());
                    }

                }


            }

        });
*/

        int type = Integer.parseInt(json_data.getType());
        if (type > 0) {

            Picasso.with(Classified.getInstance()).load(R.drawable.play).into(hc.postimg);
        } else {
            Picasso.with(Classified.getInstance()).load(json_data.getImg_path() + "0.jpg").into(hc.postimg);
        }







/*
        final ViewhcClass finalHc = hc;
        finalHc.chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

               // CheckBox cb = (CheckBox) buttonView;
                MyAdd.MyAddPojo contact = (MyAdd.MyAddPojo) finalHc.chk.getTag();
                contact.setSetChk(buttonView.isChecked());


                contact.setSetChk(contact.getSetChk());
                name.get(pos).setSetChk(contact.getSetChk());


                if (finalHc.chk.isChecked()==true) {

                    MyAdd.user_id.add(finalHc.user_id.getText().toString());
                    MyAdd.add_id.add(finalHc.add_id.getText().toString());
                    MyAdd.type.add(finalHc.type.getText().toString());


                } else {

                    if (MyAdd.add_id.contains(finalHc.user_id.getText().toString())) {
                        MyAdd.user_id.remove(finalHc.user_id.getText().toString());
                    }

                    if (MyAdd.add_id.contains(finalHc.add_id.getText().toString())) {
                        MyAdd.add_id.remove(finalHc.add_id.getText().toString());
                    }

                }

            }
        });
*/




/*
            try {
                hc.chk.setChecked(json_data.getBoolean("chk"));
            } catch (Exception e) {
                hc.chk.setChecked(false);
            }*/

        if (itemChecked[position])
           hc.chk.setChecked(true);
        else
            hc.chk.setChecked(false);

        final ViewhcClass finalHc2 = hc;
        finalHc2.chk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if ( finalHc2.chk.isChecked()) {
                    itemChecked[position] = true;

                    MyAdd.user_id.add(finalHc2.user_id.getText().toString());
                    MyAdd.add_id.add(finalHc2.add_id.getText().toString());
                    MyAdd.type.add(finalHc2.type.getText().toString());

                }
                else{
                    itemChecked[position] = false;

                    if (MyAdd.add_id.contains(finalHc2.user_id.getText().toString())) {
                        MyAdd.user_id.remove(finalHc2.user_id.getText().toString());
                    }

                    if (MyAdd.add_id.contains(finalHc2.add_id.getText().toString())) {
                        MyAdd.add_id.remove(finalHc2.add_id.getText().toString());
                    }

                }
            }
        });

        final ViewhcClass finalHc1 = hc;
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent in = new Intent(ctx, AdapAddDetail.class);
                in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                in.putExtra("v", finalHc1.v.getText().toString());
                in.putExtra("nearby", finalHc1.nearby.getText().toString());
                in.putExtra("placeid", finalHc1.placeid.getText().toString());
                in.putExtra("city", finalHc1.city.getText().toString());
                in.putExtra("discount", finalHc1.mdiscount.getText().toString());
                in.putExtra("cash_discount", finalHc1.cash_discount.getText().toString());
                in.putExtra("imageCount", finalHc1.img_cnt.getText().toString());
                in.putExtra("type", finalHc1.type.getText().toString());
                in.putExtra("add_id", finalHc1.add_id.getText().toString());
                in.putExtra("user_id", finalHc1.user_id.getText().toString());
                in.putExtra("category", finalHc1.cat.getText().toString());
                in.putExtra("subcategory", finalHc1.subcat.getText().toString());
                in.putExtra("imageLoc", finalHc1.img_path.getText().toString());

                in.putExtra("price", finalHc1.cost.getText().toString());
                in.putExtra("postTime", finalHc1.dated.getText().toString());
                in.putExtra("title", finalHc1.title.getText().toString());
                in.putExtra("description", finalHc1.desc.getText().toString());
                ctx.startActivity(in);
            }
        });


        return view;
    }


    public class ViewhcClass {

        MTextView title, desc, dated, cost, cat, subcat;
        ImageView postimg;
        CheckBox chk;

        TextView user_id, add_id, type, v, img_cnt, city, placeid, nearby, cash_discount, mdiscount, img_path;

    }

    public ArrayList<Integer> Chk_Pos(int pos){


        chk_p.add(pos);
        return chk_p;
    }
}
