package com.classifides.app.Adaptor;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.classifides.app.R;
import com.classifides.app.model.Item;
import com.google.android.gms.vision.text.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashvini on 8/25/2016.
 */
public class SectionedExpandableListViewAdapter extends BaseAdapter {

    private ArrayList<Object> mDataArrayList_sect;
    private ArrayList<Object> mDataArrayList;

    int[] myIntArray = new int[194];
    //context

    List<Integer> item1 = new ArrayList<Integer>();
    List<Integer> sect = new ArrayList<Integer>();
    Context mContext;

    public boolean s_checked = false;
    public int a = 0;

    //listeners
    ItemClickListener mItemClickListener;
    SectionStateChangeListener mSectionStateChangeListener;

    //view type
    private static final int VIEW_TYPE_SECTION = R.layout.layout_section;
    private static final int VIEW_TYPE_ITEM = R.layout.layout_item; //TODO : change this


    public SectionedExpandableListViewAdapter(Context context, ArrayList<Object> dataArrayList,
                                              ItemClickListener itemClickListener,
                                              SectionStateChangeListener sectionStateChangeListener) {
        mContext = context;
        mItemClickListener = itemClickListener;
        mSectionStateChangeListener = sectionStateChangeListener;
        mDataArrayList = dataArrayList;
        mDataArrayList_sect = dataArrayList;


    }

    private boolean isSection(int position) {

        //Log.e("IsSection", position + "");
        return mDataArrayList.get(position) instanceof Section;
    }


    @Override
    public int getCount() {
       /* return 0;*/
        return mDataArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return mDataArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        final Typeface face = Typeface.createFromAsset(mContext.getAssets(), "fonts/Roboto-Light.ttf");

        View v = convertView;
        int type = getItemViewType(position);
        if (v == null) {


            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (type == 0) {

                v = inflater.inflate(VIEW_TYPE_ITEM, parent, false);

                final Item item = (Item) mDataArrayList.get(position);
                item1.add(position);

                //   myIntArray.add


                holder.itemTextView.setText(item.getName());
                holder.itemTextView.setTypeface(face);
                holder.pos.setText(position + "");
                holder.itemTextView.setChecked(item.isChecked());
                final ViewHolder finalHolder = holder;
                holder.itemTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        item.setChecked(finalHolder.itemTextView.isChecked());
                        mItemClickListener.itemClicked(item, finalHolder.itemTextView.isChecked());





                      /*  if ((((Item) mDataArrayList.get(1)).isChecked() == true) && (((Item) mDataArrayList.get(2)).isChecked() == true) && (((Item) mDataArrayList.get(3)).isChecked() == true)) {
                            //     holder.sectionToggleButton.setChecked(((Section) mDataArrayList.get(0)).isExpanded);
                            s_checked = true;
                            mItemClickListener.itemClicked((Section) mDataArrayList.get(0), s_checked);

                        } else {
                            s_checked = false;
                            mItemClickListener.itemClicked((Section) mDataArrayList.get(0), s_checked);

                        }*/
                  /*      if ((((Item) mDataArrayList.get(5)).isChecked() == true) && (((Item) mDataArrayList.get(6)).isChecked() == true) && (((Item) mDataArrayList.get(7)).isChecked() == true)) {
                            //     holder.sectionToggleButton.setChecked(((Section) mDataArrayList.get(0)).isExpanded);
                            s_checked = true;
                            mItemClickListener.itemClicked((Section) mDataArrayList.get(4), s_checked);

                        } else {
                            s_checked = false;
                            mItemClickListener.itemClicked((Section) mDataArrayList.get(4), s_checked);

                        }*/


                    }
                });


                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //mItemClickListener.itemClicked(item);
                    }
                });

                //  holder.viewType = VIEW_TYPE_SECTION;


            } else {
                v = inflater.inflate(VIEW_TYPE_SECTION, parent, false);
                final Section section = (Section) mDataArrayList.get(position);
                sect.add(position);
                //  holder.section.setChecked(true);
                holder.sectionTextView.setTypeface(face);
                holder.sectionTextView.setText(section.getName());
                holder.text_pos.setText(position + "");
                final ViewHolder finalHolder1 = holder;
                holder.sectionTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClicked(section, finalHolder1.section.isChecked());
                        //   holder.section.setChecked(((Section) mDataArrayList.get(0)).isChecked = s_checked);

                    }
                });
                holder.sectionToggleButton.setChecked(section.isExpanded);
                holder.sectionToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        mSectionStateChangeListener.onSectionStateChanged(section, isChecked);
                    }
                });
                holder.section.setChecked(section.isChecked);
                holder.section.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        mSectionStateChangeListener.onSectionStateCheckChanged(section, isChecked);
                    }
                });

            }
        }

        holder.sectionTextView = (TextView) v.findViewById(R.id.text_section);
        holder.sectionToggleButton = (ToggleButton) v.findViewById(R.id.toggle_button_section);
        holder.section = (CheckBox) v.findViewById(R.id.section);

        if (type == 0) {
            holder.itemTextView = (CheckBox) v.findViewById(R.id.text_item);
            holder.pos = (TextView) v.findViewById(R.id.pos);
            holder.text_pos = (TextView) v.findViewById(R.id.text_pos);
        }
        return v;
    }

    @Override
    public int getItemViewType(int position) {
        if (isSection(position))
            return VIEW_TYPE_SECTION;
        else return VIEW_TYPE_ITEM;
    }

    @Override
    public int getViewTypeCount() {
        //  return super.getViewTypeCount();
        return 2;
    }

    class ViewHolder {

        //common
        View view;
        int viewType;

        //for section
        TextView sectionTextView;
        ToggleButton sectionToggleButton;


        //for item
        CheckBox itemTextView, section;
        TextView pos, text_pos;


    }
}
