package com.classifides.app.Adaptor;

/**
 * Created by Rajeshwar on 14/06/2016.
 */

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NearByAutoCompleteAdaptor extends ArrayAdapter<String> {
    ArrayList<String> resultList;

    public ArrayList<String> getPlaceid() {
        return placeid;
    }

    ArrayList<String> placeid;

    Context mContext;
    int mResource;

    Typeface font;
    private String city;

    public NearByAutoCompleteAdaptor(Context context, int resource) {
        super(context, resource);
        mContext = context;
        mResource = resource;
        font = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
    }

    @Override
    public int getCount() {
        // Last item will be the footer
        return resultList.size();
    }

    @Override
    public String getItem(int position) {
        return resultList.get(position);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {

                    resultList = getCities(constraint.toString());

                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }

    private ArrayList<String> getCities(String near) {
        ArrayList<String> cit = new ArrayList<String>();
        Map<String, String> map = new HashMap<String, String>();
        map.put("abc", "abc");
        try {

            String path = Config.NearBy+city.replaceAll("\\s+","%20")+"%20" + near;
            JSONObject cities = DAO.getJsonFromUrl(path, map);


            if (cities.getString("status").equals("OK")) {
                placeid=new ArrayList<String>();
                for (int i = 0; i < cities.getJSONArray("predictions").length(); i++) {
                placeid.add(cities.getJSONArray("predictions").getJSONObject(i).getString("place_id"));
                    JSONArray res = cities.getJSONArray("predictions").getJSONObject(i).getJSONArray("terms");

                        cit.add(res.getJSONObject(0).getString("value") + " (" + res.getJSONObject(1).getString("value") + ")");


                }
            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cit;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        view.setTypeface(font);
        view.setTextSize(16);

        return view;
    }

    // Affects opened state of the spinner
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        view.setTypeface(font);
        return view;
    }

    public void setCity(String city) {
        this.city = city;
    }


}
