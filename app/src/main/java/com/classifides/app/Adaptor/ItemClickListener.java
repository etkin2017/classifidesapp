package com.classifides.app.Adaptor;


import com.classifides.app.model.Item;

/**
 * Created by lenovo on 2/23/2016.
 */
public interface ItemClickListener {
    void itemClicked(Item item,boolean isChecked);
    void itemClicked(Section section,boolean ck);
}
