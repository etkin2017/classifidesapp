package com.classifides.app.Adaptor;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.classifides.app.R;
import com.classifides.app.model.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 2/23/2016.
 */
public class SectionedExpandableGridAdapter extends RecyclerView.Adapter<SectionedExpandableGridAdapter.ViewHolder> {

    //data array
    private ArrayList<Object> mDataArrayList_sect;
    private ArrayList<Object> mDataArrayList;

    int[] myIntArray = new int[194];
    //context

    List<Integer> item1 = new ArrayList<Integer>();
    List<Integer> sect = new ArrayList<Integer>();
    private final Context mContext;

    public boolean s_checked = false;
    public int a = 0;

    //listeners
    private final ItemClickListener mItemClickListener;
    private final SectionStateChangeListener mSectionStateChangeListener;

    //view type
    private static final int VIEW_TYPE_SECTION = R.layout.layout_section;
    private static final int VIEW_TYPE_ITEM = R.layout.layout_item; //TODO : change this

    public SectionedExpandableGridAdapter(Context context, ArrayList<Object> dataArrayList,
                                          ItemClickListener itemClickListener,
                                          SectionStateChangeListener sectionStateChangeListener) {
        mContext = context;
        mItemClickListener = itemClickListener;
        mSectionStateChangeListener = sectionStateChangeListener;
        mDataArrayList = dataArrayList;
        mDataArrayList_sect = dataArrayList;


    }

    private boolean isSection(int position) {

        //Log.e("IsSection", position + "");
        return mDataArrayList.get(position) instanceof Section;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(viewType, parent, false), viewType);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Typeface face = Typeface.createFromAsset(mContext.getAssets(), "fonts/Roboto-Light.ttf");
      /*  final Item item = (Item) mDataArrayList.get(position);
        final Section section = (Section) mDataArrayList.get(position);*/

        //Log.e("ArrayListData", mDataArrayList.toString());

        switch (holder.viewType) {
            case VIEW_TYPE_ITEM:

                //Log.e("postion", position + "");
                final Item item = (Item) mDataArrayList.get(position);
                item1.add(position);

                //   myIntArray.add


                holder.itemTextView.setText(item.getName());
                holder.itemTextView.setTypeface(face);
                holder.pos.setText(position+"");
                holder.itemTextView.setChecked(item.isChecked());
                holder.itemTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        item.setChecked(holder.itemTextView.isChecked());
                        mItemClickListener.itemClicked(item, holder.itemTextView.isChecked());





                      /*  if ((((Item) mDataArrayList.get(1)).isChecked() == true) && (((Item) mDataArrayList.get(2)).isChecked() == true) && (((Item) mDataArrayList.get(3)).isChecked() == true)) {
                            //     holder.sectionToggleButton.setChecked(((Section) mDataArrayList.get(0)).isExpanded);
                            s_checked = true;
                            mItemClickListener.itemClicked((Section) mDataArrayList.get(0), s_checked);

                        } else {
                            s_checked = false;
                            mItemClickListener.itemClicked((Section) mDataArrayList.get(0), s_checked);

                        }*/
                  /*      if ((((Item) mDataArrayList.get(5)).isChecked() == true) && (((Item) mDataArrayList.get(6)).isChecked() == true) && (((Item) mDataArrayList.get(7)).isChecked() == true)) {
                            //     holder.sectionToggleButton.setChecked(((Section) mDataArrayList.get(0)).isExpanded);
                            s_checked = true;
                            mItemClickListener.itemClicked((Section) mDataArrayList.get(4), s_checked);

                        } else {
                            s_checked = false;
                            mItemClickListener.itemClicked((Section) mDataArrayList.get(4), s_checked);

                        }*/


                    }
                });


                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //mItemClickListener.itemClicked(item);
                    }
                });

                //  holder.viewType = VIEW_TYPE_SECTION;


                break;
            case VIEW_TYPE_SECTION:


                //Log.e("sect_postion", position + "");
                final Section section = (Section) mDataArrayList.get(position);
                sect.add(position);
                //  holder.section.setChecked(true);
                holder.sectionTextView.setTypeface(face);
                holder.sectionTextView.setText(section.getName());
                holder.sectionTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClicked(section, holder.section.isChecked());
                        //   holder.section.setChecked(((Section) mDataArrayList.get(0)).isChecked = s_checked);

                    }
                });
                holder.sectionToggleButton.setChecked(section.isExpanded);
                holder.sectionToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        mSectionStateChangeListener.onSectionStateChanged(section, isChecked);
                    }
                });
                holder.section.setChecked(section.isChecked);
                holder.section.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        mSectionStateChangeListener.onSectionStateCheckChanged(section, isChecked);
                    }
                });


                break;
        }

    }


    void foo(int param1, ViewHolder holder, int position, boolean ck) {
        switch (param1) {
            case VIEW_TYPE_SECTION:

                //     getItemViewType(position);
                holder.section.setChecked(true);

                //   holder.section.setChecked(ck);
                break;


        }
    }

    @Override
    public int getItemCount() {
        return mDataArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isSection(position))
            return VIEW_TYPE_SECTION;
        else return VIEW_TYPE_ITEM;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        //common
        View view;
        int viewType;

        //for section
        TextView sectionTextView;
        ToggleButton sectionToggleButton;


        //for item
        CheckBox itemTextView, section;
        TextView pos,text_pos;

        public ViewHolder(View view, int viewType) {
            super(view);
            this.viewType = viewType;
            this.view = view;
            if (viewType == VIEW_TYPE_ITEM) {
                itemTextView = (CheckBox) view.findViewById(R.id.text_item);
                pos = (TextView) view.findViewById(R.id.pos);


            } else {
                sectionTextView = (TextView) view.findViewById(R.id.text_section);
                sectionToggleButton = (ToggleButton) view.findViewById(R.id.toggle_button_section);
                section = (CheckBox) view.findViewById(R.id.section);

            }
        }
    }
}
