package com.classifides.app.Adaptor;

import android.app.Activity;
import android.content.Context;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.classifides.app.R;
import com.classifides.app.view.MTextView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ashvini on 8/9/2016.
 */
public class CategoryAdaptor extends BaseAdapter {

    private Context mContext;


    @Override
    public int getCount() {
        // return 52;
        return cat.length;

    }

    public CategoryAdaptor(Context c, String[] cat, String[] cat_id, Integer mThumbIds[]) {
        mContext = c;
        this.cat = cat;
        this.cat_id = cat_id;
        this.mThumbIds = mThumbIds;


    }



    @Override
    public Object getItem(int i) {
       return  i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
      /*  LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View header = layoutInflater.inflate(R.layout.grid_cat, null);
*/
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.grid_cat, null);
            view.setTag(holder);

            holder.imageView = (ImageView) view.findViewById(R.id.grid_img);
            holder.catname = (MTextView) view.findViewById(R.id.catname);
            holder.cat_id = (TextView) view.findViewById(R.id.cat_id);

        } else {
            //

            holder = (ViewHolder) view.getTag();
        }

        holder.cat_id.setText(cat_id[i]);
        holder.catname.setText(cat[i]);
        // holder.imageView.setImageResource(mThumbIds[i]);

         /*   holder.imageView.setBackgroundColor(0);
            BitmapDrawable bg = (BitmapDrawable) mContext.getResources().getDrawable(mThumbIds[i]);
            bg.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
            holder.imageView.setBackgroundDrawable(bg);*/

        Picasso.with(mContext).load(mThumbIds[i]).into(holder.imageView);

        // Picasso.with(mContext).load(s)

        return view;
        // return null;
    }

    public String[] cat_id = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
            "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
            "u", "v", "w", "x", "y", "z", "aa", "bb", "cc", "dd",
            "ee", "ff", "gg", "hh", "ii", "jj", "kk", "ll", "mm", "nn",
            "oo", "pp", "qq", "rr", "ss", "tt", "uu", "vv", "ww", "xx", "yy"};

    public String[] cat = {

            "Industrial Plant & Machine",
            "Electronics & Electrical",
            "Industrial Supplies",
            "Food & Beverages",
            "Building & Construction",
            "Apparel & Garments",
            "Chemicals, Dyes & Solvents",
            "Medical & Healthcare",
            "Packaging Machines & Goods",
            "Mechanical Parts & Spares",
            "Housewares & Supplies",
            "Lab Instruments & Supplies",
            "Handicrafts & Decoratives",
            "Automobile, Parts & Spares",
            "Furniture & Supplies",
            "Hand & Machine Tools",
            "Textiles, Yarn & Fabrics",
            "Cosmetics & Personal Care",
            "Metals, Alloys & Minerals",
            "Fashion Accessories & Gear",
            "Gems, Jewelry & Astrology",
            "Home Textile & Furnishing",
            "Agriculture & Farming",
            "Bags, Belts & Wallets",
            "Engineering Services",
            "Herbal & Ayurvedic Product",
            "Sports Goods, Toys & Games",
            "Computer & IT Solutions",
            "Kitchen Utensils & Appliances",
            "Paper & Paper Products",
            "Media, PR & Publishing",
            "Business & Audit Services",
            "Books & Stationery",
            "Telecom Equipment & Goods",
            "Transportation & Logistics",
            "IT & Telecom Services",
            "Education & Training",
            "Marble, Granite & Stones",
            "Call Center & BPO Services",
            "Travel, Tourism & Hotels",
            "Bicycle, Rickshaw & Spares",
            "Financial & Legal Service",
            "Leather Products",
            "Product Rental & Leasing",
            "HR Planning & Recruitment",
            "Architecture & Interiors",
            "Event Planner & Organizer",
            "R & D and Testing Labs",
            "Facility Management",
            "Contractors & Freelancers",
            "Rail, Shipping & Aviation"};
    public Integer[] mThumbIds = {
            R.drawable.industrial_plant_n_machine, R.drawable.electronics_n_electrical,
            R.drawable.industrial_supplies, R.drawable.food_n_beverages,
            R.drawable.building_construction, R.drawable.apparel_n_garments,
            R.drawable.chemicals_dyes_n_solvents, R.drawable.medical_n_healthcare,
            R.drawable.packaging_machines_n_goods, R.drawable.mechanical_parts_n_spares,
            R.drawable.housewares_n_supplies, R.drawable.lab_instruments_n_supplies,
            R.drawable.handicrafts_n_decoratives,


            R.drawable.automobile_parts_n_spares,

            R.drawable.furniture_n_supplies, R.drawable.hand_n_machine_tools,
            R.drawable.textiles_yarn_n_fabrics, R.drawable.cosmetics_n_personal_care,
            R.drawable.metals_alloys_n_minerals, R.drawable.fashion_accessories_n_gear,
            R.drawable.gems_jewelry_n_astrology, R.drawable.home_textile_n_furnishing,
            R.drawable.agriculture_n_farming, R.drawable.bags_belts_wallets,
            R.drawable.engineering_services, R.drawable.herbal_n_ayurvedic_product,
            R.drawable.sports_goods_toys_n_games,
            R.drawable.computer_n_it_solutions,
            R.drawable.kitchen_utensils_n_appliances,


            R.drawable.paper_n_paper_products,
            R.drawable.media_pr_n_publishing, R.drawable.business_n_audit_services,
            R.drawable.books_n_stationery, R.drawable.telecom_equipment_n_goods,
            R.drawable.transportation_n_logistics, R.drawable.it_n_telecom_services,
            R.drawable.education_n_training, R.drawable.marble_n_granite_n_stones,
            R.drawable.call_center_n_bpo_services, R.drawable.travel_tourism_n_hotels,
            R.drawable.bicycle_spare_parts,
            R.drawable.financial_n_legal_service,

            R.drawable.leather_products, R.drawable.product_rental_n_leasing,

            R.drawable.hr_planning_n_recruitment, R.drawable.architecture_n_interiors,
            R.drawable.event_planner_n_organizer, R.drawable.r_n_d_and_testing_labs,
            R.drawable.facility_management, R.drawable.contractors_n_freelancers,
            R.drawable.rail_shipping_aviation
    };

    public class ViewHolder {

        ImageView imageView;
        MTextView catname;

        TextView cat_id;
    }
}
