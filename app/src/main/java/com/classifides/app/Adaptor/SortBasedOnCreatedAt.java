package com.classifides.app.Adaptor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Comparator;

/**
 * Created by Ashvini on 9/7/2016.
 */
public class SortBasedOnCreatedAt implements Comparator<JSONObject> {
    @Override
    public int compare(JSONObject lhs, JSONObject rhs) {
        try {
        java.sql.Timestamp ts1 = java.sql.Timestamp.valueOf(lhs.getString("created_at"));
        java.sql.Timestamp ts2 = java.sql.Timestamp.valueOf(rhs.getString("created_at"));

            long tsTime1 = ts1.getTime();
            long tsTime2 = ts2.getTime();

            return (tsTime1 < tsTime2 ? 1 : tsTime1 > tsTime2 ? -1 : 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
