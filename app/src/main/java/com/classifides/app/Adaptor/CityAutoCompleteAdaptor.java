package com.classifides.app.Adaptor;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CityAutoCompleteAdaptor extends ArrayAdapter<String> {
    ArrayList<String> resultList;

    Context mContext;
    int mResource;

    Typeface font;

    public ArrayList<String> getPlaceid() {
        return City;
    }
    ArrayList<String> City;

    public CityAutoCompleteAdaptor(Context context, int resource) {
        super(context, resource);
        mContext = context;
        mResource = resource;
      font  = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
    }

    @Override
    public int getCount() {
        // Last item will be the footer
        return resultList.size();
    }

    @Override
    public String getItem(int position) {
        return resultList.get(position);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {

                    resultList = getCities(constraint.toString());

                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }

    private ArrayList<String> getCities(String city) {
        ArrayList<String> cit = new ArrayList<String>();
        Map<String, String> map = new HashMap<String, String>();
        map.put("abc", "abc");
        try {
            String path = Config.Address + city;
            JSONObject cities = DAO.getJsonFromUrl(path, map);


            if (cities.getString("status").equals("OK")) {
                for (int i = 0; i < cities.getJSONArray("predictions").length(); i++) {
                    City=new ArrayList<String>();
                    JSONArray res = cities.getJSONArray("predictions").getJSONObject(i).getJSONArray("terms");
                    String citis=res.getJSONObject(0).getString("value") + " (" + res.getJSONObject(1).getString("value") + ")";
                   for(int j=0;j<res.length();j++){
                       if (res.getJSONObject(j).getString("value").equals("India")) {
                           cit.add(citis);
                       }

                   }

                     /*for(int j=0;j<res.length();j++){
                            JSONObject c=res.getJSONObject(j);
                          if(  c.getJSONArray("types").getString(0).equals("administrative_area_level_2"))
                          {
                            cit.add(c.getString("short_name"));
                          }
                        }*/
                    String abc = "dfa";
                }
            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cit;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        view.setTypeface(font);
        view.setTextSize(16);
        return view;
    }

    // Affects opened state of the spinner
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        view.setTypeface(font);
        return view;
    }
}