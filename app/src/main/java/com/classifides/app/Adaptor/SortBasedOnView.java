package com.classifides.app.Adaptor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Comparator;

/**
 * Created by Ashvini on 8/30/2016.
 */
public class SortBasedOnView implements Comparator<JSONObject> {
    @Override
    public int compare(JSONObject lhs, JSONObject rhs) {
        try {
            return (Integer.parseInt(lhs.getString("v"))) < (Integer.parseInt(rhs.getString("v"))) ? 1 : (Integer.parseInt(lhs
                    .getString("v"))) > (Integer.parseInt(rhs.getString("v"))) ? -1 : 0;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
