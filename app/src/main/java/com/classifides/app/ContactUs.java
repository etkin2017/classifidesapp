package com.classifides.app;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ContactUs extends AppCompatActivity {

    LinearLayout cont_send;
    EditText msg;

    Map<String, String> data;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        cont_send = (LinearLayout) findViewById(R.id.cont_send);
        msg = (EditText) findViewById(R.id.msg);

        dialog = new ProgressDialog(ContactUs.this);

        data = new HashMap<String, String>();

        cont_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(msg.getText().toString())) {
                    msg.setError("please enter message");
                } else {
                    sentContact(msg.getText().toString());
                }
            }
        });
    }


    private void sentContact(final String msg) {
        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ProgressDialog();



                int id = 0;
                try {
                    JSONObject user = DAO.getUser();

                    id = user.getInt("id");
                    //Log.e("user_id", id + "");
                    data.put("user_id", id + "");

                } catch (Exception e) {
                    e.printStackTrace();
                }


                data.put("msg", msg);


             /*   try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
            }

            @Override
            protected Object doInBackground(Object[] params) {

                res = DAO.getJsonFromUrl(Config.contact, data);

                  /*  if (res.getInt("success") > 0) {
                        Toast.makeText(UpdateAsValidity.this, "Plan Add Suceessfully", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(UpdateAsValidity.this, "Error ", Toast.LENGTH_LONG).show();

                    }
*/

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                DismissProgress();
                try {
                    if (res.getInt("success") > 0) {
                        Toast.makeText(getApplicationContext(), " sent", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                      //  Toast.makeText(getApplicationContext(), " not sent", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

    public void ProgressDialog() {
        dialog.setMessage("While sending your message.");
        dialog.setTitle("Please wait....");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void DismissProgress() {
        dialog.dismiss();
    }
}
