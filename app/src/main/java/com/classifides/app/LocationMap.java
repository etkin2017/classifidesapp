package com.classifides.app;

import android.*;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.view.MTextView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LocationMap extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    private String getlocation;
    private MTextView address;
    private LinearLayout ok,cancel;
    private  String add,shortadd,locality,placeid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_map);
        initView();
        addListener();
    }

    private void addListener() {
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent=new Intent();
                resultIntent.putExtra("address",add);
                resultIntent.putExtra("short",shortadd);
                resultIntent.putExtra("locality",locality);
                resultIntent.putExtra("placeid",placeid);
                setResult(Activity.RESULT_OK,resultIntent);
                finish();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initView() {
        address= (MTextView) findViewById(R.id.address);
        ok= (LinearLayout) findViewById(R.id.loc_ok);
        cancel= (LinearLayout) findViewById(R.id.loc_can);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.locationmap);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));
        if (location != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(location.getLatitude(), location.getLongitude()), 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                    .zoom(17)                   // Sets the zoom
                    .bearing(90)                // Sets the orientation of the camera to east
                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                onLocationChanged(latLng);

            }
        });

    }




    private void onLocationChanged(final LatLng location) {


        final String loc = location.latitude + "," + location.longitude;

        new AsyncTask() {
            JSONObject rest;

            @Override
            protected Object doInBackground(Object[] params) {
                try {

                    String path = Config.Location + loc;
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("a", "a");
                    JSONObject res = DAO.getJsonFromUrl(path, map);
                    JSONArray arr = res.getJSONArray("results").getJSONObject(0).getJSONArray("address_components");
                    JSONObject obj = arr.getJSONObject(0);
                    add=res.getJSONArray("results").getJSONObject(0).getString("formatted_address");
                    shortadd=obj.getString("short_name");
                    placeid=res.getJSONArray("results").getJSONObject(0).getString("place_id");
                    String city="",state="";
                    for(int i=0;i<arr.length();i++){
                        JSONObject ress=arr.getJSONObject(i);
                        if(ress.getJSONArray("types").getString(0).equals("administrative_area_level_2"))
                            city=ress.getString("short_name");
                        if(ress.getJSONArray("types").getString(0).equals("administrative_area_level_1"))
                            state=ress.getString("long_name");
                    }
                    locality=city+" ("+state+")";


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                address.setText(add);
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(location).title(add));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

            }
        }.execute(null, null, null);


    }



}
