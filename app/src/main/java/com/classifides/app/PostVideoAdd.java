package com.classifides.app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;

import com.classifides.app.fragment.PostFreeAddOne;
import com.classifides.app.fragment.PostVideoAddOne;
import com.classifides.app.view.MTextView;

public class PostVideoAdd extends AppCompatActivity {
    MTextView close;

    public static String adid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_video_add);


        try {
            adid = getIntent().getStringExtra("add_id");

        } catch (Exception ex) {

        }

        initView();
        addListener();
    }

    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

      PostVideoAddOne o=  new PostVideoAddOne();
        if (TextUtils.isEmpty(adid)) {

            getSupportFragmentManager().beginTransaction().add(R.id.container, o, "pstone").commit();
            close = (MTextView) findViewById(R.id.close);
        } else {

            Bundle bundle = new Bundle();
            bundle.putString("add_id", adid);
            o.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().add(R.id.container, o, "pstone").commit();
            close = (MTextView) findViewById(R.id.close);
        }


    }

    private void addListener() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
