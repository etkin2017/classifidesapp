package com.classifides.app;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.classifides.app.Adaptor.ItemClickListener;
import com.classifides.app.Adaptor.Section;
import com.classifides.app.Adaptor.SectionedExpandableLayoutHelper;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.data.NotiDbHelper;
import com.classifides.app.model.Item;
import com.classifides.app.model.ViewDbHelper;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ManageNotification extends AppCompatActivity implements ItemClickListener {

    RecyclerView mRecyclerView;
    private JSONObject cats;
    private ArrayList<String> categories;
    SectionedExpandableLayoutHelper sectionedExpandableLayoutHelper;
    CheckBox selectall;
    Map<String, String> map, map2, map3;
    LinearLayout save, cancel;
    private boolean isChange = false;
    public static boolean check = false;

    private NotiDbHelper mHelper;
    private SQLiteDatabase dataBase;

    public static boolean a = true;
    int k = 0;
    Iterator<String> key2, key3;
    JSONObject obj, obj2, obj3;
    boolean isCheck = false;
    boolean sect_isCheck = false;

    private void initView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.categories);
        selectall = (CheckBox) findViewById(R.id.selectall);
        save = (LinearLayout) findViewById(R.id.save);
        cancel = (LinearLayout) findViewById(R.id.cancel);
        sectionedExpandableLayoutHelper = new SectionedExpandableLayoutHelper(this, mRecyclerView, this);
        try {
            SharedPreferences sh = getSharedPreferences("categories", Context.MODE_PRIVATE);
            String cats = sh.getString("cat", "");
            //Log.e("cats_null", cats);
            String cats_ckeck = sh.getString("check_cat", "");//            sh.edit().putString("check_cat", obj1.toString()).commit();
            //Log.e("cats_ckeck_null", cats_ckeck);
            String main_cat = sh.getString("main_cat", "");
            //Log.e("main_cat_null", main_cat);
            map = new HashMap<String, String>();
            map2 = new HashMap<String, String>();
            map3 = new HashMap<String, String>();
            obj = new JSONObject(cats);
            obj2 = new JSONObject(cats_ckeck);
            obj3 = new JSONObject(main_cat);
           /* Iterator<String> key = obj.keys();
            while (key.hasNext()) {
                String c = key.next();
                map.put(c + "", c + "");
                Log.e("C", c);

            }*/

            key2 = obj2.keys();
            while (key2.hasNext()) {
                String c = key2.next();
                map2.put(c + "", c + "");
                //Log.e("C", c);

            }

            key3 = obj3.keys();
            while (key3.hasNext()) {
                String c = key3.next();
                map3.put(c + "", c + "");
                //Log.e("C", c);

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_notification);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mHelper = new NotiDbHelper(getApplicationContext());

/*
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-2758807869387921~3087322491");

        final AdView mAdView = (AdView) findViewById(R.id.adView_cat_n_sub_cat);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);


        mAdView.loadAd(adRequest);



        final Runnable adLoader = new Runnable() {
            @Override
            public void run() {
                mAdView.loadAd(new AdRequest.Builder().build());
            }
        };





        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                Log.e("onAdClosed", "onAdClosed");
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Log.e("onAdFailedToLoad", "onAdFailedToLoad");

                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        try{
                        ManageNotification.this.runOnUiThread(adLoader);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 3000);
            }


            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                Log.e("onAdLeftApplication", "onAdLeftApplication");
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                Log.e("onAdOpened", "onAdOpened");
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.e("onAdLoaded", "onAdLoaded");
            }
        });

        adLoader.run();



*/






        initView();
        addListener();

    }

    private void addListener() {
        selectall.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isChange = true;
                sectionedExpandableLayoutHelper.selectAll(isChecked);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ManageNotification.this);
                builder.setTitle("Alert");
                builder.setMessage("All configuration changes you have just made will be saved and you will get notification according to this changes. Do you wish to continue?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        sectionedExpandableLayoutHelper.save();
                        saveData("1");
                        finish();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.setCancelable(false);
                builder.show();

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
    }


    private void cancel() {
        if (isChange) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Alert");
            builder.setMessage("All configuration changes you have just made will be discarded. You wanted to cancel it?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.setCancelable(false);
            builder.show();
        } else {
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCategories();
    }

    private void getCategories() {
        new AsyncTask() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                categories = new ArrayList<String>();
            }

            @Override
            protected Object doInBackground(Object[] params) {
                Map<String, String> map = new HashMap<String, String>();
                map.put("abc", "abc");
                cats = DAO.getJsonFromUrl(Config.Server + "categories.php", map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    if (cats.getInt("success") > 0) {
                        cats = cats.getJSONObject("categories");
                        Iterator<String> category = cats.keys();
                        //Log.e("CATEGORY", category.toString());

                        while (category.hasNext()) {
                            String cat = category.next();
                            k = 0;
                            if (map3.containsValue(cat)) {
                                sect_isCheck = true;
                            } else {
                                sect_isCheck = false;
                            }

                            //Log.e("CATEGORY_cat", cat);
                            ArrayList<Item> arrayList = new ArrayList<>();
                            JSONObject sub = cats.getJSONObject(cat);
                            //Log.e("CATEGORY_sub", sub.toString());

                            int j = 0;

                            Iterator<String> subcat = sub.keys();
                            while (subcat.hasNext()) {
                                String subct = subcat.next();
                                if (map2.containsValue(subct)) {
                                    isCheck = true;
                                } else {
                                    isCheck = false;
                                }

                              /*  try {
                                    String a = map.get(sub.getInt(subct) + "");
                                    if (a != null)
                                        isCheck = true;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    isCheck = true;
                                }*/
                                arrayList.add(new Item(subct, j, sub.getInt(subct), isCheck));
                                j++;
                            }
                            sectionedExpandableLayoutHelper.addSection(cat, arrayList, sect_isCheck);

                         /*   if (map3.isEmpty() && map2.isEmpty()) {
                                sectionedExpandableLayoutHelper.selectAll(a);
                                selectall.setChecked(true);
                            }
*/
                            if (displayData("1") == true) {

                            } else {
                                sectionedExpandableLayoutHelper.selectAll(a);
                                selectall.setChecked(true);
                            }

                        }
                        sectionedExpandableLayoutHelper.notifyDataSetChanged();


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

    private void saveData(String bv2) {
        dataBase = mHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(NotiDbHelper.NEXT, bv2);
        System.out.println("");
        dataBase.insert(NotiDbHelper.TABLE_NAME_TWo, null, values);
        //close database        
        dataBase.close();

        //   updateView(bv2);


    }

    public boolean displayData(String bv) {

        dataBase = mHelper.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM " + NotiDbHelper.TABLE_NAME_TWo, null);

        if (mCursor.moveToFirst()) {
            do {

                mCursor.getString(mCursor.getColumnIndex(NotiDbHelper.NEXT));
                if (mCursor.getString(mCursor.getColumnIndex(NotiDbHelper.NEXT)).equals(bv)) {
                    check = true;
                    break;
                }
            } while (mCursor.moveToNext());
        }

        mCursor.close();
        return check;
    }

    @Override
    public void itemClicked(Item item, boolean isChecked) {
        isChange = true;
        //   Log.e("ITEM Selected", item.getName().toString());

        //  Toast.makeText(this, item.getName().toString(), Toast.LENGTH_SHORT).show();
       /* try {
            if (isChecked) {
                map.put(item.getValue() + "", item.getValue() + "");
            } else {
                map.remove(item.getValue() + "");
            }
            Collection<String> ct = map.values();
            JSONObject obj = new JSONObject();
            Iterator<String> ccc = ct.iterator();
            while (ccc.hasNext()) {
                String a = ccc.next();
                obj.put(a, a);
            }

            SharedPreferences sh = getSharedPreferences("categories", Context.MODE_PRIVATE);
            sh.edit().putString("cat", obj.toString()).commit();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        Toast.makeText(this, map.toString(), Toast.LENGTH_SHORT).show();*/
    }

    @Override
    public void itemClicked(Section section, boolean ck) {

        if (!mRecyclerView.isComputingLayout()) {
            section.isChecked = ck;
            // Toast.makeText(this, "Section 111: " + section.getName() + " clicked", Toast.LENGTH_SHORT).show();
            sectionedExpandableLayoutHelper.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {

            cancel();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
