package com.classifides.app.gcm.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.data.NotiDbHelper;
import com.classifides.app.gcm.activity.ChatRoomActivity;
import com.classifides.app.gcm.activity.FindFriends;
import com.classifides.app.gcm.activity.HomeChatActivity;
import com.classifides.app.gcm.activity.NewChatRoomActivity;
import com.classifides.app.gcm.adapter.ChatRoomsAdapter;
import com.classifides.app.gcm.gcm.UpdateContactService;
import com.classifides.app.gcm.model.ChatRoom;
import com.classifides.app.gcm.model.User;
import com.classifides.app.model.Item;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactsFragment extends Fragment {


    private ArrayList<String> mobilelist;
    String allcont = "";
    SharedPreferences sh;
    private int mid;
    RecyclerView contrecy;
    int user_id;

    ArrayList<String> uid;
    ArrayList<String> aid;
    String intent_txt;
    NotiDbHelper helper;

    HashMap<String, String> contactsname;
    ChatRoomsAdapter mAdaptor;
    SwipeRefreshLayout mySwipeRefreshLayout;

    ProgressDialog progressDialog;

    public static String friend_find = "";

    public static String block_id = "";
    public static String buid = "";

    public ContactsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_contacts, container, false);
        contactsname = new HashMap<String, String>();
        setHasOptionsMenu(true);
        HomeChatActivity.friends = 1;

        uid = new ArrayList<String>();


        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("please wait while loading your contact....");

        mobilelist = new ArrayList<String>();
        contrecy = (RecyclerView) v.findViewById(R.id.contrecyc);

        ArrayList<User> yy = User.load(Classified.getInstance());
        ArrayList<User> newyy = new ArrayList<User>();


        helper = new NotiDbHelper(Classified.getInstance());


        mAdaptor = new ChatRoomsAdapter(Classified.getInstance(), User.load(Classified.getInstance()));
        contrecy.setLayoutManager(new LinearLayoutManager(getActivity()));
        contrecy.setAdapter(mAdaptor);
        mySwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.mySwipeRefreshLayout);

        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        //Log.i("SWIPEREFRESH", "onRefresh called from SwipeRefreshLayout");

                        // This method performs the actual data-refresh operation.
                        // The method calls setRefreshing(false) when it's finished.
                        refresh();
                    }
                }
        );


        sh = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);

        String users = sh.getString("users", "");
        try {
            JSONObject j = new JSONObject(users);
            mid = j.getInt("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        //  getUsers();

        // b.putString("", sharedText);


        contrecy.addOnItemTouchListener(new ChatRoomsAdapter.RecyclerTouchListener(getActivity(), contrecy, new ChatRoomsAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                // when chat is clicked, launch full chat thread activity


                TextView long_press_icon = (TextView) view.findViewById(R.id.long_press_icon);
                String chatroomid = ((TextView) view.findViewById(R.id.chatroomid)).getText().toString();
                String id = ((TextView) view.findViewById(R.id.id)).getText().toString();
                String url = ((TextView) view.findViewById(R.id.url)).getText().toString();
                String type = ((TextView) view.findViewById(R.id.type)).getText().toString();
                String block = ((TextView) view.findViewById(R.id.block)).getText().toString();
                String name = ((TextView) view.findViewById(R.id.name)).getText().toString();
                // chatroomid, id, url, type, block


                String set_checked = ((TextView) view.findViewById(R.id.set_checked)).getText().toString();


                //contrecy.getAdapter().getItemId(position)


                if (mAdaptor.getChatRoomArrayList().get(position).isTicked() == true) {

                  /*  g_id = "";
                    if (deleteArrayId.contains(id)) {
                        deleteArrayId.remove(id);
                    }
*/
                    long_press_icon.setVisibility(View.GONE);

                    mAdaptor.getChatRoomArrayList().get(position).setTicked(false);

                    mAdaptor.notifyDataSetChanged();

                    if (uid.contains(id + "")) {
                        uid.remove(id + "");
                    }

                } else {
                    Intent intent = new Intent(getActivity(), NewChatRoomActivity.class);
                    intent.putExtra("type", type);
                    intent.putExtra("id", id);
                    intent.putExtra("name", name);
                    intent.putExtra("url", url);
                    intent.putExtra("str_txt", HomeChatActivity.sharedText);
                    intent.putExtra("block", block);
                    intent.putExtra("onlyImage", "2");
                    startActivity(intent);

                }

                //  uid.remove(id);

                friend_find = "";
                /*}
                else{

                }*/

                block_id = "";
            }

            @Override
            public void onLongClick(View view, int position) {

                TextView long_press_icon = (TextView) view.findViewById(R.id.long_press_icon);

                String chatroomid = ((TextView) view.findViewById(R.id.chatroomid)).getText().toString();
                String id = ((TextView) view.findViewById(R.id.id)).getText().toString();
                String url = ((TextView) view.findViewById(R.id.url)).getText().toString();
                String type = ((TextView) view.findViewById(R.id.type)).getText().toString();
                String block = ((TextView) view.findViewById(R.id.block)).getText().toString();
                String name = ((TextView) view.findViewById(R.id.name)).getText().toString();
                String aid = ((TextView) view.findViewById(R.id.aid)).getText().toString();
                user_id = Integer.parseInt(aid);

                mAdaptor.getChatRoomArrayList().get(position).setTicked(true);
                mAdaptor.notifyDataSetChanged();


                mAdaptor.notifyDataSetChanged();

                uid.add(aid + "");
                friend_find = "friend_find";
                block_id = id + "";


            }
        }));


        return v;


    }

    @Override
    public void onPause() {
        super.onPause();
        HomeChatActivity.friends = 0;
    }


    public void refresh() {
        mAdaptor = new ChatRoomsAdapter(Classified.getInstance(), User.load(Classified.getInstance()));
        contrecy.setLayoutManager(new LinearLayoutManager(getActivity()));
        contrecy.setAdapter(mAdaptor);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_for_friends, menu);
        //return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // return super.onOptionsItemSelected(item);
        int id = item.getItemId();

        if (id == R.id.refresh_contact) {
            Intent f = new Intent(getActivity(), UpdateContactService.class);
            f.putExtra("id", mid);
            getActivity().startService(f);
        }

        if (id == R.id.find_friend) {
            // startActivity(new Intent(getActivity(), FindFriends.class));

            Intent uu = new Intent(getActivity(), FindFriends.class);
            startActivity(uu);
        }

        if (id == R.id.unblock) {
            if (block_id.equals("")) {
                Toast.makeText(getActivity(), "Select contact", Toast.LENGTH_SHORT).show();
            } else {


                boolean c = helper.check_blocked(Integer.parseInt(block_id));

                if (c == true) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Alert");
                    builder.setMessage("you blocked this person , Do you want to unblock this person?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            helper.unFriend(Integer.parseInt(block_id), 0);
                            refresh();
                            mAdaptor.notifyDataSetChanged();


                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.setCancelable(false);
                    builder.show();
                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Alert");
                    builder.setMessage("Are you sure you want to block this person?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            helper.unFriend(Integer.parseInt(block_id), 1);
                            refresh();
                            //   getUsers();
                            mAdaptor.notifyDataSetChanged();
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.setCancelable(false);
                    builder.show();
                }


            }
        }

        return false;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        if (friend_find.equals("friend_find")) {
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(false);
            // menu.getItem(2).setVisible(false);
            //  menu.getItem(2).setVisible(true);
            menu.getItem(2).setVisible(false);
            menu.getItem(3).setVisible(true);
            menu.getItem(4).setVisible(true);
         /*   menu.getItem(5).setVisible(false);
            menu.getItem(6).setVisible(false);
            menu.getItem(7).setVisible(false);
            menu.getItem(8).setVisible(true);
            menu.getItem(9).setVisible(false);*/

        } else {
            menu.getItem(0).setVisible(true);
            menu.getItem(1).setVisible(true);
            menu.getItem(2).setVisible(true);
            //  menu.getItem(2).setVisible(true);
            menu.getItem(3).setVisible(false);
            menu.getItem(4).setVisible(true);
            // menu.getItem(4).setVisible(false);
         /*   menu.getItem(5).setVisible(false);
            menu.getItem(6).setVisible(false);
            menu.getItem(7).setVisible(false);
            menu.getItem(8).setVisible(false);
            menu.getItem(9).setVisible(true);*/
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        HomeChatActivity.friends = 1;

    }

    public void getUsers() {


        new AsyncTask() {


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //  progressDialog.show();
            }

            @Override
            protected Object doInBackground(Object[] objects) {
                readPhoneContacts();

                for (String mob : mobilelist) {
                    allcont = allcont + mob + ",";
                }
                allcont = allcont.substring(0, allcont.length() - 1);
                return null;
            }


            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                //getUsersFromServer(allcont);
                //  progressDialog.dismiss();
                getCategories();

            }


        }.execute(null, null, null);
    }


    public void getCategories() {
        new AsyncTask() {
            JSONObject cats = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //  progressDialog.dismiss();
            }

            @Override
            protected Object doInBackground(Object[] params) {
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("contacts", allcont);
                param.put("id", mid + "");
                cats = DAO.getJsonFromUrl(Classified.APP_API_URL, param);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                //   progressDialog.dismiss();
                try {
                    if (cats.getInt("success") > 0) {

                        //Log.e("CATS", cats.toString());

                        ArrayList<User> users = User.fromJSON(cats.getJSONArray("users"));
                        for (User user : users) {
                            if (user.getType() == 1) {
                                user.setName(contactsname.get(user.getMobile()));
                            }

                        }

                        User.save(Classified.getInstance(), users);


                        mAdaptor.setChatRoomArrayList(User.load(Classified.getInstance()));
                        mAdaptor.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

    public void readPhoneContacts() //This Context parameter is nothing but your Activity class's Context
    {
        Context cntx = Classified.getInstance();
        Cursor cursor = cntx.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        Integer contactsCount = cursor.getCount(); // get how many contacts you have in your contacts list
        if (contactsCount > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    //the below cursor will give you details for multiple contacts
                    Cursor pCursor = cntx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    // continue till this cursor reaches to all phone numbers which are associated with a contact in the contact list
                    while (pCursor.moveToNext()) {
                        int phoneType = pCursor.getInt(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                        //String isStarred 		= pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.STARRED));
                        String phoneNo = pCursor.getString(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //you will get all phone numbers according to it's type as below switch case.
                        //Logs.e will print the phone number along with the name in DDMS. you can use these details where ever you want.
                        switch (phoneType) {
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                //Log.e(contactName + ": TYPE_MOBILE", " " + (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString());
                                String m = ((((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).replace("*", "").replace("#", "")).trim().toString();


                                m = m.replaceAll("\\D+", "");

                                String numberOnly = phoneNo.replaceAll("[^0-9]", "");
                                ;
                                if (m.length() > 10) {
                                    m = m.substring(Math.max(m.length() - 10, 0));
                                }
                                contactsname.put(m, contactName);
                                mobilelist.add(m);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                //Log.e(contactName + ": TYPE_HOME", " " + phoneNo);
                                String m1 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m1 = m1.replaceAll("\\D+", "");

                                if (m1.length() > 10) {
                                    m1 = m1.substring(Math.max(m1.length() - 10, 0));
                                }
                                contactsname.put(m1, contactName);
                                mobilelist.add(m1);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                                //Log.e(contactName + ": TYPE_WORK", " " + phoneNo);
                                String m2 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m2 = m2.replaceAll("\\D+", "");


                                if (m2.length() > 10) {
                                    m2 = m2.substring(Math.max(m2.length() - 10, 0));
                                }
                                contactsname.put(m2, contactName);
                                mobilelist.add(m2);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE:
                                //Log.e(contactName + ": TYPE_WORK_MOBILE", " " + phoneNo);
                                String m3 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m3 = m3.replaceAll("\\D+", "");

                                if (m3.length() > 10) {
                                    m3 = m3.substring(Math.max(m3.length() - 10, 0));
                                }
                                mobilelist.add(m3);
                                contactsname.put(m3, contactName);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER:
                                //Log.e(contactName + ": TYPE_OTHER", " " + phoneNo);
                                String m4 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m4 = m4.replaceAll("\\D+", "");

                                if (m4.length() > 10) {
                                    m4 = m4.substring(Math.max(m4.length() - 10, 0));
                                }
                                mobilelist.add(m4);
                                contactsname.put(m4, contactName);
                                break;
                            default:
                                break;
                        }
                    }
                    pCursor.close();
                }
            }
            cursor.close();
        }
    }

}
