package com.classifides.app.gcm.gcm;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.util.Log;

import com.classifides.app.application.Classified;
import com.classifides.app.data.DAO;
import com.classifides.app.gcm.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Rajeshwar on 19/09/2016.
 */
public class UpdateContactService extends Service {

    private ArrayList<String> mobilelist;
    String allcont = "";
    SharedPreferences sh;
    HashMap<String, String> contactsname;
    int id;
    public static String name = "refresh";
    String mobile;
    JSONObject cats = null;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {


        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mobilelist = new ArrayList<String>();

        sh = getSharedPreferences("user", Context.MODE_PRIVATE);
        contactsname = new HashMap<String, String>();
        String users = sh.getString("users", "");
        try {
            JSONObject j = new JSONObject(users);
            id = j.getInt("id");
            mobile = j.getString("mobile");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                m1();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                stopSelf();
                User.group(Classified.getInstance());

                sendBroadcast(new Intent()
                        .setAction(name)
                        .addCategory(Intent.CATEGORY_DEFAULT)
                        .putExtra("data", "yes"));
            }
        }.execute(null, null, null);


    }

    public void m1() {


        readPhoneContacts();

        for (String mob : mobilelist) {
            allcont = allcont + mob + ",";
        }
        allcont = allcont.substring(0, allcont.length() - 1);

        HashMap<String, String> param = new HashMap<String, String>();
        param.put("contacts", allcont);
        param.put("id", id + "");
        cats = DAO.getJsonFromUrl(Classified.APP_API_URL, param);


        try {
            if (cats.getInt("success") > 0) {

              //  Log.e("CATS", cats.toString());

                ArrayList<User> users1 = User.fromJSON(cats.getJSONArray("users"));
                for (User user : users1) {
                    if (user.getType() == 1) {
                        user.setName(contactsname.get(user.getMobile()));
                    }

                }

                User.save(Classified.getInstance(), users1);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void readPhoneContacts() //This Context parameter is nothing but your Activity class's Context
    {
        Context cntx = Classified.getInstance();
        Cursor cursor = cntx.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        Integer contactsCount = cursor.getCount(); // get how many contacts you have in your contacts list
        if (contactsCount > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    //the below cursor will give you details for multiple contacts
                    Cursor pCursor = cntx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    // continue till this cursor reaches to all phone numbers which are associated with a contact in the contact list
                    while (pCursor.moveToNext()) {
                        int phoneType = pCursor.getInt(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                        //String isStarred 		= pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.STARRED));
                        String phoneNo = pCursor.getString(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //you will get all phone numbers according to it's type as below switch case.
                        //Logs.e will print the phone number along with the name in DDMS. you can use these details where ever you want.
                        switch (phoneType) {
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                              //  Log.e(contactName + ": TYPE_MOBILE", " " + (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString());
                                String m = ((((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).replace("*", "").replace("#", "")).trim().toString();


                                m = m.replaceAll("\\D+", "");

                                String numberOnly = phoneNo.replaceAll("[^0-9]", "");
                                ;
                                if (m.length() > 10) {
                                    m = m.substring(Math.max(m.length() - 10, 0));
                                }
                                if (m.equals(mobile)) {

                                } else {
                                    contactsname.put(m, contactName);
                                    mobilelist.add(m);
                                }
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                               // Log.e(contactName + ": TYPE_HOME", " " + phoneNo);
                                String m1 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m1 = m1.replaceAll("\\D+", "");

                                if (m1.length() > 10) {
                                    m1 = m1.substring(Math.max(m1.length() - 10, 0));
                                }
                                if (m1.equals(mobile)) {

                                } else {
                                    contactsname.put(m1, contactName);
                                    mobilelist.add(m1);
                                }
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                             ///   Log.e(contactName + ": TYPE_WORK", " " + phoneNo);
                                String m2 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m2 = m2.replaceAll("\\D+", "");


                                if (m2.length() > 10) {
                                    m2 = m2.substring(Math.max(m2.length() - 10, 0));
                                }
                                if (m2.equals(mobile)) {

                                } else {
                                    contactsname.put(m2, contactName);
                                    mobilelist.add(m2);
                                }
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE:
                              ///  Log.e(contactName + ": TYPE_WORK_MOBILE", " " + phoneNo);
                                String m3 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m3 = m3.replaceAll("\\D+", "");

                                if (m3.length() > 10) {
                                    m3 = m3.substring(Math.max(m3.length() - 10, 0));
                                }
                                if (m3.equals(mobile)) {

                                } else {
                                    contactsname.put(m3, contactName);
                                    mobilelist.add(m3);
                                }
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER:
                               // Log.e(contactName + ": TYPE_OTHER", " " + phoneNo);
                                String m4 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m4 = m4.replaceAll("\\D+", "");

                                if (m4.length() > 10) {
                                    m4 = m4.substring(Math.max(m4.length() - 10, 0));
                                }
                                if (m4.equals(mobile)) {

                                } else {
                                    contactsname.put(m4, contactName);
                                    mobilelist.add(m4);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    pCursor.close();
                }
            }
            cursor.close();

            SharedPreferences sharedPreferences = getSharedPreferences("PHONECONTACT", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            int no_mobile = mobilelist.size();
            editor.putInt("phone_no", no_mobile);
            editor.commit();
        }
    }


}
