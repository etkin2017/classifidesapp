package com.classifides.app.gcm.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.classifides.app.data.DAO;
import com.classifides.app.data.NotiDbHelper;
import com.classifides.app.gcm.adapter.ChatRoomsAdapter;
import com.classifides.app.gcm.adapter.StatusListAdapter;
import com.classifides.app.gcm.app.Config;
import com.classifides.app.gcm.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class EditStatus extends AppCompatActivity {
    TextView status_text;
    RecyclerView status_list;
    SharedPreferences sh;
    ProgressDialog pdd;
    StatusListAdapter mAdapter;

    int id;
    String status;
    NotiDbHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_status);
        status_text = (TextView) findViewById(R.id.status_text);
        status_list = (RecyclerView) findViewById(R.id.status_list);
        helper = new NotiDbHelper(Classified.getInstance());
        pdd = new ProgressDialog(EditStatus.this);
        pdd.setTitle("Please Wait");
        pdd.setMessage("update status .....");

        status_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dd = new Dialog(EditStatus.this);
                dd.setCancelable(true);
                dd.setContentView(R.layout.activity_edit_text);
                final EditText status_ed = (EditText) dd.findViewById(R.id.edit_status);
                final Button update = (Button) dd.findViewById(R.id.update);
                update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (TextUtils.isEmpty(status_ed.getText().toString())) {
                            status_ed.setError("please enter status");
                        } else {

                            updateStatus(status_ed.getText().toString());
                            dd.dismiss();
                        }
                    }

                    // dd.show();


                });
                dd.show();
            }
        });

        sh = getSharedPreferences("user", Context.MODE_PRIVATE);

        String users = sh.getString("users", "");
        try {
            JSONObject j = new JSONObject(users);
            id = j.getInt("id");
            status = j.getString("status");
            status_text.setText(status + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ArrayList<String> st = helper.getStatus();

        mAdapter = new StatusListAdapter(Classified.getInstance(), st);
        status_list.setLayoutManager(new LinearLayoutManager(Classified.getInstance()));

        mAdapter.setStatusListAdapter(st);
        status_list.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();


        status_list.addOnItemTouchListener(new ChatRoomsAdapter.RecyclerTouchListener(getApplicationContext(), status_list, new ChatRoomsAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                String sss = ((TextView) view.findViewById(R.id.s_text)).getText().toString();
                updateStatus(sss);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


    }

    private void updateStatus(final String stat) {

        new AsyncTask() {
            JSONObject cats = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pdd.show();
            }

            @Override
            protected Object doInBackground(Object[] params) {
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("status", stat);
                param.put("id", id + "");
                cats = DAO.getJsonFromUrl(Classified.updateStatus, param);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                pdd.dismiss();
                try {
                    if (cats.getInt("success") > 0) {

                        //Log.e("users", cats.toString());

                        SharedPreferences sh = getSharedPreferences("user", Context.MODE_PRIVATE);
                        SharedPreferences.Editor ed = sh.edit();
                        ed.putString("users", cats.getJSONArray("user").getJSONObject(0).toString());
                        ed.commit();

                        status_text.setText(cats.getJSONArray("user").getJSONObject(0).getString("status"));
                        String addsta = (cats.getJSONArray("user").getJSONObject(0).getString("status"));

                        ArrayList<String> as = new ArrayList<String>();
                        as.add(addsta);
                        try {

                            ArrayList<String> all_status = helper.getAllStatus();
                            if (all_status.contains(addsta)) {

                            } else {
                                helper.addStatus(as);

                                mAdapter.notifyDataSetChanged();
                            }
                            mAdapter.setStatusListAdapter(helper.getAllStatus());
                            status_list.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        mAdapter.notifyDataSetChanged();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

}
