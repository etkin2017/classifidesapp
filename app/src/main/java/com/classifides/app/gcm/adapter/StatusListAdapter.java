package com.classifides.app.gcm.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.classifides.app.gcm.activity.CreateGroupActivity;
import com.classifides.app.gcm.model.User;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ashvini on 9/12/2016.
 */
public class StatusListAdapter extends RecyclerView.Adapter<StatusListAdapter.ViewHolder> {

    private Context mContext;

    public void StatusListAdapter(ArrayList<String> statusListAdapter) {
        this.statusListAdapter = statusListAdapter;
    }

    public ArrayList<String> getStatusListAdapter() {
        return statusListAdapter;
    }

    public void setStatusListAdapter(ArrayList<String> statusListAdapter) {
        this.statusListAdapter = statusListAdapter;
    }

    private ArrayList<String> statusListAdapter;

    public ArrayList<String> getGroupAdapter() {
        return statusListAdapter;
    }

    private static String today;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, uid_id;


        public ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.s_text);


        }
    }


    public StatusListAdapter(Context mContext, ArrayList<String> statusListAdapter) {
        this.mContext = mContext;
        this.statusListAdapter = statusListAdapter;

        Calendar calendar = Calendar.getInstance();
        today = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.status_list_row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        //  User user = statusListAdapter.get(position);

        holder.name.setText(statusListAdapter.get(position));

    }

    @Override
    public int getItemCount() {
        return statusListAdapter.size();
    }





}