package com.classifides.app.gcm.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.classifides.app.data.DAO;
import com.classifides.app.gcm.activity.HomeChatActivity;
import com.classifides.app.gcm.adapter.ChatRoomsAdapter;
import com.classifides.app.gcm.adapter.GroupAdapter;
import com.classifides.app.gcm.model.NewMessage;
import com.classifides.app.gcm.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupFragment extends Fragment {

    private ArrayList<String> mobilelist;
    String allcont = "";
    SharedPreferences sh;
    private int id;
    RecyclerView contrecy;

    TextView creategp;
    HashMap<String, String> contactsname;
    GroupAdapter mAdaptor;
    EditText egname;
    public static ArrayList<String> user_id;
    String ulist = "";

    public GroupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_group, container, false);
        contactsname = new HashMap<String, String>();
        mobilelist = new ArrayList<String>();

        HomeChatActivity.group=1;

        contrecy = (RecyclerView) v.findViewById(R.id.contrecyc);
        mAdaptor = new GroupAdapter(Classified.getInstance(), User.load(Classified.getInstance()));
        contrecy.setLayoutManager(new LinearLayoutManager(getActivity()));
        contrecy.setAdapter(mAdaptor);
        egname = (EditText) v.findViewById(R.id.egname);
        sh = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);

        String users = sh.getString("users", "");
        try {
            JSONObject j = new JSONObject(users);
            id = j.getInt("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        creategp = (TextView) v.findViewById(R.id.creategp);

        creategp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (user_id.size() == 0) {
                    Toast.makeText(getActivity(), "select contacts", Toast.LENGTH_LONG).show();
                } else {


                    for (int i = 0; i < user_id.size(); i++) {
                        ulist = ulist + user_id.get(i) + ",";
                    }


                    int len = ulist.length();

                    String ul = ulist + Classified.getInstance().getPrefManager().getUser().getId();

                    //Log.e("ULIST", ul);

                    if (TextUtils.isEmpty(egname.getText().toString())) {
                        egname.setError("please enter group name");
                    } else {
                        createGroup(ul, egname.getText().toString());
                    }


                }

            }
        });

        getUsers();
        return v;

    }

    private void createGroup(final String m, final String gname) {
        final Map<String, String> data = new HashMap<String, String>();


        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                data.put("gname", gname);
                data.put("a_id", id + "");
                data.put("u_id", m);


            /*    try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/

              /*  ByteArrayOutputStream stream = new ByteArrayOutputStream();
                PostBannerAddOne.bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);

                new Encode_image().execute();*/
            }

            @Override
            protected Object doInBackground(Object[] params) {

                res = DAO.getJsonFromUrl(Classified.CREATE_GROUP, data);
                   /* if (res.getInt("success") > 0) {
                        int j = 0;
                        byte[] image = streams.toByteArray();
                        Map<String, String> d = new HashMap<String, String>();
                        d.put("filename", data.get("imagename") + j + ".jpg");
                        d.put("image", Base64.encodeToString(image, 0));
                        DAO.getJsonFromUrl(Config.Server + "uploadimage.php", d);
                        j++;



                    }*/


                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                egname.setText("");
                Toast.makeText(getActivity(),"Group Created", Toast.LENGTH_SHORT).show();
                /* if (res.getInt("success") > 0) {
                     Toast.makeText(getActivity(), res.toString(), Toast.LENGTH_SHORT).show();

                 } else {
                     Toast.makeText(getActivity(), res.toString(), Toast.LENGTH_SHORT).show();
                 }*/
            }
        }.execute(null, null, null);


    }

    private void getUsers() {


        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                readPhoneContacts();

                for (String mob : mobilelist) {
                    allcont = allcont + mob + ",";
                }
                allcont = allcont.substring(0, allcont.length() - 1);
                return null;
            }


            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                //getUsersFromServer(allcont);
                getCategories();

            }


        }.execute(null, null, null);
    }

    private void getUsersFromServer(final String allcont) {
        HashMap<String, String> param = new HashMap<String, String>();
        param.put("contacts", allcont);
        param.put("id", id + "");
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Classified.APP_API_URL, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt("success") > 0) {
                        ArrayList<User> users = User.fromJSON(response.getJSONArray("users"));
                        for (User user : users) {
                            user.setName(contactsname.get(user.getMobile()));
                        }

                        User.save(Classified.getInstance(), users);

                        mAdaptor.setGroupAdapter(User.load(Classified.getInstance()));
                        mAdaptor.notifyDataSetChanged();


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("contacts", allcont);
                param.put("id", id + "");
                return param;
            }
        };
        Classified.getInstance().addToRequestQueue(request);
    }


    private void getCategories() {
        new AsyncTask() {
            JSONObject cats = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected Object doInBackground(Object[] params) {
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("contacts", allcont);
                param.put("id", id + "");
                cats = DAO.getJsonFromUrl(Classified.APP_API_URL, param);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    if (cats.getInt("success") > 0) {

                        ArrayList<User> users = User.fromJSON(cats.getJSONArray("users"));
                        for (User user : users) {
                            if (user.getType() == 1)
                                user.setName(contactsname.get(user.getMobile()));
                        }

                        User.save(Classified.getInstance(), users);

                        mAdaptor.setGroupAdapter(User.load(Classified.getInstance()));
                        mAdaptor.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

    public void readPhoneContacts() //This Context parameter is nothing but your Activity class's Context
    {
        Context cntx = Classified.getInstance();
        Cursor cursor = cntx.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        Integer contactsCount = cursor.getCount(); // get how many contacts you have in your contacts list
        if (contactsCount > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    //the below cursor will give you details for multiple contacts
                    Cursor pCursor = cntx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    // continue till this cursor reaches to all phone numbers which are associated with a contact in the contact list
                    while (pCursor.moveToNext()) {
                        int phoneType = pCursor.getInt(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                        //String isStarred 		= pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.STARRED));
                        String phoneNo = pCursor.getString(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //you will get all phone numbers according to it's type as below switch case.
                        //Logs.e will print the phone number along with the name in DDMS. you can use these details where ever you want.
                        switch (phoneType) {
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                //Log.e(contactName + ": TYPE_MOBILE", " " + (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString());
                                String m = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();
                                if (m.length() > 10) {
                                    m = m.substring(Math.max(m.length() - 10, 0));
                                }
                                contactsname.put(m, contactName);
                                mobilelist.add(m);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                //Log.e(contactName + ": TYPE_HOME", " " + phoneNo);
                                String m1 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();
                                if (m1.length() > 10) {
                                    m1 = m1.substring(Math.max(m1.length() - 10, 0));
                                }
                                contactsname.put(m1, contactName);
                                mobilelist.add(m1);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                                //Log.e(contactName + ": TYPE_WORK", " " + phoneNo);
                                String m2 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();
                                if (m2.length() > 10) {
                                    m2 = m2.substring(Math.max(m2.length() - 10, 0));
                                }
                                contactsname.put(m2, contactName);
                                mobilelist.add(m2);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE:
                                //Log.e(contactName + ": TYPE_WORK_MOBILE", " " + phoneNo);
                                String m3 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                if (m3.length() > 10) {
                                    m3 = m3.substring(Math.max(m3.length() - 10, 0));
                                }
                                mobilelist.add(m3);
                                contactsname.put(m3, contactName);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER:
                                //Log.e(contactName + ": TYPE_OTHER", " " + phoneNo);
                                String m4 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();
                                if (m4.length() > 10) {
                                    m4 = m4.substring(Math.max(m4.length() - 10, 0));
                                }
                                mobilelist.add(m4);
                                contactsname.put(m4, contactName);
                                break;
                            default:
                                break;
                        }
                    }
                    pCursor.close();
                }
            }
            cursor.close();
        }
    }


}
