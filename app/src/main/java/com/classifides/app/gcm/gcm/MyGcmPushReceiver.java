/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.classifides.app.gcm.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.classifides.app.data.NotiDbHelper;
import com.classifides.app.gcm.activity.ChatRoomActivity;
import com.classifides.app.gcm.activity.HomeChatActivity;
import com.classifides.app.gcm.activity.MainActivity;
import com.classifides.app.gcm.activity.NewChatRoomActivity;
import com.classifides.app.gcm.app.Config;

import com.classifides.app.gcm.model.Message;
import com.classifides.app.gcm.model.NewMessage;
import com.classifides.app.gcm.model.User;
import com.google.android.gms.gcm.GcmListenerService;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class MyGcmPushReceiver extends FirebaseMessagingService {

    private static final String TAG = MyGcmPushReceiver.class.getSimpleName();

    private NotificationUtils notificationUtils;
    private static String today;
    boolean log;
    public static final String name = "Notification";
    public static final String name2 = "Notification2";
    public static final String name3 = "Notification3";
    public static final String name5 = "Notification5";
    public static long downloadedsize;

    NotiDbHelper mHelper;
    int idd;
    String flag = "1";
    boolean isBackground = false;
    SharedPreferences shhh;
    String filepath, fname, imgnm, videonm;
    File file;
    String name1, name12;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        mHelper = new NotiDbHelper(getApplicationContext());

        //Log.e(TAG, "From: " + remoteMessage.getFrom());
        String from = remoteMessage.getFrom();
        String title = remoteMessage.getData().get("title");//("title");
        String message = remoteMessage.getData().get("message");
        String mmmmm = remoteMessage.getData().get("message");
        String sid = remoteMessage.getData().get("sid");
        //  String name1 = remoteMessage.getData().get("name");
        String img = remoteMessage.getData().get("image");

        String unknown = remoteMessage.getData().get("unknown");

        try {
            JSONArray unknown_user_array = new JSONArray(unknown);
            String mobile1 = unknown_user_array.getJSONObject(0).getString("mobile").trim();
            String name1 = unknown_user_array.getJSONObject(0).getString("name").trim();
            String image1 = unknown_user_array.getJSONObject(0).getString("image").trim();
            String type1 = unknown_user_array.getJSONObject(0).getString("type").trim();
            String aid1 = unknown_user_array.getJSONObject(0).getString("aid").trim();
            String status1 = unknown_user_array.getJSONObject(0).getString("status").trim();
            String id1 = unknown_user_array.getJSONObject(0).getString("id").trim();

            ArrayList<User> all_user1 = mHelper.getAllUser();

            for (User uu1 : all_user1) {
                if (uu1.getMobile().equals(mobile1)) {

                } else {
                    User uk_user = new User(mobile1 + "-" + name1, image1, Integer.parseInt(type1), Integer.parseInt(id1), mobile1, status1, Integer.parseInt(aid1), 0, false);
                    mHelper.saveUnknownUsers(uk_user);
                }
            }


            /*for (int i = 0; i < all_user1.size(); i++) {
                if (all_user1.get(i).getMobile().equals(mobile1)) {

                } else {
                    User uk_user = new User(mobile1 + "-" + name1, image1, Integer.parseInt(type1), Integer.parseInt(id1), mobile1, status1, Integer.parseInt(aid1), 0, false);
                    mHelper.saveUnknownUsers(uk_user);
                }
            }*/
        } catch (JSONException e) {
            e.printStackTrace();
        }


        String rid = remoteMessage.getData().get("rid");
        //Log.e("RID", rid);
        String rtime = remoteMessage.getData().get("rtime");
        String type = remoteMessage.getData().get("type");
        //Log.e("TYPE", type);
        String flag = remoteMessage.getData().get("flag");
        String chatRoomId = remoteMessage.getData().get("chatRoomId");


        shhh = getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);

        if ((Integer.parseInt(type) == 1)) {
            name1 = mHelper.getNameOfUser(Integer.parseInt(sid));
            message = name1 + " : " + message;
            imgnm = name1 + " : sent a photo";
        } else if ((Integer.parseInt(type) == 2)) {
            name12 = mHelper.getNameOfUser(Integer.parseInt(sid));
            name1 = mHelper.getNameOfUser(Integer.parseInt(rid));
            if (Integer.parseInt(img) == 1) {
                imgnm = name1 + " : sent a photo";
            } else if (Integer.parseInt(img) == 2) {
                imgnm = name1 + " : sent a video";
            }
            mmmmm = name12 + " : " + mmmmm;
            message = name1 + " : " + name12 + " : " + message;
        } else if ((Integer.parseInt(type) == 4)) {
           /* Intent i = new Intent(getApplicationContext(), UpdateContactService.class);
            startService(i);*/

            sendBroadcast(new Intent()
                    .setAction(name5)
                    .addCategory(Intent.CATEGORY_DEFAULT)
                    .putExtra("data", "yes"));


        }

        String users = shhh.getString("users", "");
        try {
            JSONObject j = new JSONObject(users);
            idd = j.getInt("id");
            // uname=j.getString("name");

        } catch (JSONException e) {
            e.printStackTrace();
        }


        videonm = name1 + " :  sent a video";
        //Log.e("SID", sid);


        if (Integer.parseInt(type) == 1) {
            flag = "1";
        } else {
            flag = "2";
        }
        SharedPreferences sh1 = getApplicationContext().getSharedPreferences("chat_rm", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sh1.edit();
      /*  editor.putString("rid",chatRoomId+"");
        editor.putString("sid",id+"");
        editor.putString("type",type+"");
        */
        String _rid = sh1.getString("rid", "");
        //Log.e("_rid", _rid);
        String _sid = sh1.getString("sid", "");
        //Log.e("_sid", _sid);
        String _type = sh1.getString("type", "");
        //Log.e("_type", _type);

        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        //Log.e("TTTT", timeStamp + "");

        cntByID(chatRoomId + "", Integer.parseInt(sid));

        SharedPreferences sh = getSharedPreferences("log", MODE_PRIVATE);
        log = sh.getBoolean("login", false);
        if (log) {

            if (Integer.parseInt(type) == 1) {
                NewMessage m = new NewMessage(0, Integer.parseInt(rid), Integer.parseInt(sid), mmmmm, timeStamp + "", timeStamp + "", Integer.parseInt(type), Integer.parseInt(sid), Integer.parseInt(img), 1);
                mHelper.saveSingleMessage(m);
                //Log.e("Message1", m.toString());
            } else if (Integer.parseInt(type) == 2) {
                NewMessage m = new NewMessage(0, Integer.parseInt(rid), Integer.parseInt(sid), mmmmm, timeStamp + "", timeStamp + "", Integer.parseInt(type), Integer.parseInt(rid), Integer.parseInt(img), 1);
                mHelper.saveSingleMessage(m);
                //Log.e("Message2", m.toString());
            }


            //Log.e("NewChatRoomActivity", (NewChatRoomActivity.activate1) + "");
            if ((_rid.equals(sid)) && (_sid.equals(rid)) && (_type.equals(type)) && (NewChatRoomActivity.activate1 == true)) {
                sendBroadcast(new Intent()
                        .setAction(name)
                        .addCategory(Intent.CATEGORY_DEFAULT)
                        .putExtra("data", "yes"));
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();


            } else if ((_rid.equals(rid)) && (_type.equals(type)) && (NewChatRoomActivity.activate1 == true)) {
                sendBroadcast(new Intent()
                        .setAction(name)
                        .addCategory(Intent.CATEGORY_DEFAULT)
                        .putExtra("data", "yes"));
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();


            } else {

                if (Integer.parseInt(type) == 1) {

                    if (Integer.parseInt(img) == 0) {
                        Intent intentres = new Intent(getApplicationContext(), HomeChatActivity.class);
                        showNotificationMessage(getApplicationContext(), title, message, sid + "", Integer.parseInt(type), rtime, intentres);
                        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                        notificationUtils.playNotificationSound();
                    } else if (Integer.parseInt(img) == 1) {
                        Intent intentres = new Intent(getApplicationContext(), HomeChatActivity.class);
                        showImageMessage(getApplicationContext(), title, imgnm, sid + "", Integer.parseInt(type), rtime, intentres);
                        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                        notificationUtils.playNotificationSound();
                    } else if (Integer.parseInt(img) == 2) {
                        Intent intentres = new Intent(getApplicationContext(), HomeChatActivity.class);
                        showImageMessage(getApplicationContext(), title, imgnm, rid + "", Integer.parseInt(type), rtime, intentres);
                        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                        notificationUtils.playNotificationSound();
                    }
                } else if (Integer.parseInt(type) == 2) {
                    if (Integer.parseInt(img) == 0) {
                        Intent intentres = new Intent(getApplicationContext(), HomeChatActivity.class);
                        showNotificationMessage(getApplicationContext(), title, message, rid + "", Integer.parseInt(type), rtime, intentres);
                        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                        notificationUtils.playNotificationSound();
                    } else if (Integer.parseInt(img) == 1) {
                        Intent intentres = new Intent(getApplicationContext(), HomeChatActivity.class);
                        showImageMessage(getApplicationContext(), title, imgnm, rid + "", Integer.parseInt(type), rtime, intentres);
                        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                        notificationUtils.playNotificationSound();
                    } else if (Integer.parseInt(img) == 2) {
                        Intent intentres = new Intent(getApplicationContext(), HomeChatActivity.class);
                        showImageMessage(getApplicationContext(), title, imgnm, rid + "", Integer.parseInt(type), rtime, intentres);
                        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                        notificationUtils.playNotificationSound();
                    }
                }
                if (Integer.parseInt(type) == 1) {
                    sendBroadcast(new Intent()
                            .setAction(name2)
                            .addCategory(Intent.CATEGORY_DEFAULT)
                            .putExtra("data", "yes"));

                }
                sendBroadcast(new Intent()
                        .setAction(name3)
                        .addCategory(Intent.CATEGORY_DEFAULT)
                        .putExtra("data", "yes"));
                //sendNotification(title, message);
            }


        } else {

        }

//2016-06-10 15:05:49

        //  NewMessage m = new NewMessage(0, id, chatRoomId, message, timeStamp + "", timeStamp + "", type, chatRoomId);

        //if(rid.equals("2"))

        if (remoteMessage.getData().size() > 0) {
            //Log.e(TAG, "Message data payload: " + remoteMessage.getData());
        }


    }


    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String id, int type, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, id, type, timeStamp, intent, imageUrl);
    }


    private void sendNotification(String messageBody, String message) {


        int notify_no = 1;

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, notify_no /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        if (notify_no < 9) {
            notify_no = notify_no + 1;
        } else {
            notify_no = 0;
        }

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.logo)
                .setContentTitle(messageBody)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(notify_no + 2 /* ID of notification */, notificationBuilder.build());
    }


    private void showNotificationMessage(Context context, String title, String message, String id, int type, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, id, type, timeStamp, intent);
    }


    private void showImageMessage(Context context, String title, String message, String id, int type, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, id, type, timeStamp, intent);
    }


    public void cntByID(String chat_rm_id, int cnt) {
        HashMap<String, String> m1 = new HashMap<String, String>();
        m1.put(chat_rm_id, cnt + "");
    }


}

