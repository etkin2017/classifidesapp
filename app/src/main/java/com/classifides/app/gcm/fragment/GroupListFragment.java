package com.classifides.app.gcm.fragment;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.classifides.app.LoginActivity;
import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.data.NotiDbHelper;
import com.classifides.app.gcm.activity.CreateGroupActivity;
import com.classifides.app.gcm.activity.EditGroup;
import com.classifides.app.gcm.activity.EditStatus;
import com.classifides.app.gcm.activity.HomeChatActivity;
import com.classifides.app.gcm.activity.NewChatRoomActivity;
import com.classifides.app.gcm.adapter.ChatRoomsAdapter;
import com.classifides.app.gcm.adapter.GroupChatAdapter;
import com.classifides.app.gcm.adapter.RecentChatRoomAdapter;
import com.classifides.app.gcm.gcm.MyGcmPushReceiver;
import com.classifides.app.gcm.gcm.UpdateContactService;
import com.classifides.app.gcm.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupListFragment extends Fragment {

    private ArrayList<String> mobilelist;
    String allcont = "";
    SharedPreferences sh;
    private int id;
    public int m_id;
    public static String intent_txt;
    RecyclerView contrecy;

    ProgressDialog pd;

    public static String g_id = "";


    HashMap<String, String> contactsname;
    GroupChatAdapter mAdaptor;
    SwipeRefreshLayout mySwipeRefreshLayout;
    public ArrayList<String> deleteArrayId;
    public ArrayList<String> editArrayId;
    public ArrayList<String> leaveGroupId;
    public String action = "";
    public String gid = "";

    public static String status11 = "";
    public ArrayList<String> del_group;
    NotiDbHelper mhelper;

    public static String mobile;

    String imUrl;

    MyNewBroadcast broadcast;
    MyNewBroadcast1 broadcast1;
    HashSet<User> all, one, two;
    ArrayList<User> minus_array;
    ArrayList<User> allGroup;
    ArrayList<User> recentChatgroup;

    public GroupListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_group_list, container, false);
        HomeChatActivity.group = 1;
        contactsname = new HashMap<String, String>();
        setHasOptionsMenu(true);

        mhelper = new NotiDbHelper(Classified.getInstance());
        minus_array = new ArrayList<User>();
        mobilelist = new ArrayList<String>();
        contrecy = (RecyclerView) v.findViewById(R.id.contrecyc);


        sh = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);

        final String users = sh.getString("users", "");
        try {
            JSONObject j = new JSONObject(users);
            m_id = j.getInt("id");
            mobile = j.getString("mobile");

        } catch (JSONException e) {
            e.printStackTrace();
        }


        try {

            /*allGroup = mhelper.getAllGroup();//User.group(Classified.getInstance());
            if (allGroup == null) {
                allGroup = new ArrayList<>();
            }
*/
            User.group(Classified.getInstance());
            recentChatgroup = mhelper.getUsersGroupByMessage();


            if (recentChatgroup.size() == 0) {
                recentChatgroup = new ArrayList<>();
                getAllGroupById();
                mAdaptor = new GroupChatAdapter(Classified.getInstance(), recentChatgroup);
                contrecy.setLayoutManager(new LinearLayoutManager(getActivity()));
                mAdaptor.setChatRoomArrayList(recentChatgroup);
                contrecy.setAdapter(mAdaptor);
            } else {


                mAdaptor = new GroupChatAdapter(Classified.getInstance(), recentChatgroup);
                contrecy.setLayoutManager(new LinearLayoutManager(getActivity()));
                mAdaptor.setChatRoomArrayList(recentChatgroup);
                contrecy.setAdapter(mAdaptor);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        mySwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.mySwipeRefreshLayout);
        deleteArrayId = new ArrayList<String>();
        editArrayId = new ArrayList<String>();
        del_group = new ArrayList<String>();

        pd = new ProgressDialog(getActivity());
        pd.setMessage("Please Wait");

        leaveGroupId = new ArrayList<String>();


        if (recentChatgroup == null) {
            recentChatgroup = new ArrayList<User>();
        } else {

            mAdaptor = new GroupChatAdapter(Classified.getInstance(), recentChatgroup);
            mAdaptor.setChatRoomArrayList(recentChatgroup);
        }


        try {

            mAdaptor.setChatRoomArrayList(recentChatgroup);
        } catch (Exception e) {
            e.printStackTrace();
        }
        contrecy.setLayoutManager(new LinearLayoutManager(getActivity()));
        contrecy.setAdapter(mAdaptor);


        broadcast = new MyNewBroadcast();
        IntentFilter filter = new IntentFilter(MyGcmPushReceiver.name3);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        try {
            getActivity().registerReceiver(broadcast, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }



        broadcast1 = new MyNewBroadcast1();
        IntentFilter filter1 = new IntentFilter(MyGcmPushReceiver.name5);
        filter1.addCategory(Intent.CATEGORY_DEFAULT);
        try {
            getActivity().registerReceiver(broadcast1, filter1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        contrecy.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), contrecy, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                TextView long_press_icon = (TextView) view.findViewById(R.id.long_press_icon);
                String chatroomid = ((TextView) view.findViewById(R.id.chatroomid)).getText().toString();
                String id = ((TextView) view.findViewById(R.id.id)).getText().toString();
                String url = ((TextView) view.findViewById(R.id.url)).getText().toString();
                String type = ((TextView) view.findViewById(R.id.type)).getText().toString();
                String block = ((TextView) view.findViewById(R.id.block)).getText().toString();
                String name = ((TextView) view.findViewById(R.id.name)).getText().toString();
                // chatroomid, id, url, type, block
                String aid = ((TextView) view.findViewById(R.id.aid)).getText().toString();

                String set_checked = ((TextView) view.findViewById(R.id.set_checked)).getText().toString();

                long_press_icon.getTag(position);
                //  long_press_icon.setVisibility(View.GONE);

                //contrecy.getAdapter().getItemId(position)


                if (mAdaptor.getChatRoomArrayList().get(position).isTicked() == true) {

                  /*  g_id = "";
                    if (deleteArrayId.contains(id)) {
                        deleteArrayId.remove(id);
                    }
*/
                    /*((TextView) view.findViewById(R.id.set_checked)).setText("0");

                    if (((TextView) view.findViewById(R.id.set_checked)).getText().toString().equals("0")) {
                        long_press_icon.setVisibility(View.GONE);
                    } else {
                        long_press_icon.setVisibility(View.VISIBLE);
                    }*/
                    long_press_icon.setVisibility(View.GONE);

                    mAdaptor.getChatRoomArrayList().get(position).setTicked(false);
                    del_group.remove(aid + "");
                    deleteArrayId.remove(id + "");
                } else {
                    Intent intent = new Intent(getActivity(), NewChatRoomActivity.class);
                    intent.putExtra("type", type);
                    intent.putExtra("id", id);
                    intent.putExtra("name", name);
                    intent.putExtra("url", url);
                    intent.putExtra("str_txt", HomeChatActivity.sharedText);
                    intent.putExtra("block", block);
                    intent.putExtra("onlyImage", "1");
                    startActivity(intent);

                }

                if (del_group.size() == 0) {

                    action = "";
                }
                if (deleteArrayId.size() == 0) {

                    action = "";
                }
                // del_group.remove()
            }

            @Override
            public void onLongClick(View view, int position) {
                TextView long_press_icon = (TextView) view.findViewById(R.id.long_press_icon);
                long_press_icon.getTag(position);


                String chatroomid = ((TextView) view.findViewById(R.id.chatroomid)).getText().toString();
                String id = ((TextView) view.findViewById(R.id.id)).getText().toString();
                String url = ((TextView) view.findViewById(R.id.url)).getText().toString();
                String type = ((TextView) view.findViewById(R.id.type)).getText().toString();
                String block = ((TextView) view.findViewById(R.id.block)).getText().toString();
                String name = ((TextView) view.findViewById(R.id.name)).getText().toString();
                String aid = ((TextView) view.findViewById(R.id.aid)).getText().toString();
                ((TextView) view.findViewById(R.id.set_checked)).setText("1");

             /*   if (((TextView) view.findViewById(R.id.set_checked)).getText().toString().equals("1")) {
                    long_press_icon.setVisibility(View.VISIBLE);
                } else {
                    long_press_icon.setVisibility(View.GONE);
                }*/

                mAdaptor.getChatRoomArrayList().get(position).setTicked(true);
                mAdaptor.notifyDataSetChanged();

                action = "action";

                String aidd = aid + "";
                //Log.e("AIIDDD", aidd);
                g_id = id + "";
                imUrl = url;
                status11 = name;
                try {
                    deleteArrayId.add(id + "");
                    del_group.add(aid + "");
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }));


        //Method call for populating the view
        // populateRecyclerViewValues();


        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {

                        refresh();
                    }
                }
        );


        return v;
    }

    public static interface ClickListener {
        public void onClick(View view, int position);

        public void onLongClick(View view, int position);
    }


    public class MyNewBroadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            //  String bd = intent.getExtras().getString("data");
            refresh();

            recentChatgroup = mhelper.getUsersGroupByMessage();

           /* for (int i = 0; i < allGroup.size(); i++) {//6

                for (int j = 0; j < recentChatgroup.size(); j++) {//3
                    if (allGroup.get(i).getId() == (recentChatgroup).get(j).getId()) {
                        allGroup.remove(i);
                    } else {
                        minus_array.add(allGroup.get(j));
                    }
                }
            }*/

            mAdaptor = new GroupChatAdapter(Classified.getInstance(), recentChatgroup);
            contrecy.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdaptor.setChatRoomArrayList(recentChatgroup);
            contrecy.setAdapter(mAdaptor);
            mAdaptor.notifyDataSetChanged();

        }
    }

    public class MyNewBroadcast1 extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            //  String bd = intent.getExtras().getString("data");
            getAllGroupById();

       /*     recentChatgroup = mhelper.getUsersGroupByMessage();
            mAdaptor = new GroupChatAdapter(Classified.getInstance(), recentChatgroup);
            contrecy.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdaptor.setChatRoomArrayList(recentChatgroup);
            contrecy.setAdapter(mAdaptor);
            mAdaptor.notifyDataSetChanged();*/

        }
    }


    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private ClickListener clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ClickListener clicklistener) {

            this.clicklistener = clicklistener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recycleView.findChildViewUnder(e.getX(), e.getY());

                    if (child != null && clicklistener != null) {
                        clicklistener.onLongClick(child, recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clicklistener != null && gestureDetector.onTouchEvent(e)) {
                clicklistener.onClick(child, rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //  getUsers();
        HomeChatActivity.group = 1;
        refresh();
    }

    @Override
    public void onPause() {
        super.onPause();
        HomeChatActivity.group = 0;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_for_group, menu);

    }

    public void refresh() {
        del_group.clear();
        deleteArrayId.clear();
        User.group(Classified.getInstance());
        recentChatgroup = mhelper.getUsersGroupByMessage();
        mAdaptor = new GroupChatAdapter(Classified.getInstance(), recentChatgroup);
        contrecy.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdaptor.setChatRoomArrayList(recentChatgroup);
        contrecy.setAdapter(mAdaptor);
        mAdaptor.notifyDataSetChanged();
        mySwipeRefreshLayout.setRefreshing(false);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // return super.onOptionsItemSelected(item);
        int id = item.getItemId();

        if (id == R.id.delete_group) {

            if (deleteArrayId.size() == 0) {
                Toast.makeText(getActivity(), "Please select group", Toast.LENGTH_LONG).show();
                refresh();
            } else if (deleteArrayId.size() > 1) {
                Toast.makeText(getActivity(), "Please select one  group", Toast.LENGTH_LONG).show();
                refresh();

            } else {
                if (Integer.parseInt(del_group.get(0)) == m_id) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Alert");
                    builder.setMessage("Are you sure you want to delete this group?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            deleteGroup(g_id);
                            del_group.clear();
                            deleteArrayId.clear();

                            refresh();
                            //   getUsers();
                            mAdaptor.notifyDataSetChanged();
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.setCancelable(false);
                    builder.show();

                } else {
                    Toast.makeText(getActivity(), "Deletion failed, your not admin of this group", Toast.LENGTH_LONG).show();
                }
            }
        }
        if (id == R.id.edit_group) {
            if (deleteArrayId.size() == 0) {
                Toast.makeText(getActivity(), "Please select group", Toast.LENGTH_LONG).show();
                refresh();
                del_group.clear();
                deleteArrayId.clear();
            } else if (deleteArrayId.size() > 1) {
                Toast.makeText(getActivity(), "Please one select group", Toast.LENGTH_LONG).show();
                refresh();
                del_group.clear();
                deleteArrayId.clear();
            } else {
                String gg = deleteArrayId.get(0);
                refresh();
                Intent ia = new Intent(getActivity(), EditGroup.class);
                ia.putExtra("gid", gg);
                ia.putExtra("status", status11);
                ia.putExtra("imgurl", imUrl);
                startActivity(ia);

                del_group.clear();
                deleteArrayId.clear();

              /*  del_group.clear();
                deleteArrayId.clear();*/

            }
        }
        if (id == R.id.create_group) {
            startActivity(new Intent(getActivity(), CreateGroupActivity.class));
        }
        if (id == R.id.leave_group) {
            if (deleteArrayId.size() == 0) {
                Toast.makeText(getActivity(), "Please select group", Toast.LENGTH_LONG).show();
                refresh();
                del_group.clear();
                deleteArrayId.clear();
            } else if (deleteArrayId.size() > 1) {
                Toast.makeText(getActivity(), "Please one select group", Toast.LENGTH_LONG).show();
                refresh();
                del_group.clear();
                deleteArrayId.clear();

            } else {


                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Alert");
                builder.setMessage("Are you sure you want to leave this group?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        leaveGroup(deleteArrayId.get(0));
                        refresh();
                        del_group.clear();
                        deleteArrayId.clear();
                        mAdaptor.notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.setCancelable(false);
                builder.show();


            }
        }
        return false;
    }

    private void deleteGroup(final String g_id) {

        new AsyncTask() {
            JSONObject cats = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pd.show();

            }

            @Override
            protected Object doInBackground(Object[] params) {
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("gid", g_id);
                param.put("aid", m_id + "");
                cats = DAO.getJsonFromUrl(Classified.deleteGroup, param);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                pd.dismiss();
                try {
                    if (cats.getInt("success") > 0) {
                        //Log.e("LEAVE GROUP", cats.toString());
                        boolean t = mhelper.delgroup(Integer.parseInt(g_id));
                        if (t == true) {
                            //Log.e("Success GROUP", t + "");
                            mAdaptor.notifyDataSetChanged();
                            refresh();

                        } else {
                            //Log.e("Success GROUP", t + "");
                            mAdaptor.notifyDataSetChanged();
                            refresh();
                        }
                        // getUsers();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);

    }

    private void leaveGroup(final String gid) {

        new AsyncTask() {
            JSONObject cats = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pd.show();
            }

            @Override
            protected Object doInBackground(Object[] params) {
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("gid", gid);
                param.put("uid", m_id + "");
                cats = DAO.getJsonFromUrl(Classified.leave_Group, param);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                pd.dismiss();
                //Log.e("CATS", cats.toString());
                try {
                    if (cats.getInt("success") > 0) {

                        //Log.e("CATS", cats.toString());
                        boolean t = mhelper.delgroup(Integer.parseInt(gid));
                        if (t == true) {
                            //Log.e("Success GROUP", t + "");
                            mAdaptor.notifyDataSetChanged();
                            refresh();
                        } else {
                            //Log.e("Success GROUP", t + "");
                            mAdaptor.notifyDataSetChanged();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);


    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (action.equals("action")) {
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(false);
            menu.getItem(2).setVisible(false);
            menu.getItem(3).setVisible(true);
            menu.getItem(4).setVisible(true);
            menu.getItem(5).setVisible(true);
            //       menu.getItem(6).setVisible(true);

        } else {
            menu.getItem(0).setVisible(true);
            menu.getItem(1).setVisible(true);
            menu.getItem(2).setVisible(true);
            menu.getItem(3).setVisible(false);
            menu.getItem(4).setVisible(false);
            menu.getItem(5).setVisible(false);
            //   menu.getItem(6).setVisible(false);
        }

    }


    public void getAllGroupById() {
        new AsyncTask() {
            JSONObject cats = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected Object doInBackground(Object[] params) {
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("id", m_id + "");
                cats = DAO.getJsonFromUrl(Config.getAllGroupById, param);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    if (cats.getInt("success") > 0) {

                        //Log.e("CATS", cats.toString());

                        ArrayList<User> users = User.fromJSON(cats.getJSONArray("users"));
                        for (User user : users) {
                            if (user.getType() == 1) {
                                user.setName(contactsname.get(user.getMobile()));
                            }

                        }
                        User.saveGroup(Classified.getInstance(), users);

                        refresh();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

  /*  public void getUsers() {


        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                readPhoneContacts();

                for (String mob : mobilelist) {
                    allcont = allcont + mob + ",";
                }
                allcont = allcont.substring(0, allcont.length() - 1);
                return null;
            }


            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                //getUsersFromServer(allcont);
                getCategories();

            }


        }.execute(null, null, null);
    }

    private void getUsersFromServer(final String allcont) {
        HashMap<String, String> param = new HashMap<String, String>();
        param.put("contacts", allcont);
        param.put("id", id + "");
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Classified.APP_API_URL, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt("success") > 0) {
                        ArrayList<User> users = User.fromJSON(response.getJSONArray("users"));
                        for (User user : users) {
                            user.setName(contactsname.get(user.getMobile()));
                        }

                        User.save(Classified.getInstance(), users);

                        mAdaptor.setChatRoomArrayList(User.load(Classified.getInstance()));
                        mAdaptor.notifyDataSetChanged();


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("contacts", allcont);
                param.put("id", id + "");
                return param;
            }
        };
        Classified.getInstance().addToRequestQueue(request);
    }


    public void getCategories() {
        new AsyncTask() {
            JSONObject cats = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected Object doInBackground(Object[] params) {
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("contacts", allcont);
                param.put("id", m_id + "");
                cats = DAO.getJsonFromUrl(Classified.APP_API_URL, param);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    if (cats.getInt("success") > 0) {

                        mySwipeRefreshLayout.setRefreshing(false);

                        Log.e("CATS", cats.toString());

                        ArrayList<User> users = User.fromJSON(cats.getJSONArray("users"));
                        for (User user : users) {
                            if (user.getType() == 1) {
                                user.setName(contactsname.get(user.getMobile()));
                            }

                        }

                        User.save(Classified.getInstance(), users);


                        mAdaptor.setChatRoomArrayList(User.group(Classified.getInstance()));
                        mAdaptor.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

*/

    public void readPhoneContacts() //This Context parameter is nothing but your Activity class's Context
    {
        Context cntx = Classified.getInstance();
        Cursor cursor = cntx.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        Integer contactsCount = cursor.getCount(); // get how many contacts you have in your contacts list
        if (contactsCount > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    //the below cursor will give you details for multiple contacts
                    Cursor pCursor = cntx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    // continue till this cursor reaches to all phone numbers which are associated with a contact in the contact list
                    while (pCursor.moveToNext()) {
                        int phoneType = pCursor.getInt(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                        //String isStarred 		= pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.STARRED));
                        String phoneNo = pCursor.getString(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //you will get all phone numbers according to it's type as below switch case.
                        //Logs.e will print the phone number along with the name in DDMS. you can use these details where ever you want.
                        switch (phoneType) {
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                //Log.e(contactName + ": TYPE_MOBILE", " " + (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString());
                                String m = ((((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).replace("*", "").replace("#", "")).trim().toString();


                                m = m.replaceAll("\\D+", "");

                                String numberOnly = phoneNo.replaceAll("[^0-9]", "");

                                if (m.length() > 10) {
                                    m = m.substring(Math.max(m.length() - 10, 0));
                                }

                                if (m.equals(mobile)) {

                                } else {
                                    contactsname.put(m, contactName);
                                    mobilelist.add(m);
                                }

                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                //Log.e(contactName + ": TYPE_HOME", " " + phoneNo);
                                String m1 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m1 = m1.replaceAll("\\D+", "");

                                if (m1.length() > 10) {
                                    m1 = m1.substring(Math.max(m1.length() - 10, 0));
                                }
                                if (m1.equals(mobile)) {

                                } else {
                                    contactsname.put(m1, contactName);
                                    mobilelist.add(m1);
                                }
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                                //Log.e(contactName + ": TYPE_WORK", " " + phoneNo);
                                String m2 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m2 = m2.replaceAll("\\D+", "");

                                if (m2.length() > 10) {
                                    m2 = m2.substring(Math.max(m2.length() - 10, 0));
                                }
                                if (m2.equals(mobile)) {

                                } else {
                                    contactsname.put(m2, contactName);
                                    mobilelist.add(m2);
                                }
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE:
                                //Log.e(contactName + ": TYPE_WORK_MOBILE", " " + phoneNo);
                                String m3 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m3 = m3.replaceAll("\\D+", "");

                                if (m3.length() > 10) {
                                    m3 = m3.substring(Math.max(m3.length() - 10, 0));
                                }
                                if (m3.equals(mobile)) {

                                } else {
                                    contactsname.put(m3, contactName);
                                    mobilelist.add(m3);
                                }
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER:
                                //Log.e(contactName + ": TYPE_OTHER", " " + phoneNo);
                                String m4 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m4 = m4.replaceAll("\\D+", "");

                                if (m4.length() > 10) {
                                    m4 = m4.substring(Math.max(m4.length() - 10, 0));
                                }
                                if (m4.equals(mobile)) {

                                } else {
                                    contactsname.put(m4, contactName);
                                    mobilelist.add(m4);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    pCursor.close();
                }
            }
            cursor.close();
        }
    }

}
