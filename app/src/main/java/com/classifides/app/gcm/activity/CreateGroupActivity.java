

package com.classifides.app.gcm.activity;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.classifides.app.*;
import com.classifides.app.application.Classified;
import com.classifides.app.data.DAO;
import com.classifides.app.data.NotiDbHelper;
import com.classifides.app.gcm.adapter.ChatRoomsAdapter;
import com.classifides.app.gcm.adapter.GroupAdapter;

import com.classifides.app.gcm.gcm.MyGcmPushReceiver;
import com.classifides.app.gcm.gcm.UpdateContactService;
import com.classifides.app.gcm.model.User;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class CreateGroupActivity extends AppCompatActivity {

 /*   private String TAG = CreateGroupActivity.class.getSimpleName();
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ArrayList<ChatRoom> chatRoomArrayList;
    private GroupChatAdapter mAdapter;
    private RecyclerView recyclerView;
    ArrayList<String> mobilelist;
    TextView create_gp;
    public static ArrayList<String> user_id;
    String ulist = "";

    EditText gname;*/

    String text_gname;

    public static String ul;

    private ArrayList<String> mobilelist;
    String allcont = "";
    SharedPreferences sh;
    private int id;
    RecyclerView contrecy;

    TextView creategp;
    HashMap<String, String> contactsname;
    GroupAdapter mAdaptor;
    EditText egname;
    public static ArrayList<String> user_id;
    String ulist = "";

    String encoded_string;
    Bitmap bm;

    String imagename;
    Calendar c = Calendar.getInstance();
    SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
    String cdate = df.format(c.getTime());

    NotiDbHelper mHelper;

    ProgressDialog ppdd;

    CircleImageView gicon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_group);

        imagename = "create_gid_classified" + cdate + "0.jpg";
        mHelper = new NotiDbHelper(Classified.getInstance());
        user_id = new ArrayList<String>();
        contactsname = new HashMap<String, String>();
        mobilelist = new ArrayList<String>();
        contrecy = (RecyclerView) findViewById(R.id.contrecyc);
        gicon = (CircleImageView) findViewById(R.id.gicon);

        ppdd = new ProgressDialog(CreateGroupActivity.this);
        ppdd.setMessage("Please Wait.......");
        ppdd.setCancelable(false);
        ArrayList<User> ur2 = User.load(Classified.getInstance());
        ArrayList<User> ur4 = new ArrayList<User>();
        if (ur2 != null) {
            for (int i = 0; i < ur2.size(); i++) {
                if (ur2.get(i).getType() == 1) {
                    ur4.add(ur2.get(i));
                }
            }
        }

        mAdaptor = new GroupAdapter(Classified.getInstance(), ur4);
        contrecy.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        contrecy.setAdapter(mAdaptor);
        egname = (EditText) findViewById(R.id.egname);
        sh = getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);

        String users = sh.getString("users", "");
        try {
            JSONObject j = new JSONObject(users);
            id = j.getInt("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        gicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImageFromGalary();
            }
        });
        creategp = (TextView) findViewById(R.id.creategp);

        creategp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (user_id.size() == 0) {
                    Toast.makeText(getApplicationContext(), "select contacts", Toast.LENGTH_LONG).show();
                } else {


                    for (int i = 0; i < user_id.size(); i++) {
                        try {
                            if (ulist.contains(user_id.get(i))) {

                            } else {
                                ulist = ulist + user_id.get(i) + ",";
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    int len = ulist.length();


                    ul = ulist + id + "";//Classified.getInstance().getPrefManager().getUser().getId();

                    //Log.e("ULIST", ul);

                    text_gname = egname.getText().toString();

                    if (TextUtils.isEmpty(egname.getText().toString())) {
                        egname.setError("please enter group name");
                    } else {

                        new Encode_image().execute();

                    }


                }

            }
        });

        ArrayList<User> check_store_contact = mHelper.getAllUser();
        if (check_store_contact.size() > 0) {

        } else {
            getUsers();
        }


    }

    public void pickImageFromGalary() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 1);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 1) {
            try {

              /*  Uri selectedImage = data.getData();
                InputStream imageStream = getActivity().getContentResolver().openInputStream(selectedImage);
                Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                *//*String[] projection = {MediaStore.MediaColumns.DATA};

                CursorLoader cursorLoader = new CursorLoader(getActivity(), selectedImage, projection, null, null,
                        null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                path= cursor.getString(column_index);*//*
                video_container.setVisibility(View.VISIBLE);
                banner.setImageBitmap(yourSelectedImage);*/


                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                        null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();

                String selectedImagePath = cursor.getString(column_index);


                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(selectedImagePath, options);


                //  Picasso.with(Classified.getInstance()).load(bm);

                gicon.setImageBitmap(bm);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    private class Encode_image extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ppdd.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // bitmap = BitmapFactory.decodeFile(file_uri.getPath());

            try {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                //   Picasso.with(getApplicationContext()).load(fileUri.getPath()).resize(100,stream);
                byte[] array = stream.toByteArray();
                encoded_string = Base64.encodeToString(array, 0);

            } catch (Exception e) {
                e.printStackTrace();

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            makeRequest();

        }


        private void makeRequest() {
            // RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

//              pd.show();
            StringRequest request = new StringRequest(Request.Method.POST, com.classifides.app.data.Config.setImage, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {

                        JSONObject obj = new JSONObject(response);
                        int result = obj.getInt("success");
                        if (result == 1) {


                            createGroup(ul, text_gname);

                        } else {

                            finish();
                        }

                    } catch (Exception e) {
                        // pd.dismiss();

                        e.printStackTrace();
                        // Toast.makeText(getApplicationContext(), "Failed t0 upload Image", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {

                public void onErrorResponse(VolleyError error) {
                    //   pd.dismiss();
                    error.printStackTrace();
                    error.toString();
                    Toast.makeText(getApplicationContext(), "Please Select Group Icon", Toast.LENGTH_LONG).show();
                    ppdd.dismiss();
                }

            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {


                    HashMap<String, String> map = new HashMap<>();
                    map.put("image_name", imagename);
                    map.put("encoded_string", encoded_string);
                    return map;
                }


            };

            com.classifides.app.application.Classified.getInstance().addToReqQueue(request);
        }

    }

    private void createGroup(final String m, final String gname) {
        final Map<String, String> data = new HashMap<String, String>();


        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                data.put("gname", gname);
                data.put("a_id", id + "");
                data.put("u_id", m);
                data.put("imgurl", imagename);
                ppdd.show();
            }

            @Override
            protected Object doInBackground(Object[] params) {

                res = DAO.getJsonFromUrl(Classified.CREATE_GROUP, data);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                ppdd.dismiss();
                egname.setText("");
                ulist = "";
                //  Toast.makeText(getApplicationContext(),"Group Created", Toast.LENGTH_SHORT).show();
                //Log.e("Groupres", res.toString());
                try {
                    if (res.getInt("success") > 0) {
                        Toast.makeText(getApplicationContext(), "Group Created", Toast.LENGTH_SHORT).show();



                      /*  sendBroadcast(new Intent()
                                .setAction(MyGcmPushReceiver.name5)
                                .addCategory(Intent.CATEGORY_DEFAULT)
                                .putExtra("data", "yes"));*/

                    } else {
                        // Toast.makeText(getApplicationContext(), "Failed to create", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                sendBroadcast(new Intent()
                        .setAction(MyGcmPushReceiver.name5)
                        .addCategory(Intent.CATEGORY_DEFAULT)
                        .putExtra("data", "yes"));
                finish();
            }
        }.execute(null, null, null);


    }

    private void getUsers() {


        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                readPhoneContacts();

                for (String mob : mobilelist) {
                    allcont = allcont + mob + ",";
                }
                allcont = allcont.substring(0, allcont.length() - 1);
                return null;
            }


            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                //getUsersFromServer(allcont);
                getCategories();

            }


        }.execute(null, null, null);
    }


    private void getCategories() {
        new AsyncTask() {
            JSONObject cats = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected Object doInBackground(Object[] params) {
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("contacts", allcont);
                param.put("id", id + "");
                cats = DAO.getJsonFromUrl(Classified.APP_API_URL, param);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    if (cats.getInt("success") > 0) {

                        ArrayList<User> users = User.fromJSON(cats.getJSONArray("users"));
                        for (User user : users) {
                            if (user.getType() == 1)
                                user.setName(contactsname.get(user.getMobile()));
                        }

                        User.save(Classified.getInstance(), users);

                        ArrayList<User> ur2 = User.load(Classified.getInstance());
                        ArrayList<User> ur4 = new ArrayList<User>();

                        for (int i = 0; i < ur2.size(); i++) {
                            if (ur2.get(i).getType() == 1) {
                                ur4.add(ur2.get(i));
                            }
                        }

                        mAdaptor.setGroupAdapter(ur4);
                        mAdaptor.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

    public void readPhoneContacts() //This Context parameter is nothing but your Activity class's Context
    {
        Context cntx = Classified.getInstance();
        Cursor cursor = cntx.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        Integer contactsCount = cursor.getCount(); // get how many contacts you have in your contacts list
        if (contactsCount > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    //the below cursor will give you details for multiple contacts
                    Cursor pCursor = cntx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    // continue till this cursor reaches to all phone numbers which are associated with a contact in the contact list
                    while (pCursor.moveToNext()) {
                        int phoneType = pCursor.getInt(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                        //String isStarred 		= pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.STARRED));
                        String phoneNo = pCursor.getString(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //you will get all phone numbers according to it's type as below switch case.
                        //Logs.e will print the phone number along with the name in DDMS. you can use these details where ever you want.
                        switch (phoneType) {
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                //Log.e(contactName + ": TYPE_MOBILE", " " + (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString());
                                String m = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();
                                m = m.replaceAll("\\D+", "");
                                //Log.e("MMMMM", m);

                                if (m.length() > 10) {
                                    m = m.substring(Math.max(m.length() - 10, 0));
                                }
                                contactsname.put(m, contactName);
                                mobilelist.add(m);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                //Log.e(contactName + ": TYPE_HOME", " " + phoneNo);
                                String m1 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();
                                m1 = m1.replaceAll("\\D+", "");
                                //Log.e("MMMMM", m1);

                                if (m1.length() > 10) {
                                    m1 = m1.substring(Math.max(m1.length() - 10, 0));
                                }
                                contactsname.put(m1, contactName);
                                mobilelist.add(m1);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                                //Log.e(contactName + ": TYPE_WORK", " " + phoneNo);
                                String m2 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();
                                m2 = m2.replaceAll("\\D+", "");
                                //Log.e("MMMMM", m2);

                                if (m2.length() > 10) {
                                    m2 = m2.substring(Math.max(m2.length() - 10, 0));
                                }
                                contactsname.put(m2, contactName);
                                mobilelist.add(m2);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE:
                                //Log.e(contactName + ": TYPE_WORK_MOBILE", " " + phoneNo);
                                String m3 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();
                                m3 = m3.replaceAll("\\D+", "");
                                //Log.e("MMMMM", m3);
                                if (m3.length() > 10) {
                                    m3 = m3.substring(Math.max(m3.length() - 10, 0));
                                }
                                mobilelist.add(m3);
                                contactsname.put(m3, contactName);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER:
                                //Log.e(contactName + ": TYPE_OTHER", " " + phoneNo);
                                String m4 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();
                                m4 = m4.replaceAll("\\D+", "");
                                //Log.e("MMMMM", m4);

                                if (m4.length() > 10) {
                                    m4 = m4.substring(Math.max(m4.length() - 10, 0));
                                }
                                mobilelist.add(m4);
                                contactsname.put(m4, contactName);
                                break;
                            default:
                                break;
                        }
                    }
                    pCursor.close();
                }
            }
            cursor.close();
        }
    }

}
       /* user_id = new ArrayList<String>();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        create_gp = (TextView) findViewById(R.id.create_gp);
        gname = (EditText) findViewById(R.id.gname);
        create_gp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (user_id.size() == 0) {
                    Toast.makeText(getApplicationContext(), "select contacts", Toast.LENGTH_LONG).show();
                } else {


                    for (int i = 0; i < user_id.size(); i++) {
                        ulist = ulist + user_id.get(i) + ",";
                    }


                    int len = ulist.length();

                    String ul = ulist + Classified.getInstance().getPrefManager().getUser().getId();

                    Log.e("ULIST", ul);

                    if (TextUtils.isEmpty(gname.getText().toString())) {
                        gname.setError("please enter group name");
                    } else {
                        createGroup(ul, gname.getText().toString());
                    }


                }
            }
        });
           *//* TextView cgp= (TextView)findViewById(R.id.cgp);
            cgp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });*//*
        mobilelist = new ArrayList<String>();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        *//**
 * Broadcast receiver calls in two scenarios
 * 1. gcm registration is completed
 * 2. when new push notification is received
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 * <p>
 * Always check for google play services availability before
 * proceeding further with GCM
 * <p>
 * Handles new push notification
 * <p>
 * Updates the chat list unread count and the last message
 * <p>
 * fetching the chat rooms by making http call
 *//*
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
*//*
                    // checking for type intent filter
                    if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                        // gcm successfully registered
                        // now subscribe to `global` topic to receive app wide notifications
                        subscribeToGlobalTopic();

                    } else if (intent.getAction().equals(Config.SENT_TOKEN_TO_SERVER)) {
                        // gcm registration id is stored in our server's MySQL
                        Log.e(TAG, "GCM registration id is sent to our server");

                    } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                        // new push notification is received
                        handlePushNotification(intent);
                    }*//*
            }
        };

        chatRoomArrayList = new ArrayList<>();
        mAdapter = new GroupChatAdapter(this, chatRoomArrayList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(
                getApplicationContext()
        ));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

    *//*    recyclerView.addOnItemTouchListener(new ChatRoomsAdapter.RecyclerTouchListener(getApplicationContext(), recyclerView, new ChatRoomsAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                // when chat is clicked, launch full chat thread activity
                ChatRoom chatRoom = chatRoomArrayList.get(position);
                Intent intent = new Intent(CreateGroupActivity.this, ChatRoomActivity.class);
                intent.putExtra("chat_room_id", chatRoom.getId());
                intent.putExtra("name", chatRoom.getName());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
*//*
        ReadPhoneContacts(CreateGroupActivity.this);

        *//**
 * Always check for google play services availability before
 * proceeding further with GCM
 * *//*
        if (checkPlayServices()) {
            //registerGCM();

        }
        fetchChatRooms();
    }

    *//**
 * Handles new push notification
 *//*
    private void handlePushNotification(Intent intent) {
        int type = intent.getIntExtra("type", -1);

        // if the push is of chat room message
        // simply update the UI unread messages count
        if (type == Config.PUSH_TYPE_CHATROOM) {
            Message message = (Message) intent.getSerializableExtra("message");
            String chatRoomId = intent.getStringExtra("chat_room_id");

            if (message != null && chatRoomId != null) {
                updateRow(chatRoomId, message);
            }
        } else if (type == Config.PUSH_TYPE_USER) {
            // push belongs to user alone
            // just showing the message in a toast
            Message message = (Message) intent.getSerializableExtra("message");
            Toast.makeText(getApplicationContext(), "New push: " + message.getMessage(), Toast.LENGTH_LONG).show();
        }


    }

    *//**
 * Updates the chat list unread count and the last message
 *//*
    private void updateRow(String chatRoomId, Message message) {
        for (ChatRoom cr : chatRoomArrayList) {
            if (cr.getId().equals(chatRoomId)) {
                int index = chatRoomArrayList.indexOf(cr);
                cr.setLastMessage(message.getMessage());
                cr.setUnreadCount(cr.getUnreadCount() + 1);
                chatRoomArrayList.remove(index);
                chatRoomArrayList.add(index, cr);
                break;
            }
        }
        mAdapter.notifyDataSetChanged();
    }


    *//**
 * fetching the chat rooms by making http call
 *//*
    private void fetchChatRooms() {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.CHAT_ROOMS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        JSONArray chatRoomsArray = obj.getJSONArray("chat_rooms");
                        for (int i = 0; i < chatRoomsArray.length(); i++) {
                            JSONObject chatRoomsObj = (JSONObject) chatRoomsArray.get(i);
                            ChatRoom cr = new ChatRoom();
                            cr.setId(chatRoomsObj.getString("chat_room_id"));
                            cr.setName(chatRoomsObj.getString("name"));
                            cr.setLastMessage("");
                            cr.setUnreadCount(0);
                            cr.setTimestamp(chatRoomsObj.getString("created_at"));
                            if (mobilelist.contains(chatRoomsObj.getString("mobile"))) {
                                chatRoomArrayList.add(cr);
                            } else {

                            }
                        }

                    } else {
                        // error in fetching chat rooms
                        Toast.makeText(getApplicationContext(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

                mAdapter.notifyDataSetChanged();

                // subscribing to all chat room topics
                subscribeToAllTopics();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
            //    String user_id = Classified.getInstance().getPrefManager().getUser().getId();
           *//*     params.put("user_id", user_id);
                Log.e(TAG, "params: " + params.toString());*//*
                return params;
            }
        };
        ;

        //Adding request to request queue
        Classified.getInstance().addToRequestQueue(strReq);
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


    private void createGroup(final String ulist, final String gname) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.CREATE_GROUP, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        //  JSONArray chatRoomsArray = obj.getJSONArray("chat_rooms");
                       *//* for (int i = 0; i < chatRoomsArray.length(); i++) {
                            JSONObject chatRoomsObj = (JSONObject) chatRoomsArray.get(i);
                            Group cr = new Group();
                            cr.setGid(chatRoomsObj.getString("chat_room_id"));
                            cr.setGname(chatRoomsObj.getString("name"));

                            cr.setCreated_at(chatRoomsObj.getString("created_at"));
                            if (mobilelist.contains(chatRoomsObj.getString("mobile"))) {
                                chatRoomArrayList.add(cr);
                            } else {

                            }
                        }*//*
                        Toast.makeText(getApplicationContext(), "group created successfully", Toast.LENGTH_LONG).show();
                        // startActivity(new Intent(CreateGroupActivity.this, MainActivity.class));
                        finish();
                    } else {
                        // error in fetching chat rooms
                        Toast.makeText(getApplicationContext(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

                mAdapter.notifyDataSetChanged();

                // subscribing to all chat room topics
                subscribeToAllTopics();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                NetworkResponse networkResponse = error.networkResponse;

                error.printStackTrace();
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }

        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                  *//*  String user_id = Classified.getInstance().getPrefManager().getUser().getId();
                    params.put("a_id", Classified.getInstance().getPrefManager().getUser().getId());
                    params.put("u_id", ulist);
                    params.put("gname", gname);
                    Log.e(TAG, "params: " + params.toString());*//*
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                volleyError.printStackTrace();
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    volleyError.printStackTrace();
                }

                return volleyError;
            }
        };
        ;

        //Adding request to request queue
        Classified.getInstance().addToRequestQueue(strReq);
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


    // subscribing to global topic
    private void subscribeToGlobalTopic() {
        Intent intent = new Intent(this, GcmIntentService.class);
        intent.putExtra(GcmIntentService.KEY, GcmIntentService.SUBSCRIBE);
        intent.putExtra(GcmIntentService.TOPIC, Config.TOPIC_GLOBAL);
        startService(intent);
    }

    // Subscribing to all chat room topics
    // each topic name starts with `topic_` followed by the ID of the chat room
    // Ex: topic_1, topic_2
    private void subscribeToAllTopics() {
        for (ChatRoom cr : chatRoomArrayList) {
            Intent intent = new Intent(this, GcmIntentService.class);
            intent.putExtra(GcmIntentService.KEY, GcmIntentService.SUBSCRIBE);
            intent.putExtra(GcmIntentService.TOPIC, "topic_" + cr.getId());
            startService(intent);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();


    }

    private void cg(final String ulist, final String gname) {


        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.CREATE_GROUP, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error
                    if (obj.getBoolean("error") == false) {
                       *//* JSONObject commentObj = obj.getJSONObject("message");

                        String commentId = commentObj.getString("message_id");
                        String commentText = commentObj.getString("message");
                        String createdAt = commentObj.getString("created_at");

                        JSONObject userObj = obj.getJSONObject("user");
                        String userId = userObj.getString("user_id");
                        String userName = userObj.getString("name");
                        User user = new User( Classified.getInstance().getPrefManager().getUser().getId(), Classified.getInstance().getPrefManager().getUser().getName(), null);

                        Message message = new Message();
                        message.setId(Classified.getInstance().getPrefManager().getUser().getId());
                        message.setMessage(commentText);
                        message.setCreatedAt(createdAt);
                        message.setUser(user);

                        messageArrayList.add(message);

                        mAdapter.notifyDataSetChanged();
                        if (mAdapter.getItemCount() > 1) {
                            // scrolling to bottom of the recycler view
                            recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() - 1);
                        }
*//*
                    } else {
                        Toast.makeText(getApplicationContext(), "" + obj.getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
             *//*   params.put("a_id", Classified.getInstance().getPrefManager().getUser().getId());
                params.put("u_id", ulist);
                params.put("gname", gname);*//*
                Log.e(TAG, "Params: " + params.toString());

                return params;
            }

            ;
        };


        // disabling retry policy so that it won't make
        // multiple http calls
        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strReq.setRetryPolicy(policy);

        //Adding request to request queue
        Classified.getInstance().addToRequestQueue(strReq);
    }


    // starting the service to register with GCM

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported. Google Play Services not installed!");
                Toast.makeText(getApplicationContext(), "This device is not supported. Google Play Services not installed!", Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }


    public void ReadPhoneContacts(Context cntx) //This Context parameter is nothing but your Activity class's Context
    {
        Cursor cursor = cntx.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        Integer contactsCount = cursor.getCount(); // get how many contacts you have in your contacts list
        if (contactsCount > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    //the below cursor will give you details for multiple contacts
                    Cursor pCursor = cntx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    // continue till this cursor reaches to all phone numbers which are associated with a contact in the contact list
                    while (pCursor.moveToNext()) {
                        int phoneType = pCursor.getInt(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                        //String isStarred 		= pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.STARRED));
                        String phoneNo = pCursor.getString(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //you will get all phone numbers according to it's type as below switch case.
                        //Logs.e will print the phone number along with the name in DDMS. you can use these details where ever you want.
                        switch (phoneType) {
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                Log.e(contactName + ": TYPE_MOBILE", " " + (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString());
                                String m = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();
                                mobilelist.add(m);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                Log.e(contactName + ": TYPE_HOME", " " + phoneNo);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                                Log.e(contactName + ": TYPE_WORK", " " + phoneNo);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE:
                                Log.e(contactName + ": TYPE_WORK_MOBILE", " " + phoneNo);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER:
                                Log.e(contactName + ": TYPE_OTHER", " " + phoneNo);
                                break;
                            default:
                                break;
                        }
                    }
                    pCursor.close();
                }
            }
            cursor.close();
        }
    }

}*/