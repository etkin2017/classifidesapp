package com.classifides.app.gcm.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.classifides.app.R;
import com.classifides.app.application.Classified;

import com.classifides.app.gcm.activity.CreateGroupActivity;

import com.classifides.app.gcm.model.User;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ashvini on 9/9/2016.
 */
public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.ViewHolder> {

    private Context mContext;
    ArrayList<Integer> chk_p;
    boolean[] itemChecked;

    public void setGroupAdapter(ArrayList<User> chatRoomArrayList) {

        this.chatRoomArrayList = chatRoomArrayList;

    }

    private ArrayList<User> chatRoomArrayList;

    public ArrayList<User> getGroupAdapter() {
        return chatRoomArrayList;
    }

    private static String today;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, uid_id;
        CheckBox checkBox_id;
        CircleImageView imageView;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.unm);
            checkBox_id = (CheckBox) view.findViewById(R.id.uid_ck);
            uid_id = (TextView) view.findViewById(R.id.uid_id);
            imageView = (CircleImageView) view.findViewById(R.id.user_icon);

        }
    }


    public GroupAdapter(Context mContext, ArrayList<User> chatRoomArrayList) {
        this.mContext = mContext;
        this.chatRoomArrayList = chatRoomArrayList;

        Calendar calendar = Calendar.getInstance();
        today = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        chk_p = new ArrayList<Integer>();
        itemChecked = new boolean[chatRoomArrayList.size()];
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.group_row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        User user = chatRoomArrayList.get(position);
        holder.name.setText(user.getName());
        holder.uid_id.setText(user.getId() + "");
        Picasso.with(Classified.getInstance()).load(user.getImage()).placeholder(R.mipmap.logo).into(holder.imageView);


        holder.checkBox_id.setTag(user);


        try {
            if (itemChecked[position])
                holder.checkBox_id.setChecked(true);
            else
                holder.checkBox_id.setChecked(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.checkBox_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (holder.checkBox_id.isChecked() == true) {
                    //  itemChecked[position] = true;
                    itemChecked[position] = true;
                    CreateGroupActivity.user_id.add(holder.uid_id.getText().toString());

                }

                if (holder.checkBox_id.isChecked() == false) {
                    //  itemChecked[position] = false;
                    itemChecked[position] = false;
                    if (CreateGroupActivity.user_id.contains(holder.uid_id.getText().toString())) {
                        CreateGroupActivity.user_id.remove(holder.uid_id.getText().toString());
                    }

                    if (CreateGroupActivity.user_id.contains(holder.uid_id.getText().toString())) {
                        CreateGroupActivity.user_id.remove(holder.uid_id.getText().toString());
                    }

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return chatRoomArrayList.size();
    }

    public static String getTimeStamp(String dateStr) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = "";

        today = today.length() < 2 ? "0" + today : today;

        try {
            Date date = format.parse(dateStr);
            SimpleDateFormat todayFormat = new SimpleDateFormat("dd");
            String dateToday = todayFormat.format(date);
            format = dateToday.equals(today) ? new SimpleDateFormat("hh:mm a") : new SimpleDateFormat("dd LLL, hh:mm a");
            String date1 = format.format(date);
            timestamp = date1.toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timestamp;
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }


}
