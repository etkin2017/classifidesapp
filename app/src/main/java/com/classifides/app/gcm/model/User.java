package com.classifides.app.gcm.model;

import android.content.Context;
import android.util.Log;

import com.classifides.app.application.Classified;
import com.classifides.app.data.NotiDbHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by Lincoln on 07/01/16.
 */
public class User {

    private static final String TAG = User.class.getSimpleName();


    private static User instance;

    private String name;
    private String image;
    private String mobile;
    private int type;
    private int id;
    private int aid;
    private String lastMsg;
    private int unReadCnt;
    private String status;

    private boolean ticked;


    public User(String lastMsg, int unReadCnt) {
        this.lastMsg = lastMsg;
        this.unReadCnt = unReadCnt;

    }

    public boolean isTicked() {
        return ticked;
    }

    public void setTicked(boolean ticked) {
        this.ticked = ticked;
    }

    private int block;

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public String getLastMsg() {
        return lastMsg;
    }

    public void setLastMsg(String lastMsg) {
        this.lastMsg = lastMsg;
    }

    public int getUnReadCnt() {
        return unReadCnt;
    }

    public void setUnReadCnt(int unReadCnt) {
        this.unReadCnt = unReadCnt;
    }

    public User(String name, String image, int type, int id, String mobile, String status, int aid, int block, boolean ticked) {
        this.name = name;
        this.image = image;
        this.type = type;
        this.id = id;
        this.mobile = mobile;
        this.status = status;
        this.aid = aid;
        this.block = block;
        this.ticked = ticked;
    }

    public int getBlock() {
        return block;
    }

    public void setBlock(int block) {
        this.block = block;
    }


    /*    public User clone() {
        try {
            return fromJSON(toJSON(this));
        } catch (JSONException | ParseException | UnsupportedEncodingException e) {
            Log.e(TAG, "Failed to clone user. " + e.getMessage());
            throw new RuntimeException("Failed to clone User", e);
        }
    }*/

/*    public static User getInstance(Context context) {
        if (User.instance == null) {
            synchronized (User.class) {
                if (User.instance == null) {
                    User.instance = load(context);
                }
            }
        }

        return User.instance;
    }*/


/*    public static User setInstance(Context context, User instance) {
        synchronized (User.class) {
            User.instance = instance;
          *//*  if (User.instance != null)
                save(context, User.instance);
            else
                clear(context);*//*
        }
        return getInstance(context);
    }*/

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static void save(Context context, ArrayList<User> user) {
        try {

            NotiDbHelper helper = new NotiDbHelper(context);
            helper.saveUsers(user);
            User.group(Classified.getInstance());

        } catch (Exception e) {
            //Log.e(TAG, "Failed to save user. " + e.getMessage());
            throw new RuntimeException("Failed to save User", e);
        }
    }

    public static ArrayList<User> group(Context context) {


        try {
            NotiDbHelper helper = new NotiDbHelper(context);
            return helper.getAllGroup();
        } catch (Exception e) {
            //Log.e(TAG, "Failed to load user. " + e.getMessage());
            return new ArrayList<User>();


        }

    }

    public static void saveGroup(Context context, ArrayList<User> user) {
        try {

            NotiDbHelper helper = new NotiDbHelper(context);
            helper.saveUsers(user);

        } catch (Exception e) {
            //Log.e(TAG, "Failed to save user. " + e.getMessage());
            throw new RuntimeException("Failed to save User", e);
        }
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public static ArrayList<User> load(Context context) {


        try {
            NotiDbHelper helper = new NotiDbHelper(context);
            return helper.getAllUser();
        } catch (Exception e) {
            //Log.e(TAG, "Failed to load user. " + e.getMessage());
            return new ArrayList<User>();


        }

    }


    private static void clear(Context context) {
  /*      context.getSharedPreferences(SHARED_PREFERENCE_FILENAME, Context.MODE_PRIVATE)
                .edit()
                .remove("VERSION")
                .remove("PROFILE")
                .commit();*/
    }


    public static ArrayList<User> fromJSON(JSONArray source)
            throws JSONException, ParseException, UnsupportedEncodingException {
        ArrayList<User> users = new ArrayList<User>();
        for (int i = 0; i < source.length(); i++) {
            JSONObject obj = source.getJSONObject(i);
            users.add(new User(obj.optString("name", ""), obj.optString("image", ""), obj.optInt("type", 0), obj.optInt("id", 0), obj.optString("mobile", ""), obj.optString("status", ""), obj.optInt("aid", 0), 0, false));
        }

        return users;

    }

    public static JSONObject toJSON(User user)
            throws JSONException, UnsupportedEncodingException {
        return new JSONObject()
                .putOpt("name", user.getName())
                .putOpt("image", user.getImage())
                .putOpt("type", user.getType())
                .putOpt("id", user.getId())
                .putOpt("status", user.getStatus())
                .putOpt("a_id", user.getAid());


    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
