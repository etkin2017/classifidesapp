package com.classifides.app.gcm.app;

/**
 * Created by Lincoln on 06/01/16.
 */
public class EndPoints {

    // localhost url   192.168.43.233
    public static final String BASE_URL = "http://192.168.1.100:3333/GCM/gcm_chat/v1";
    // public static final String BASE_URL = "http://172.20.10.2/gcm_chat/v1";
    public static final String LOGIN = BASE_URL + "/user/login";
    public static final String CREATE_GROUP = "http://192.168.1.1003:3333/GCM/gcm_chat/v1/create/group";
    public static final String USER = BASE_URL + "/user/_ID_";
    public static final String CHAT_ROOMS = BASE_URL + "/chat_rooms";
    public static final String CHAT_THREAD = BASE_URL + "/chat_rooms/_ID_";
    public static final String GROUP_CHAT_THREAD =BASE_URL +"/chat_rooms/group/_ID_";
    public static final String GROUP_DETAILS = BASE_URL + "/chat_rooms/gdetails";
    public static final String CHAT_ROOM_MESSAGE = BASE_URL + "/chat_rooms/_ID_/message";
    public static final String GROUP_ROOM_MESSAGE =BASE_URL +"/chat_rooms/group/_ID_/message";

    public static final String  GCMP="http://192.168.1.100:3333/GCM/gcm_chat/gcmp.php";
}
