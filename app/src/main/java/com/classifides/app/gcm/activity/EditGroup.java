package com.classifides.app.gcm.activity;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.classifides.app.MyAdPojo;
import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.gcm.gcm.MyGcmPushReceiver;
import com.classifides.app.gcm.model.GR;
import com.classifides.app.view.MTextView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditGroup extends AppCompatActivity {


    EditText edit_group_nm;
    CircleImageView update_group_image;

    Button update_group;
    String imagename;
    String img_url;
    ProgressDialog pdd, pd;
    public static String encoded_string;
    Bitmap bm;
    String gid;
    MAdaptor adaptor;

    String imgurl;

    RecyclerView rec_paymet;
    SharedPreferences sh;
    public static int id;
    ProgressDialog pdd1;

    public void getGroupDetails(final String gid) {

        final ProgressDialog p = new ProgressDialog(EditGroup.this);

        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                p.setTitle("Please wait");
            }

            @Override
            protected Object doInBackground(Object[] params) {

                Map<String, String> map = new HashMap<String, String>();
                map.put("id", gid.trim());
                rest = DAO.getJsonFromUrl(Classified.Group_Details, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                p.dismiss();
                try {


                    if (rest.getInt("success") > 0) {

                        adaptor.setData(rest.getJSONArray("groupid"));
                        //Log.e("GROUBYID", rest.getJSONArray("groupid").toString());
                       /* for(int i=0;i<rest.getJSONArray("groupid").length();i++){
                            adaptor.setData(rest.getJSONArray("groupid").getJSONArray(i));
                            Log.e("GROUBYID", rest.getJSONArray("groupid").getJSONArray(i).toString());
                            rec_paymet.setAdapter(adaptor);
                        }*/
                        rec_paymet.setAdapter(adaptor);
                    } else {


                        adaptor.setData(new JSONArray());
                        rec_paymet.setAdapter(adaptor);
                        Toast.makeText(EditGroup.this, rest.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

    Dialog d;


    String photo_d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_group);
        gid = getIntent().getStringExtra("gid");
        String s = getIntent().getStringExtra("status");
        imgurl = getIntent().getStringExtra("imgurl");

        try {
            String a = imgurl;
//String[] array = x.split("/+");
            String[] pat = a.split("/+");
            for (int i = 0; i < pat.length; i++) {
                Log.e(pat[i], i + "");
            }

            photo_d = pat[4];
        } catch (Exception e) {
            e.printStackTrace();
        }
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        String cdate = df.format(c.getTime());
        imagename = "gid_classified" + cdate + "0.jpg";
        adaptor = new MAdaptor();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        CircleImageView add_member = (CircleImageView) findViewById(R.id.add_member);

        d = new Dialog(EditGroup.this);
        sh = getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);

        String users = sh.getString("users", "");
        try {
            JSONObject j = new JSONObject(users);
            id = j.getInt("id");


        } catch (JSONException e) {
            e.printStackTrace();
        }


        add_member.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {


                                              d.setContentView(R.layout.dialog_group);
                                              d.setTitle("Group Member");
                                              rec_paymet = (RecyclerView) d.findViewById(R.id.group_details);
                                              LinearLayoutManager layoutManager = new LinearLayoutManager(EditGroup.this);
                                              rec_paymet.setLayoutManager(layoutManager);
                                              rec_paymet.setItemAnimator(new DefaultItemAnimator());
                                              // rec_paymet.setAdapter(adaptor);

                                              getGroupDetails(gid);
                                              d.show();

                                          }
                                      }


        );


        pdd = new ProgressDialog(EditGroup.this);
        pd = new ProgressDialog(EditGroup.this);
        pdd.setTitle("Please Wait");
        pd.setTitle("Please Wait");
        pdd.setMessage("Updating ........");
        update_group_image = (CircleImageView) findViewById(R.id.update_group_image);
        edit_group_nm = (EditText) findViewById(R.id.edit_group_nm);

        edit_group_nm.setText(s + "");

        update_group = (Button) findViewById(R.id.update_group);


        Picasso.with(Classified.getInstance()).load(imgurl).placeholder(R.mipmap.logo).into(update_group_image);
        update_group_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImageFromGalary();
            }
        });

        update_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edit_group_nm.getText().toString())) {
                    edit_group_nm.setError("enter group name");
                } else {
                    editGroup(edit_group_nm.getText().toString(), imagename, gid);
                }
            }
        });


    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 1) {
            try {

              /*  Uri selectedImage = data.getData();
                InputStream imageStream = getActivity().getContentResolver().openInputStream(selectedImage);
                Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                *//*String[] projection = {MediaStore.MediaColumns.DATA};

                CursorLoader cursorLoader = new CursorLoader(getActivity(), selectedImage, projection, null, null,
                        null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                path= cursor.getString(column_index);*//*
                video_container.setVisibility(View.VISIBLE);
                banner.setImageBitmap(yourSelectedImage);*/


                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                        null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();

                String selectedImagePath = cursor.getString(column_index);


                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(selectedImagePath, options);


                //  Picasso.with(Classified.getInstance()).load(bm);

                update_group_image.setImageBitmap(bm);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    public void pickImageFromGalary() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 1);
    }


    private void editGroup(final String gname, final String img_url, final String idd) {

        new AsyncTask() {
            JSONObject cats = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                try {
                    pdd.show();
                    if (bm != null) {
                        new Encode_image().execute();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            protected Object doInBackground(Object[] params) {
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("gname", gname);
                param.put("id", idd);
                if (bm == null) {
                    //   img_url = imgurl;
                    //http://spiritsecret.com/catchmeon/postimage/create_gid_classified201610031924200.jpg
                    //http://spiritsecret.com/catchmeon/postimage/create_gid_classified201610081341350.jpg


                    param.put("img_url", photo_d);
                } else {
                    param.put("img_url", img_url);
                }

                cats = DAO.getJsonFromUrl(Classified.updateGroup, param);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    pdd.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (cats.getInt("success") > 0) {

                        sendBroadcast(new Intent()
                                .setAction(MyGcmPushReceiver.name5)
                                .addCategory(Intent.CATEGORY_DEFAULT)
                                .putExtra("data", "yes"));
                        finish();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }


    private class Encode_image extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            // bitmap = BitmapFactory.decodeFile(file_uri.getPath());
            try {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                //   Picasso.with(getApplicationContext()).load(fileUri.getPath()).resize(100,stream);
                byte[] array = stream.toByteArray();
                encoded_string = Base64.encodeToString(array, 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (bm == null) {
                finish();
            } else {
                makeRequest();
            }
        }


        private void makeRequest() {
            // RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

//              pd.show();
            StringRequest request = new StringRequest(Request.Method.POST, Config.setImage, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {

                        JSONObject obj = new JSONObject(response);
                        int result = obj.getInt("success");
                        if (result == 1) {

                            Toast.makeText(getApplicationContext(), "Group Icon Changed", Toast.LENGTH_LONG).show();
                            //  pd.dismiss();


                        } else {


                            //   Toast.makeText(getApplicationContext(), "Image Not Uploaded", Toast.LENGTH_LONG).show();

                            // insertion();
                            //   pd.dismiss();


                        }

                    } catch (Exception e) {
                        // pd.dismiss();

                        e.printStackTrace();
                        //Toast.makeText(getApplicationContext(), "Failed t0 upload Image", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {

                public void onErrorResponse(VolleyError error) {
                    //   pd.dismiss();

                    error.printStackTrace();
                    error.toString();
                    // Toast.makeText(getApplicationContext(), "Check Your Internet Connection!", Toast.LENGTH_LONG).show();
                }

            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {


                    HashMap<String, String> map = new HashMap<>();
                    map.put("image_name", imagename);
                    map.put("encoded_string", encoded_string);
                    return map;
                }


            };

            com.classifides.app.application.Classified.getInstance().addToReqQueue(request);
        }

    }


    public void sendimage() {
        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                // pd.setTitle("Please wait");
            }

            @Override
            protected Object doInBackground(Object[] params) {


                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                //   Picasso.with(getApplicationContext()).load(fileUri.getPath()).resize(100,stream);
                byte[] array = stream.toByteArray();
                encoded_string = Base64.encodeToString(array, 0);


                Map<String, String> map = new HashMap<String, String>();
                map.put("image_name", imagename);
                map.put("encoded_string", encoded_string);
                rest = DAO.getJsonFromUrl(Classified.Group_Details, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                //    p.dismiss();
                try {


                    if (rest.getInt("success") > 0) {

                        Toast.makeText(getApplicationContext(), "Successful", Toast.LENGTH_LONG).show();
                    } else {

                        Toast.makeText(getApplicationContext(), "failed", Toast.LENGTH_LONG).show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);

    }


    private class MAdaptor extends RecyclerView.Adapter<MAdaptor.ViewHolder> {

        private JSONArray data;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_details, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            try {


                if (data.getJSONObject(position).getInt("id") != id) {
                    holder.uname.setText(data.getJSONObject(position).getString("name"));
                } else {
                    holder.uname.setText("you");
                }
                holder.umobi.setText(data.getJSONObject(position).getString("mobile"));


                //holder.viewers.setVisibility(View.GONE);

                Picasso.with(Classified.getInstance()).load(data.getJSONObject(position).getString("image")).placeholder(R.drawable.user).into(holder.uicon);

                holder.itemView.setTag(data.getJSONObject(position));


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        public int getItemCount() {
            return data.length();
        }

        public void setData(JSONArray data) {
            this.data = data;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView uname, umobi;
            CircleImageView uicon;

            public ViewHolder(View itemView) {
                super(itemView);
                uname = (TextView) itemView.findViewById(R.id.uname);
                //  hidden_container=(LinearLayout)itemView.findViewById(R.id.hidden_container);
                uicon = (CircleImageView) itemView.findViewById(R.id.uicon);
                umobi = (TextView) itemView.findViewById(R.id.umobi);

            }
        }
    }
}
