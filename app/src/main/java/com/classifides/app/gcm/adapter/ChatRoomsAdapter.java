package com.classifides.app.gcm.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.classifides.app.data.NotiDbHelper;
import com.classifides.app.gcm.model.ChatRoom;
import com.classifides.app.gcm.model.User;
import com.google.android.gms.vision.text.Text;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;


public class ChatRoomsAdapter extends RecyclerView.Adapter<ChatRoomsAdapter.ViewHolder> {

    private Context mContext;
    NotiDbHelper mhelper;

    public void setChatRoomArrayList(ArrayList<User> chatRoomArrayList) {
        this.chatRoomArrayList = chatRoomArrayList;
    }


    int a = 0;

    private ArrayList<User> chatRoomArrayList;

    public ArrayList<User> getChatRoomArrayList() {
        return chatRoomArrayList;
    }

    private static String today;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, message, timestamp, count, single, chatroomid, id, url, type, block, aid, set_checked, long_press_icon;
        CircleImageView user_icon;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            message = (TextView) view.findViewById(R.id.message);
            timestamp = (TextView) view.findViewById(R.id.timestamp);
            count = (TextView) view.findViewById(R.id.count);
            single = (TextView) view.findViewById(R.id.single);
            user_icon = (CircleImageView) view.findViewById(R.id.user_icon);
            chatroomid = (TextView) view.findViewById(R.id.chatroomid);
            id = (TextView) view.findViewById(R.id.id);
            url = (TextView) view.findViewById(R.id.url);
            type = (TextView) view.findViewById(R.id.type);
            block = (TextView) view.findViewById(R.id.block);
            aid = (TextView) view.findViewById(R.id.aid);
            set_checked = (TextView) view.findViewById(R.id.set_checked);
            long_press_icon = (TextView) view.findViewById(R.id.long_press_icon);
            // chatroomid, id, url, type, block
        }
    }


    public ChatRoomsAdapter(Context mContext, ArrayList<User> chatRoomArrayList) {
        this.mContext = mContext;
        this.chatRoomArrayList = chatRoomArrayList;

        Calendar calendar = Calendar.getInstance();
        today = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        mhelper = new NotiDbHelper(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_rooms_list_row, parent, false);

        return new ViewHolder(itemView);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final User user = chatRoomArrayList.get(position);
        try {
            if (user.isTicked()) {
                holder.long_press_icon.setVisibility(View.VISIBLE);

            } else {
                holder.long_press_icon.setVisibility(View.GONE);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.name.setText(user.getName());
        holder.aid.setText(user.getAid() + "");
        holder.chatroomid.setText(user.getId() + "");
        holder.id.setText(user.getId() + "");
        holder.type.setText(user.getType() + "");
        holder.url.setText(user.getImage() + "");
        holder.block.setText(user.getBlock() + "");


        if (user.getStatus().equals("null")) {
            holder.message.setText("");
        } else {
            holder.message.setText(user.getStatus());
        }

        if (user.getBlock() == 1) {
            holder.itemView.setClickable(false);
            holder.itemView.setBackgroundResource(R.color.blue_50);

        } else {
            holder.itemView.setClickable(true);

        }
        Picasso.with(Classified.getInstance()).load(user.getImage()).placeholder(R.mipmap.logo).into(holder.user_icon);
        if (msgcnt(a) == 1) {
         /*   holder.message.setText(user.getLastMsg());
            if (user.getUnReadCnt() > 0) {
                holder.count.setText(String.valueOf(user.getUnReadCnt()));
                holder.count.setVisibility(View.VISIBLE);
            } else {
                holder.count.setVisibility(View.GONE);
            }*/

         /*   String str = mhelper.getLastMessage(user.getId());
            Log.e("strstr", str);
            holder.message.setText(str + "");*/


        }

        holder.itemView.setTag(holder);


        holder.itemView.getTag();

        holder.set_checked.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                user.setTicked(true);
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return chatRoomArrayList.size();
    }

    public static String getTimeStamp(String dateStr) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = "";

        today = today.length() < 2 ? "0" + today : today;

        try {
            Date date = format.parse(dateStr);
            SimpleDateFormat todayFormat = new SimpleDateFormat("dd");
            String dateToday = todayFormat.format(date);
            format = dateToday.equals(today) ? new SimpleDateFormat("hh:mm a") : new SimpleDateFormat("dd LLL, hh:mm a");
            String date1 = format.format(date);
            timestamp = date1.toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timestamp;
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public int msgcnt(int a) {
        return a;
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ChatRoomsAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ChatRoomsAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
