package com.classifides.app.gcm.activity;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class DetailsPageUser extends AppCompatActivity {


    CircleImageView group_details_icon;
    TextView phone, status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_page_user);

        String sta = getIntent().getStringExtra("status");
        String pho = getIntent().getStringExtra("phone");
        final String image_url = getIntent().getStringExtra("imgurl");


        group_details_icon = (CircleImageView) findViewById(R.id.group_details_icon);
        phone = (TextView) findViewById(R.id.pppp);
        status = (TextView) findViewById(R.id.sss);
        phone.setText(pho);
        status.setText(sta);

        Picasso.with(Classified.getInstance()).load(image_url).into(group_details_icon);

        group_details_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Dialog d = new Dialog(DetailsPageUser.this);
                d.requestWindowFeature(Window.FEATURE_NO_TITLE);
                d.setContentView(R.layout.profile_img);
                d.setCancelable(true);
                ImageView iii = (ImageView) d.findViewById(R.id.iiii);
                Picasso.with(Classified.getInstance()).load(image_url).into(iii);
                d.show();

            }
        });

    }
}
