package com.classifides.app.gcm.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.classifides.app.MyAdd;
import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.classifides.app.data.NotiDbHelper;
import com.classifides.app.gcm.activity.CreateGroupActivity;
import com.classifides.app.gcm.model.ChatRoom;
import com.classifides.app.gcm.model.NewMessage;
import com.classifides.app.gcm.model.User;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;


public class GroupChatAdapter extends RecyclerView.Adapter<GroupChatAdapter.ViewHolder> {

    private Context mContext;
    NotiDbHelper mhelper;
    NewMessage str;

    public void setChatRoomArrayList(ArrayList<User> chatRoomArrayList) {
        this.chatRoomArrayList = chatRoomArrayList;
    }


    int a = 0;

    private ArrayList<User> chatRoomArrayList;

    public ArrayList<User> getChatRoomArrayList() {
        return chatRoomArrayList;
    }

    private static String today;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, message, timestamp, count, single, chatroomid, id, url, type, block, aid, set_checked;
        CircleImageView user_icon;
        TextView long_press_icon;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            message = (TextView) view.findViewById(R.id.message);
            timestamp = (TextView) view.findViewById(R.id.timestamp);
            count = (TextView) view.findViewById(R.id.count);
            single = (TextView) view.findViewById(R.id.single);
            user_icon = (CircleImageView) view.findViewById(R.id.user_icon);
            chatroomid = (TextView) view.findViewById(R.id.chatroomid);
            id = (TextView) view.findViewById(R.id.id);
            url = (TextView) view.findViewById(R.id.url);
            type = (TextView) view.findViewById(R.id.type);
            block = (TextView) view.findViewById(R.id.block);
            aid = (TextView) view.findViewById(R.id.aid);
            set_checked = (TextView) view.findViewById(R.id.set_checked);
            long_press_icon = (TextView) view.findViewById(R.id.long_press_icon);
            // chatroomid, id, url, type, block
        }
    }


    public GroupChatAdapter(Context mContext, ArrayList<User> chatRoomArrayList) {
        this.mContext = mContext;
        this.chatRoomArrayList = chatRoomArrayList;

        Calendar calendar = Calendar.getInstance();
        today = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        mhelper = new NotiDbHelper(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_rooms_list_row, parent, false);

        return new ViewHolder(itemView);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final User user = chatRoomArrayList.get(position);
        try {
            if (user.isTicked()) {
                holder.long_press_icon.setVisibility(View.VISIBLE);

            } else {
                holder.long_press_icon.setVisibility(View.GONE);

            }

            holder.name.setText(user.getName());
            holder.aid.setText(user.getAid() + "");
            holder.chatroomid.setText(user.getId() + "");
            holder.id.setText(user.getId() + "");
            holder.type.setText(user.getType() + "");
            holder.url.setText(user.getImage() + "");
            holder.block.setText(user.getBlock() + "");
            str = mhelper.getLastGroupMessage(user.getId());
            try {
                if (str.getCh_image() == 0) {
                    holder.message.setText(str.getMsg());
                } else  if (str.getCh_image() == 1){
                    holder.message.setText("Image");
                }else  if (str.getCh_image() == 2){
                    holder.message.setText("Video");
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                String ts = getTimeStamp(str.getRtime());
                holder.timestamp.setText(ts);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //holder.count.setText(str.get);
            // holder.single.setText(str.get);
            holder.message.setVisibility(View.VISIBLE);
            holder.timestamp.setVisibility(View.VISIBLE);

            holder.itemView.setTag(holder.long_press_icon);



        } catch (Exception e) {
            e.printStackTrace();
        }
        try {

            int a = 0;
            int b = 0;
            String oldNotification = Classified.getInstance().getPrefManager().getNotificationsByGroup(holder.chatroomid.getText().toString());
            List<String> messages = Arrays.asList(oldNotification.split("\\|"));

            Set<String> unique = new HashSet<String>(messages);
            for (String key : unique) {
                System.out.println(key + ": " + Collections.frequency(messages, key));
                a = Integer.parseInt(key);
                b = Collections.frequency(messages, key);
                //Log.e("A", a + "");
                //Log.e("B", b + "");

            }

            if (user.getId() == a) {
                holder.count.setText(b + "");
                holder.count.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        Picasso.with(Classified.getInstance()).load(user.getImage()).placeholder(R.mipmap.logo).into(holder.user_icon);
        holder.itemView.setTag(user);

     /*   holder.set_checked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.set_checked.setText("1");
                user.setTicked(true);
            }
        });*/

        holder.set_checked.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                user.setTicked(true);
                return false;
            }
        });


    }

    @Override
    public int getItemCount() {
        return chatRoomArrayList.size();
    }

    public static String getTimeStamp(String dateStr) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = "";

        today = today.length() < 2 ? "0" + today : today;

        try {
            Date date = format.parse(dateStr);
            SimpleDateFormat todayFormat = new SimpleDateFormat("dd");
            String dateToday = todayFormat.format(date);
            format = dateToday.equals(today) ? new SimpleDateFormat("hh:mm a") : new SimpleDateFormat("dd LLL, hh:mm a");
            String date1 = format.format(date);
            timestamp = date1.toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timestamp;
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public int msgcnt(int a) {
        return a;
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ChatRoomsAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ChatRoomsAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }


}









































/*extends RecyclerView.Adapter<GroupChatAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<ChatRoom> chatRoomArrayList;
    private static String today;
    boolean[] itemChecked;
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView uid_id;
        CheckBox uid_ck;

        public ViewHolder(View view) {
            super(view);
            uid_id = (TextView) view.findViewById(R.id.uid_id);
            uid_ck = (CheckBox) view.findViewById(R.id.uid_ck);

        }
    }


    public GroupChatAdapter(Context mContext, ArrayList<ChatRoom> chatRoomArrayList) {
        this.mContext = mContext;
        this.chatRoomArrayList = chatRoomArrayList;
        itemChecked = new boolean[chatRoomArrayList.size()];
        Calendar calendar = Calendar.getInstance();
        today = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.group_row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        ChatRoom chatRoom = chatRoomArrayList.get(position);
        try{
            holder.uid_ck.setText(chatRoom.getName());
            holder.uid_id.setText(chatRoom.getId());

        }catch (Exception e){
            e.printStackTrace();
        }
     *//*   holder.message.setText(chatRoom.getLastMessage());
        if (chatRoom.getUnreadCount() > 0) {
            holder.count.setText(String.valueOf(chatRoom.getUnreadCount()));
            holder.count.setVisibility(View.GONE);
        } else {
            holder.count.setVisibility(View.GONE);
        }

        holder.timestamp.setText(getTimeStamp(chatRoom.getTimestamp()));*//*
        holder.uid_ck.setTag(chatRoom);
       *//* if (itemChecked[position])
            holder.uid_ck.setChecked(true);
        else
            holder.uid_ck.setChecked(false);*//*



        holder.uid_ck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (holder.uid_ck.isChecked() == true) {
                  //  itemChecked[position] = true;

                    CreateGroupActivity.user_id.add(holder.uid_id.getText().toString());

                }

                if (holder.uid_ck.isChecked() == false) {
                  //  itemChecked[position] = false;

                    if (CreateGroupActivity.user_id.contains(holder.uid_id.getText().toString())) {
                        CreateGroupActivity.user_id.remove(holder.uid_id.getText().toString());
                    }

                    if (CreateGroupActivity.user_id.contains(holder.uid_id.getText().toString())) {
                        CreateGroupActivity.user_id.remove(holder.uid_id.getText().toString());
                    }

                }
            }
        });
    }



    @Override
    public int getItemCount() {
        return chatRoomArrayList.size();
    }

    public static String getTimeStamp(String dateStr) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = "";

        today = today.length() < 2 ? "0" + today : today;

        try {
            Date date = format.parse(dateStr);
            SimpleDateFormat todayFormat = new SimpleDateFormat("dd");
            String dateToday = todayFormat.format(date);
            format = dateToday.equals(today) ? new SimpleDateFormat("hh:mm a") : new SimpleDateFormat("dd LLL, hh:mm a");
            String date1 = format.format(date);
            timestamp = date1.toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timestamp;
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ChatRoomsAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ChatRoomsAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
*/