package com.classifides.app.gcm.model;

/**
 * Created by Ashvini on 9/16/2016.
 */
public class GR {
    String unm,uicon,umobi;

    public String getUnm() {
        return unm;
    }

    public void setUnm(String unm) {
        this.unm = unm;
    }

    public String getUicon() {
        return uicon;
    }

    public void setUicon(String uicon) {
        this.uicon = uicon;
    }

    public String getUmobi() {
        return umobi;
    }

    public void setUmobi(String umobi) {
        this.umobi = umobi;
    }
}
