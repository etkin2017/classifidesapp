package com.classifides.app.gcm.gcm;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.classifides.app.data.DAO;
import com.classifides.app.gcm.app.Config;
import com.classifides.app.gcm.app.EndPoints;

import com.classifides.app.gcm.model.User;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class GcmIntentService extends IntentService {

    private static final String TAG = GcmIntentService.class.getSimpleName();

    public GcmIntentService() {
        super(TAG);
    }

    public static final String KEY = "key";
    public static final String TOPIC = "topic";
    public static final String SUBSCRIBE = "subscribe";
    public static final String UNSUBSCRIBE = "unsubscribe";

    SharedPreferences sh;
    int id;

    public Map<String,String> data = new HashMap<String, String>();

    String key;

    @Override
    protected void onHandleIntent(Intent intent) {

          /*  key = intent.getStringExtra(KEY);

            sh = getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);

            String users = sh.getString("users", "");
            try {
                JSONObject j = new JSONObject(users);
                id = j.getInt("id");
            } catch (JSONException e) {
                e.printStackTrace();
            }



        switch (key) {
            case SUBSCRIBE:
                // subscribe to a topic
                String topic = intent.getStringExtra(TOPIC);
             //   subscribeToTopic(topic);
                break;
            case UNSUBSCRIBE:
                break;
            default:
                // if key is specified, register with GCM
                registerGCM();
        }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        registerGCM();

    }

    /**
     * Registering with GCM and obtaining the gcm registration id
     */
    private void registerGCM() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {
           /* InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
*/

            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            //Log.e(TAG, "GCM Registration Token: " + refreshedToken);

            // sending the registration id to our server

            sh = getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);

            String users = sh.getString("users", "");
            try {
                JSONObject j = new JSONObject(users);
                id = j.getInt("id");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            sendRegId(refreshedToken,id+"");

            sharedPreferences.edit().putBoolean(Config.SENT_TOKEN_TO_SERVER, true).apply();
        } catch (Exception e) {
            //Log.e(TAG, "Failed to complete token refresh", e);

            sharedPreferences.edit().putBoolean(Config.SENT_TOKEN_TO_SERVER, false).apply();
        }
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }



    public void sendRegId(final String rid,final String id){
        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                data.put("gcm_reg_id", rid);
                data.put("id", id);


            /*    try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/

              /*  ByteArrayOutputStream stream = new ByteArrayOutputStream();
                PostBannerAddOne.bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);

                new Encode_image().execute();*/
            }

            @Override
            protected Object doInBackground(Object[] params) {

                res = DAO.getJsonFromUrl(Classified.UPDATE_CHAT, data);
                   /* if (res.getInt("success") > 0) {
                        int j = 0;
                        byte[] image = streams.toByteArray();
                        Map<String, String> d = new HashMap<String, String>();
                        d.put("filename", data.get("imagename") + j + ".jpg");
                        d.put("image", Base64.encodeToString(image, 0));
                        DAO.getJsonFromUrl(Config.Server + "uploadimage.php", d);
                        j++;



                    }*/


                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                try {
                    if (res.getInt("success") > 0) {
                       // Toast.makeText(getApplicationContext(), res.toString(), Toast.LENGTH_SHORT).show();

                    } else {
                       // Toast.makeText(getApplicationContext(), res.toString(), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);


    }

    /**
     * Subscribe to a topic
  /*   *//*
    public static void subscribeToTopic(String topic) {
        GcmPubSub pubSub = GcmPubSub.getInstance(Classified.getInstance().getApplicationContext());
        InstanceID instanceID = InstanceID.getInstance(Classified.getInstance().getApplicationContext());
        String token = null;
        try {
            token = instanceID.getToken(Classified.getInstance().getApplicationContext().getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            if (token != null) {
                //   pubSub.subscribe(token, "/topics/" + topic, null);

                FirebaseMessaging.getInstance().subscribeToTopic(topic);
                Log.e(TAG, "Subscribed to topic: " + topic);
            } else {
                Log.e(TAG, "error: gcm registration id is null");
            }
        } catch (IOException e) {
            Log.e(TAG, "Topic subscribe error. Topic: " + topic + ", error: " + e.getMessage());
            Toast.makeText(Classified.getInstance().getApplicationContext(), "Topic subscribe error. Topic: " + topic + ", error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void unsubscribeFromTopic(String topic) {
        GcmPubSub pubSub = GcmPubSub.getInstance(getApplicationContext());
        InstanceID instanceID = InstanceID.getInstance(getApplicationContext());
        String token = null;
        try {
            token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            if (token != null) {
                FirebaseMessaging.getInstance().unsubscribeFromTopic(token);
                Log.e(TAG, "Unsubscribed from topic: " + topic);
            } else {
                Log.e(TAG, "error: gcm registration id is null");
            }
        } catch (IOException e) {
            Log.e(TAG, "Topic unsubscribe error. Topic: " + topic + ", error: " + e.getMessage());
            Toast.makeText(getApplicationContext(), "Topic subscribe error. Topic: " + topic + ", error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }*/
}