package com.classifides.app.gcm.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.util.Linkify;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.classifides.app.gcm.model.Message;
import com.classifides.app.gcm.model.NewMessage;
import com.classifides.app.gcm.model.User;
import com.classifides.app.util.ImageLoadingUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;

public class ChatRoomThreadAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static String TAG = ChatRoomThreadAdapter.class.getSimpleName();
    ProgressDialog pd;
    private String userId;
    private int SELF = 100;
    private static String today;

    Bitmap mBitmap;

    private Context mContext;
    private ArrayList<NewMessage> messageArrayList;
    private String name1;
    private ImageLoadingUtils utils;
    Bitmap scaledBitmap = null;
    File file;
    String filename;
    File SDCardRoo1t;

    //   File ff = new File(Environment.getExternalStorageDirectory().getPath() + "/Classifieds/sentImage/");
    private static String location = Environment.getExternalStorageDirectory().getPath() + "/Classifieds/Images/";
    private static String location1 = Environment.getExternalStorageDirectory().getPath() + "/Classifieds/Videos/";

    private static String location2 = Environment.getExternalStorageDirectory().getPath() + "/Classifieds/RImages/";
    File ff = new File(Environment.getExternalStorageDirectory().getPath() + "/Classifieds/RImages/");

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView message, timestamp, rs, mtype, chk_img;
        ImageView others_img;
        TextView download_logo;
        JCVideoPlayerStandard others_video;
        FrameLayout image_container1;

        public ViewHolder(View view) {
            super(view);
            message = (TextView) itemView.findViewById(R.id.message);
            timestamp = (TextView) itemView.findViewById(R.id.timestamp);
            others_img = (ImageView) itemView.findViewById(R.id.others_image);
            others_video = (JCVideoPlayerStandard) itemView.findViewById(R.id.others_video);
            download_logo = (TextView) itemView.findViewById(R.id.download_logo);
            image_container1 = (FrameLayout) itemView.findViewById(R.id.image_container1);

            rs = (TextView) itemView.findViewById(R.id.rs);
            mtype = (TextView) itemView.findViewById(R.id.mtype);
            chk_img = (TextView) itemView.findViewById(R.id.chk_img);
        }
    }


    public ChatRoomThreadAdapter(Context mContext, ArrayList<NewMessage> messageArrayList, String userId, String name) {
        this.mContext = mContext;
        this.messageArrayList = messageArrayList;
        this.userId = userId;

        Calendar calendar = Calendar.getInstance();
        today = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        this.name1 = name;
        utils = new ImageLoadingUtils(com.classifides.app.application.Classified.getInstance());
        pd = new ProgressDialog(mContext);


        if (!ff.exists()) {
            ff.getParentFile().mkdirs();
            if (ff.getParentFile().mkdirs()) {
                System.out.println("Directory created");
            } else {
                System.out.println("Directory is not created");
            }
            // Log.e("DIRR",ff.toString());
        }


    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        // view type is to identify where to render the chat message
        // left or right
        if (viewType == SELF) {
            // self message
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_item_self, parent, false);
        } else {
            // others message
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_item_other, parent, false);
        }


        return new ViewHolder(itemView);
    }


    @Override
    public int getItemViewType(int position) {
       /* Message message = messageArrayList.get(position);
        if (message.getUser().getId()==Integer.parseInt(userId)) {
            return SELF;
        }*/

        NewMessage message = messageArrayList.get(position);
        if (message.getMtypr() == 1) {
            if (message.getRs() == 0) {
                return SELF;
            }
        } else {
            if (message.getRs() == 0) {
                return SELF;
            }
        }

        return position;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {


        final NewMessage message = messageArrayList.get(position);
        String timestamp = getTimeStamp(message.getRtime());
        ((ViewHolder) holder).rs.setText(message.getRs() + "");
        ((ViewHolder) holder).mtype.setText(message.getMtypr() + "");
        ((ViewHolder) holder).chk_img.setText(message.getCh_image() + "");


        Linkify.addLinks(((ViewHolder) holder).message, Linkify.ALL);

        if (message.getId() + "" != null) {
          /*  timestamp = message.getUid() + ", " + timestamp;*/


            holder.setIsRecyclable(false);


           /* ((ViewHolder) holder).message.setVisibility(View.VISIBLE);
            ((ViewHolder) holder).others_img.setVisibility(View.VISIBLE);
            ((ViewHolder) holder).others_video.setVisibility(View.VISIBLE);
            ((ViewHolder) holder).image_container1.setVisibility(View.VISIBLE);*/


            if (((ViewHolder) holder).mtype.getText().toString().equals("1")) {
                if (((ViewHolder) holder).rs.getText().toString().equals("0")) {
                    timestamp = "me" + ", " + timestamp;
                    //  ((ViewHolder) holder).message.setText(message.getMsg());
                    ((ViewHolder) holder).download_logo.setVisibility(View.GONE);


                    if (((ViewHolder) holder).chk_img.getText().toString().equals("1")) {


                        Picasso.with(Classified.getInstance()).load("file:" + location + message.getMsg()).placeholder(R.drawable.classifieds).resize(270, 200).into(((ViewHolder) holder).others_img);

                        ((ViewHolder) holder).others_img.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.parse("file://" + location + message.getMsg()), "image/*");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                Classified.getInstance().startActivity(intent);
                            }
                        });

                        /// ((ViewHolder) holder).message.setText(message.getMsg());
                        ((ViewHolder) holder).message.setVisibility(View.GONE);
                        ((ViewHolder) holder).others_video.setVisibility(View.GONE);

                    } else if (((ViewHolder) holder).chk_img.getText().toString().equals("2")) {
                        ((ViewHolder) holder).others_video.setUp("file:" + location1 + message.getMsg(), "");
                        ((ViewHolder) holder).others_video.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.parse("file://" + location1 + message.getMsg()), "video");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                Classified.getInstance().startActivity(intent);
                            }
                        });

                        //    holder.single.setText(Html.fromHtml(details.get(position)));
                        ((ViewHolder) holder).message.setText(Html.fromHtml(message.getMsg()));
                        ((ViewHolder) holder).message.setVisibility(View.GONE);
                        ((ViewHolder) holder).others_img.setVisibility(View.GONE);
                        ((ViewHolder) holder).others_video.setVisibility(View.VISIBLE);
                        ((ViewHolder) holder).image_container1.setVisibility(View.GONE);


                    } else {
                        ((ViewHolder) holder).others_img.setVisibility(View.GONE);
                        ((ViewHolder) holder).image_container1.setVisibility(View.GONE);
                        ((ViewHolder) holder).others_video.setVisibility(View.GONE);
                        ((ViewHolder) holder).message.setVisibility(View.VISIBLE);
                        ((ViewHolder) holder).message.setText(Html.fromHtml(message.getMsg()));//image_container1

                    }

                } else {
                    timestamp = name1 + "," + timestamp;


                    if (message.getCh_image() == 1) {

                        Picasso.with(Classified.getInstance()).load("http://catchmeon.net/catchmeon/messanger/resize/" + message.getMsg()).placeholder(R.drawable.classifieds).into(((ViewHolder) holder).others_img);
                        ((ViewHolder) holder).message.setText(Html.fromHtml(message.getMsg()));
                        ((ViewHolder) holder).message.setVisibility(View.GONE);
                        File extStore = Environment.getExternalStorageDirectory();
                        File myFile = new File(extStore.getAbsolutePath() + "/Classifieds/RImages/" + message.getMsg());
                        if (myFile.exists()) {
                            ((ViewHolder) holder).download_logo.setVisibility(View.GONE);
                            Picasso.with(Classified.getInstance()).load("file:" + location2 + message.getMsg()).placeholder(R.drawable.classifieds).resize(200, 140).into(((ViewHolder) holder).others_img);

                        }


                        ((ViewHolder) holder).others_img.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                File extStore = Environment.getExternalStorageDirectory();
                                File myFile = new File(extStore.getAbsolutePath() + "/Classifieds/RImages/" + message.getMsg());

                                if (myFile.exists()) {
                                    ((ViewHolder) holder).download_logo.setVisibility(View.GONE);
                                    Intent intent = new Intent();
                                    intent.setAction(Intent.ACTION_VIEW);
                                    //       intent.setDataAndType(Uri.parse("file://" + location2 + message.getMsg()), "image*//*");
                                    intent.setDataAndType(Uri.parse("file://" + location2 + message.getMsg()), "image/*");

                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    Classified.getInstance().startActivity(intent);
                                } else {
                                    try {
                                        downloadimg("http://catchmeon.net/catchmeon/messanger/original/" + message.getMsg(), message.getMsg());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }


                            }
                        });

                    } else if (message.getCh_image() == 2) {
                        ((ViewHolder) holder).others_video.setUp("http://catchmeon.net/catchmeon/messanger/videos/" + message.getMsg(), "");
                        ((ViewHolder) holder).others_video.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.parse("file://" + location1 + message.getMsg()), "video");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                Classified.getInstance().startActivity(intent);
                            }
                        });

                        ((ViewHolder) holder).message.setText(Html.fromHtml(message.getMsg()));
                        ((ViewHolder) holder).message.setVisibility(View.GONE);
                        ((ViewHolder) holder).others_img.setVisibility(View.GONE);
                        ((ViewHolder) holder).image_container1.setVisibility(View.GONE);
                        ((ViewHolder) holder).others_video.setVisibility(View.VISIBLE);
                    } else {
                        ((ViewHolder) holder).others_img.setVisibility(View.GONE);
                        ((ViewHolder) holder).others_video.setVisibility(View.GONE);
                        ((ViewHolder) holder).message.setText(Html.fromHtml(message.getMsg()));
                        ((ViewHolder) holder).image_container1.setVisibility(View.GONE);

                    }
                }


            } else if (message.getMtypr() == 2) {
                if (message.getRs() == 0) {
                    timestamp = "me" + ", " + timestamp;


                    //  ((ViewHolder) holder).message.setText(message.getMsg());
                    ((ViewHolder) holder).download_logo.setVisibility(View.GONE);
                    if (message.getCh_image() == 1) {
                        //Log.e("FilePath121", "file:" + location + message.getMsg());
                        Picasso.with(Classified.getInstance()).load("file:" + location + message.getMsg()).placeholder(R.drawable.classifieds).resize(200, 140).into(((ViewHolder) holder).others_img);
                        // ((ViewHolder) holder).message.setText(message.getMsg());
                        ((ViewHolder) holder).message.setVisibility(View.GONE);
                        ((ViewHolder) holder).others_img.setVisibility(View.VISIBLE);
                        ((ViewHolder) holder).others_img.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.parse("file://" + location + message.getMsg()), "image/*");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                Classified.getInstance().startActivity(intent);
                            }
                        });


                    } else if (message.getCh_image() == 2) {

                        ((ViewHolder) holder).others_video.setUp("file:" + location1 + message.getMsg(), "");
                        ((ViewHolder) holder).others_video.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.parse("file://" + location1 + message.getMsg()), "video");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                Classified.getInstance().startActivity(intent);
                            }
                        });

                        //  ((ViewHolder) holder).message.setText(message.getMsg());
                        ((ViewHolder) holder).message.setVisibility(View.GONE);
                        ((ViewHolder) holder).others_img.setVisibility(View.GONE);
                        ((ViewHolder) holder).others_video.setVisibility(View.VISIBLE);
                        ((ViewHolder) holder).image_container1.setVisibility(View.GONE);

                    } else {
                 /*       ((ViewHolder) holder).others_img.setVisibility(View.GONE);
                        ((ViewHolder) holder).download_logo.setVisibility(View.GONE);
                        ((ViewHolder) holder).image_container1.setVisibility(View.GONE);*/


                        ((ViewHolder) holder).others_img.setVisibility(View.GONE);
                        ((ViewHolder) holder).image_container1.setVisibility(View.GONE);
                        ((ViewHolder) holder).others_video.setVisibility(View.GONE);
                        ((ViewHolder) holder).message.setVisibility(View.VISIBLE);
                        ((ViewHolder) holder).message.setText(Html.fromHtml(message.getMsg()));

                    }

                } else {
                    timestamp = timestamp + "";


                    if (message.getCh_image() == 1) {

                        String namepass[] = message.getMsg().split(":");
                        String name = namepass[0];
                        final String pass = namepass[1].trim().toString();


                        Picasso.with(Classified.getInstance()).load("http://catchmeon.net/catchmeon/messanger/resize/" + pass.trim().toString()).placeholder(R.drawable.classifieds).into(((ViewHolder) holder).others_img);
                        //     Picasso.with(Classified.getInstance()).load("http://catchmeon.net/catchmeon/messanger/original/" + message.getMsg()).into(target);


                        ((ViewHolder) holder).message.setText(Html.fromHtml(message.getMsg()));
                        ((ViewHolder) holder).message.setVisibility(View.GONE);


                        File extStore = Environment.getExternalStorageDirectory();
                        File myFile = new File(extStore.getAbsolutePath() + "/Classifieds/RImages/" + pass.toString().trim());

                        if (myFile.exists()) {
                            ((ViewHolder) holder).download_logo.setVisibility(View.GONE);
                            Picasso.with(Classified.getInstance()).load("file:" + location2 + pass.toString().trim()).placeholder(R.drawable.classifieds).resize(200, 140).into(((ViewHolder) holder).others_img);

                        }


                        ((ViewHolder) holder).others_img.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {


                                File extStore = Environment.getExternalStorageDirectory();
                                File myFile = new File(extStore.getAbsolutePath() + "/Classifieds/RImages/" + pass.toString().trim());

                                if (myFile.exists()) {
                                    Intent intent = new Intent();
                                    intent.setAction(Intent.ACTION_VIEW);
                                    // intent.setDataAndType(Uri.parse("file://" + location2 + message.getMsg()), "image*//*");
                                    intent.setDataAndType(Uri.parse("file://" + location2 + pass.toString().trim()), "image/*");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    Classified.getInstance().startActivity(intent);
                                } else {
                                    try {

                                        String namepass[] = message.getMsg().split(":");
                                        String name = namepass[0];
                                        String pass = namepass[1].trim().toString();

                                        downloadimg("http://catchmeon.net/catchmeon/messanger/original/" + pass.trim().toString(), pass.trim().toString());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                             /*   Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.parse("file://" + location2 + message.getMsg()), "image*//*");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                Classified.getInstance().startActivity(intent);*/
                                // mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://catchmeon.net/catchmeon/messanger/original/" + message.getMsg()))); /** replace with your own uri */


                            }
                        });


                        // mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("content://media/external/images/media/16"))); /** replace with your own uri */

                    } else {
                        ((ViewHolder) holder).others_img.setVisibility(View.GONE);
                        ((ViewHolder) holder).download_logo.setVisibility(View.GONE);
                        ((ViewHolder) holder).message.setText(Html.fromHtml(message.getMsg()));
                        ((ViewHolder) holder).image_container1.setVisibility(View.GONE);

                    }
                }


            }
        }

        ((ViewHolder) holder).timestamp.setText(timestamp);
    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public void downloadimg(final String Url, final String imgNM) {

        final String[] filepath = {null};
        new AsyncTask() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();


                pd.setTitle("Please wait");
                pd.show();
            }

            @Override
            protected Object doInBackground(Object[] objects) {
                try {
                    URL url = new URL(Url);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestProperty("Accept-Encoding", "identity");
                    urlConnection.setRequestProperty("Connection", "Keep-alive");
                    File SDCardRoot = new File(Environment.getExternalStorageDirectory().getParentFile() + "/Classifieds/RImages/sentimages");

                    if (!SDCardRoot.getParentFile().exists()) {
                        SDCardRoot.getParentFile().mkdirs();
                    }
                    SDCardRoo1t = Environment.getExternalStorageDirectory();
                    //create a new file, specifying the path, and the filename
                    //which we want to save the file as.

                    String filename = imgNM;   // you can download to any type of file ex:.jpeg (image) ,.txt(text file),.mp3 (audio file)
                    //Log.i("Local filename:", "" + filename);
                    file = new File(SDCardRoo1t, filename);
                    if (file.createNewFile()) {
                        file.createNewFile();
                    }


                    //this will be used to write the downloaded data into the file we created
                    FileOutputStream fileOutput = new FileOutputStream(file);

                    //this will be used in reading the data from the internet
                    InputStream inputStream = urlConnection.getInputStream();


                    //this is the total size of the file
                    int totalSize = urlConnection.getContentLength();
                    //variable to store total downloaded bytes
                    int downloadedSize = 0;

                    //create a buffer...
                    byte[] buffer = new byte[1024];
                    int bufferLength = 0; //used to store a temporary size of the buffer

                    //now, read through the input buffer and write the contents to the file
                    while ((bufferLength = inputStream.read(buffer)) > 0) {
                        //add the data in the buffer to the file in the file output stream (the file on the sd card
                        fileOutput.write(buffer, 0, bufferLength);
                        //add up the size so we know how much is downloaded
                        downloadedSize += bufferLength;
                        //this is where you would do something to report the prgress, like this maybe
                        //Log.i("Progress:", "downloadedSize:" + downloadedSize + "totalSize:" + totalSize);

                    }
                    //close the output stream when done
                    fileOutput.close();
                    if (downloadedSize == totalSize) filepath[0] = file.getPath();


                    urlConnection.connect();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    filepath[0] = null;
                    e.printStackTrace();
                }
                //Log.i("filepath:", " " + filepath[0]);

                return filepath[0];
            }


            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                //getUsersFromServer(allcont);

                pd.dismiss();

                if (filepath[0].equals("")) {

                } else {
                    File h = new File(Environment.getExternalStorageDirectory().getPath());
                    try {
                        copyFile(file, h, imgNM);
                        notifyDataSetChanged();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

            }


        }.execute(null, null, null);
    }


    private void copyFile(File sourceFile, File destFile, String img_nm) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }

        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        File ff = new File(Environment.getExternalStorageDirectory().getPath() + "/Classifieds/RImages/sentImage/");
        if (!ff.getParentFile().exists()) {
            if (ff.getParentFile().mkdirs()) ;
        }

        //   String uimg_nm = email + "" + System.currentTimeMillis() + ".jpg";

        File f1 = new File(ff.getAbsoluteFile().getParentFile().getPath() + "/" + img_nm);
        f1.getParentFile().mkdirs();

        //Log.e("FILEPATH", f1.toString());

        // if(ff.getAbsolutePath()+"/")
        destination = new FileOutputStream(f1).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse("file://" + location2 + img_nm), "image/*");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Classified.getInstance().startActivity(intent);
        }
        //Log.e("SOURCE FILE", source.toString());
        //Log.e("DESTFILE", destination.toString());
        if (source != null) {

            source.close();
        }
        if (destination != null) {
            destination.close();
        }


        //;

    }

    @Override
    public int getItemCount() {
        return messageArrayList.size();
    }

    public static String getTimeStamp(String dateStr) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = "";

        today = today.length() < 2 ? "0" + today : today;

        try {
            Date date = format.parse(dateStr);
            SimpleDateFormat todayFormat = new SimpleDateFormat("dd");
            String dateToday = todayFormat.format(date);
            format = dateToday.equals(today) ? new SimpleDateFormat("hh:mm a") : new SimpleDateFormat("dd LLL, hh:mm a");
            String date1 = format.format(date);
            timestamp = date1.toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timestamp;
    }

    public ArrayList<NewMessage> getMessageArrayList() {
        return messageArrayList;
    }

    public void setMessageArrayList(ArrayList<NewMessage> messageArrayList) {
        this.messageArrayList = messageArrayList;
    }
}









































/*
package com.classifides.app.gcm.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.classifides.app.gcm.model.Message;
import com.classifides.app.gcm.model.NewMessage;
import com.classifides.app.gcm.model.User;
import com.classifides.app.util.ImageLoadingUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;

public class ChatRoomThreadAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static String TAG = ChatRoomThreadAdapter.class.getSimpleName();
    ProgressDialog pd;
    private String userId;
    private int SELF = 100;
    private static String today;

    Bitmap mBitmap;

    private Context mContext;
    private ArrayList<NewMessage> messageArrayList;
    private String name1;
    private ImageLoadingUtils utils;
    Bitmap scaledBitmap = null;
    File file;
    String filename;
    File SDCardRoo1t;

    //   File ff = new File(Environment.getExternalStorageDirectory().getPath() + "/Classifieds/sentImage/");
    private static String location = Environment.getExternalStorageDirectory().getPath() + "/Classifieds/Images/";
    private static String location1 = Environment.getExternalStorageDirectory().getPath() + "/Classifieds/Videos/";

    private static String location2 = Environment.getExternalStorageDirectory().getPath() + "/Classifieds/RImages/";
    File ff = new File(Environment.getExternalStorageDirectory().getPath() + "/Classifieds/RImages/");

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView message, timestamp;
        ImageView others_img;
        TextView download_logo;
        JCVideoPlayerStandard others_video;
        FrameLayout image_container1;

        public ViewHolder(View view) {
            super(view);
            message = (TextView) itemView.findViewById(R.id.message);
            timestamp = (TextView) itemView.findViewById(R.id.timestamp);
            others_img = (ImageView) itemView.findViewById(R.id.others_image);
            others_video = (JCVideoPlayerStandard) itemView.findViewById(R.id.others_video);
            download_logo = (TextView) itemView.findViewById(R.id.download_logo);
            image_container1 = (FrameLayout) itemView.findViewById(R.id.image_container1);
        }
    }

    public String compressImage(String imageUri, String img_nm) {

        String filePath = getRealPathFromURI(imageUri);


        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = utils.calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));


        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileOutputStream out = null;
        filename = getFilename(img_nm);


        return filename;

    }

    public String getFilename(String img_nma) {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "/Classifieds/Images/");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + img_nma);
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = Classified.getInstance().getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public ChatRoomThreadAdapter(Context mContext, ArrayList<NewMessage> messageArrayList, String userId, String name) {
        this.mContext = mContext;
        this.messageArrayList = messageArrayList;
        this.userId = userId;

        Calendar calendar = Calendar.getInstance();
        today = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        this.name1 = name;
        utils = new ImageLoadingUtils(com.classifides.app.application.Classified.getInstance());
        pd = new ProgressDialog(mContext);


        if (!ff.exists()) {
            ff.getParentFile().mkdirs();
            if (ff.getParentFile().mkdirs()) {
                System.out.println("Directory created");
            } else {
                System.out.println("Directory is not created");
            }
            // Log.e("DIRR",ff.toString());
        }


    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        // view type is to identify where to render the chat message
        // left or right
        if (viewType == SELF) {
            // self message
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_item_self, parent, false);
        } else {
            // others message
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_item_other, parent, false);
        }


        return new ViewHolder(itemView);
    }


    @Override
    public int getItemViewType(int position) {
       */
/* Message message = messageArrayList.get(position);
        if (message.getUser().getId()==Integer.parseInt(userId)) {
            return SELF;
        }*//*


        NewMessage message = messageArrayList.get(position);
        if (message.getMtypr() == 1) {
            if (message.getRs() == 0) {
                return SELF;
            }
        } else {
            if (message.getRs() == 0) {
                return SELF;
            }
        }

        return position;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final NewMessage message = messageArrayList.get(position);
        String timestamp = getTimeStamp(message.getRtime());

        if (message.getId() + "" != null) {
          */
/*  timestamp = message.getUid() + ", " + timestamp;*//*

            if (message.getMtypr() == 1) {
                if (message.getRs() == 0) {
                    timestamp = "me" + ", " + timestamp;
                    ((ViewHolder) holder).message.setText(message.getMsg());
                    ((ViewHolder) holder).download_logo.setVisibility(View.GONE);
                    if (message.getCh_image() == 1) {

                        Picasso.with(Classified.getInstance()).load("file:" + location + message.getMsg()).placeholder(R.drawable.classifieds).resize(270, 200).into(((ViewHolder) holder).others_img);

                        ((ViewHolder) holder).others_img.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.parse("file://" + location + message.getMsg()), "image*/
/*");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                Classified.getInstance().startActivity(intent);
                            }
                        });

                        /// ((ViewHolder) holder).message.setText(message.getMsg());
                        ((ViewHolder) holder).message.setVisibility(View.GONE);
                        ((ViewHolder) holder).others_video.setVisibility(View.GONE);

                    } else if (message.getCh_image() == 2) {
                        ((ViewHolder) holder).others_video.setUp("file:" + location1 + message.getMsg(), "");
                        ((ViewHolder) holder).others_video.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.parse("file://" + location1 + message.getMsg()), "video");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                Classified.getInstance().startActivity(intent);
                            }
                        });

                        ((ViewHolder) holder).message.setText(message.getMsg());
                        ((ViewHolder) holder).message.setVisibility(View.GONE);
                        ((ViewHolder) holder).others_img.setVisibility(View.GONE);

                        ((ViewHolder) holder).others_video.setVisibility(View.VISIBLE);


                    } else {
                        ((ViewHolder) holder).others_img.setVisibility(View.GONE);
                        ((ViewHolder) holder).image_container1.setVisibility(View.GONE);
                        ((ViewHolder) holder).message.setText(message.getMsg());//image_container1
                    }

                } else  if (message.getRs() == 1) {
                    timestamp = name1 + "," + timestamp;
                    if (message.getCh_image() == 1) {
                        Picasso.with(Classified.getInstance()).load("http://catchmeon.net/catchmeon/messanger/resize/" + message.getMsg()).placeholder(R.drawable.classifieds).into(((ViewHolder) holder).others_img);
                        ((ViewHolder) holder).message.setText(message.getMsg());
                        ((ViewHolder) holder).message.setVisibility(View.GONE);


                        File extStore = Environment.getExternalStorageDirectory();
                        File myFile = new File(extStore.getAbsolutePath() + "/Classifieds/RImages/" + message.getMsg());

                        if (myFile.exists()) {
                            ((ViewHolder) holder).download_logo.setVisibility(View.GONE);
                            Picasso.with(Classified.getInstance()).load("file:" + location2 + message.getMsg()).placeholder(R.drawable.classifieds).resize(200, 140).into(((ViewHolder) holder).others_img);

                        }


                        ((ViewHolder) holder).others_img.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                File extStore = Environment.getExternalStorageDirectory();
                                File myFile = new File(extStore.getAbsolutePath() + "/Classifieds/RImages/" + message.getMsg());

                                if (myFile.exists()) {
                                    ((ViewHolder) holder).download_logo.setVisibility(View.GONE);
                                    Intent intent = new Intent();
                                    intent.setAction(Intent.ACTION_VIEW);
                                    //       intent.setDataAndType(Uri.parse("file://" + location2 + message.getMsg()), "image*//*
*/
/*");
                                    intent.setDataAndType(Uri.parse("file://" + location2 + message.getMsg()), "image*/
/*");

                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    Classified.getInstance().startActivity(intent);
                                } else {
                                    downloadimg("http://catchmeon.net/catchmeon/messanger/original/" + message.getMsg(), message.getMsg());

                                }


                            }
                        });

                    } else if (message.getCh_image() == 2) {
                        ((ViewHolder) holder).others_video.setUp("http://catchmeon.net/catchmeon/messanger/videos/"+message.getMsg(), "");
                        ((ViewHolder) holder).others_video.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.parse("file://" + location1 + message.getMsg()), "video");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                Classified.getInstance().startActivity(intent);
                            }
                        });

                        ((ViewHolder) holder).message.setText(message.getMsg());
                        ((ViewHolder) holder).message.setVisibility(View.GONE);
                        ((ViewHolder) holder).others_img.setVisibility(View.GONE);
                        ((ViewHolder) holder).image_container1.setVisibility(View.GONE);
                        ((ViewHolder) holder).others_video.setVisibility(View.VISIBLE);
                    } else {
                        ((ViewHolder) holder).others_img.setVisibility(View.GONE);
                        ((ViewHolder) holder).others_video.setVisibility(View.GONE);
                        ((ViewHolder) holder).message.setText(message.getMsg());
                        ((ViewHolder) holder).message.setVisibility(View.VISIBLE);
                        ((ViewHolder) holder).image_container1.setVisibility(View.GONE);

                    }
                }


            } else {
                if (message.getRs() == 0) {
                    timestamp = "me" + ", " + timestamp;
                    ((ViewHolder) holder).message.setText(message.getMsg());
                    ((ViewHolder) holder).download_logo.setVisibility(View.GONE);
                    if (message.getCh_image() == 1) {
                        Picasso.with(Classified.getInstance()).load("file:" + location + message.getMsg()).placeholder(R.drawable.classifieds).resize(70, 70).into(((ViewHolder) holder).others_img);
                        ((ViewHolder) holder).message.setText(message.getMsg());
                        ((ViewHolder) holder).message.setVisibility(View.GONE);


                        ((ViewHolder) holder).others_img.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.parse("file://" + location + message.getMsg()), "image*/
/*");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                Classified.getInstance().startActivity(intent);
                            }
                        });


                    } else {
                        ((ViewHolder) holder).others_img.setVisibility(View.GONE);
                        ((ViewHolder) holder).download_logo.setVisibility(View.GONE);
                        ((ViewHolder) holder).image_container1.setVisibility(View.GONE);


                    }

                } else {
                    timestamp = timestamp + "";
                    if (message.getCh_image() == 1) {

                        Picasso.with(Classified.getInstance()).load("http://catchmeon.net/catchmeon/messanger/resize/" + message.getMsg()).placeholder(R.drawable.classifieds).into(((ViewHolder) holder).others_img);
                        //     Picasso.with(Classified.getInstance()).load("http://catchmeon.net/catchmeon/messanger/original/" + message.getMsg()).into(target);


                        ((ViewHolder) holder).message.setText(message.getMsg());
                        ((ViewHolder) holder).message.setVisibility(View.GONE);


                        File extStore = Environment.getExternalStorageDirectory();
                        File myFile = new File(extStore.getAbsolutePath() + "/Classifieds/RImages/" + message.getMsg());

                        if (myFile.exists()) {
                            ((ViewHolder) holder).download_logo.setVisibility(View.GONE);
                            Picasso.with(Classified.getInstance()).load("file:" + location2 + message.getMsg()).placeholder(R.drawable.classifieds).resize(200, 140).into(((ViewHolder) holder).others_img);

                        }


                        ((ViewHolder) holder).others_img.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {


                                File extStore = Environment.getExternalStorageDirectory();
                                File myFile = new File(extStore.getAbsolutePath() + "/Classifieds/RImages/" + message.getMsg());

                                if (myFile.exists()) {
                                    Intent intent = new Intent();
                                    intent.setAction(Intent.ACTION_VIEW);
                                    // intent.setDataAndType(Uri.parse("file://" + location2 + message.getMsg()), "image*//*
*/
/*");
                                    intent.setDataAndType(Uri.parse("file://" + location2 + message.getMsg()), "image*/
/*");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    Classified.getInstance().startActivity(intent);
                                } else {
                                    downloadimg("http://catchmeon.net/catchmeon/messanger/original/" + message.getMsg(), message.getMsg());

                                }
                             */
/*   Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.parse("file://" + location2 + message.getMsg()), "image*//*
*/
/*");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                Classified.getInstance().startActivity(intent);*//*

                                // mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://catchmeon.net/catchmeon/messanger/original/" + message.getMsg()))); */
/**
 * replace with your own uri  replace with your own uri
 * replace with your own uri
 * replace with your own uri
 * replace with your own uri
 * replace with your own uri
 * replace with your own uri
 * replace with your own uri
 * replace with your own uri
 * replace with your own uri
 *//*



                            }
                        });


                        // mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("content://media/external/images/media/16"))); */
/** replace with your own uri *//*


                    } else {
                        ((ViewHolder) holder).others_img.setVisibility(View.GONE);
                        ((ViewHolder) holder).download_logo.setVisibility(View.GONE);
                        ((ViewHolder) holder).message.setText(message.getMsg());
                        ((ViewHolder) holder).image_container1.setVisibility(View.GONE);

                    }
                }


            }
        }

        ((ViewHolder) holder).timestamp.setText(timestamp);
    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public void downloadimg(final String Url, final String imgNM) {

        final String[] filepath = {null};
        new AsyncTask() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();


                pd.setTitle("Please wait");
                pd.show();
            }

            @Override
            protected Object doInBackground(Object[] objects) {
                try {
                    URL url = new URL(Url);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestProperty("Accept-Encoding", "identity");
                    urlConnection.setRequestProperty("Connection", "Keep-alive");
                    File SDCardRoot = new File(Environment.getExternalStorageDirectory().getParentFile() + "/Classifieds/RImages/sentimages");

                    if (!SDCardRoot.getParentFile().exists()) {
                        SDCardRoot.getParentFile().mkdirs();
                    }
                    SDCardRoo1t = Environment.getExternalStorageDirectory();
                    //create a new file, specifying the path, and the filename
                    //which we want to save the file as.

                    String filename = imgNM;   // you can download to any type of file ex:.jpeg (image) ,.txt(text file),.mp3 (audio file)
                    Log.i("Local filename:", "" + filename);
                    file = new File(SDCardRoo1t, filename);
                    if (file.createNewFile()) {
                        file.createNewFile();
                    }


                    //this will be used to write the downloaded data into the file we created
                    FileOutputStream fileOutput = new FileOutputStream(file);

                    //this will be used in reading the data from the internet
                    InputStream inputStream = urlConnection.getInputStream();


                    //this is the total size of the file
                    int totalSize = urlConnection.getContentLength();
                    //variable to store total downloaded bytes
                    int downloadedSize = 0;

                    //create a buffer...
                    byte[] buffer = new byte[1024];
                    int bufferLength = 0; //used to store a temporary size of the buffer

                    //now, read through the input buffer and write the contents to the file
                    while ((bufferLength = inputStream.read(buffer)) > 0) {
                        //add the data in the buffer to the file in the file output stream (the file on the sd card
                        fileOutput.write(buffer, 0, bufferLength);
                        //add up the size so we know how much is downloaded
                        downloadedSize += bufferLength;
                        //this is where you would do something to report the prgress, like this maybe
                        Log.i("Progress:", "downloadedSize:" + downloadedSize + "totalSize:" + totalSize);

                    }
                    //close the output stream when done
                    fileOutput.close();
                    if (downloadedSize == totalSize) filepath[0] = file.getPath();


                    urlConnection.connect();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    filepath[0] = null;
                    e.printStackTrace();
                }
                Log.i("filepath:", " " + filepath[0]);

                return filepath[0];
            }


            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                //getUsersFromServer(allcont);

                pd.dismiss();

                if (filepath[0].equals("")) {

                } else {
                    File h = new File(Environment.getExternalStorageDirectory().getPath());
                    try {
                        copyFile(file, h, imgNM);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

            }


        }.execute(null, null, null);
    }


    private void copyFile(File sourceFile, File destFile, String img_nm) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }

        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        File ff = new File(Environment.getExternalStorageDirectory().getPath() + "/Classifieds/RImages/sentImage/");
        if (!ff.getParentFile().exists()) {
            if (ff.getParentFile().mkdirs()) ;
        }

        //   String uimg_nm = email + "" + System.currentTimeMillis() + ".jpg";

        File f1 = new File(ff.getAbsoluteFile().getParentFile().getPath() + "/" + img_nm);
        f1.getParentFile().mkdirs();

        Log.e("FILEPATH", f1.toString());

        // if(ff.getAbsolutePath()+"/")
        destination = new FileOutputStream(f1).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse("file://" + location2 + img_nm), "image*/
/*");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Classified.getInstance().startActivity(intent);
        }
        Log.e("SOURCE FILE", source.toString());
        Log.e("DESTFILE", destination.toString());
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }


        //;

    }

    @Override
    public int getItemCount() {
        return messageArrayList.size();
    }

    public static String getTimeStamp(String dateStr) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = "";

        today = today.length() < 2 ? "0" + today : today;

        try {
            Date date = format.parse(dateStr);
            SimpleDateFormat todayFormat = new SimpleDateFormat("dd");
            String dateToday = todayFormat.format(date);
            format = dateToday.equals(today) ? new SimpleDateFormat("hh:mm a") : new SimpleDateFormat("dd LLL, hh:mm a");
            String date1 = format.format(date);
            timestamp = date1.toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timestamp;
    }

    public ArrayList<NewMessage> getMessageArrayList() {
        return messageArrayList;
    }

    public void setMessageArrayList(ArrayList<NewMessage> messageArrayList) {
        this.messageArrayList = messageArrayList;
    }
}

*/
