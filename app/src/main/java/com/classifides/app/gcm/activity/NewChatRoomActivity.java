package com.classifides.app.gcm.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.PersistableBundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.data.NotiDbHelper;
import com.classifides.app.gcm.adapter.ChatRoomThreadAdapter;
import com.classifides.app.gcm.fragment.ChatFragment;
import com.classifides.app.gcm.gcm.MyGcmPushReceiver;
import com.classifides.app.gcm.model.Message;
import com.classifides.app.gcm.model.NewMessage;
import com.classifides.app.gcm.model.User;
import com.classifides.app.services.LocationTressService;
import com.classifides.app.util.ImageLoadingUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.FileChannel;
import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;

public class NewChatRoomActivity extends AppCompatActivity {

    private String TAG = NewChatRoomActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private ChatRoomThreadAdapter mAdapter;
    private ArrayList<NewMessage> messageArrayList;
    private ArrayList<Message> msgArraylist;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private EditText inputMessage;
    private Button btnSend;
    NotiDbHelper mHelper;
    SharedPreferences sh;
    int id;
    int chatRoomId, type;
    Map<String, String> data;
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    int REQUEST_TAKE_GALLERY_VIDEO = 100;
    Uri file_uri;
    private ImageLoadingUtils utils;
    Bitmap scaledBitmap = null;
    File ff = new File(Environment.getExternalStorageDirectory().getPath() + "/Classifieds/sentImage/");
    Dialog a;
    private Dialog dialog1;

    String selectedImagePath, filemanagerstring;

    NotiDbHelper helper;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;

    private int PICK_IMAGE_REQUEST = 1;
    private Uri fileUri;
    //  File dir = new File(Environment.getExternalStorageDirectory().getPath() + "/android/data/com.classifides.app/Classifieds/sentImage");

    String filename;
    ArrayList<NewMessage> mArraylistAdapter;


    MyNewBroadcast broadcast;
    String uname;

    Uri mCapturedImageURI;
    public static boolean activate1 = true;
    Calendar c = Calendar.getInstance();
    SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
    String cdate = df.format(c.getTime());

    CircleImageView send_attachment;

    TextView chat_room_name;
    public static String encoded_string, imagename;
    public static Bitmap bm;

    String block = "0";

    @Override
    protected void onResume() {
        super.onResume();

        //  activate1 = false;


    }

    @Override
    protected void onPause() {
        super.onPause();

        //  activate1 = false;
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        a.dismiss();

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
            else if (resultCode == RESULT_OK && requestCode == 2) {
                try {

                    Uri selectedImage = data.getData();
                    String[] projection = {MediaStore.MediaColumns.DATA};

                    android.support.v4.content.CursorLoader cursorLoader = new android.support.v4.content.CursorLoader(getApplicationContext(), selectedImage, projection, null, null,
                            null);
                    Cursor cursor = cursorLoader.loadInBackground();
                    int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                    //Log.e("column_index", column_index + "");
                    cursor.moveToFirst();
                    String path = cursor.getString(column_index);
                    try {
                        copyVideoFile(new File(path), ff);
                    } catch (Exception e) {

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

    }

    private String getRealPathFromURI1(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Video.VideoColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }


    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);


        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(selectedImagePath, options);

        // btnCapturePicture.setImageBitmap(bm);
        //Log.e("IMAGENAME", data.getData().toString());
        try {
            //     copyFile(new File(getRealPathFromURI(data.getData())), ff);
            copyFile(new File(getRealPathFromURI(selectedImageUri)), ff);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private void onCaptureImageResult(Intent data) {
        //mCapturedImageURI



    /*    bm = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
       /* Uri selectedImage = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        CursorLoader cursorLoader = new CursorLoader(getApplicationContext(), selectedImage, projection, null, null,
                null);
        Cursor cursor = cursorLoader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        String path = cursor.getString(column_index);*/

        String capturedImageFilePath = "";
        try {
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = managedQuery(mCapturedImageURI, projection, null, null, null);
            int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            capturedImageFilePath = cursor.getString(column_index_data);
            //Log.e("capturedImageFilePath1", capturedImageFilePath);


            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(capturedImageFilePath, options);
            final int REQUIRED_SIZE = 200;
            int scale = 1;
            while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                    && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;
            options.inSampleSize = scale;
            options.inJustDecodeBounds = false;
            bm = BitmapFactory.decodeFile(capturedImageFilePath, options);


        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            copyFile(new File(capturedImageFilePath), ff);

        } catch (IOException e) {
            e.printStackTrace();
        }

        // btnCapturePicture.setImageBitmap(bm);

    }


    public void setImage() {
        final CharSequence[] items = {"Click Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(NewChatRoomActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Click Photo")) {
                  /*  Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                    hasPermissionInManifest(getApplicationContext(), MediaStore.ACTION_IMAGE_CAPTURE);*/


                    String fileName = "temp.jpg";
                    ContentValues values = new ContentValues();
                    values.put(MediaStore.Images.Media.TITLE, fileName);
                    mCapturedImageURI = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
                    startActivityForResult(intent, REQUEST_CAMERA);


                } else if (items[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    //  hasPermissionInManifest(getApplicationContext(),);

                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public boolean hasPermissionInManifest(Context context, String permissionName) {
        final String packageName = context.getPackageName();
        try {
            final PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
            final String[] declaredPermisisons = packageInfo.requestedPermissions;
            if (declaredPermisisons != null && declaredPermisisons.length > 0) {
                for (String p : declaredPermisisons) {
                    if (p.equals(permissionName)) {
                        return true;
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e) {

        }
        return false;
    }


    public void encode_image(final NewMessage message) {

        new AsyncTask() {
            JSONObject cats = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected Object doInBackground(Object[] params) {


                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                //   Picasso.with(getApplicationContext()).load(fileUri.getPath()).resize(100,stream);
                byte[] array = stream.toByteArray();
                encoded_string = Base64.encodeToString(array, 0);
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("image_name", imagename);
                param.put("encoded_string", encoded_string);
                cats = DAO.getJsonFromUrl(Classified.upImage, param);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    if (cats.getInt("success") > 0) {

                        try {
                            sendVideoToServer(message);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);



      /*  HashMap<String, String> map = new HashMap<>();
        map.put("image_name", imagename);
        map.put("encoded_string", encoded_string);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                //   Picasso.with(getApplicationContext()).load(fileUri.getPath()).resize(100,stream);
                byte[] array = stream.toByteArray();
                encoded_string = Base64.encodeToString(array, 0);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                makeRequestMethood(message);
            }
        }.execute(null, null, null);
*/

    }


    private class Encode_image extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            // bitmap = BitmapFactory.decodeFile(file_uri.getPath());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            //   Picasso.with(getApplicationContext()).load(fileUri.getPath()).resize(100,stream);
            byte[] array = stream.toByteArray();
            encoded_string = Base64.encodeToString(array, 0);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            makeRequest();
        }


        private void makeRequest() {
            // RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

//              pd.show();
            StringRequest request = new StringRequest(Request.Method.POST, Classified.upImage, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {

                        JSONObject obj = new JSONObject(response);
                        int result = obj.getInt("success");
                        if (result == 1) {

                            //   Toast.makeText(getApplicationContext(), "Image Uploaded", Toast.LENGTH_LONG).show();
                            //  pd.dismiss();


                        } else {


                            //   Toast.makeText(getApplicationContext(), "Image Not Uploaded", Toast.LENGTH_LONG).show();

                            // insertion();
                            //   pd.dismiss();


                        }

                    } catch (Exception e) {
                        // pd.dismiss();

                        e.printStackTrace();
                        //    Toast.makeText(getApplicationContext(), "Failed t0 upload Image", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {

                public void onErrorResponse(VolleyError error) {
                    //   pd.dismiss();

                    error.printStackTrace();
                    error.toString();
                    // Toast.makeText(getApplicationContext(), " No  Internet Connection!", Toast.LENGTH_LONG).show();
                }

            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {


                    HashMap<String, String> map = new HashMap<>();
                    map.put("image_name", imagename);
                    map.put("encoded_string", encoded_string);
                    return map;
                }


            };

            com.classifides.app.application.Classified.getInstance().addToReqQueue(request);
        }

    }


    private void makeRequestMethood(final NewMessage message) {
        // RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

//              pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, Classified.upImage, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONObject obj = new JSONObject(response);
                    int result = obj.getInt("success");
                    if (result == 1) {

                        try {
                            sendVideoToServer(message);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else {


                    }

                } catch (Exception e) {
                    // pd.dismiss();

                    e.printStackTrace();
                    //    Toast.makeText(getApplicationContext(), "Failed t0 upload Image", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            public void onErrorResponse(VolleyError error) {
                //   pd.dismiss();

                error.printStackTrace();
                error.toString();
                // Toast.makeText(getApplicationContext(), " No  Internet Connection!", Toast.LENGTH_LONG).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {


                HashMap<String, String> map = new HashMap<>();
                map.put("image_name", imagename);
                map.put("encoded_string", encoded_string);
                return map;
            }


        };

        com.classifides.app.application.Classified.getInstance().addToReqQueue(request);
    }

    private void copyFile(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }

        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        File ff = new File(Environment.getExternalStorageDirectory().getPath() + "/Classifieds/Images/sentImage/");
        if (!ff.getParentFile().exists()) {
            if (ff.getParentFile().mkdirs()) ;
        }

        String uimg_nm = email + "" + System.currentTimeMillis() + ".jpg";

        File f1 = new File(ff.getAbsoluteFile().getParentFile().getPath() + "/" + uimg_nm);
        f1.getParentFile().mkdirs();

        //Log.e("FILEPATH", f1.toString());
        // if(ff.getAbsolutePath()+"/")
        destination = new FileOutputStream(f1).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());


            String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            //Log.e("SSSS", timeStamp + "");
            NewMessage m;
            if (type == 1) {
                m = new NewMessage(0, id, chatRoomId, uimg_nm, timeStamp + "", timeStamp + "", type, chatRoomId, 1, 0);
            } else {
                m = new NewMessage(0, chatRoomId, chatRoomId, uimg_nm, timeStamp + "", timeStamp + "", type, chatRoomId, 1, 0);

            }
            mHelper.saveSingleMessage(m);
            refresh();
            //Log.e("MMM", m.toString());
            //   mAdapter.notifyDataSetChanged();
            //   mAdapter.notifyItemRangeChanged(0, mAdapter.getItemCount());

            if (mAdapter.getItemCount() > 1) {
                recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() + 1);
            }
            imagename = uimg_nm;
            mAdapter.notifyDataSetChanged();

            //   mAdapter.notifyItemRangeChanged(0, mAdapter.getItemCount());

            inputMessage.setText("");
            refresh();
            if (onlyImage.equals("2")) {
                sendImageToServer(m);
            } else {
                encode_image(m);
            }


        }
        //Log.e("SOURCE FILE", source.toString());
        //Log.e("DESTFILE", destFile.toString());
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }


    }

    private void copyVideoFile(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }

        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        File ff = new File(Environment.getExternalStorageDirectory().getPath() + "/Classifieds/Videos/sentImage/");
        if (!ff.getParentFile().exists()) {
            if (ff.getParentFile().mkdirs()) ;
        }

        String uimg_nm = "video_" + email + "" + System.currentTimeMillis() + ".mp4";

        File f1 = new File(ff.getAbsoluteFile().getParentFile().getPath() + "/" + uimg_nm);
        f1.getParentFile().mkdirs();

        //Log.e("FILEPATH", f1.toString());
        // if(ff.getAbsolutePath()+"/")
        destination = new FileOutputStream(f1).getChannel();

        long length = sourceFile.length();
        long ll = 1024 * 1024;
        length = length / ll;
        if (length > 10) {
            Toast.makeText(getApplicationContext(), "video size must be 10mb or less", Toast.LENGTH_LONG).show();
        } else {
            if (destination != null && source != null) {
                destination.transferFrom(source, 0, source.size());


                String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                //Log.e("SSSS", timeStamp + "");
                NewMessage m = new NewMessage(0, id, chatRoomId, uimg_nm, timeStamp + "", timeStamp + "", type, chatRoomId, 2, 0);

                mHelper.saveSingleMessage(m);
                refresh();
                //Log.e("MMM", m.toString());
                //     mAdapter.notifyDataSetChanged();
                //   mAdapter.notifyItemRangeChanged(0, mAdapter.getItemCount());

                if (mAdapter.getItemCount() > 1) {
                    recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() + 1);
                }
                imagename = uimg_nm;
                mAdapter.notifyDataSetChanged();
                //   mAdapter.notifyItemRangeChanged(0, mAdapter.getItemCount());

                inputMessage.setText("");
                refresh();
                //   sendVideoToServer(m, f1.toString());


                postNext(f1.toString(), uimg_nm, m);

            }
        }
        //Log.e("SOURCE FILE", source.toString());
        //Log.e("DESTFILE", destFile.toString());
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }


    }

    private String getRealPathFromURI(Uri contentUri) {

        String[] proj = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    Uri mMediaUrl;
    public static String email;
    String onlyImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       /* StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyLog()
                .penaltyDeath().build());*/


        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


        setContentView(R.layout.activity_new_chat_room);

        utils = new ImageLoadingUtils(com.classifides.app.application.Classified.getInstance());


        if (!ff.exists()) {
            ff.getParentFile().mkdirs();
            if (ff.getParentFile().mkdirs()) {
                System.out.println("Directory created");
            } else {
                System.out.println("Directory is not created");
            }
            // Log.e("DIRR",ff.toString());
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        send_attachment = (CircleImageView) findViewById(R.id.send_attachment);
        setSupportActionBar(toolbar);

        chat_room_name = (TextView) findViewById(R.id.chat_room_name);

        data = new HashMap<String, String>();
        mHelper = new NotiDbHelper(Classified.getInstance());
        inputMessage = (EditText) findViewById(R.id.message);
        btnSend = (Button) findViewById(R.id.btn_send);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        try {
            String i = getIntent().getStringExtra("str_txt");
            if (i.equals("null")) {
                inputMessage.setText("");
            } else {
                inputMessage.setText(i + "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        CircleImageView ug_icon;
        a = new Dialog(NewChatRoomActivity.this);
        // imagename = "gid_classified" + cdate + "0.jpg";
        //setImage();

        a.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = a.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // a.setCancelable(fa);

        a.setContentView(R.layout.attchment_dialog);


        TextView photos = (TextView) a.findViewById(R.id.photos);
        TextView video = (TextView) a.findViewById(R.id.videos);


        photos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setImage();
            }
        });

        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("video/*");
                startActivityForResult(photoPickerIntent, 2);
                a.dismiss();

            }
        });


        send_attachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Integer.parseInt(block) == 1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(NewChatRoomActivity.this);
                    builder.setTitle("Alert");
                    builder.setMessage("you blocked this person , Do you want to unblock this person?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            helper.unFriend(chatRoomId, 0);
                            //    mAdaptor.notifyDataSetChanged();
                            block = "0";


                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            block = "1";
                        }
                    });
                    builder.setCancelable(false);
                    builder.show();
                } else {
                    if (onlyImage.equals("1")) {
                        setImage();
                    } else {
                        a.show();
                    }
                }


                //  a.show();
            }
        });
        ug_icon = (CircleImageView) findViewById(R.id.ug_icon);

        ug_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == 1) {
                    User user = mHelper.getUserById(chatRoomId);
                    String sss = user.getStatus();
                    String pho = user.getMobile();
                    String im = user.getImage();
                    Intent ss = new Intent(NewChatRoomActivity.this, DetailsPageUser.class);
                    ss.putExtra("status", sss);
                    ss.putExtra("phone", pho);
                    ss.putExtra("imgurl", im);
                    startActivity(ss);
                } else {
                    User user = mHelper.getUserById(chatRoomId);
                    String gg = user.getId() + "";
                    String status11 = user.getName();
                    Intent ia = new Intent(getApplicationContext(), EditGroup.class);
                    ia.putExtra("gid", gg);
                    ia.putExtra("status", status11);
                    ia.putExtra("imgurl", user.getImage());
                    startActivity(ia);
                }
            }
        });

       /* toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = mHelper.getUserById(chatRoomId);
                String sss = user.getStatus();
                String pho = user.getMobile();
                String im = user.getImage();

                Intent ss = new Intent(NewChatRoomActivity.this, DetailsPageUser.class);
                ss.putExtra("status", sss);
                ss.putExtra("phone", pho);
                ss.putExtra("imgurl", im);
                startActivity(ss);
            }
        });
*/
        activate1 = true;
        mArraylistAdapter = new ArrayList<NewMessage>();


        Intent intent = getIntent();
        chatRoomId = Integer.parseInt(intent.getStringExtra("id"));
        String title = intent.getStringExtra("name");
        type = Integer.parseInt(intent.getStringExtra("type"));

        onlyImage = intent.getStringExtra("onlyImage");

        //Log.e("TYPE1", type + "");

        String u = getIntent().getStringExtra("url");

        //Log.e("URLLL", u + "");
        // getSupportActionBar().setTitle(title);

        Picasso.with(Classified.getInstance()).load(u).placeholder(R.drawable.user).into(ug_icon);
        chat_room_name.setText(title);
        if (type == 1) {
            try {
                String oldNotification = Classified.getInstance().getPrefManager().getNotificationsById(chatRoomId + "");
                List<String> messages = Arrays.asList(oldNotification.split("\\|"));

                Set<String> unique = new HashSet<String>(messages);

                Classified.getInstance().getPrefManager().removeNotificaion(chatRoomId + "");
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            try {
                String oldNotification = Classified.getInstance().getPrefManager().getNotificationsByGroup(chatRoomId + "");
                List<String> messages = Arrays.asList(oldNotification.split("\\|"));

                Set<String> unique = new HashSet<String>(messages);

                Classified.getInstance().getPrefManager().removeGroupNotificaion(chatRoomId + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            block = getIntent().getStringExtra("block");
        } catch (Exception e) {
            e.printStackTrace();
        }
        helper = new NotiDbHelper(Classified.getInstance());

        chat_room_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == 1) {
                    User user = mHelper.getUserById(chatRoomId);
                    String sss = user.getStatus();
                    String pho = user.getMobile();
                    String im = user.getImage();

                    Intent ss = new Intent(NewChatRoomActivity.this, DetailsPageUser.class);
                    ss.putExtra("status", sss);
                    ss.putExtra("phone", pho);
                    ss.putExtra("imgurl", im);
                    startActivity(ss);
                } else {

                    User user = mHelper.getUserById(chatRoomId);
                    String gg = user.getId() + "";
                    String status11 = user.getName();
                    Intent ia = new Intent(getApplicationContext(), EditGroup.class);
                    ia.putExtra("gid", gg);
                    ia.putExtra("status", status11);
                    ia.putExtra("imgurl", user.getImage() + "");
                    startActivity(ia);

                }
            }
        });


        sh = getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);

        String users = sh.getString("users", "");
        try {
            JSONObject j = new JSONObject(users);
            id = j.getInt("id");
            uname = j.getString("name");
            email = j.getString("email");

        } catch (JSONException e) {
            e.printStackTrace();
        }


        SharedPreferences sh = getApplicationContext().getSharedPreferences("chat_rm", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sh.edit();
        editor.putString("rid", chatRoomId + "");
        editor.putString("sid", id + "");
        editor.putString("type", type + "");
        editor.commit();

        if (type == 1) {
            mAdapter = new ChatRoomThreadAdapter(this, mHelper.getMessageByRid_Sid(id, chatRoomId), id + "", title);
        } else {
            mAdapter = new ChatRoomThreadAdapter(this, mHelper.getMessageByGid(chatRoomId), id + "", title);

        }
        // mAdapter = new ChatRoomThreadAdapter(this,)


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        //    mAdapter.notifyDataSetChanged();
        //  mAdapter.notifyItemRangeChanged(0, mAdapter.getItemCount());

        if (mAdapter.getItemCount() > 1) {
            recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() + 1);
        }

        broadcast = new MyNewBroadcast();
        IntentFilter filter = new IntentFilter(MyGcmPushReceiver.name);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        try {
            registerReceiver(broadcast, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        btnSend.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View view) {

                                           final String message = inputMessage.getText().toString().trim();

                                           if (TextUtils.isEmpty(message)) {
                                               Toast.makeText(getApplicationContext(), "Enter a message", Toast.LENGTH_SHORT).show();
                                               return;
                                           } else if (Integer.parseInt(block) == 1) {
                                               AlertDialog.Builder builder = new AlertDialog.Builder(NewChatRoomActivity.this);
                                               builder.setTitle("Alert");
                                               builder.setMessage("you blocked this person , Do you want to unblock this person?");
                                               builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                   @Override
                                                   public void onClick(DialogInterface dialog, int which) {
                                                       dialog.dismiss();

                                                       helper.unFriend(chatRoomId, 0);
                                                       //    mAdaptor.notifyDataSetChanged();
                                                       block = "0";


                                                   }
                                               });
                                               builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                   @Override
                                                   public void onClick(DialogInterface dialog, int which) {
                                                       dialog.dismiss();
                                                       block = "1";
                                                   }
                                               });
                                               builder.setCancelable(false);
                                               builder.show();
                                           } else {
                                               sendMessage(message);
                                               HomeChatActivity.sharedText = "";
                                           }


                                       }
                                   }

        );

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreferences sh = getApplicationContext().getSharedPreferences("chat_rm", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sh.edit();
        editor.putString("rid", 0 + "");
        editor.putString("sid", 0 + "");
        editor.putString("type", 0 + "");
        editor.commit();
    }

    private void sendMessage(String message) {
        // NewMessage m;
        NewMessage m;
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        //Log.e("SSSS", timeStamp + "");
        if (type == 1) {
            m = new NewMessage(0, id, chatRoomId, message, timeStamp + "", timeStamp + "", type, chatRoomId, 0, 0);
            //Log.e("LastMsg1", m.toString());
        } else {
            m = new NewMessage(0, chatRoomId, id, message, timeStamp + "", timeStamp + "", type, chatRoomId, 0, 0);
            //Log.e("LastMsgGroup", m.toString());
        }
        mHelper.saveSingleMessage(m);
        inputMessage.setText("");
        refresh();
        // mAdapter.notifyDataSetChanged();

        //     mAdapter.notifyItemRangeChanged(0, mAdapter.getItemCount());

        if (mAdapter.getItemCount() > 1) {
            recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() + 1);
        }

        mAdapter.notifyDataSetChanged();
        sendMsgToServer(m);


        // refresh();


    }


    /*@Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent yy = new Intent(NewChatRoomActivity.this, HomeChatActivity.class);
        yy.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(yy);
    }
*/
    private void sendMsgToServer(NewMessage m) {

        final String msg1 = m.getMsg();
        final String sid1 = m.getSid() + "";
        final String rid1 = m.getRid() + "";
        //Log.e("RID", rid1);
        final String rtime1 = m.getRtime();
        String uid1 = m.getUid() + "";
        final String type1 = m.getMtypr() + "";
        final String img = m.getCh_image() + "";

        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();


            /*    try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/

              /*  ByteArrayOutputStream stream = new ByteArrayOutputStream();
                PostBannerAddOne.bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);

                new Encode_image().execute();*/
            }

            @Override
            protected Object doInBackground(Object[] params) {

                if (type == 1) {
                    data.put("type", type1);//mtypr
                    data.put("message", msg1);
                    data.put("sid", sid1);
                    data.put("rid", rid1);
                    data.put("rtime", rtime1 + "");//rtime
                    // data.put("stime", rtime1 + "");
                    data.put("chatRoomId", chatRoomId + "");
                    data.put("image", img);
                    res = DAO.getJsonFromUrl(Classified.INSERT_MSG, data);

                } else {
                    data.put("mtypr", type1);//mtypr
                    data.put("name", uname);
                    data.put("sid", rid1);
                    data.put("rid", sid1);
                    data.put("rtime", rtime1 + "");//rtime
                    data.put("stime", rtime1 + "");
                    data.put("message", msg1);
                    data.put("chatRoomId", chatRoomId + "");
                    data.put("image", img);
                    res = DAO.getJsonFromUrl(Classified.SEND_TO_GROUP, data);

                }


                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                //   inputMessage.setText("");

                try {

                    //Log.e("RES1", res.toString());
                    if (res.getInt("success") > 0) {
                        //       Toast.makeText(getApplicationContext(), "message sent", Toast.LENGTH_SHORT).show();

                    } else {
                        //     Toast.makeText(getApplicationContext(), "message failed", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);


    }


    private void sendImageToServer(NewMessage m) {

        final String msg1 = m.getMsg();
        final String sid1 = m.getSid() + "";
        final String rid1 = m.getRid() + "";
        //Log.e("RID", rid1);
        final String rtime1 = m.getRtime();
        String uid1 = m.getUid() + "";
        final String type1 = m.getMtypr() + "";
        final String img = m.getCh_image() + "";

        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                new Encode_image().execute();

            }

            @Override
            protected Object doInBackground(Object[] params) {

                if (type == 1) {
                    data.put("type", type1);//mtypr
                    data.put("message", msg1);
                    data.put("sid", sid1);
                    data.put("rid", rid1);
                    data.put("rtime", rtime1 + "");//rtime
                    // data.put("stime", rtime1 + "");
                    data.put("chatRoomId", chatRoomId + "");
                    data.put("image", img);
                    res = DAO.getJsonFromUrl(Classified.INSERT_MSG, data);

                } else {
                    data.put("mtypr", type1);//mtypr
                    data.put("name", uname);
                    data.put("sid", sid1);
                    data.put("rid", rid1);
                    data.put("rtime", rtime1 + "");//rtime
                    data.put("stime", rtime1 + "");
                    data.put("message", msg1);
                    data.put("chatRoomId", chatRoomId + "");
                    data.put("image", img);
                    res = DAO.getJsonFromUrl(Classified.SEND_TO_GROUP, data);

                }

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                //   inputMessage.setText("");

                try {

                    //Log.e("RES1", res.toString());
                    if (res.getInt("success") > 0) {
                        //     Toast.makeText(getApplicationContext(), "message sent", Toast.LENGTH_SHORT).show();

                    } else {
                        //    Toast.makeText(getApplicationContext(), "message failed", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);


    }

    private void postNext(final String videoPath, final String fileName, final NewMessage message) {
        new AsyncTask<String, Integer, String>() {
            String rest = "";
            private String boundary;
            private static final String LINE_FEED = "\r\n";
            private HttpURLConnection httpConn;
            private String charset = "UTF-8";
            private OutputStream outputStream;
            private PrintWriter writer;


            @Override
            protected String doInBackground(String[] params) {

                try {
                    File uploadFile = new File(videoPath);

                    String requestURL = Classified.uploadVideo;
                    // String requestURL = " http://192.168.1.108:8086/up/upload.php";


                    // creates a unique boundary based on time stamp
                    boundary = "===" + System.currentTimeMillis() + "===";
                    URL url = new URL(requestURL);
                    httpConn = (HttpURLConnection) url.openConnection();
                    httpConn.setRequestProperty("Connection", "keep-alive");
                    httpConn.setRequestMethod("POST");
                    httpConn.setRequestProperty("Accept-Encoding", "identity");
                    httpConn.setRequestProperty("Content-Type",
                            "multipart/form-data; boundary=" + boundary);
                    httpConn.setRequestProperty("User-Agent", "Etkin Agent");
                    httpConn.setRequestProperty("Test", "Etkin");


                    //httpConn.setUseCaches(false);
                    httpConn.setDoOutput(true);    // indicates POST method
                    //
                    httpConn.setDoInput(true);

                    String data = "--" + boundary + LINE_FEED + "Content-Disposition: form-data; name=\"" + "file" + "\"; filename=\"" + fileName + "\"" + LINE_FEED + "Content-Type: " + URLConnection.guessContentTypeFromName(fileName) + LINE_FEED + "Content-Transfer-Encoding: binary" + LINE_FEED + LINE_FEED;
                    String data2 = LINE_FEED + LINE_FEED + "--" + boundary + "--" + LINE_FEED;
                    int len = data.getBytes().length + (int) uploadFile.length() + data2.getBytes().length;

                    httpConn.setFixedLengthStreamingMode(len);
                    outputStream = httpConn.getOutputStream();
                    writer = new PrintWriter(new OutputStreamWriter(outputStream, charset), true);


                    writer.append("--" + boundary).append(LINE_FEED);
                    writer.append("Content-Disposition: form-data; name=\"" + "file" + "\"; filename=\"" + fileName + "\"")
                            .append(LINE_FEED);
                    writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(fileName))
                            .append(LINE_FEED);
                    writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
                    writer.append(LINE_FEED);
                    writer.flush();

                    publishProgress(0, (int) uploadFile.length() / 1024);
                    FileInputStream inputStream = new FileInputStream(uploadFile);
                    byte[] buffer = new byte[4096];
                    int bytesRead = -1;
                    int s = 0;
                    // BufferedWriter writer=new BufferedWriter(new PrintWriter(outputStream));
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                        s = s + (int) bytesRead / 1024;
                        publishProgress(s);
                                /*if (s > 8000)
                                    break;*/
                    }
                    outputStream.flush();
                    inputStream.close();

                    writer.append(LINE_FEED);
                    writer.flush();


                    writer.append(LINE_FEED).flush();
                    writer.append("--" + boundary + "--").append(LINE_FEED);
                    writer.close();

                    List<String> response = new ArrayList<String>();
                    // checks server's status code first
                    int status = httpConn.getResponseCode();
                    if (status == HttpURLConnection.HTTP_OK) {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(
                                httpConn.getInputStream()));
                        String line = null;
                        while ((line = reader.readLine()) != null) {
                            response.add(line);
                        }
                        reader.close();
                        httpConn.disconnect();
                    } else {
                        throw new IOException("Server returned non-OK status: " + status);
                    }





                           /* MultipartUtility mp=new MultipartUtility(requestURL,charset);
                            mp.addFilePart("file",img,this);
                            List<String> res=mp.finish();*/

                    for (String r : response) {
                        rest += "\n" + r;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    sendVideoToServer(message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return rest;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    dialog1.dismiss();
                    ///Log.e("In Video Post", "In Video Post update");


                } catch (Exception e) {
                    e.printStackTrace();
                }
                //   Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                if (values.length == 2) {
                    dialog1 = new ProgressDialog(NewChatRoomActivity.this);
                    dialog1.setTitle("sending video....");
                    dialog1.setCancelable(false);



                 /*   dialog1.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    dialog1.setMessage("Please wait while we uploading your add!");
                    dialog1.setMax(values[1]);
                    dialog1.setButton(DialogInterface.BUTTON_POSITIVE, "cancel", new DialogInterface.OnClickListener() {
                        Override
                        public void onClick(DialogInterface dialog, int which) {


                        }
                    });*/
                    dialog1.show();


                } /*else
                    dialog1.setProgress(values[0]);*/

            }
        }.execute(null, null, null);
    }

    private void sendVideoToServer(NewMessage m) {
        JSONObject res = null;
        final String msg1 = m.getMsg();
        final String sid1 = m.getSid() + "";
        final String rid1 = m.getRid() + "";
        //Log.e("RID", rid1);
        final String rtime1 = m.getRtime();
        String uid1 = m.getUid() + "";
        final String type1 = m.getMtypr() + "";
        final String img = m.getCh_image() + "";


        if (type == 1) {
            data.put("type", type1);//mtypr
            data.put("message", msg1);
            data.put("sid", sid1);
            data.put("rid", rid1);
            data.put("rtime", rtime1 + "");//rtime
            // data.put("stime", rtime1 + "");
            data.put("chatRoomId", chatRoomId + "");
            data.put("image", img);
            res = DAO.getJsonFromUrl(Classified.INSERT_MSG, data);

        } else {
            data.put("mtypr", type1);//mtypr
            data.put("name", uname);
            //  data.put("sid", sid1);
            data.put("sid", id + "");
            data.put("rid", rid1);
            data.put("rtime", rtime1 + "");//rtime
            data.put("stime", rtime1 + "");
            data.put("message", msg1);
            data.put("chatRoomId", chatRoomId + "");
            data.put("image", img);
            res = DAO.getJsonFromUrl(Classified.SEND_TO_GROUP, data);

        }
        try {

            //Log.e("RES1", res.toString());
            if (res.getInt("success") > 0) {
                // Toast.makeText(getApplicationContext(), "message sent", Toast.LENGTH_SHORT).show();

            } else {
                //    Toast.makeText(getApplicationContext(), "message sent", Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void refresh() {
        if (type == 1) {
            mAdapter.setMessageArrayList(mHelper.getMessageByRid_Sid(id, chatRoomId));
        } else {
            mAdapter.setMessageArrayList(mHelper.getMessageByGid(chatRoomId));
        }


        //   mAdapter.notifyDataSetChanged();

        //    mAdapter.notifyItemRangeChanged(0, mAdapter.getItemCount());
        if (mAdapter.getItemCount() > 1) {
            recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() + 1);
        }
        mAdapter.notifyDataSetChanged();

        //     mAdapter.notifyItemRangeChanged(0, mAdapter.getItemCount());


    }


    public class MyNewBroadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String bd = intent.getExtras().getString("data");
            refresh();
            //Log.e("badge_data", bd);


            try {
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds

                v.vibrate(1000);
                r.play();
/*
                JSONObject j = new JSONObject(DAO.getNotification());
                JSONArray ar = new JSONArray();
                ar.put(bd.getString("msg"));
                ar.put(bd.getString("title"));
                ar.put(bd.getString("link"));
                ar.put(true);
                j.put(bd.getString("key"), ar);
                DAO.storeNotification(j.toString());*/


                //       Log.e("badge_data1", badge_data + "");


                // }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }


}
