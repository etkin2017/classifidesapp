package com.classifides.app.gcm.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.design.widget.TabLayout;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.classifides.app.BC;
import com.classifides.app.BrowseByCategory;
import com.classifides.app.R;
import com.classifides.app.UserAccount;
import com.classifides.app.application.Classified;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.data.NotiDbHelper;
import com.classifides.app.gcm.fragment.ChatFragment;
import com.classifides.app.gcm.fragment.ContactsFragment;

import com.classifides.app.gcm.fragment.GroupListFragment;
import com.classifides.app.gcm.fragment.MessageFragment;
import com.classifides.app.gcm.gcm.NotificationUtils;
import com.classifides.app.gcm.gcm.UpdateContactService;
import com.classifides.app.gcm.model.User;
import com.github.clans.fab.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class HomeChatActivity extends AppCompatActivity {

    private ArrayList<String> mobilelist;
    String allcont = "";
    SharedPreferences sh;
    private int id;
    RecyclerView contrecy;
    public static String sharedText = "";
    NotiDbHelper mHelper;


    public static int message = 0, recent_chat = 0, group = 0, friends = 0;


    private BroadcastReceiver mRegistrationBroadcastReceiver;
    HashMap<String, String> contactsname;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    FloatingActionButton floatingActionButton;
    public static int m_id;
    public static String mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_chat);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("messenger");
/*
        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            // only for gingerbread and newer versions
        } else {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Messanger");

        }*/

        contactsname = new HashMap<String, String>();
        mobilelist = new ArrayList<String>();
        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeChatActivity.this, CreateGroupActivity.class));
            }
        });

        mHelper = new NotiDbHelper(Classified.getInstance());
        if (mHelper.getEntryInStatus() == true) {

        } else {
            mHelper.addValuesToStatus();
        }


        sh = getSharedPreferences("user", Context.MODE_PRIVATE);

        String users = sh.getString("users", "");
        try {
            JSONObject j = new JSONObject(users);
            m_id = j.getInt("id");
            mobile = j.getString("mobile");

        } catch (JSONException e) {
            e.printStackTrace();
        }


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


        Intent intent1 = getIntent();
        String action1 = intent1.getAction();
        String type1 = intent1.getType();

        if (Intent.ACTION_SEND.equals(action1) && type1 != null) {
            if ("text/plain".equals(type1)) {
                handleSendText(intent1); // Handle text being sent
            } else if (type1.startsWith("image/")) {
                handleSendImage(intent1); // Handle single image being sent
            }
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action1) && type1 != null) {
            if (type1.startsWith("image/")) {
                handleSendMultipleImages(intent1); // Handle multiple images being sent
            }
        } else {
            // Handle other intents, such as being started from the home screen
        }

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push message is received
                    ChatFragment cff = new ChatFragment();
                    Bundle b = new Bundle();
                    b.putString("stt", "st");
                    //   b.putString("intent",intent+"");

                    cff.setArguments(b);
                    getSupportFragmentManager().beginTransaction().add(R.id.container, cff, "supp").addToBackStack("supp").commit();
                }
            }
        };


        try {
            ArrayList<User> check_store_contact = mHelper.getAllUser();
            if (check_store_contact.size() > 0) {

            } else {
                Intent i = new Intent(getApplicationContext(), UpdateContactService.class);
                startService(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            readPhoneContacts();
        } catch (Exception e) {
            e.printStackTrace();
        }


        // getUsers();


        try {
            SharedPreferences sharedPreferences = getSharedPreferences("PHONECONTACT", Context.MODE_PRIVATE);
            int m = sharedPreferences.getInt("phone_no", 0);
            //  Log.e("mobile No", m + "");
            if (m == mobilelist.size()) {

            } else {
                Intent i = new Intent(getApplicationContext(), UpdateContactService.class);
                startService(i);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent yy = new Intent(HomeChatActivity.this, BrowseByCategory.class);
        yy.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(yy);
    }

    void handleSendText(Intent intent) {
        sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
           /* ContactsFragment cff = new ContactsFragment();
            Bundle b = new Bundle();
            b.putString("intent_str_text", sharedText);
            cff.setArguments(b);
            getSupportFragmentManager().beginTransaction().add(R.id.container, cff, "supp1").addToBackStack("supp1").commit();*/
        }
    }


    public void handleSendImage(Intent intent) {
        Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            // Update UI to reflect image being shared
        }
    }

    public void handleSendMultipleImages(Intent intent) {
        ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        if (imageUris != null) {
            // Update UI to reflect multiple images being shared
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.settings) {
            startActivity(new Intent(HomeChatActivity.this, EditStatus.class));
        }
        if (id == R.id.profile) {
            startActivity(new Intent(HomeChatActivity.this, UserAccount.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        menu.getItem(0).setVisible(true);
        menu.getItem(1).setVisible(true);
        menu.getItem(2).setVisible(false);
        return super.onPrepareOptionsMenu(menu);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {


                case 0:

                    //    getSupportFragmentManager().beginTransaction().add(R.id.container, cff, "supp1").addToBackStack("supp1").commit();
                    return new ChatFragment();

                case 2:

                    //   getSupportFragmentManager().beginTransaction().add(R.id.container, cff1, "supp2").addToBackStack("supp2").commit();
                    return new ContactsFragment();

                case 1:


                    //   getSupportFragmentManager().beginTransaction().add(R.id.container, cff2, "supp3").addToBackStack("supp3").commit();
                    return new GroupListFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {

                case 0:
                    return "Recent Chat";
                case 1:
                    return "Group";
                case 2:
                    return "Friends";

            }
            return null;
        }
    }


    public void getUsers() {


        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                readPhoneContacts();

                for (String mob : mobilelist) {
                    allcont = allcont + mob + ",";
                }
                allcont = allcont.substring(0, allcont.length() - 1);
                return null;
            }


            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                //getUsersFromServer(allcont);
                getCategories();

            }


        }.execute(null, null, null);
    }

    public void getCategories() {
        new AsyncTask() {
            JSONObject cats = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected Object doInBackground(Object[] params) {
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("contacts", allcont);
                param.put("id", m_id + "");
                cats = DAO.getJsonFromUrl(Classified.APP_API_URL, param);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    if (cats.getInt("success") > 0) {

                        //  Log.e("CATS", cats.toString());

                        ArrayList<User> users = User.fromJSON(cats.getJSONArray("users"));
                        for (User user : users) {
                            if (user.getType() == 1) {
                                user.setName(contactsname.get(user.getMobile()));
                            }

                        }
                        User.save(Classified.getInstance(), users);


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

    public void readPhoneContacts() //This Context parameter is nothing but your Activity class's Context
    {
        Context cntx = Classified.getInstance();
        Cursor cursor = cntx.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        Integer contactsCount = cursor.getCount(); // get how many contacts you have in your contacts list
        if (contactsCount > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    //the below cursor will give you details for multiple contacts
                    Cursor pCursor = cntx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    // continue till this cursor reaches to all phone numbers which are associated with a contact in the contact list
                    while (pCursor.moveToNext()) {
                        int phoneType = pCursor.getInt(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                        //String isStarred 		= pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.STARRED));
                        String phoneNo = pCursor.getString(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //you will get all phone numbers according to it's type as below switch case.
                        //Logs.e will print the phone number along with the name in DDMS. you can use these details where ever you want.
                        String m = ((((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).replace("*", "").replace("#", "")).trim().toString();
                        m = m.replaceAll("\\D+", "");
                        String numberOnly = phoneNo.replaceAll("[^0-9]", "");

                        if (m.length() > 10) {
                            m = m.substring(Math.max(m.length() - 10, 0));
                        }
                        if (m.equals(mobile)) {

                        } else {
                            contactsname.put(m, contactName);
                            mobilelist.add(m);
                        }

                        /*switch (phoneType) {
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                             //   Log.e(contactName + ": TYPE_MOBILE", " " + (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString());
                                String m = ((((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).replace("*", "").replace("#", "")).trim().toString();


                                m = m.replaceAll("\\D+", "");

                                String numberOnly = phoneNo.replaceAll("[^0-9]", "");

                                if (m.length() > 10) {
                                    m = m.substring(Math.max(m.length() - 10, 0));
                                }
                                if (m.equals(mobile)) {

                                } else {
                                    contactsname.put(m, contactName);
                                    mobilelist.add(m);
                                }
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                             //   Log.e(contactName + ": TYPE_HOME", " " + phoneNo);
                                String m1 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m1 = m1.replaceAll("\\D+", "");

                                if (m1.length() > 10) {
                                    m1 = m1.substring(Math.max(m1.length() - 10, 0));
                                }
                                if (m1.equals(mobile)) {

                                } else {
                                    contactsname.put(m1, contactName);
                                    mobilelist.add(m1);
                                }
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                              //  Log.e(contactName + ": TYPE_WORK", " " + phoneNo);
                                String m2 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m2 = m2.replaceAll("\\D+", "");


                                if (m2.length() > 10) {
                                    m2 = m2.substring(Math.max(m2.length() - 10, 0));
                                }
                                if (m2.equals(mobile)) {

                                } else {
                                    contactsname.put(m2, contactName);
                                    mobilelist.add(m2);
                                }
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE:
                              //  Log.e(contactName + ": TYPE_WORK_MOBILE", " " + phoneNo);
                                String m3 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m3 = m3.replaceAll("\\D+", "");

                                if (m3.length() > 10) {
                                    m3 = m3.substring(Math.max(m3.length() - 10, 0));
                                }
                                if (m3.equals(mobile)) {

                                } else {
                                    contactsname.put(m3, contactName);
                                    mobilelist.add(m3);
                                }
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER:
                              //  Log.e(contactName + ": TYPE_OTHER", " " + phoneNo);
                                String m4 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m4 = m4.replaceAll("\\D+", "");

                                if (m4.length() > 10) {
                                    m4 = m4.substring(Math.max(m4.length() - 10, 0));
                                }
                                if (m4.equals(mobile)) {

                                } else {
                                    contactsname.put(m4, contactName);
                                    mobilelist.add(m4);
                                }
                                break;
                            default:
                                break;
                        }*/
                    }
                    pCursor.close();
                }

            }
            cursor.close();
        /*    try {
                SharedPreferences sharedPreferences = getSharedPreferences("PHONECONTACT", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                int no_mobile = mobilelist.size();
                editor.putInt("phone_no", no_mobile);
                editor.commit();

            } catch (Exception e) {
                e.printStackTrace();
            }*/
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

    }
}
