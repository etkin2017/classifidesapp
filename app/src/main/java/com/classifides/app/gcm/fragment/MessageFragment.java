package com.classifides.app.gcm.fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.classifides.app.data.NotiDbHelper;
import com.classifides.app.gcm.activity.HomeChatActivity;
import com.classifides.app.gcm.activity.NewChatRoomActivity;
import com.classifides.app.gcm.adapter.ChatRoomsAdapter;
import com.classifides.app.gcm.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessageFragment extends Fragment {


    public MessageFragment() {
        // Required empty public constructor
    }

    RecyclerView contrecy;
    ChatRoomsAdapter mAdaptor;
    NotiDbHelper mHelper;
    SharedPreferences sh;

    int id;
    ArrayList<User> ur, ur2;
    ArrayList<User> both;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_message, container, false);

        mHelper = new NotiDbHelper(Classified.getInstance());
        HomeChatActivity.message = 1;


        contrecy = (RecyclerView) v.findViewById(R.id.contrecyc);
        ur = mHelper.getUserByChat();


        if (ur == null) {
            ur = new ArrayList<User>();
        }

        mAdaptor = new ChatRoomsAdapter(Classified.getInstance(), ur);
        //   mAdaptor.setChatRoomArrayList(ur);
        contrecy.setLayoutManager(new LinearLayoutManager(getActivity()));
        contrecy.setAdapter(mAdaptor);


        sh = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);

        String users = sh.getString("users", "");
        try {
            JSONObject j = new JSONObject(users);
            id = j.getInt("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        contrecy.addOnItemTouchListener(new ChatRoomsAdapter.RecyclerTouchListener(getActivity(), contrecy, new ChatRoomsAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                // when chat is clicked, launch full chat thread activity
                ArrayList<User> uu = mAdaptor.getChatRoomArrayList();
                User u = uu.get(position);
                //   if (chatRoom.getSingle().equals("1")) {
                Intent intent = new Intent(getActivity(), NewChatRoomActivity.class);
                intent.putExtra("type", u.getType());
                intent.putExtra("id", u.getId());
                intent.putExtra("name", u.getName());
                startActivity(intent);
                /*}
                else{

                }*/
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
        refresh();
        HomeChatActivity.message=1;
    }

    @Override
    public void onPause() {
        super.onPause();
        HomeChatActivity.message=0;
    }

    public void refresh() {


        ArrayList<User> ur = mHelper.getUserByChat();

        mAdaptor.setChatRoomArrayList(ur);
        mAdaptor.notifyDataSetChanged();
    }

}
