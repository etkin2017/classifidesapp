package com.classifides.app.gcm.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.classifides.app.R;
import com.classifides.app.UserAccount;
import com.classifides.app.application.Classified;
import com.classifides.app.data.Config;
import com.classifides.app.data.NotiDbHelper;
import com.classifides.app.gcm.activity.EditStatus;
import com.classifides.app.gcm.activity.HomeChatActivity;
import com.classifides.app.gcm.activity.NewChatRoomActivity;
import com.classifides.app.gcm.adapter.ChatRoomsAdapter;
import com.classifides.app.gcm.adapter.RecentChatRoomAdapter;
import com.classifides.app.gcm.gcm.MyGcmPushReceiver;
import com.classifides.app.gcm.gcm.NotificationUtils;
import com.classifides.app.gcm.model.NewMessage;
import com.classifides.app.gcm.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends Fragment {


    public ChatFragment() {
        // Required empty public constructor
    }

    RecyclerView contrecy;
    RecentChatRoomAdapter mAdaptor;
    NotiDbHelper mHelper;
    SharedPreferences sh;

    int xid;
    ArrayList<User> ur, ur2;
    ArrayList<User> both;
    Intent intent;
    public static String menuname = "";

    ArrayList<String> delete_chat;
    ArrayList<String> chat_room_id;

    SwipeRefreshLayout mySwipeRefreshLayout;

    MyNewBroadcast broadcast;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_chat, container, false);
        mHelper = new NotiDbHelper(Classified.getInstance());

        setHasOptionsMenu(true);

        HomeChatActivity.recent_chat = 1;
        delete_chat = new ArrayList<String>();

        chat_room_id = new ArrayList<String>();
        contrecy = (RecyclerView) v.findViewById(R.id.contrecyc);
        ur = mHelper.getUserByChat();
        mAdaptor = new RecentChatRoomAdapter(Classified.getInstance(), ur);
        //  ur2 = mHelper.getAllGroup();


        broadcast = new MyNewBroadcast();
        IntentFilter filter = new IntentFilter(MyGcmPushReceiver.name2);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        try{
        getActivity().registerReceiver(broadcast, filter);
        }catch (Exception e){
            e.printStackTrace();
        }

        mySwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.mySwipeRefreshLayout);

        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        //Log.i("SWIPEREFRESH", "onRefresh called from SwipeRefreshLayout");

                        // This method performs the actual data-refresh operation.
                        // The method calls setRefreshing(false) when it's finished.
                        if (ur == null) {
                            ur = new ArrayList<User>();
                        } else {

                            mAdaptor = new RecentChatRoomAdapter(Classified.getInstance(), ur);
                            mAdaptor.setChatRoomArrayList(ur);
                        }

                    }
                }
        );

        if (ur == null) {
            ur = new ArrayList<User>();
        } else {

            mAdaptor = new RecentChatRoomAdapter(Classified.getInstance(), ur);
            mAdaptor.setChatRoomArrayList(ur);
        }


        try {
            mAdaptor.msgcnt(1);
            mAdaptor.setChatRoomArrayList(ur);
        } catch (Exception e) {
            e.printStackTrace();
        }
        contrecy.setLayoutManager(new LinearLayoutManager(getActivity()));
        contrecy.setAdapter(mAdaptor);


        sh = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);

        String users = sh.getString("users", "");
        try {
            JSONObject j = new JSONObject(users);
            xid = j.getInt("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        contrecy.addOnItemTouchListener(new ChatRoomsAdapter.RecyclerTouchListener(getActivity(), contrecy, new ChatRoomsAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                TextView long_press_icon = (TextView) view.findViewById(R.id.long_press_icon);
                long_press_icon.setVisibility(View.GONE);

                String chatroomid = ((TextView) view.findViewById(R.id.chatroomid)).getText().toString();
                String id = ((TextView) view.findViewById(R.id.id)).getText().toString();
                String url = ((TextView) view.findViewById(R.id.url)).getText().toString();
                String type = ((TextView) view.findViewById(R.id.type)).getText().toString();
                String block = ((TextView) view.findViewById(R.id.block)).getText().toString();
                String name = ((TextView) view.findViewById(R.id.name)).getText().toString();
                // chatroomid, id, url, type, block

                String set_checked = ((TextView) view.findViewById(R.id.set_checked)).getText().toString();




                //contrecy.getAdapter().getItemId(position)



                if ( mAdaptor.getChatRoomArrayList().get(position).isTicked() == true) {

                  /*  g_id = "";
                    if (deleteArrayId.contains(id)) {
                        deleteArrayId.remove(id);
                    }


*/


                    long_press_icon.setVisibility(View.GONE);

                    mAdaptor.getChatRoomArrayList().get(position).setTicked(false);

                    if (delete_chat.contains(id)) {
                        delete_chat.remove(id);
                    }
                    if (chat_room_id.contains(chatroomid)) {
                        chat_room_id.remove(chatroomid);
                    }

                } else {
                    Intent intent = new Intent(getActivity(), NewChatRoomActivity.class);
                    intent.putExtra("type", type);
                    intent.putExtra("id", id);
                    intent.putExtra("name", name);
                    intent.putExtra("url", url);
                    intent.putExtra("str_txt", HomeChatActivity.sharedText);
                    intent.putExtra("block", block);
                    intent.putExtra("onlyImage", "2");
                    startActivity(intent);

                }


                menuname = "";

            }

            @Override
            public void onLongClick(View view, int position) {

                TextView long_press_icon = (TextView) view.findViewById(R.id.long_press_icon);

                String chatroomid = ((TextView) view.findViewById(R.id.chatroomid)).getText().toString();
                String id = ((TextView) view.findViewById(R.id.id)).getText().toString();
                String url = ((TextView) view.findViewById(R.id.url)).getText().toString();
                String type = ((TextView) view.findViewById(R.id.type)).getText().toString();
                String block = ((TextView) view.findViewById(R.id.block)).getText().toString();
                String name = ((TextView) view.findViewById(R.id.name)).getText().toString();
                String aid = ((TextView) view.findViewById(R.id.aid)).getText().toString();

                ((TextView) view.findViewById(R.id.set_checked)).setText("1");

                mAdaptor.getChatRoomArrayList().get(position).setTicked(true);
                mAdaptor.notifyDataSetChanged();



                mAdaptor.notifyDataSetChanged();


                delete_chat.add(id);
                chat_room_id.add(chatroomid + "");
                menuname = "nm";

            }
        }));


        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.recent_chat, menu);

    }

    public class MyNewBroadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            //  String bd = intent.getExtras().getString("data");
            refresh();
            ur = mHelper.getUserByChat();
            if (ur == null) {
                ur = new ArrayList<User>();
            } else {

                mAdaptor = new RecentChatRoomAdapter(Classified.getInstance(), ur);
                mAdaptor.setChatRoomArrayList(ur);
            }


            try {
                mAdaptor.msgcnt(1);
                mAdaptor.setChatRoomArrayList(ur);
            } catch (Exception e) {
                e.printStackTrace();
            }
            contrecy.setLayoutManager(new LinearLayoutManager(getActivity()));
            contrecy.setAdapter(mAdaptor);


        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        final int id = item.getItemId();
        if (id == R.id.delete_chat) {

            if (delete_chat.size() == 0) {
                Toast.makeText(getActivity(), "Please select group", Toast.LENGTH_LONG).show();
            } else if (delete_chat.size() > 1) {
                Toast.makeText(getActivity(), "Please one select group", Toast.LENGTH_LONG).show();

            } else {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Alert");
                builder.setMessage("Are you sure you want to delete this chat?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        menuname = "";
                        mHelper.deleteUsersChatById(xid, Integer.parseInt(delete_chat.get(0)));
                        delete_chat.clear();
                        refresh();
                        mAdaptor.notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });
                builder.setCancelable(false);
                builder.show();

            }
        }
    /*    if (id == R.id.settings) {
            startActivity(new Intent(getActivity(), EditStatus.class));
        }
        if (id == R.id.profile) {
            startActivity(new Intent(getActivity(), UserAccount.class));
        }
*/

        return super.onOptionsItemSelected(item);


    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        if (menuname.equals("")) {
            menu.getItem(0).setVisible(true);
            menu.getItem(1).setVisible(true);
            menu.getItem(2).setVisible(false);
        } else {
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(false);
            menu.getItem(2).setVisible(true);

        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
        HomeChatActivity.recent_chat = 1;
        NotificationUtils.clearNotifications();
        Classified.getInstance().getPrefManager().removeNotimessage();
    }

    @Override
    public void onPause() {
        super.onPause();
        HomeChatActivity.recent_chat = 0;

    }

    public void refresh() {


        ArrayList<User> ur = mHelper.getUserByChat();
        mAdaptor = new RecentChatRoomAdapter(Classified.getInstance(), ur);

        mAdaptor.setChatRoomArrayList(ur);
        contrecy.setAdapter(mAdaptor);
        mAdaptor.notifyDataSetChanged();
    }
}
