package com.classifides.app.gcm.model;

/**
 * Created by Ashvini on 9/8/2016.
 */
public class NewMessage {
    private int id;
    private int sid;
    private int rid;
    private String msg;
    private String stime;
    private String rtime;
    private int mtypr;
    private int uid;
    private int ch_image;
    private int rs;

    public int getCh_image() {
        return ch_image;
    }

    public void setCh_image(int ch_image) {
        this.ch_image = ch_image;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    @Override
    public String toString() {
        return "NewMessage{" +
                "id=" + id +
                ", sid=" + sid +
                ", rid=" + rid +
                ", msg='" + msg + '\'' +
                ", stime='" + stime + '\'' +
                ", rtime='" + rtime + '\'' +
                ", mtypr=" + mtypr +
                ", uid=" + uid +
                ", ch_image =" +ch_image+
                ", rs =" +rs+
                '}';
    }

    public int getRs() {
        return rs;
    }

    public void setRs(int rs) {
        this.rs = rs;
    }

    public NewMessage(int id, int sid, int rid, String msg, String stime, String rtime, int mtypr, int uid, int ch_image, int rs) {
        this.id = id;
        this.sid = sid;
        this.rid = rid;
        this.msg = msg;
        this.stime = stime;
        this.rtime = rtime;
        this.mtypr = mtypr;
        this.uid = uid;
        this.ch_image = ch_image;
        this.rs=rs;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStime() {
        return stime;
    }

    public void setStime(String stime) {
        this.stime = stime;
    }

    public String getRtime() {
        return rtime;
    }

    public void setRtime(String rtime) {
        this.rtime = rtime;
    }

    public int getMtypr() {
        return mtypr;
    }

    public void setMtypr(int mtypr) {
        this.mtypr = mtypr;
    }
}
