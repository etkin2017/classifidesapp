package com.classifides.app.gcm.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.classifides.app.data.DAO;
import com.classifides.app.gcm.adapter.ChatRoomsAdapter;
import com.classifides.app.gcm.model.User;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class GroupActivity extends AppCompatActivity {
    private ArrayList<String> mobilelist;
    String allcont = "";
    SharedPreferences sh;
    private int id;
    RecyclerView contrecy;

    HashMap<String, String> contactsname;
    ChatRoomsAdapter mAdaptor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);
        getUsers();

    }
    private void getCategories() {
        new AsyncTask() {
            JSONObject cats = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected Object doInBackground(Object[] params) {
                HashMap<String, String> param = new HashMap<String, String>();
                param.put("contacts", allcont);
                param.put("id", id + "");
                cats = DAO.getJsonFromUrl(Classified.APP_API_URL, param);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    if (cats.getInt("success") > 0) {

                        ArrayList<User> users = User.fromJSON(cats.getJSONArray("users"));
                        for (User user : users) {
                            if (user.getType() == 1)
                                user.setName(contactsname.get(user.getMobile()));
                        }

                        User.save(Classified.getInstance(), users);

                        mAdaptor.setChatRoomArrayList(User.load(Classified.getInstance()));
                        mAdaptor.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }
    public void readPhoneContacts() //This Context parameter is nothing but your Activity class's Context
    {
        Context cntx = Classified.getInstance();
        Cursor cursor = cntx.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        Integer contactsCount = cursor.getCount(); // get how many contacts you have in your contacts list
        if (contactsCount > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    //the below cursor will give you details for multiple contacts
                    Cursor pCursor = cntx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    // continue till this cursor reaches to all phone numbers which are associated with a contact in the contact list
                    while (pCursor.moveToNext()) {
                        int phoneType = pCursor.getInt(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                        //String isStarred 		= pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.STARRED));
                        String phoneNo = pCursor.getString(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //you will get all phone numbers according to it's type as below switch case.
                        //Logs.e will print the phone number along with the name in DDMS. you can use these details where ever you want.
                        switch (phoneType) {
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                //Log.e(contactName + ": TYPE_MOBILE", " " + (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString());
                                String m = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();
                                if (m.length() > 10) {
                                    m = m.substring(Math.max(m.length() - 10, 0));
                                }
                                contactsname.put(m, contactName);
                                mobilelist.add(m);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                //Log.e(contactName + ": TYPE_HOME", " " + phoneNo);
                                String m1 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();
                                if (m1.length() > 10) {
                                    m1 = m1.substring(Math.max(m1.length() - 10, 0));
                                }
                                contactsname.put(m1, contactName);
                                mobilelist.add(m1);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                                //Log.e(contactName + ": TYPE_WORK", " " + phoneNo);
                                String m2 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();
                                if (m2.length() > 10) {
                                    m2 = m2.substring(Math.max(m2.length() - 10, 0));
                                }
                                contactsname.put(m2, contactName);
                                mobilelist.add(m2);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE:
                                //Log.e(contactName + ": TYPE_WORK_MOBILE", " " + phoneNo);
                                String m3 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                if (m3.length() > 10) {
                                    m3 = m3.substring(Math.max(m3.length() - 10, 0));
                                }
                                mobilelist.add(m3);
                                contactsname.put(m3, contactName);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER:
                                //Log.e(contactName + ": TYPE_OTHER", " " + phoneNo);
                                String m4 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();
                                if (m4.length() > 10) {
                                    m4 = m4.substring(Math.max(m4.length() - 10, 0));
                                }
                                mobilelist.add(m4);
                                contactsname.put(m4, contactName);
                                break;
                            default:
                                break;
                        }
                    }
                    pCursor.close();
                }
            }
            cursor.close();
        }
    }

    private void getUsers() {


        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                readPhoneContacts();

                for (String mob : mobilelist) {
                    allcont = allcont + mob + ",";
                }
                allcont = allcont.substring(0, allcont.length() - 1);
                return null;
            }


            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                //getUsersFromServer(allcont);
                getCategories();

            }


        }.execute(null, null, null);
    }
}
