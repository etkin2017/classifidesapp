package com.classifides.app.gcm.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.classifides.app.data.NotiDbHelper;
import com.classifides.app.gcm.adapter.ChatRoomsAdapter;
import com.classifides.app.gcm.model.User;

import java.util.ArrayList;

public class FindFriends extends AppCompatActivity {

    EditText u_no;
    Button search_no;

    RecyclerView contact_list;
    NotiDbHelper mh;

    ChatRoomsAdapter madapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.find_friends);


        mh = new NotiDbHelper(Classified.getInstance());
        ArrayList<User> users = mh.getAllUser();
        u_no = (EditText) findViewById(R.id.u_no);
        search_no = (Button) findViewById(R.id.search_no);
        contact_list = (RecyclerView) findViewById(R.id.contact_list);

        search_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (TextUtils.isEmpty(u_no.getText().toString())) {
                    u_no.setError("enter number");
                } else {
                    ArrayList<User> u = mh.findfriend(u_no.getText().toString());
                    ArrayList<User> u11 = new ArrayList<User>();
                    if (u != null) {
                        //u11.add(u);
                        madapter.setChatRoomArrayList(u);
                        contact_list.setAdapter(madapter);
                    }
                }

            }
        });


        if (users == null) {
            users = new ArrayList<User>();
        }
        madapter = new ChatRoomsAdapter(Classified.getInstance(), users);

        contact_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        contact_list.setAdapter(madapter);


        contact_list.addOnItemTouchListener(new ChatRoomsAdapter.RecyclerTouchListener(getApplicationContext(), contact_list, new ChatRoomsAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                // when chat is clicked, launch full chat thread activity


                TextView long_press_icon = (TextView) view.findViewById(R.id.long_press_icon);
                long_press_icon.setVisibility(View.GONE);

                String chatroomid = ((TextView) view.findViewById(R.id.chatroomid)).getText().toString();
                String id = ((TextView) view.findViewById(R.id.id)).getText().toString();
                String url = ((TextView) view.findViewById(R.id.url)).getText().toString();
                String type = ((TextView) view.findViewById(R.id.type)).getText().toString();
                String block = ((TextView) view.findViewById(R.id.block)).getText().toString();
                String name = ((TextView) view.findViewById(R.id.name)).getText().toString();
                // chatroomid, id, url, type, block

                long_press_icon.setVisibility(View.GONE);

              /*  ArrayList<User> uu = mAdaptor.getChatRoomArrayList();
                User u = uu.get(position);*/
                //   if (chatRoom.getSingle().equals("1")) {

/*
                intent.putExtra("type", type);
                intent.putExtra("id", id);
                intent.putExtra("name", name);
                intent.putExtra("url", url);
                intent.putExtra("str_txt", HomeChatActivity.sharedText);
                intent.putExtra("block", block);
                intent.putExtra("onlyImage", "2");*/
                Intent intent = new Intent(getApplicationContext(), NewChatRoomActivity.class);
                intent.putExtra("type", type);
                intent.putExtra("block", block);
                intent.putExtra("id", id);
                intent.putExtra("name", name);
                intent.putExtra("url", url);
                intent.putExtra("str_txt", HomeChatActivity.sharedText);
                intent.putExtra("onlyImage", "2");
                startActivity(intent);


            }

            @Override
            public void onLongClick(View view, int position) {

               /* TextView long_press_icon = (TextView) v+iew.findViewById(R.id.long_press_icon);
                long_press_icon.setVisibility(View.VISIBLE);
                ArrayList<User> uu = mAdaptor.getChatRoomArrayList();
                User u = uu.get(position);
                user_id = u.getId();
                uid.add(u.getId() + "");
                friend_find = "friend_find";
                block_id = u.getId() + "";*/


            }
        }));


        try {
            if (users.size() <= 0) {

            } else {
                madapter.setChatRoomArrayList(users);
                contact_list.setAdapter(madapter);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
