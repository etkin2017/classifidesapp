package com.classifides.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.view.MTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPassword extends AppCompatActivity {


    EditText etemail;
    LinearLayout get_pswd;

    ProgressDialog dialog;

    Map<String, String> data;

    MTextView about;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        data = new HashMap<String, String>();

        about = (MTextView) findViewById(R.id.about);


        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ForgotPassword.this, LoginActivity.class));
            }
        });

        dialog = new ProgressDialog(ForgotPassword.this);
        etemail = (EditText) findViewById(R.id.etemail);

        get_pswd = (LinearLayout) findViewById(R.id.get_pswd);

        get_pswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etemail.getText().toString())) {
                    etemail.setError("enter your email id");
                } else {
                    getPassword(etemail.getText().toString().trim());
                }
            }
        });


    }

    private void getPassword(final String eid) {
        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ProgressDialog();


                data.put("eid", eid);


             /*   try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
            }

            @Override
            protected Object doInBackground(Object[] params) {

                res = DAO.getJsonFromUrl(Config.getPassword, data);

                  /*  if (res.getInt("success") > 0) {
                        Toast.makeText(UpdateAsValidity.this, "Plan Add Suceessfully", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(UpdateAsValidity.this, "Error ", Toast.LENGTH_LONG).show();

                    }
*/

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                DismissProgress();
                try {
                    if (res.getInt("success") > 0) {

                        String emailid = res.getJSONArray("password").getJSONObject(0).getString("email");
                        String password = res.getJSONArray("password").getJSONObject(0).getString("p");
                        // Toast.makeText(getApplicationContext(),"Plan submitted", Toast.LENGTH_SHORT).show();

                        SendMail sm = new SendMail(ForgotPassword.this, emailid, "Catch Me On App ","<!DOCTYPE html>\n" +
                                "<html>\n" +
                                "<body bgcolor=\"#FF9800\"> <b> <h1> Forget Password </h1></b> </br> We have  a request to resend the password for this email address .<br> <h3>Your Catch Me On Account's Password is  "+ password+".</h3>  <h4>If you have any problems, questions , opinions, praise, comments , suggestions, Please feel to free not to contact us at any time. </h4> <br> <br>  Warm Regards, <br> "+Config.EMAIL +"</body> </html>");

                        //Executing sendmail to send email
                        sm.execute();

                        finish();

                    } else {
                        Toast.makeText(getApplicationContext(), "This is not email address", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }


    public void ProgressDialog() {
        dialog.setMessage("While we getting password..");
        dialog.setTitle("Please wait....");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void DismissProgress() {
        dialog.dismiss();
    }
}
