package com.classifides.app;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.classifides.app.application.Classified;
import com.classifides.app.data.Blur;
import com.classifides.app.data.ComplexPreferences;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.data.NotiDbHelper;
import com.classifides.app.fragment.Ads_Notification;
import com.classifides.app.model.BadgeView;
import com.classifides.app.model.locations;
import com.classifides.app.services.LocationTressService;
import com.classifides.app.view.MTextView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    CircleImageView img;
    String img_nm;

    CircleImageView noti, home;
    TextView logout;
    String img1;
    SharedPreferences sh;
    public static int id;

    public static String eID, Name;
    RelativeLayout ad_container_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ad_container_layout = (RelativeLayout) findViewById(R.id.ad_container_layout);

        MobileAds.initialize(getApplicationContext(), "ca-app-pub-2758807869387921~3087322491");

        final AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                //   .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                //.addTestDevice("CA7416822EE39EB5991049AECDE61839")
                //.addTestDevice(device_id)
                .build();
        mAdView.loadAd(adRequest);

        final Runnable adLoader = new Runnable() {
            @Override
            public void run() {
                mAdView.loadAd(new AdRequest.Builder().build());
            }
        };





        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                Log.e("onAdClosed", "onAdClosed");
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Log.e("onAdFailedToLoad", "onAdFailedToLoad");

                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        try{
                            HomeActivity.this.runOnUiThread(adLoader);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 3000);
            }


            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                Log.e("onAdLeftApplication", "onAdLeftApplication");
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                Log.e("onAdOpened", "onAdOpened");
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.e("onAdLoaded", "onAdLoaded");
            }
        });

        adLoader.run();




        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            // only for gingerbread and newer versions
        } else {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Home");
        }
        //  setSupportActionBar(toolbar);

        img = (CircleImageView) findViewById(R.id.uimg);
        logout = (TextView) findViewById(R.id.logout);
        home = (CircleImageView) findViewById(R.id.home);
        Config.noti_click = 1;
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    List<Fragment> fragments = getSupportFragmentManager().getFragments();
                    if (fragments != null) {
                        for (Fragment fragment : fragments) {
                            if (fragment.equals("browser")) {

                            } else {
                                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Intent i3 = new Intent(HomeActivity.this, BrowseByCategory.class);
                // i3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i3);

            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                builder.setTitle("Alert");
                builder.setMessage("Are you sure you want to logout?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                 /*       try {
                            List<Fragment> fragments = getSupportFragmentManager().getFragments();
                            if (fragments != null) {
                                for (Fragment fragment : fragments) {
                                    if (fragment.equals("browser")) {

                                    } else {
                                        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }*/
                        SharedPreferences sh = getSharedPreferences("log", MODE_PRIVATE);
                        sh.edit().putBoolean("login", false).commit();
                        //   startActivity(new Intent(HomeActivity.this, LoginActivity.class));

                        try {
                            List<Fragment> fragments = getSupportFragmentManager().getFragments();
                            if (fragments != null) {
                                for (Fragment fragment : fragments) {
                                    if (fragment.equals("browser")) {

                                    } else {
                                        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        new NotiDbHelper(HomeActivity.this).deleteAllData();


                        getSharedPreferences("RedeemCashbackCode", Context.MODE_PRIVATE)
                                .edit()
                                .putString("total_amt", "0")
                                .commit();


                        JSONArray js = new JSONArray();

                        getSharedPreferences("saved_ad", Context.MODE_PRIVATE)
                                .edit()
                                .putString("sads", js.toString())
                                .commit();


                        Intent ii = new Intent(HomeActivity.this, LoginActivity.class);
                        ii.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);


                        startActivity(ii);
                        //    getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.container)).commit();
                        //     clearSharedData();
                        finish();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.setCancelable(false);
                builder.show();
            }
        });
        //   getProducts();
        noti = (CircleImageView) findViewById(R.id.noti);
        try {
            JSONObject user = DAO.getUser();
            img_nm = user.getString("image");
            id = user.getInt("id");
            eID = user.getString("email");
            Name = user.getString("name");
            Picasso.with(this).load(img_nm).into(img);
        } catch (Exception e) {
            e.printStackTrace();
        }


        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Config.noti_click = 1;
                startActivity(new Intent(HomeActivity.this, HomeActivity.class));
            }
        });

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, UserAccount.class));
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, new Ads_Notification(), "not").commit();


        Intent in = new Intent(HomeActivity.this, LocationTressService.class);
        startService(in);


        View v = navigationView.getHeaderView(0);//(R.layout.nav_header_home);
        ImageView img1 = (ImageView) v.findViewById(R.id.imageView);
        TextView name_textView = (TextView) v.findViewById(R.id.name_textView);
        TextView email_textView = (TextView) v.findViewById(R.id.textView);
        name_textView.setText(Name);
        email_textView.setText(eID);
        ImageView blur_header_img = (ImageView) v.findViewById(R.id.blur_header_img);

        Picasso.with(Classified.getInstance()).load(img_nm).placeholder(R.drawable.user).into(img1);
        Picasso.with(com.classifides.app.application.Classified.getInstance()).load(img_nm).placeholder(R.drawable.user).error(R.drawable.user).transform(new Blur(getApplicationContext(), 80)).into(blur_header_img);

    }

   /* @Override
    protected void onDestroy() {
        super.onDestroy();
        clearBackStackInclusive("not"); // tag (addToBackStack tag) should be the same which was used while transacting the F2 fragment
        clearBackStackInclusive("browser");


    }*/

    public void clearSharedData() {
        SharedPreferences ppff = PreferenceManager.getDefaultSharedPreferences(HomeActivity.this);
        SharedPreferences.Editor editor;
        //Log.e("shredPref", ppff.getAll().toString());
        editor = ppff.edit();
        editor.clear().commit();
    }


    public void clearBackStackInclusive(String tag) {
        getSupportFragmentManager().popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    private void getProducts() {

        new AsyncTask() {
            JSONObject rest = null;
            String city = "";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(getApplicationContext(), "lc", 0);
                locations lt = complexPreferences.getObject("locs", locations.class);
                ArrayList<String> locs = new ArrayList<String>();
                if (lt != null) {
                    locs = lt.getLocs();
                    if (locs != null) {
                        if (locs.size() > 0) {
                            for (int i = 0; i < locs.size(); i++)
                                city = city + locs.get(i) + "|";
                        } else {
                            city = "";
                        }
                    }

                } else
                    city = "";

                if (city.length() > 0) {
                    city = city.substring(0, city.length() - 1);
                }

            }

            @Override
            protected Object doInBackground(Object[] params) {
                String filter = "";
                try {
                    SharedPreferences sh = com.classifides.app.application.Classified.getInstance().getSharedPreferences("categories", Context.MODE_PRIVATE);
                    String cats = sh.getString("cat", "");

                    JSONObject obj = new JSONObject(cats);
                    Iterator<String> key = obj.keys();
                    while (key.hasNext()) {
                        String c = key.next();
                        filter = filter + c + ",";

                        //Log.e("filter" + c, filter);
                    }
                    if (!filter.equals("")) {
                        filter = " and a.category in (" + filter.substring(0, filter.length() - 1) + ")";
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Map<String, String> map = new HashMap<String, String>();
                map.put("filter", filter);
                map.put("city", city);
                rest = DAO.getJsonFromUrl(Config.Server + "advertise.php", map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    // MAdaptor adaptor = new MAdaptor();
                    if (rest.getInt("success") > 0) {

                        int i = rest.getJSONArray("advertis").length();

                        if (Config.noti_click == 0) {
                            View target = findViewById(R.id.noti);
                            BadgeView badge = new BadgeView(getApplicationContext(), target);
                            badge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
                            badge.setText(i + "");
                            badge.show();
                            Config.noti_click = 1;
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

    @Override
    protected void onResume() {
        super.onResume();

        sh = getSharedPreferences("user", Context.MODE_PRIVATE);
        String user = sh.getString("users", "");
        try {
            JSONObject us = new JSONObject(user);
            img1 = us.getString("image");
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.app_bar_home, null);
            CircleImageView img = (CircleImageView) v.findViewById(R.id.uimg);
            Picasso.with(Classified.getInstance()).load(img1).into(img);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Config.noti_click = 1;
    }

    @Override
    protected void onPause() {
        super.onPause();
        Config.noti_click = 1;
    }

    @Override
    public void onBackPressed() {
      /*  DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Config.noti_click = 1;
            super.onBackPressed();
        }

        if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            Config.noti_click = 1;
            super.onBackPressed();
        }

        getUserInfo(id + "");

        sh = getSharedPreferences("user", Context.MODE_PRIVATE);
        String user = sh.getString("users", "");
        try {
            JSONObject us = new JSONObject(user);
            img1 = us.getString("image");
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.app_bar_home, null);
            CircleImageView img = (CircleImageView) v.findViewById(R.id.uimg);
            Picasso.with(Classified.getInstance()).load(img1).placeholder(R.drawable.user).into(img);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Config.noti_click = 1;
*/


        //   startActivity(new Intent(HomeActivity.this,BrowseByCategory.class));
      /*  DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
            getSupportFragmentManager().popBackStack();
        } else {

            super.onBackPressed();
        }*/


        if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }

    }

 /*   @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_screen, menu);
        return true;
    }*/

/*    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
       *//* int id = item.getItemId();

        if (id == R.id.profile) {
            startActivity(new Intent(HomeActivity.this, UserAccount.class));
            *//**//*SharedPreferences sh=getSharedPreferences("log",MODE_PRIVATE);
            sh.edit().putBoolean("login",false).commit();
            finish();
            startActivity(new Intent(HomeScreen.this,LoginActivity.class));*//**//*
            return true;
        }*//*

        return super.onOptionsItemSelected(item);
    }*/

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.one) {
           // startActivity(new Intent(HomeActivity.this, PostFreeAdd.class));

            Intent in = new Intent(HomeActivity.this, PostFreeAdd.class);
          //  in.putExtra("add_id", "");
            startActivity(in);

        } else if (id == R.id.two) {
          //  startActivity(new Intent(HomeActivity.this, PostVideoAdd.class));

            Intent in = new Intent(HomeActivity.this, PostVideoAdd.class);
           // in.putExtra("add_id", "");
            startActivity(in);

        } else if (id == R.id.three) {
            startActivity(new Intent(HomeActivity.this, ManageNotification.class));
        } else if (id == R.id.fore) {
            startActivity(new Intent(HomeActivity.this, MyAdd.class));
        } else if (id == R.id.banner) {
         //   startActivity(new Intent(HomeActivity.this, PostBannerAdd.class));

            Intent in = new Intent(HomeActivity.this, PostBannerAdd.class);
           // in.putExtra("add_id", "");
            startActivity(in);


        } else if (id == R.id.twelve) {

            startActivity(new Intent(HomeActivity.this, ReplyToMyAdd.class));
        } else if (id == R.id.five) {
            // MyReplyToAds.getProducts();
            startActivity(new Intent(HomeActivity.this, MyReplyToAds.class));

        } else if (id == R.id.six) {
            startActivity(new Intent(HomeActivity.this, RedeemCashbackCode.class));
        } else if (id == R.id.seven) {
            startActivity(new Intent(HomeActivity.this, ReferNEarnActivity.class));

        } else if (id == R.id.eight) {
            startActivity(new Intent(HomeActivity.this, ContactUs.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void getUserInfo(final String user_id) {

        final Map<String, String> data = new HashMap<String, String>();


        new AsyncTask() {
            JSONObject res = null;
            ProgressDialog pdd;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();


                data.put("id", user_id);

            /*    try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
            }

            @Override
            protected Object doInBackground(Object[] params) {
                res = DAO.getJsonFromUrl(Config.userInfo, data);

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                try {
                    if (res.getInt("success") > 0) {
                        //  Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_SHORT).show();
                        //  getActivity().finish();

                        JSONArray jsonArray = res.getJSONArray("user");
                        // Log.e("JsONArray", jsonArray.toString());
                        JSONObject jobj = jsonArray.getJSONObject(0);

                        sh = getSharedPreferences("user", Context.MODE_PRIVATE);
                        sh.edit().putString("users", res.getJSONArray("user").getJSONObject(0).toString()).commit();


                        //Log.e("image", jobj.getString("image"));

                        String iiii = jobj.getString("image");

                        Picasso.with(Classified.getInstance()).load(iiii).into(img);

                        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View v = inflater.inflate(R.layout.app_bar_home, null);
                        CircleImageView img1 = (CircleImageView) v.findViewById(R.id.uimg);
                        Picasso.with(Classified.getInstance()).load(iiii).placeholder(R.drawable.user).into(img1);


                        //  title.setText(jobj.getString("title"));


                    } else {
                        //  Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }


}
