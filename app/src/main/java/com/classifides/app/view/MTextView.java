package com.classifides.app.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.classifides.app.R;


/**
 * Created by Administrator on 07/11/2015.
 */
public class MTextView extends TextView {

    Context context;
    public MTextView(Context context) {
        this(context,null);
    }

    public MTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context=context;
        final TypedArray attributes = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MTextView, defStyleAttr, 0);
        setFont(attributes.getString(R.styleable.MTextView_font_name));

    }


    public void setFont(String Text)
    {
        Typeface face=Typeface.createFromAsset(context.getAssets(), "fonts/"+Text+".ttf");
        this.setTypeface(face);
        invalidate();
    }
}
