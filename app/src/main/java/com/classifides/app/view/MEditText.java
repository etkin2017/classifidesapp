package com.classifides.app.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import com.classifides.app.R;

/**
 * Created by Rajeshwar on 09/06/2016.
 */
public class MEditText extends EditText {
    private final Context context;

    public MEditText(Context context) {
        this(context,null);
    }

    public MEditText(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context=context;
        final TypedArray attributes = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MTextView, defStyleAttr, 0);
        setFont(attributes.getString(R.styleable.MTextView_font_name));
    }
    public void setFont(String Text)
    {
        Typeface face=Typeface.createFromAsset(context.getAssets(), "fonts/"+Text+".ttf");
        this.setTypeface(face);
        invalidate();
    }
}
