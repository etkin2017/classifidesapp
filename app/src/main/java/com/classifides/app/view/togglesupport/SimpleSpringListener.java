package com.classifides.app.view.togglesupport;

/**
 * Created by Rajeshwar on 15/07/2016.
 */

public class SimpleSpringListener implements SpringListener {
    @Override
    public void onSpringUpdate(Spring spring) {
    }

    @Override
    public void onSpringAtRest(Spring spring) {
    }

    @Override
    public void onSpringActivate(Spring spring) {
    }

    @Override
    public void onSpringEndStateChange(Spring spring) {
    }
}
