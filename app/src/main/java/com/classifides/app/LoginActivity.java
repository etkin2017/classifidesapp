package com.classifides.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.classifides.app.application.Classified;
import com.classifides.app.data.Common;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.gcm.model.User;
import com.classifides.app.view.MTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    EditText email, pass;
    LinearLayout login;
    MTextView forgot, signup, result, about;
    private ProgressDialog pd;

    CheckBox rem_pass;

    private void init() {

        rem_pass = (CheckBox) findViewById(R.id.rem_pass);
        email = (EditText) findViewById(R.id.etemail);
        pass = (EditText) findViewById(R.id.etpass);
        login = (LinearLayout) findViewById(R.id.login);
        signup = (MTextView) findViewById(R.id.signup);
        result = (MTextView) findViewById(R.id.result);
        about = (MTextView) findViewById(R.id.about);
        forgot = (MTextView) findViewById(R.id.forgot);
        pd = new ProgressDialog(this);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        email.setTypeface(face);
        pass.setTypeface(face);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        getFragmentManager().beginTransaction().remove(getFragmentManager().findFragmentById(R.id.container)).commit();


        finish();
        System.exit(0);


    }

    private void addListener() {
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String semail = email.getText().toString().trim();
                String spass = pass.getText().toString().trim();
                result.setVisibility(View.GONE);
                if (!Common.isValidEmail(semail)) {
                    result.setText("please enter valid Email");
                    result.setVisibility(View.VISIBLE);
                    return;
                }
                if (TextUtils.isEmpty(spass)) {
                    result.setText("please enter Password");
                    result.setVisibility(View.VISIBLE);
                    return;
                }
                Map<String, String> map = new HashMap<String, String>();
                map.put("email", semail);
                map.put("password", spass);
                checkLogin(map);
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //    finish();
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, AboutUs.class));
            }
        });

        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgotPassword.class));
            }
        });
    }

    private void checkLogin(final Map<String, String> map) {
        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ProgressDialog();
            }

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    res = DAO.getJsonFromUrl(Config.Server + "login.php", map);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                DismissProgress();

                try {
                    if (res.getInt("success") > 0) {
                        Toast.makeText(LoginActivity.this, res.getString("message"), Toast.LENGTH_LONG).show();
                        SharedPreferences sh = getSharedPreferences("log", MODE_PRIVATE);
                        sh.edit().putBoolean("login", true).commit();

                        sh = getSharedPreferences("user", Context.MODE_PRIVATE);
                        sh.edit().putString("users", res.getJSONArray("user").getJSONObject(0).toString()).commit();

                      /*  JSONObject userObj = res.getJSONArray("user").getJSONObject(0);
                        User user = new User(userObj.getString("id"),
                                userObj.getString("name"),
                                userObj.getString("email"),userObj.getString("mobile"));
                        User user2 = new User(userObj.getString("id"),
                                userObj.getString("name"),
                                userObj.getString("email"),userObj.getString("mobile"));*/


                        // storing user in shared preferences
                      //  Classified.getInstance().getPrefManager().storeUser(user);
                     //   Classified.getInstance().getPrefManager().storeUser(user2);



                        finish();
                        startActivity(new Intent(LoginActivity.this, BrowseByCategory.class));
                    } else {
                        result.setText(res.getString("message"));
                        result.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    result.setText("Error in Login");
                    result.setVisibility(View.VISIBLE);
                }
            }
        }.execute(null, null, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        checkPreviousLogin();
        init();
        addListener();
    }

    private void checkPreviousLogin() {
        SharedPreferences sh = getSharedPreferences("log", MODE_PRIVATE);
        Boolean log = sh.getBoolean("login", false);
        if (log) {

           // startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            startActivity(new Intent(LoginActivity.this, BrowseByCategory.class));
            finish();
        }
    }


    public void ProgressDialog() {
        pd.setMessage("We are Logging In");
        pd.setTitle("Please wait....");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setIndeterminate(true);
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    public void DismissProgress() {
        pd.dismiss();
    }
}
