package com.classifides.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CashbackCode extends AppCompatActivity {

    TextView ad_id, total_rs;
    RecyclerView rec_paymet;

    Button make_payment;
    CheckBox cb1, cb2, cb3, cb4, cb5;
    ProgressDialog dialog;
    Date today = new Date();
    Calendar c = Calendar.getInstance();
    public static String u_id, a_id, uid, aid;
    int[] anArray;

    ArrayList<String> strArrayList;

    MAdaptor adaptor;
    public int pp = 0;
    public int p = 0;
    public int v = 0;
    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    String cdate = df.format(c.getTime());

    private Map<String, String> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cashback_code);
        //   Log.e("cdate", cdate);
        u_id = getIntent().getStringExtra("user_id");
        a_id = getIntent().getStringExtra("add_id");

        anArray = new int[5];
        strArrayList = new ArrayList<String>();
        a_id = a_id.replaceAll("\\[", "").replaceAll("\\]", "");

        u_id = u_id.replaceAll("\\[", "").replaceAll("\\]", "");

        uid = u_id.replaceAll("\\[", "").replaceAll("\\]", "").trim().toString();
        //   Log.e("u_ID", u_id.toString().trim());

        ad_id = (TextView) findViewById(R.id.id_no);
        ad_id.setText(a_id.toString().trim());


        total_rs = (TextView) findViewById(R.id.total_rs);
        make_payment = (Button) findViewById(R.id.make_payment);
        data = new HashMap<String, String>();

        dialog = new ProgressDialog(CashbackCode.this);
        rec_paymet = (RecyclerView) findViewById(R.id.rec_payment);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rec_paymet.setLayoutManager(llm);

        cb1 = (CheckBox) findViewById(R.id.cb1);
        cb2 = (CheckBox) findViewById(R.id.cb2);
        cb3 = (CheckBox) findViewById(R.id.cb3);

        cb4 = (CheckBox) findViewById(R.id.cb4);
        cb5 = (CheckBox) findViewById(R.id.cb5);
        adaptor = new MAdaptor();

        rec_paymet.setVisibility(View.VISIBLE);


        getHistory();


        cb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (cb1.isChecked() == true) {
                 /*   p = p + 50;
                    v = v + 100;*/
                    pp = pp + 50;
                    total_rs.setText(pp + "");

                } else {

                    pp = pp - 50;
                    // v = v - 100;
                    total_rs.setText(pp + "");

                }

            }
        });


        cb2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (cb2.isChecked() == true) {

                    pp = pp + 100;
//                    v = v + 200;
                    total_rs.setText(pp + "");


                } else {
                    pp = pp - 100;
                    //v = v - 200;
                    total_rs.setText(pp + "");
                }

            }
        });


        cb3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (cb3.isChecked() == true) {

                    pp = pp + 200;
                    //  v = v + 400;
                    total_rs.setText(pp + "");


                } else {

                    pp = pp - 200;
                    //  v = v - 400;
                    total_rs.setText(pp + "");

                }

            }
        });


        cb4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (cb4.isChecked() == true) {

                    pp = pp + 300;
                    //    v = v + 600;
                    total_rs.setText(pp + "");


                } else {

                    pp = pp - 300;
                    //   v = v - 600;

                    total_rs.setText(pp + "");
                }

            }
        });


        cb5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (cb5.isChecked() == true) {

                    pp = pp + 400;
                    //   v = v + 800;
                    total_rs.setText(pp + "");

                } else if (cb5.isChecked() == false) {

                    pp = pp - 400;
                    //     v = v - 800;

                    total_rs.setText(pp + "");
                }

            }
        });

        make_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ((cb1.isChecked() == false) && (cb2.isChecked() == false) && (cb3.isChecked() == false) &&
                        (cb4.isChecked() == false) && (cb5.isChecked() == false)) {

                    Toast.makeText(getApplicationContext(), "Please Select Plan", Toast.LENGTH_LONG).show();

                } else {


                    if (cb1.isChecked() == true) {
                        String a3 = "50";
                        String b3 = "100";
                        rec_paymet.setVisibility(View.VISIBLE);
                        //  makePayment(50 + "", 100 + "");

                        strArrayList.add(a3);
                        //  Toast.makeText(getApplicationContext(), "CB1", Toast.LENGTH_LONG).show();
                    }
                    if (cb2.isChecked() == true) {

                        rec_paymet.setVisibility(View.VISIBLE);
                        //    makePayment(100 + "", 200 + "");

                        strArrayList.add(100 + "");

                        // Toast.makeText(getApplicationContext(), "CB2", Toast.LENGTH_LONG).show();

                    }
                    if (cb3.isChecked() == true) {
                        String a1 = "200";
                        String b1 = "400";
                        rec_paymet.setVisibility(View.VISIBLE);
                        //    makePayment(200 + "", 400 + "");

                        strArrayList.add(a1);

                        // Toast.makeText(getApplicationContext(), "CB3", Toast.LENGTH_LONG).show();

                    }
                    if (cb4.isChecked() == true) {
                        String p4 = "300";
                        String v4 = "600";
                        rec_paymet.setVisibility(View.VISIBLE);
                        //makePayment(300 + "", 600 + "");
                        strArrayList.add(300 + "");                      //    Toast.makeText(getApplicationContext(), "CB4", Toast.LENGTH_LONG).show();

                    }
                    if (cb5.isChecked() == true) {

                        String p1 = "400";
                        String v1 = "800";
                        rec_paymet.setVisibility(View.VISIBLE);
                        //makePayment(400 + "", 800 + "");


                        strArrayList.add(400 + "");

                        // Toast.makeText(getApplicationContext(), "CB5", Toast.LENGTH_LONG).show();

                    }


                    makePayment();
                }


                //}
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        getHistory();
    }

    private void makePayment() {

        String planlist = "";


        for (int i = 0; i < strArrayList.size(); i++) {

            if (planlist.contains(strArrayList.get(i))) {
            } else {
                planlist = planlist + strArrayList.get(i) + ",";
            }


        }


        planlist = planlist.substring(0, planlist.length() - 1);

        Log.e("planList", planlist);

        final String finalPlanlist = planlist;


        int id = 1212;
        //   Log.e("uID", uid);
        data.put("add_id", a_id.trim());
        data.put("user_id", uid);
        data.put("plan", finalPlanlist);
        data.put("views", "");
        data.put("view_date", cdate);
        data.put("cash_code", id + "");
        data.put("amount", total_rs.getText().toString());


        JSONObject jobj1 = new JSONObject(data);
        Intent i = new Intent(CashbackCode.this, PaymenActivity.class);
        i.putExtra("submit_ad", jobj1.toString());
        i.putExtra("which", "2");
        startActivity(i);



      /*  new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ProgressDialog();

                int id = 1212;
                //   Log.e("uID", uid);
                data.put("add_id", a_id.trim());
                data.put("user_id", uid);
                data.put("plan", finalPlanlist);
                data.put("views", "");
                data.put("view_date", cdate);
                data.put("cash_code", id + "");
             *//*   try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }*//*
            }

            @Override
            protected Object doInBackground(Object[] params) {

                res = DAO.getJsonFromUrl(Config.addCashPlan, data);

                  *//*  if (res.getInt("success") > 0) {
                        Toast.makeText(UpdateAsValidity.this, "Plan Add Suceessfully", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(UpdateAsValidity.this, "Error ", Toast.LENGTH_LONG).show();

                    }
*//*

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                DismissProgress();
                Toast.makeText(getApplicationContext(), "Plan submitted", Toast.LENGTH_SHORT).show();
                getHistory();
                *//*try {
                    if (res.getInt("success") > 0) {
                        Toast.makeText(getApplicationContext(), "Plan submitted", Toast.LENGTH_SHORT).show();
                        getHistory();
                    } else {
                        Toast.makeText(getApplicationContext(), "plan not submitted", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }*//*
            }
        }.execute(null, null, null);*/
    }

    public void br(char i) {
        switch (i) {
            case 'a':
                break;
        }

    }


    public void getHistory() {

        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Object doInBackground(Object[] params) {

                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", u_id.trim());
                rest = DAO.getJsonFromUrl(Config.cashBackHistory, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {


                    if (rest.getInt("success") > 0) {
                        adaptor.setData(rest.getJSONArray("plan"));
                        rec_paymet.setAdapter(adaptor);
                    } else {


                        adaptor.setData(new JSONArray());
                        rec_paymet.setAdapter(adaptor);
                        Toast.makeText(CashbackCode.this, rest.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }


    public void ProgressDialog() {
        dialog.setMessage("While we posting your ad.");
        dialog.setTitle("Please wait....");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void DismissProgress() {
        dialog.dismiss();
    }


    private class MAdaptor extends RecyclerView.Adapter<MAdaptor.ViewHolder> {

        private JSONArray data;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.plan_history, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            try {
                holder.ad_id.setText(data.getJSONObject(position).getString("add_id"));
                holder.sel_plan.setText("Rs " + data.getJSONObject(position).getString("plan") + " + " + data.getJSONObject(position).getString("p_view") + " Views");
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");


                holder.plan_date_one.setText(data.getJSONObject(position).getString("view_date"));
                holder.viewers.setText(data.getJSONObject(position).getString("cashback_code"));

                //holder.viewers.setVisibility(View.GONE);


                holder.itemView.setTag(data.getJSONObject(position));


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        public int getItemCount() {
            return data.length();
        }

        public void setData(JSONArray data) {
            this.data = data;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView sel_plan, ad_id, plan_date_one, viewers;


            TextView user_id, add_id;

            public ViewHolder(View itemView) {
                super(itemView);
                sel_plan = (TextView) itemView.findViewById(R.id.sel_plan);
                ad_id = (TextView) itemView.findViewById(R.id.ad_id);
                plan_date_one = (TextView) itemView.findViewById(R.id.plan_date_one);
                viewers = (TextView) itemView.findViewById(R.id.viewers);

            }
        }
    }
}
































/*
package com.classifides.app;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CashbackCode extends AppCompatActivity {

    TextView ad_id, total_rs;
    RecyclerView rec_paymet;

    Button make_payment;
    CheckBox cb1, cb2, cb3, cb4, cb5;
    ProgressDialog dialog;
    Date today = new Date();
    Calendar c = Calendar.getInstance();
    public static String u_id, a_id, uid, aid;


    ArrayList<String> cashcodePlans;


    MAdaptor adaptor;
    public int pp = 0;
    public int p = 0;
    public int v = 0;
    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    String cdate = df.format(c.getTime());

    private Map<String, String> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cashback_code);
        //Log.e("cdate", cdate);
        u_id = getIntent().getStringExtra("user_id");
        a_id = getIntent().getStringExtra("add_id");

        cashcodePlans = new ArrayList<String>();
        a_id = a_id.replaceAll("\\[", "").replaceAll("\\]", "");

        u_id = u_id.replaceAll("\\[", "").replaceAll("\\]", "");

        uid = u_id.replaceAll("\\[", "").replaceAll("\\]", "").trim().toString();
        //Log.e("u_ID", u_id.toString().trim());

        ad_id = (TextView) findViewById(R.id.id_no);
        ad_id.setText(a_id.toString().trim());


        total_rs = (TextView) findViewById(R.id.total_rs);
        make_payment = (Button) findViewById(R.id.make_payment);
        data = new HashMap<String, String>();

        dialog = new ProgressDialog(CashbackCode.this);
        rec_paymet = (RecyclerView) findViewById(R.id.rec_payment);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rec_paymet.setLayoutManager(llm);

        cb1 = (CheckBox) findViewById(R.id.cb1);
        cb2 = (CheckBox) findViewById(R.id.cb2);
        cb3 = (CheckBox) findViewById(R.id.cb3);

        cb4 = (CheckBox) findViewById(R.id.cb4);
        cb5 = (CheckBox) findViewById(R.id.cb5);
        adaptor = new MAdaptor();

        rec_paymet.setVisibility(View.VISIBLE);


        getHistory();


        cb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (cb1.isChecked() == true) {
                 */
/*   p = p + 50;
                    v = v + 100;*//*

                    pp = pp + 50;
                    total_rs.setText(pp + "");

                } else {

                    pp = pp - 50;
                    // v = v - 100;
                    total_rs.setText(pp + "");

                }

            }
        });


        cb2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (cb2.isChecked() == true) {

                    pp = pp + 100;
//                    v = v + 200;
                    total_rs.setText(pp + "");


                } else {
                    pp = pp - 100;
                    //v = v - 200;
                    total_rs.setText(pp + "");
                }

            }
        });


        cb3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (cb3.isChecked() == true) {

                    pp = pp + 200;
                    //  v = v + 400;
                    total_rs.setText(pp + "");


                } else {

                    pp = pp - 200;
                    //  v = v - 400;
                    total_rs.setText(pp + "");

                }

            }
        });


        cb4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (cb4.isChecked() == true) {

                    pp = pp + 300;
                    //    v = v + 600;
                    total_rs.setText(pp + "");


                } else {

                    pp = pp - 300;
                    //   v = v - 600;

                    total_rs.setText(pp + "");
                }

            }
        });


        cb5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (cb5.isChecked() == true) {

                    pp = pp + 400;
                    //   v = v + 800;
                    total_rs.setText(pp + "");

                } else if (cb5.isChecked() == false) {

                    pp = pp - 400;
                    //     v = v - 800;

                    total_rs.setText(pp + "");
                }

            }
        });

        make_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ((cb1.isChecked() == false) && (cb2.isChecked() == false) && (cb3.isChecked() == false) &&
                        (cb4.isChecked() == false) && (cb5.isChecked() == false)) {

                    Toast.makeText(getApplicationContext(), "Please Select Plan", Toast.LENGTH_LONG).show();

                } else {
                    if (cb1.isChecked() == true) {
                        String a3 = "50";
                        String b3 = "100";
                        rec_paymet.setVisibility(View.VISIBLE);
                       // makePayment(50 + "", 100 + "");
                        cashcodePlans.add(a3);

                        //  Toast.makeText(getApplicationContext(), "CB1", Toast.LENGTH_LONG).show();
                    } else {
                        if (cashcodePlans.contains(50 + "")) {
                            cashcodePlans.remove(50 + "");
                        }
                    }

                    if (cb2.isChecked() == true) {
                        String a2 = "100";
                        String b2 = "200";
                        rec_paymet.setVisibility(View.VISIBLE);
                       // makePayment(100 + "", 200 + "");

                        cashcodePlans.add(a2);

                        // Toast.makeText(getApplicationContext(), "CB2", Toast.LENGTH_LONG).show();

                    } else {
                        if (cashcodePlans.contains(100 + "")) {
                            cashcodePlans.remove(100 + "");
                        }
                    }
                    if (cb3.isChecked() == true) {
                        String a1 = "200";
                        String b1 = "400";
                        rec_paymet.setVisibility(View.VISIBLE);
                      //  makePayment(200 + "", 400 + "");
                        // Toast.makeText(getApplicationContext(), "CB3", Toast.LENGTH_LONG).show();


                        cashcodePlans.add(a1);

                    } else {
                        if (cashcodePlans.contains(200 + "")) {
                            cashcodePlans.remove(200 + "");
                        }
                    }
                    if (cb4.isChecked() == true) {
                        String p4 = "300";
                        String v4 = "600";
                        rec_paymet.setVisibility(View.VISIBLE);
                      //  makePayment(300 + "", 600 + "");
                        //    Toast.makeText(getApplicationContext(), "CB4", Toast.LENGTH_LONG).show();

                        cashcodePlans.add(300 + "");

                    } else {
                        if (cashcodePlans.contains(300 + "")) {
                            cashcodePlans.remove(300 + "");
                        }
                    }

                    if (cb5.isChecked() == true) {

                        String p1 = "400";
                        String v1 = "800";
                        rec_paymet.setVisibility(View.VISIBLE);
                       // makePayment(400 + "", 800 + "");
                        // Toast.makeText(getApplicationContext(), "CB5", Toast.LENGTH_LONG).show();

                        cashcodePlans.add(p1);

                    } else {
                        if (cashcodePlans.contains(400 + "")) {
                            cashcodePlans.remove(400 + "");
                        }
                    }

                    makePayment();
                }


                //}
            }
        });

    }


    private void makePayment() {

        final ArrayList<String> newPlans = new ArrayList<String>();

        for (int i = 0; i < cashcodePlans.size(); i++) {
            for (int j = 0; j <= newPlans.size(); i++) {
                if (newPlans.get(j).equals(cashcodePlans.get(i))) {

                } else {
                    newPlans.add(cashcodePlans.get(i));
                }
            }
        }


        for (final int[] k = {0}; k[0] < newPlans.size(); ) {

            String pp1 = "";
            final String[] vv2 = {""};

            new AsyncTask() {
                JSONObject res = null;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    ProgressDialog();

                    int id = 1212;
                    //Log.e("uID", uid);
                    data.put("add_id", a_id.trim());
                    data.put("user_id", uid);
                    if (newPlans.get(k[0]).equals("50")) {
                        vv2[0] = "100";
                    } else if (newPlans.get(k[0]).equals("100")) {
                        vv2[0] = "200";
                    } else if (newPlans.get(k[0]).equals("200")) {
                        vv2[0] = "400";
                    } else if (newPlans.get(k[0]).equals("300")) {
                        vv2[0] = "600";
                    } else if (newPlans.get(k[0]).equals("400")) {
                        vv2[0] = "800";
                    }


                    data.put("plan", newPlans.get(k[0]) + "");
                    data.put("views", vv2[0]);
                    data.put("view_date", cdate);
                    data.put("cash_code", id + "");
             */
/*   try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }*//*

                }

                @Override
                protected Object doInBackground(Object[] params) {

                    res = DAO.getJsonFromUrl(Config.addCashPlan, data);
                    return null;
                }

                @Override
                protected void onPostExecute(Object o) {
                    super.onPostExecute(o);
                    DismissProgress();
                    try {
                        if (res.getInt("success") > 0) {
                            Toast.makeText(getApplicationContext(), "Plan submitted", Toast.LENGTH_SHORT).show();
                            k[0]++;
                            //    getHistory();
                        } else {
                            Toast.makeText(getApplicationContext(), "plan not submitted", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }.execute(null, null, null);
        }


    }


    public void getHistory() {

        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Object doInBackground(Object[] params) {

                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", u_id.trim());
                rest = DAO.getJsonFromUrl(Config.cashBackHistory, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {


                    if (rest.getInt("success") > 0) {
                        adaptor.setData(rest.getJSONArray("plan"));
                        rec_paymet.setAdapter(adaptor);
                    } else {


                        adaptor.setData(new JSONArray());
                        rec_paymet.setAdapter(adaptor);
                    //    Toast.makeText(CashbackCode.this, rest.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }


    public void ProgressDialog() {
        dialog.setMessage("While we posting your add.");
        dialog.setTitle("Please wait....");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void DismissProgress() {
        dialog.dismiss();
    }


    private class MAdaptor extends RecyclerView.Adapter<MAdaptor.ViewHolder> {

        private JSONArray data;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.plan_history, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            try {
                holder.ad_id.setText(data.getJSONObject(position).getString("add_id"));
                holder.sel_plan.setText("Rs " + data.getJSONObject(position).getString("plan") + " + " + data.getJSONObject(position).getString("p_view") + " Views");
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");


                holder.plan_date_one.setText(data.getJSONObject(position).getString("view_date"));
                holder.viewers.setText(data.getJSONObject(position).getString("cashback_code"));

                //holder.viewers.setVisibility(View.GONE);


                holder.itemView.setTag(data.getJSONObject(position));


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        public int getItemCount() {
            return data.length();
        }

        public void setData(JSONArray data) {
            this.data = data;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView sel_plan, ad_id, plan_date_one, viewers;


            TextView user_id, add_id;

            public ViewHolder(View itemView) {
                super(itemView);
                sel_plan = (TextView) itemView.findViewById(R.id.sel_plan);
                ad_id = (TextView) itemView.findViewById(R.id.ad_id);
                plan_date_one = (TextView) itemView.findViewById(R.id.plan_date_one);
                viewers = (TextView) itemView.findViewById(R.id.viewers);

            }
        }
    }
}
*/
