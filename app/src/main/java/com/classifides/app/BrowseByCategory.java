package com.classifides.app;

import android.*;
import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;

import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.classifides.app.Adaptor.CategoryAdaptor;
import com.classifides.app.application.Classified;
import com.classifides.app.data.Blur;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.data.NotiDbHelper;
import com.classifides.app.fragment.Ads_Notification;
import com.classifides.app.fragment.BrowseCategory;
import com.classifides.app.gcm.activity.CreateGroupActivity;
import com.classifides.app.gcm.activity.HomeChatActivity;
import com.classifides.app.gcm.gcm.GcmIntentService;
import com.classifides.app.gcm.gcm.UpdateContactService;
import com.classifides.app.gcm.model.User;
import com.classifides.app.model.BadgeView;
import com.classifides.app.services.LocationTressService;
import com.github.clans.fab.FloatingActionButton;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.fitness.data.Device;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;

public class BrowseByCategory extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static boolean activate = true;
    CategoryAdaptor adaptor;
    GridView gridview;
    NavigationView navigationView;

    CircleImageView img;
    String img_nm;

    CircleImageView noti, home;
    TextView logout;
    public static int id;
    TextView icon_test;
    View target;
    BadgeView badge;
    SharedPreferences sh, sh1;
    public static int badge_data = 0;

    MyBroadcast broadcast;

    RelativeLayout ad_container_layout;

    public static DrawerLayout drawer2;

    public static String eID, Name;
    FloatingActionButton floatingActionButton;


    TextView messenger;

    private ArrayList<String> mobilelist;
    String allcont = "";
    HashMap<String, String> contactsname;
    public static String mobile;

   /* public BrowseByCategory(Context ctx) {

    }*/


    public String md5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_by_category);
        //     drawer2 = (DrawerLayout) findViewById(R.id.drawer_layout_two);

         ad_container_layout = (RelativeLayout) findViewById(R.id.ad_container_layout);

        MobileAds.initialize(getApplicationContext(), "ca-app-pub-2758807869387921~3087322491");
        String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.e("android_id", android_id);

        String device_id = md5(android_id).toUpperCase();
        Log.e("device_id", device_id);

        final AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
             //   .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                //.addTestDevice("CA7416822EE39EB5991049AECDE61839")
                //.addTestDevice(device_id)
                .build();
        mAdView.loadAd(adRequest);

        final Runnable adLoader = new Runnable() {
            @Override
            public void run() {
                mAdView.loadAd(new AdRequest.Builder().build());
            }
        };





        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                Log.e("onAdClosed", "onAdClosed");
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Log.e("onAdFailedToLoad", "onAdFailedToLoad");

                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        try{
                        BrowseByCategory.this.runOnUiThread(adLoader);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 3000);
            }


            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                Log.e("onAdLeftApplication", "onAdLeftApplication");
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                Log.e("onAdOpened", "onAdOpened");
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.e("onAdLoaded", "onAdLoaded");
            }
        });

        adLoader.run();


        sh1 = getSharedPreferences("user", Context.MODE_PRIVATE);

        String users = sh1.getString("users", "");
        try {
            JSONObject j = new JSONObject(users);

            mobile = j.getString("mobile");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        contactsname = new HashMap<String, String>();
        mobilelist = new ArrayList<String>();

        messenger = (TextView) findViewById(R.id.messenger);
        messenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //  startActivity(new Intent(BrowseByCategory.this, com.classifides.app.gcm.activity.MainActivity.class));
                startActivity(new Intent(BrowseByCategory.this, HomeChatActivity.class));
            }
        });
        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab1);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(BrowseByCategory.this, HomeChatActivity.class));
            }
        });


        icon_test = (TextView) findViewById(R.id.icon_test);
        //   adaptor = new CategoryAdaptor(getApplicationContext());
        home = (CircleImageView) findViewById(R.id.home);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Fragment> fragments = getSupportFragmentManager().getFragments();

          /*      if(fragments.size()!=0) {
                    if (fragments != null) {
                        for (Fragment fragment : fragments) {
                            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                        }
                    }
                }else*/
                //{

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new BrowseCategory(), "browser").commit();

                //  }

                ad_container_layout.setVisibility(View.VISIBLE);

              /*  Intent i3 = new Intent(BrowseByCategory.this, BrowseByCategory.class);
               i3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i3);*/


            }
        });


        logout = (TextView) findViewById(R.id.logout);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* try {
                    List<Fragment> fragments = getSupportFragmentManager().getFragments();
                    if (fragments != null) {
                        for (Fragment fragment : fragments) {
                            if (fragment.equals("browser")) {

                            } else {
                                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                            }
                            //  Log.e("fragmentmain2"+fragment.toString(),fragment.toString());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
                AlertDialog.Builder builder = new AlertDialog.Builder(BrowseByCategory.this);
                builder.setTitle("Alert");
                builder.setMessage("Are you sure you want to logout?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        SharedPreferences sh = getSharedPreferences("log", MODE_PRIVATE);
                        sh.edit().putBoolean("login", false).commit();
                        try {
                            List<Fragment> fragments = getSupportFragmentManager().getFragments();
                            if (fragments != null) {
                                for (Fragment fragment : fragments) {
                                    if (fragment.equals("browser")) {

                                    } else {
                                        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        getSharedPreferences("RedeemCashbackCode", Context.MODE_PRIVATE)
                                .edit()
                                .putString("total_amt", "0")
                                .commit();


                        JSONArray js = new JSONArray();

                        getSharedPreferences("saved_ad", Context.MODE_PRIVATE)
                                .edit()
                                .putString("sads", js.toString())
                                .commit();

                        new NotiDbHelper(BrowseByCategory.this).deleteAllData();
                        Intent ii = new Intent(BrowseByCategory.this, LoginActivity.class);
                        ii.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(ii);
                        //   getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.container)).commit();
                        //    clearSharedData();
                        finish();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.setCancelable(false);
                builder.show();


                //    finish();
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            // only for gingerbread and newer versions
        } else {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Home");
        }
        img = (CircleImageView) findViewById(R.id.uimg);
        //  getProducts();
        noti = (CircleImageView) findViewById(R.id.noti);
        try {
            JSONObject user = DAO.getUser();
            img_nm = user.getString("image");
            eID = user.getString("email");
            Name = user.getString("name");
            Picasso.with(this).load(img_nm).placeholder(R.drawable.user).into(img);


            id = user.getInt("id");

        } catch (Exception e) {
            e.printStackTrace();
        }
        broadcast = new MyBroadcast();
        IntentFilter filter = new IntentFilter(LocationTressService.name);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        try {
            registerReceiver(broadcast, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(this, GcmIntentService.class);
        startService(intent);

        target = findViewById(R.id.noti);
        badge = new BadgeView(getApplicationContext(), target);
        badge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Config.noti_click = 1;

                badge.hide();
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, new Ads_Notification(), "not").commit();

                ad_container_layout.setVisibility(View.GONE);


                //trigger notification service
             /*   Intent in = new Intent(BrowseByCategory.this, LocationTressService.class);
                startService(in);
*/
            }
        });

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(BrowseByCategory.this, UserAccount.class));
            }
        });
/*

        Typeface fontAwesomeFont = Typeface.createFromAsset(getAssets(), "fonts/fontawesome-webfont.ttf");
        icon_test.setTypeface(fontAwesomeFont);
*/

        Intent in = new Intent(BrowseByCategory.this, LocationTressService.class);
        startService(in);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(BrowseByCategory.this);
        navigationView.setItemIconTintList(null);


        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new BrowseCategory(), "browser").commit();

/*
        if(checkInternetOn() == true) {

            Toast.makeText(getApplicationContext(),"true",Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(getApplicationContext(),"No Internet Connect",Toast.LENGTH_LONG).show();
        }

        NotiDbHelper mHelper = new NotiDbHelper(Classified.getInstance());

        ArrayList<User> check_store_contact = mHelper.getAllUser();
        if (check_store_contact.size() > 0) {
            try {
                User.group(Classified.getInstance());
            }catch (Exception e){
                e.printStackTrace();
            }
        } else {
            Intent f = new Intent(BrowseByCategory.this, UpdateContactService.class);
            f.putExtra("id", "4");
            startService(f);

        }*/

        //   LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = navigationView.getHeaderView(0);//(R.layout.nav_header_home);
        ImageView img1 = (ImageView) v.findViewById(R.id.imageView);
        TextView name_textView = (TextView) v.findViewById(R.id.name_textView);
        TextView email_textView = (TextView) v.findViewById(R.id.textView);
        ImageView blur_header_img = (ImageView) v.findViewById(R.id.blur_header_img);
        name_textView.setText(Name);
        email_textView.setText(eID);
        Picasso.with(Classified.getInstance()).load(img_nm).placeholder(R.drawable.user).into(img1);
        Picasso.with(com.classifides.app.application.Classified.getInstance()).load(img_nm).placeholder(R.drawable.user).error(R.drawable.user).transform(new Blur(getApplicationContext(), 80)).into(blur_header_img);

        try {

            SharedPreferences sharedPreferences = getSharedPreferences("PHONECONTACT", Context.MODE_PRIVATE);
            int m = sharedPreferences.getInt("phone_no", 0);
            if (m > 0) {

            } else {
                readPhoneContacts();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public boolean checkInternetOn() {

        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {


            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {


            return false;
        }
        return false;
    }


   /* @Override
    protected void onDestroy() {
        super.onDestroy();
        clearBackStackInclusive("not"); // tag (addToBackStack tag) should be the same which was used while transacting the F2 fragment
        clearBackStackInclusive("browser");
    }*/

    public void clearBackStackInclusive(String tag) {
        getSupportFragmentManager().popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onDestroy() {
        try {
            if (broadcast != null)
                unregisterReceiver(broadcast);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }

       /* DrawerLayout drawer2 = (DrawerLayout) findViewById(R.id.drawer_layout_two);
        if (drawer2.isDrawerOpen(GravityCompat.END)) {
            drawer2.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
*/
       /* if (Config.noti_click == 1) {

            badge.hide();
        }*/
    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }


    @Override
    protected void onPause() {
        super.onPause();
        activate = false;

    }

    public void clearSharedData() {
        SharedPreferences ppff = PreferenceManager.getDefaultSharedPreferences(BrowseByCategory.this);
        SharedPreferences.Editor editor;
        //Log.e("shredPref", ppff.getAll().toString());
        editor = ppff.edit();
        editor.clear().commit();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // unregisterReceiver(broadcast);
    }


    @Override
    protected void onResume() {
        super.onResume();
        ad_container_layout.setVisibility(View.VISIBLE);

        activate = false;


        if (ActivityCompat.checkSelfPermission(BrowseByCategory.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(BrowseByCategory.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA, android.Manifest.permission.READ_CONTACTS},
                    110);

        }


        try {
            JSONObject user = DAO.getUser();
            img_nm = user.getString("image");
            eID = user.getString("email");
            Name = user.getString("name");
            Picasso.with(this).load(img_nm).placeholder(R.drawable.user).into(img);


            id = user.getInt("id");

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            View v = navigationView.getHeaderView(0);//(R.layout.nav_header_home);
            ImageView img1 = (ImageView) v.findViewById(R.id.imageView);
            TextView name_textView = (TextView) v.findViewById(R.id.name_textView);
            TextView email_textView = (TextView) v.findViewById(R.id.textView);
            ImageView blur_header_img = (ImageView) v.findViewById(R.id.blur_header_img);
            name_textView.setText(Name);
            email_textView.setText(eID);
            Picasso.with(Classified.getInstance()).load(img_nm).placeholder(R.drawable.user).into(img1);
            Picasso.with(com.classifides.app.application.Classified.getInstance()).load(img_nm).placeholder(R.drawable.user).error(R.drawable.user).transform(new Blur(getApplicationContext(), 80)).into(blur_header_img);

        } catch (Exception e) {

        }


    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.one) {
            //    startActivity(new Intent(BrowseByCategory.this, PostFreeAdd.class));

            Intent in = new Intent(BrowseByCategory.this, PostFreeAdd.class);
            //  in.putExtra("add_id", "");
            startActivity(in);


        } else if (id == R.id.two) {
            //startActivity(new Intent(BrowseByCategory.this, PostVideoAdd.class));

            Intent in = new Intent(BrowseByCategory.this, PostVideoAdd.class);
            //  in.putExtra("add_id", "");
            startActivity(in);

        } else if (id == R.id.three) {
            startActivity(new Intent(BrowseByCategory.this, ManageNotification.class));
        } else if (id == R.id.fore) {
            startActivity(new Intent(BrowseByCategory.this, MyAdd.class));
        } else if (id == R.id.banner) {
            // startActivity(new Intent(BrowseByCategory.this, PostBannerAdd.class));

            Intent in = new Intent(BrowseByCategory.this, PostBannerAdd.class);
            //   in.putExtra("add_id", "");
            startActivity(in);
        } else if (id == R.id.twelve) {

            startActivity(new Intent(BrowseByCategory.this, ReplyToMyAdd.class));
        } else if (id == R.id.five) {
            // MyReplyToAds.getProducts();
            startActivity(new Intent(BrowseByCategory.this, MyReplyToAds.class));

        } else if (id == R.id.six) {
            startActivity(new Intent(BrowseByCategory.this, RedeemCashbackCode.class));
        } else if (id == R.id.seven) {
            startActivity(new Intent(BrowseByCategory.this, ReferNEarnActivity.class));

        } else if (id == R.id.eight) {
            startActivity(new Intent(BrowseByCategory.this, ContactUs.class));
        } else if (id == R.id.nine) {
            startActivity(new Intent(BrowseByCategory.this, HomeChatActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private class MAdaptor extends RecyclerView.Adapter<MAdaptor.ViewHolder> {

        private JSONArray data;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.plan_history, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            try {
                holder.ad_id.setText(data.getJSONObject(position).getString("add_id"));
                holder.sel_plan.setText("Rs " + data.getJSONObject(position).getString("plan") + " + " + data.getJSONObject(position).getString("p_view") + " Views");
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");


                holder.plan_date_one.setText(data.getJSONObject(position).getString("view_date"));
                holder.viewers.setText(data.getJSONObject(position).getString("cashback_code"));

                //holder.viewers.setVisibility(View.GONE);


                holder.itemView.setTag(data.getJSONObject(position));


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        public int getItemCount() {
            return data.length();
        }

        public void setData(JSONArray data) {
            this.data = data;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView sel_plan, ad_id, plan_date_one, viewers;


            TextView user_id, add_id;

            public ViewHolder(View itemView) {
                super(itemView);
                sel_plan = (TextView) itemView.findViewById(R.id.sel_plan);
                ad_id = (TextView) itemView.findViewById(R.id.ad_id);
                plan_date_one = (TextView) itemView.findViewById(R.id.plan_date_one);
                viewers = (TextView) itemView.findViewById(R.id.viewers);

            }
        }

    }


    public void readPhoneContacts() //This Context parameter is nothing but your Activity class's Context
    {
        Context cntx = Classified.getInstance();
        Cursor cursor = cntx.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        Integer contactsCount = cursor.getCount(); // get how many contacts you have in your contacts list
        if (contactsCount > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    //the below cursor will give you details for multiple contacts
                    Cursor pCursor = cntx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    // continue till this cursor reaches to all phone numbers which are associated with a contact in the contact list
                    while (pCursor.moveToNext()) {
                        int phoneType = pCursor.getInt(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                        //String isStarred 		= pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.STARRED));
                        String phoneNo = pCursor.getString(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //you will get all phone numbers according to it's type as below switch case.
                        //Logs.e will print the phone number along with the name in DDMS. you can use these details where ever you want.
                        switch (phoneType) {
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                //Log.e(contactName + ": TYPE_MOBILE", " " + (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString());
                                String m = ((((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).replace("*", "").replace("#", "")).trim().toString();


                                m = m.replaceAll("\\D+", "");

                                String numberOnly = phoneNo.replaceAll("[^0-9]", "");

                                if (m.length() > 10) {
                                    m = m.substring(Math.max(m.length() - 10, 0));
                                }
                                if (m.equals(mobile)) {

                                } else {
                                    contactsname.put(m, contactName);
                                    mobilelist.add(m);
                                }
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                //Log.e(contactName + ": TYPE_HOME", " " + phoneNo);
                                String m1 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m1 = m1.replaceAll("\\D+", "");

                                if (m1.length() > 10) {
                                    m1 = m1.substring(Math.max(m1.length() - 10, 0));
                                }
                                if (m1.equals(mobile)) {

                                } else {
                                    contactsname.put(m1, contactName);
                                    mobilelist.add(m1);
                                }
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                                //Log.e(contactName + ": TYPE_WORK", " " + phoneNo);
                                String m2 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m2 = m2.replaceAll("\\D+", "");


                                if (m2.length() > 10) {
                                    m2 = m2.substring(Math.max(m2.length() - 10, 0));
                                }
                                if (m2.equals(mobile)) {

                                } else {
                                    contactsname.put(m2, contactName);
                                    mobilelist.add(m2);
                                }
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE:
                                //Log.e(contactName + ": TYPE_WORK_MOBILE", " " + phoneNo);
                                String m3 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m3 = m3.replaceAll("\\D+", "");

                                if (m3.length() > 10) {
                                    m3 = m3.substring(Math.max(m3.length() - 10, 0));
                                }
                                if (m3.equals(mobile)) {

                                } else {
                                    contactsname.put(m3, contactName);
                                    mobilelist.add(m3);
                                }
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER:
                                //Log.e(contactName + ": TYPE_OTHER", " " + phoneNo);
                                String m4 = (((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).trim().toString();

                                m4 = m4.replaceAll("\\D+", "");

                                if (m4.length() > 10) {
                                    m4 = m4.substring(Math.max(m4.length() - 10, 0));
                                }
                                if (m4.equals(mobile)) {

                                } else {
                                    contactsname.put(m4, contactName);
                                    mobilelist.add(m4);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    pCursor.close();
                }

            }
            cursor.close();
            try {
                SharedPreferences sharedPreferences = getSharedPreferences("PHONECONTACT", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                int no_mobile = mobilelist.size();
                editor.putInt("phone_no", no_mobile);
                editor.commit();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public class MyBroadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String bd = intent.getExtras().getString("data");
            badge_data = Integer.parseInt(bd);
            //     Toast.makeText(context, bd, Toast.LENGTH_LONG).show();
            //Log.e("badge_data", bd);


            SharedPreferences sh = getSharedPreferences("count", MODE_PRIVATE);
            SharedPreferences.Editor editor = sh.edit();
            editor.putInt("cnt", Integer.parseInt(bd));
            editor.commit();


            try {
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds

                v.vibrate(1000);
                r.play();
/*
                JSONObject j = new JSONObject(DAO.getNotification());
                JSONArray ar = new JSONArray();
                ar.put(bd.getString("msg"));
                ar.put(bd.getString("title"));
                ar.put(bd.getString("link"));
                ar.put(true);
                j.put(bd.getString("key"), ar);
                DAO.storeNotification(j.toString());*/


                //Log.e("badge_data1", badge_data + "");

                //  if (Config.noti_click == 0) {

                if ((badge_data + "").equals("") && (badge_data + "").equals("null") && badge_data == 0) {
                    badge.hide();
                } else {
                    badge.setText(badge_data + "");
                    badge.show();
                }


                // }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }


    public class MyDownloadBroadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String bd = intent.getExtras().getString("data");
            badge_data = Integer.parseInt(bd);
            //     Toast.makeText(context, bd, Toast.LENGTH_LONG).show();
            //Log.e("badge_data", bd);


            try {
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds

                v.vibrate(1000);
                r.play();

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }


}

