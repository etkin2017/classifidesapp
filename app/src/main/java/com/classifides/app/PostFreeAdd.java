package com.classifides.app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.classifides.app.fragment.PostFreeAddOne;
import com.classifides.app.view.MTextView;

public class PostFreeAdd extends AppCompatActivity {
    MTextView close;

    String adid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_free_add);

        try {
             adid = getIntent().getStringExtra("add_id");

            //Log.e("bundle",adid);
            initView();
            addListener();
        }catch (Exception ex){

            initView();
            addListener();

        }



    }


    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Bundle bundle = new Bundle();
        bundle.putString("add_id", adid);

        PostFreeAddOne freeAddOne=new PostFreeAddOne();
        freeAddOne.setArguments(bundle);

        getSupportFragmentManager().beginTransaction().add(R.id.container, freeAddOne, "pstone").commit();
        close = (MTextView) findViewById(R.id.close);
    }

    private void addListener() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
