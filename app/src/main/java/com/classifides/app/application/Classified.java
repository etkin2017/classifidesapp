package com.classifides.app.application;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;

import com.classifides.app.gcm.helper.MyPreferenceManager;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.ads.MobileAds;


public class Classified extends MultiDexApplication {
    private static Classified mInstance;
    private static Context myContext;

    private MyPreferenceManager pref;
    public static final String TAG = Classified.class.getSimpleName();

    private RequestQueue mRequestQueue;

    private static final RetryPolicy VOLLEY_REQUEST_RETRY_POLICY = new DefaultRetryPolicy(
            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 10,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);


    public static String updateStatus = "http://catchmeon.net/catchmeon/updateStatusById.php";
    public static String updateGroup = "http://catchmeon.net/catchmeon/editGroup.php";
    public static String upImage ="http://catchmeon.net/catchmeon/up.php";


    //192.168.43.233:3333
    public static final String APP_API_IP = "http://catchmeon.net/catchmeon/";
    public static final String APP_API_URL = APP_API_IP + "contacts.php";
    public static final String APP_SEND_MSG = APP_API_IP + "sendNotif.php";
    public static final String UPDATE_CHAT = "http://catchmeon.net/catchmeon/updateGcm.php";
    public static final String CREATE_GROUP = "http://catchmeon.net/catchmeon/cGroup.php";
    public static final String SEND_TO_GROUP = "http://catchmeon.net/catchmeon/stog.php";

    public static final String leave_Group = "http://catchmeon.net/catchmeon/leaveGroup.php";

    public static final String deleteGroup = "http://catchmeon.net/catchmeon/deleteGroup.php";

    public static final String INSERT_MSG = "http://catchmeon.net/catchmeon/sendNotif.php";

    public static final String Group_Details ="http://catchmeon.net/catchmeon/group_details.php";

    public static final String GetChatByRdiSid = "http://192.168.1.101:3333/GCM/gcm_chat/show_chat_by_rid_sid.php";

    public static final String uploadVideo ="http://catchmeon.net/catchmeon/uploadvideo.php";

    // public static final String ONLY_CONTCAT="http://192.168.1.103:3333/GCM/gcm_chat/onlycontact.php";


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

    }


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        myContext = getApplicationContext();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-2758807869387921~3087322491");


    }

    public RequestQueue getRequestQueue() {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(myContext);

        }

        return mRequestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        req.setRetryPolicy(VOLLEY_REQUEST_RETRY_POLICY);
        VolleyLog.d("Adding request to queue: %s", req.getUrl());
        getRequestQueue().add(req);
    }


    public <T> void addToReqQueue(Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(TAG);
        req.setRetryPolicy(VOLLEY_REQUEST_RETRY_POLICY);
        getRequestQueue().add(req);

    }

    public <T> void addToRequestQueue(Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(TAG);
        req.setRetryPolicy(VOLLEY_REQUEST_RETRY_POLICY);
        getRequestQueue().add(req);

    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


    public MyPreferenceManager getPrefManager() {
        if (pref == null) {
            pref = new MyPreferenceManager(this);
        }

        return pref;
    }


    public static synchronized Classified getInstance() {
        return mInstance;
    }

   /* public void logout() {
        pref.clear();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
*/
}
