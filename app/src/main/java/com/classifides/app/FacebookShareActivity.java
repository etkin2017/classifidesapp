package com.classifides.app;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.classifides.app.application.Classified;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareMedia;
import com.facebook.share.model.ShareMediaContent;
import com.facebook.share.model.ShareOpenGraphAction;
import com.facebook.share.model.ShareOpenGraphContent;
import com.facebook.share.model.ShareOpenGraphObject;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.widget.ShareDialog;

public class FacebookShareActivity extends AppCompatActivity {
    ShareDialog shareDialog;
    CallbackManager callbackManager;
    String apk_link = "";
    String title = "";
    String description = "";
    String image = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_share);

        FacebookSdk.sdkInitialize(Classified.getInstance());
        shareDialog = new ShareDialog(this);

        callbackManager = CallbackManager.Factory.create();
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {

            @Override
            public void onSuccess(Sharer.Result result) {
                onBackPressed();
            }

            @Override
            public void onCancel() {

                onBackPressed();
            }

            @Override
            public void onError(FacebookException error) {

                error.printStackTrace();
                Log.e("Newtork ", "Problem");
                onBackPressed();
            }
        });

        try {
            try {
                apk_link = getIntent().getStringExtra("apk_link");
                title = getIntent().getStringExtra("title");
                image = getIntent().getStringExtra("image");
                description = getIntent().getStringExtra("desc");

                if (!apk_link.equals("null")) {
                    if (ShareDialog.canShow(ShareLinkContent.class)) {
                        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                               // .setContentUrl(Uri.parse("http://catchmeon.net/catchmeon/rrl.php"))
                              //  .setContentUrl(Uri.parse("http://spiritsecret.com/catchmeon/rrl.php"))
                                .setContentUrl(Uri.parse("http://catchmeon.net/catchmeon/ply_str.html"))
                                .setContentTitle("Refer and Earn")
                                .setImageUrl(Uri.parse("http://catchmeon.net/catchmeon/applogo.png"))
                                .setContentDescription(apk_link)
                                .build();

                        shareDialog.show(linkContent, ShareDialog.Mode.AUTOMATIC);

                        // ShareApi.share(linkContent, new FacebookCallback<Sharer.Result>() {...});
                    }








                }

            } catch (Exception e) {
                e.printStackTrace();
                onBackPressed();
            }
            try {
                if ((!title.equals("null"))) {
                    if (ShareDialog.canShow(ShareLinkContent.class)) {
                        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                              //  .setContentUrl(Uri.parse("http://catchmeon.net/catchmeon/rrl.php"))
                              //  .setContentUrl(Uri.parse("http://spiritsecret.com/catchmeon/rrl.php"))
                                .setContentUrl(Uri.parse("http://catchmeon.net/catchmeon/ply_str.html"))
                                .setContentTitle("Hey, Check out Catch Me On App . It has awesome ads and my ad is " + title)
                                .setImageUrl(Uri.parse(image))
                                .setContentDescription(description)
                                .build();


                        shareDialog.show(linkContent, ShareDialog.Mode.AUTOMATIC);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                onBackPressed();
            }


        } catch (Exception e) {
            e.printStackTrace();
            onBackPressed();
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }


}
