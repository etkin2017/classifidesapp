package com.classifides.app;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.classifides.app.data.Common;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.view.MTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    EditText name, mob, email, pass, confpass;
    RadioGroup sex_grp;
    RadioButton male, female;
    LinearLayout createAccount;
    MTextView goToLogin, result, dp;
    private ProgressDialog pd;

    LinearLayout date_picker;
    Calendar myCalendar = Calendar.getInstance();

    private void initView() {

        date_picker = (LinearLayout) findViewById(R.id.date_picker);
        dp = (MTextView) findViewById(R.id.dp);
        name = (EditText) findViewById(R.id.et_name);
        mob = (EditText) findViewById(R.id.et_mob);
        email = (EditText) findViewById(R.id.et_email);
        pass = (EditText) findViewById(R.id.et_password);
        confpass = (EditText) findViewById(R.id.et_confpass);
        male = (RadioButton) findViewById(R.id.sex_male);
        female = (RadioButton) findViewById(R.id.sex_female);
        goToLogin = (MTextView) findViewById(R.id.gotologin);
        result = (MTextView) findViewById(R.id.result);
        createAccount = (LinearLayout) findViewById(R.id.createreg);
        sex_grp = (RadioGroup) findViewById(R.id.sex_group);

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        name.setTypeface(face);
        mob.setTypeface(face);
        email.setTypeface(face);
        pass.setTypeface(face);
        confpass.setTypeface(face);
        male.setTypeface(face);
        female.setTypeface(face);
        pd = new ProgressDialog(this);

        date_picker.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(RegisterActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {

        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dp.setText(sdf.format(myCalendar.getTime()));
    }


    String semail;
    String sconfpass123;

    private void initListener() {
        goToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            }
        });
        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sname = name.getText().toString().trim();
                String smob = mob.getText().toString().trim();
                semail = email.getText().toString().trim();
                String spass = pass.getText().toString().trim();
                String sconfpass = confpass.getText().toString().trim();
                sconfpass123 = confpass.getText().toString().trim();
                int selectedId = sex_grp.getCheckedRadioButtonId();
                String sex = ((RadioButton) findViewById(selectedId)).getText().toString();

                result.setVisibility(View.GONE);
                if (TextUtils.isEmpty(sname)) {
                    result.setText("please fill the Name.");
                    result.setVisibility(View.VISIBLE);
                    return;
                }
                if (TextUtils.isEmpty(smob)) {
                    result.setText("please fill the Mobile.");
                    result.setVisibility(View.VISIBLE);
                    return;
                } else if (smob.length() < 10) {
                    result.setText("please fill proper Mobile Number.");
                    result.setVisibility(View.VISIBLE);
                    return;
                }
                if (!Common.isValidEmail(semail)) {
                    result.setText("please enter valid Email");
                    result.setVisibility(View.VISIBLE);
                    return;
                }
                if (TextUtils.isEmpty(sex)) {
                    result.setText("please select the Gender.");
                    result.setVisibility(View.VISIBLE);
                    return;
                }

                if (TextUtils.isEmpty(spass)) {
                    result.setText("please fill the Password.");
                    result.setVisibility(View.VISIBLE);
                    return;
                } else if (spass.length() < 6) {
                    result.setText("Password must be of 6 character.");
                    result.setVisibility(View.VISIBLE);
                    return;
                }
                if (TextUtils.isEmpty(sconfpass)) {
                    result.setText("please fill the Confirm Password.");
                    result.setVisibility(View.VISIBLE);
                    return;
                }

                if (!Common.isPasswordMatching(spass, sconfpass)) {
                    result.setText("Password and Confirm Password not mached");
                    result.setVisibility(View.VISIBLE);
                    return;
                }


                Map<String, String> map = new HashMap<String, String>();
                map.put("name", sname);
                map.put("mobile", smob);
                map.put("email", semail);
                map.put("image", semail.substring(0, semail.indexOf("@")));
                map.put("sex", sex);
                map.put("password", spass);
                map.put("dob", dp.getText().toString());
                registerUser(map);

            }
        });


    }

    private void registerUser(final Map<String, String> map) {
        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ProgressDialog();
            }

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    res = DAO.getJsonFromUrl(Config.Server + "register.php", map);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                DismissProgress();


                try {
                    if (res.getInt("success") > 0) {
                        Toast.makeText(RegisterActivity.this, res.getString("message"), Toast.LENGTH_LONG).show();

                        SendMail sm = new SendMail(RegisterActivity.this, semail, "Catch Me On App  ", "<!DOCTYPE html>\n" +
                                "<html>\n" +
                                "<body bgcolor=\"#FF9800\"> <b> <h1>Congratulations!! Registration is Successful. </h1></b> </br> <h3>Thank you for Joining the Catch Me On App. <br>Your Catch Me On's Email id is <b> <u>" + semail + "</u></b> AND password is <b> <u>" + sconfpass123 + "</u> </b>. <br></h3>  <h4>We hope you enjoy your stay on Catch Me On App. If you have any problems, questions , opinions, praise, comments , suggestions, Please feel to free not to contact us at any time.</h4> <br> <br>  Warm Regards, <br> " + Config.EMAIL + "</body> </html>");


                        //   SendMail sm = new SendMail(RegisterActivity.this, semail, "Classifieds App  " , R.string.sample0_string+ semail +" AND password is "+sconfpass123 +R.string.sample1_string );

                        //Executing sendmail to send email
                        sm.execute();

                         finish();

                     //   startActivity(new Intent(RegisterActivity.this, LoginActivity.class));

                    } else {
                        result.setText(res.getString("message"));
                        result.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    result.setText("Error in Registration");
                    result.setVisibility(View.VISIBLE);
                }
            }
        }.execute(null, null, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();
        initListener();
    }


    public void ProgressDialog() {
        pd.setMessage("While we creating your account on" + getResources().getString(R.string.app_name));
        pd.setTitle("Please wait....");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setIndeterminate(true);
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    public void DismissProgress() {
        pd.dismiss();
    }


}
