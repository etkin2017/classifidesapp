package com.classifides.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.annotation.IntegerRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.classifides.app.application.Classified;
import com.classifides.app.data.Blur;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.fragment.PostFreeAddTwo;
import com.classifides.app.view.MTextView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RedeemCashbackCode extends AppCompatActivity {


    MTextView total_amt;
    EditText cashback_code;
    LinearLayout redeem_ok, redeem_total_amt;
    SharedPreferences shp, sh;

    SharedPreferences.Editor ed;
    String TAG = "RedeemCashbackCode";

    ProgressDialog pd;

    RecyclerView redeem_cashback_code;
    MAdaptor adaptor;

    int pp = 0;

    public static String code_s = "";

    String cashback_amt;

    Calendar c = Calendar.getInstance();
    SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy");
    String cdate = df.format(c.getTime());

    LinearLayout note;

    int id1;

    MTextView close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem_cashback_code);

        note = (LinearLayout) findViewById(R.id.note);
        shp = getApplicationContext().getSharedPreferences(TAG, Context.MODE_PRIVATE);

        close = (MTextView) findViewById(R.id.close);

        pd = new ProgressDialog(RedeemCashbackCode.this);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        try {
            JSONObject user = DAO.getUser();
            id1 = user.getInt("id");
            //Log.e("id1", id1 + "");
            getUserInfo(id1 + "");

        } catch (Exception e) {
            e.printStackTrace();
        }

        adaptor = new MAdaptor();
        redeem_cashback_code = (RecyclerView) findViewById(R.id.redeem_cashback_code);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        redeem_cashback_code.setLayoutManager(llm);


        total_amt = (MTextView) findViewById(R.id.total_amt);
        cashback_code = (EditText) findViewById(R.id.cashback_code);
        redeem_ok = (LinearLayout) findViewById(R.id.redeem_ok);
        redeem_total_amt = (LinearLayout) findViewById(R.id.redeem_total_amt);
        cashback_code.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        // .setKeyListener(DigitsKeyListener.getInstance("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"));
        redeem_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(cashback_code.getText().toString())) {
                    cashback_code.setError("enter cashback code");
                } else {
                    checkCode(cashback_code.getText().toString());
                }
            }
        });

        String ta = shp.getString("total_amt", "");


        try {
            JSONObject user = DAO.getUser();
            cashback_amt = user.getString("cashback_amt");
            //Log.e("cashback_amt", cashback_amt + "");

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (TextUtils.isEmpty(cashback_amt) || cashback_amt.equals("null")) {
            total_amt.setText("");
            pp = 0;
        } else {
            //Log.e("ta", cashback_amt);
            // total_amt.setText(cashback_amt);
            //  pp = pp + Integer.parseInt(cashback_amt);
        }

        redeem_total_amt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(total_amt.getText().toString()) < 200) {
                    note.setVisibility(View.VISIBLE);
                } else {

                    //Log.e("code_s", code_s);
                    redeemCashCode(total_amt.getText().toString(), code_s);
                    Date d = new Date();
                    //  SimpleDateFormat sdf=new SimpleDateFormat("hh:mm a");
                    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
                    final String currentDateTimeString = sdf.format(d);
                 /*   int id = 0;
                    try {
                        JSONObject user = DAO.getUser();

                        id = user.getInt("id");
                        //Log.e("user_id", id + "");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("amount", total_amt.getText().toString());
                    map.put("r_date", cdate + " " + currentDateTimeString);
                    map.put("user_id", id + "");
                    map.put("cash_code", code_s);

                    JSONObject jobj1 = new JSONObject(map);
                    Intent i = new Intent(RedeemCashbackCode.this, PaymenActivity.class);
                    i.putExtra("submit_ad", jobj1.toString());
                    i.putExtra("which", "1");
                    startActivity(i);*/

/*
                    Bundle b = new Bundle();


                    PaymentFragment obj = new PaymentFragment();
                    b.putString("submit_ad", jobj1.toString());
                    obj.setArguments(b);

                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.container, obj, "ps2").addToBackStack("ps2").commit();*/


                }
            }
        });


        //  getRedeemRequest();

    }


    @Override
    protected void onResume() {
        super.onResume();
        getUserInfo(id1 + "");
    }

    public void getUserInfo(final String user_id) {

        final Map<String, String> data = new HashMap<String, String>();


        new AsyncTask() {
            JSONObject res = null;
            ProgressDialog pdd;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();


                data.put("id", user_id);

            /*    try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
            }

            @Override
            protected Object doInBackground(Object[] params) {
                res = DAO.getJsonFromUrl(Config.userInfo, data);

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                try {
                    if (res.getInt("success") > 0) {

                        JSONArray jsonArray = res.getJSONArray("user");
                        // Log.e("JsONArray", jsonArray.toString());
                        JSONObject jobj = jsonArray.getJSONObject(0);

                        sh = getSharedPreferences("user", Context.MODE_PRIVATE);
                        sh.edit().putString("users", res.getJSONArray("user").getJSONObject(0).toString()).commit();
                        String cash_amt = res.getJSONArray("user").getJSONObject(0).getString("cashback_amt").trim().toString();
                        if (cash_amt.equals("null")) {
                            total_amt.setText("0");
                            //  pp = pp + 0;
                            pp = 0;
                        } else {
                            total_amt.setText(res.getJSONArray("user").getJSONObject(0).getString("cashback_amt").trim().toString());
                            pp = Integer.parseInt(res.getJSONArray("user").getJSONObject(0).getString("cashback_amt").trim().toString());

                        }


                    } else {
                        //  Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                getRedeemRequest();
            }
        }.execute(null, null, null);
    }

    public void redeemCashCode(final String amount, final String c_code) {

        Date d = new Date();
        //  SimpleDateFormat sdf=new SimpleDateFormat("hh:mm a");
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
        final String currentDateTimeString = sdf.format(d);

        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pd.setCancelable(false);
                pd.setMessage("Please Wait");
                pd.show();

            }

            @Override
            protected Object doInBackground(Object[] params) {
                int id = 0;
                try {
                    JSONObject user = DAO.getUser();

                    id = user.getInt("id");
                    //Log.e("user_id", id + "");

                } catch (Exception e) {
                    e.printStackTrace();
                }

                Map<String, String> map = new HashMap<String, String>();
                map.put("amount", amount);
                map.put("r_date", cdate + " " + currentDateTimeString);
                map.put("user_id", id + "");
                map.put("cash_code", c_code);

                map.put("payment_status","");
                map.put("txtid", "");



                rest = DAO.getJsonFromUrl(Config.insertRedeemCode, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                pd.dismiss();
                try {


                    if (rest.getInt("success") > 0) {

                        ed = shp.edit();
                        ed.putString("total_amt", 0 + "");
                        ed.commit();
                        total_amt.setText("0");
                        Toast.makeText(RedeemCashbackCode.this, "added", Toast.LENGTH_SHORT).show();
                        getRedeemRequest();
                        //Log.e("main_code_s", code_s);
                       /* String[] splited = code_s.split("\\s+");

                        //Log.e("spilted", splited.toString());
                        for (int i = 0; i < splited.length; i++) {
                            updateCashcode(splited[i], total_amt.getText().toString());
                        }
*/


                        pp = 0;
                    } else {
                        Toast.makeText(RedeemCashbackCode.this, "not added", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);


    }


    private void updateCashcode(final String cash_code, final String cashback_amt, final ProgressDialog progressDialog) {

        new AsyncTask() {
            ProgressDialog p;
            JSONObject res;
            int id = 0;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            /*    progressDialog = new ProgressDialog(RedeemCashbackCode.this);

                progressDialog.setTitle("Please wait....");
                progressDialog.setCancelable(false);*/

                //  progressDialog.show();

               /* pd.setCancelable(false);
                pd.setMessage("Please Wait");
                pd.show();*/

                redeem_ok.setClickable(false);

                try {
                    JSONObject user = DAO.getUser();

                    id = user.getInt("id");
                    //Log.e("user_id", id + "");

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    Map<String, String> d = new HashMap<String, String>();
                    d.put("cash_code", cash_code);
                    d.put("cashback_amt", cashback_amt);
                    d.put("id", id + "");


                    res = DAO.getJsonFromUrl(Config.updateCashCode, d);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                progressDialog.dismiss();
                redeem_ok.setClickable(true);
                try {
                    if (res.getInt("success") > 0) {

                        Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_LONG).show();
                        //     setValues();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "error in updation", Toast.LENGTH_LONG).show();
                }
            }
        }.execute(null, null, null);

    }


    public void checkCode(final String code) {


        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pd.setCancelable(false);
                pd.setMessage("Please Wait");
                pd.show();


            }

            @Override
            protected Object doInBackground(Object[] params) {


                int id = 0;
                try {
                    JSONObject user = DAO.getUser();

                    id = user.getInt("id");
                    //Log.e("user_id", id + "");

                } catch (Exception e) {
                    e.printStackTrace();
                }


                Map<String, String> map = new HashMap<String, String>();
                map.put("code", code);
                map.put("user_id", id + "");
                rest = DAO.getJsonFromUrl(Config.CheckCode, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                try {


                    if (rest.getInt("success") > 0) {
                        String plan = rest.getJSONArray("user").getJSONObject(0).getString("plan");

                        int p = Integer.parseInt(plan) / 2;
                        pp = pp + p;
                        total_amt.setText(pp + "");
                        code_s = code_s + " " + code;
                        cashback_code.setText("");

                        ed = shp.edit();
                        ed.putString("total_amt", total_amt.getText().toString());
                        ed.commit();

                        updateCashcode(code, total_amt.getText().toString(), pd);
                        /*if (Integer.parseInt(plan) >= 200) {

                        } else {
                            cashback_code.setText("");
                            cashback_code.setError("Minimum request amount should be atleast Rs 200");
                        }*/


                    } else {
                        pd.dismiss();
                        Toast.makeText(RedeemCashbackCode.this, rest.getString("message"), Toast.LENGTH_SHORT).show();
                        cashback_code.setText("");
                        cashback_code.setError("this code is already");
                    }

                } catch (JSONException e) {
                    pd.dismiss();
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);


    }

    public void getRedeemRequest() {

        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Object doInBackground(Object[] params) {

                int id = 0;
                try {
                    JSONObject user = DAO.getUser();

                    id = user.getInt("id");
                    //Log.e("user_id", id + "");

                } catch (Exception e) {
                    e.printStackTrace();
                }

                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", id + "");
                rest = DAO.getJsonFromUrl(Config.getRedeemRequest, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {


                    if (rest.getInt("success") > 0) {
                        adaptor.setData(rest.getJSONArray("request"));
                        redeem_cashback_code.setAdapter(adaptor);
                    } else {


                        adaptor.setData(new JSONArray());
                        redeem_cashback_code.setAdapter(adaptor);
                        Toast.makeText(RedeemCashbackCode.this, rest.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);


    }


    private class MAdaptor extends RecyclerView.Adapter<MAdaptor.ViewHolder> {

        private JSONArray data;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.redeem_history, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            try {

                String d = data.getJSONObject(position).getString("status_c");
                if (d.equals("1")) {
                    holder.status_r.setText("Request sent");
                } else {
                    holder.status_r.setText("No");
                }

                holder.amt.setText("Rs " + data.getJSONObject(position).getString("amount"));
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");


                holder.r_date.setText(data.getJSONObject(position).getString("r_date"));
                // holder.viewers.setText(data.getJSONObject(position).getString("cashback_code"));

                //holder.viewers.setVisibility(View.GONE);


                holder.itemView.setTag(data.getJSONObject(position));


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        public int getItemCount() {
            return data.length();
        }

        public void setData(JSONArray data) {
            this.data = data;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView status_r, amt, r_date, viewers;


            TextView user_id, add_id;

            public ViewHolder(View itemView) {
                super(itemView);
                status_r = (TextView) itemView.findViewById(R.id.status_r);
                amt = (TextView) itemView.findViewById(R.id.amt);
                r_date = (TextView) itemView.findViewById(R.id.r_date);


            }
        }
    }
}
