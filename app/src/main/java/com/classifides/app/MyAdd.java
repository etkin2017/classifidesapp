package com.classifides.app;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.classifides.app.Adaptor.MyAddAdapter;
import com.classifides.app.application.Classified;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.fragment.PostBannerAddOne;
import com.classifides.app.fragment.PostFreeAddOne;
import com.classifides.app.view.MTextView;
import com.squareup.picasso.Picasso;
import com.universalvideoview.UniversalMediaController;
import com.universalvideoview.UniversalVideoView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;

public class MyAdd extends AppCompatActivity {
    RecyclerView prod;
    List<MyAddPojo> arr_anp;

    MAdaptor adaptor;
    //   LinearLayout tobar;

    private MTextView close;
    MTextView update, cashback, addseting;
    ProgressDialog pd;
    public static ArrayList<String> user_id, add_id, type;


    String TAG = "MyAdd";

    boolean d;
    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();

    // We can be in one of these 3 states
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;

    // Remember some things for zooming
    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 1f;


    private void initView() {

        //  tobar = (LinearLayout) findViewById(R.id.tobar);

        arr_anp = new ArrayList<MyAddPojo>();
        prod = (RecyclerView) findViewById(R.id.postrec);
        //  prod.setLayoutManager(new LinearLayoutManager(this));
        close = (MTextView) findViewById(R.id.close);
        update = (MTextView) findViewById(R.id.updateadd);
        cashback = (MTextView) findViewById(R.id.cashback);
        addseting = (MTextView) findViewById(R.id.adsetting);
        pd = new ProgressDialog(MyAdd.this);

        user_id = new ArrayList<String>();
        add_id = new ArrayList<String>();
        type = new ArrayList<String>();

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        prod.setLayoutManager(llm);
        // prod.setAdapter( adapter );

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        adaptor = new MAdaptor();

        initView();
        addListener();
        getProducts();
    }

    private void addListener() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e("MyAdd_Add_id", MyAdd.add_id + "");

                if (MyAdd.add_id.size() == 1) {
                    Intent i = new Intent(MyAdd.this, UpdateAsValidity.class);
                    i.putExtra("user_id", MyAdd.user_id.get(0).toString());
                    i.putExtra("add_id", MyAdd.add_id.toString());
                    startActivity(i);

                } else {

                    Toast.makeText(getApplicationContext(), "Please Select One Ad", Toast.LENGTH_LONG).show();

                    MyAdd.user_id.clear();
                    MyAdd.add_id.clear();
                }
            }
        });

        cashback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Log.e("cashback", MyAdd.add_id + "");

                if (MyAdd.add_id.size() == 1) {
                    Intent i = new Intent(MyAdd.this, CashbackCode.class);
                    i.putExtra("user_id", MyAdd.user_id.get(0).toString());
                    i.putExtra("add_id", MyAdd.add_id.toString());
                    startActivity(i);

                } else {

                    Toast.makeText(getApplicationContext(), "Please Select One Ad", Toast.LENGTH_LONG).show();
                    MyAdd.user_id.clear();
                    MyAdd.add_id.clear();
                }


            }
        });
        addseting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(MyAdd.this, addseting);
                popup.getMenuInflater().inflate(R.menu.add_setting, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        //   Toast.makeText(MyAdd.this, "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                        //Log.e("setting", MyAdd.add_id + "");

                        int id = item.getItemId();

                        if (id == R.id.act_ad) {


                            String id_one = "1";

                          /*  for (int i = 0;i <= MyAdd.add_id.size()-1; i++) {

                                if(i<(MyAdd.add_id.size()-1))
                                    activateAd(MyAdd.user_id.get(i), MyAdd.add_id.get(i));


                            }
*/
                            inactivateID(MyAdd.user_id, MyAdd.add_id, id_one);


                        }
                        if (id == R.id.inact_ad) {

                            String id_two = "0";
                            inactivateID(MyAdd.user_id, MyAdd.add_id, id_two);
                        }
                        if (id == R.id.edt_ad) {


                            if (MyAdd.add_id.size() == 1) {


                                if (MyAdd.type.get(0).equals("0")) {

                                    Intent in = new Intent(MyAdd.this, PostFreeAdd.class);
                                    in.putExtra("add_id", MyAdd.add_id.get(0));
                                    startActivity(in);


                                } else if (MyAdd.type.get(0).equals("1")) {

                                    Intent in = new Intent(MyAdd.this, PostVideoAdd.class);
                                    in.putExtra("add_id", MyAdd.add_id.get(0));
                                    startActivity(in);
                                } else {
                                    Intent in = new Intent(MyAdd.this, PostBannerAdd.class);
                                    in.putExtra("add_id", MyAdd.add_id.get(0));
                                    startActivity(in);
                                }

                            } else {
                                Toast.makeText(getApplicationContext(), "Plz Select One Ad", Toast.LENGTH_LONG).show();
                                MyAdd.add_id.clear();
                                MyAdd.user_id.clear();
                            }

                        }
                        if (id == R.id.del_ad) {


                            if (MyAdd.add_id.size() == 1) {


                                AlertDialog.Builder builder = new AlertDialog.Builder(MyAdd.this);

                                builder.setMessage("Do you wnat to delete this Ad?");

                                String positiveText = getString(android.R.string.ok);
                                builder.setPositiveButton(positiveText,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                // positive button logic

                                                delAd(MyAdd.add_id.get(0));
                                            }
                                        });

                                String negativeText = getString(android.R.string.cancel);
                                builder.setNegativeButton(negativeText,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                // negative button logic

                                                dialog.dismiss();
                                            }
                                        });

                                AlertDialog dialog = builder.create();
                                // display dialog
                                dialog.show();

                            } else {
                                Toast.makeText(getApplicationContext(), "Plz Select  One Ad", Toast.LENGTH_LONG).show();
                                MyAdd.add_id.clear();
                                MyAdd.user_id.clear();
                            }


                        }

                        return true;
                    }
                });
                popup.show();
            }
        });
    }


    public void refresh() {          //refresh is onClick name given to the button
        onRestart();
    }

    @Override
    protected void onRestart() {

        // TODO Auto-generated method stub
        super.onRestart();
        Intent i = new Intent(MyAdd.this, MyAdd.class);  //your class
        startActivity(i);
        finish();

    }


    private void inactivateID(final ArrayList<String> ar_user_id, final ArrayList<String> ar_add_id, final String o) {
        new AsyncTask() {


            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                pd = new ProgressDialog(MyAdd.this);
                pd.setMessage("We are updating Ad detail");
                pd.setTitle("Please wait....");
                pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                pd.setIndeterminate(true);
                pd.setCanceledOnTouchOutside(false);
                pd.show();

            }

            @Override
            protected Object doInBackground(Object[] objects) {

                for (int i = 0; i < ar_add_id.size(); i++) {
                    deactivateAd(ar_user_id.get(i), ar_add_id.get(i), o);

                }


                return null;
            }


            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                refresh();

                pd.dismiss();
            }
        }.execute(null, null, null);

    }


    private void delAd(final String add_id) {

        new AsyncTask() {
            ProgressDialog p;
            JSONObject res;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
              /*  p = new ProgressDialog(MyAdd.this);
                pd.setMessage("We are updating Ad detail");
                pd.setTitle("Please wait....");
                pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                pd.setIndeterminate(true);
                pd.setCanceledOnTouchOutside(false);
                pd.show();*/
            }

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    Map<String, String> d = new HashMap<String, String>();
                    d.put("id", add_id);

                    res = DAO.getJsonFromUrl(Config.delAdd, d);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
              /*  pd.dismiss();*/
                try {
                    if (res.getInt("success") > 0) {

                        MAdaptor adaptor1 = new MAdaptor();
                        Toast.makeText(getApplicationContext(), "Your has been deleted", Toast.LENGTH_LONG).show();
                        adaptor1.notifyDataSetChanged();
                        refresh();

                    } else {
                        Toast.makeText(getApplicationContext(), "Ad not deleted", Toast.LENGTH_LONG).show();
                        //     setValues();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "error in deletion", Toast.LENGTH_LONG).show();
                }
            }
        }.execute(null, null, null);

    }

    private void activateAd(final String user_id, final String add_id) {

        new AsyncTask() {
            ProgressDialog p;
            JSONObject res;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
              /*  p = new ProgressDialog(MyAdd.this);
                pd.setMessage("We are updating Ad detail");
                pd.setTitle("Please wait....");
                pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                pd.setIndeterminate(true);
                pd.setCanceledOnTouchOutside(false);
                pd.show();*/
            }

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    Map<String, String> d = new HashMap<String, String>();
                    d.put("user_id", user_id);
                    d.put("flag", "1");
                    d.put("add_id", add_id);

                    res = DAO.getJsonFromUrl(Config.ActivateAd, d);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
              /*  pd.dismiss();*/
                try {
                    if (res.getInt("success") > 0) {

                        Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_LONG).show();
                        //     setValues();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "error in updation", Toast.LENGTH_LONG).show();
                }
            }
        }.execute(null, null, null);

    }


    private void deactivateAd(final String user_id, final String add_id, final String oo) {

        new AsyncTask() {
            ProgressDialog p;
            JSONObject res;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
              /*  p = new ProgressDialog(MyAdd.this);
                pd.setMessage("We are updating Ad detail");
                pd.setTitle("Please wait....");
                pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                pd.setIndeterminate(true);
                pd.setCanceledOnTouchOutside(false);
                pd.show();*/
            }

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    Map<String, String> d = new HashMap<String, String>();
                    d.put("user_id", user_id);
                    d.put("flag", oo);
                    d.put("add_id", add_id);

                    res = DAO.getJsonFromUrl(Config.ActivateAd, d);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
              /*  pd.dismiss();*/
                try {
                    if (res.getInt("success") > 0) {

                        Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_LONG).show();
                        //     setValues();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                  //  Toast.makeText(getApplicationContext(), "error in updation", Toast.LENGTH_LONG).show();
                }
            }
        }.execute(null, null, null);

    }

    public void pDialog() {

        pd.setMessage("We are updating Ad detail");
        pd.setTitle("Please wait....");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setIndeterminate(true);
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    private void getProducts() {

        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Object doInBackground(Object[] params) {
                int id = 0;
                try {
                    JSONObject user = DAO.getUser();
                    id = user.getInt("id");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Map<String, String> map = new HashMap<String, String>();
                map.put("id", id + "");
                rest = DAO.getJsonFromUrl(Config.Server + "AdvertiseById.php", map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {

                    if (rest.getInt("success") > 0) {


                        for (int i = 0; i < rest.getJSONArray("advertis").length(); i++) {
                            JSONObject build = rest.getJSONArray("advertis").getJSONObject(i);

                           // Log.e("Advertise", build.toString());

                            MyAddPojo thumb = new MyAddPojo();


                            thumb.setTitle(build.getString("title"));
                            ;

                            thumb.setFlag(build.getString("flag"));

                            thumb.setDesc(build.getString("description"));
                            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");


                            thumb.setDated(build.getString("postTime"));
                            thumb.setCost(build.getString("price"));
                            thumb.setCat(build.getString("category"));
                            thumb.setSubcat(build.getString("subcategory"));

                            thumb.setUser_id(build.getString("user_id"));
                            thumb.setAdd_id(build.getString("add_id"));
                            thumb.setType(build.getInt("type") + "");


                            thumb.setImg_path(build.getString("imageLoc"));

                            thumb.setImageCount(build.getInt("imageCount") + "");
                            thumb.setCash_discount(build.getString("cash_discount") + "");
                            thumb.setDiscount(build.getInt("discount") + "");

                            thumb.setCity(build.getString("city"));
                            thumb.setPlaceid(build.getString("placeid"));
                            thumb.setNearby(build.getString("nearby"));
                            thumb.setV(build.getInt("v") + "");

                            //  int type = build.getInt("type");


                            arr_anp.add(thumb);
                        }
                        // customerAdapter.setData(arr_anp);

                        adaptor.setData(getApplicationContext(), arr_anp);
                        prod.setAdapter(adaptor);
                    } else {
                       // Toast.makeText(getApplicationContext(), "No Data is Present", Toast.LENGTH_SHORT).show();
                    }
                      /*  adaptor.setData(getApplicationContext(), rest.getJSONArray("advertis"));
                        prod.setAdapter(adaptor);
                    } else {
                        adaptor = new MyAddAdapter();
                        adaptor.setData(getApplicationContext(), new JSONArray());
                        prod.setAdapter(adaptor);
                        Toast.makeText(MyAdd.this, rest.getString("message"), Toast.LENGTH_SHORT).show();
                    }*/

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

    /*public void clearAnimation() {
        prod.clearAnimation();
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            super.onBackPressed();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public class MyAddPojo {

        String placeid, nearby, city, cat_id, discount, cash_discount, imageCount, v, flag;

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getPlaceid() {
            return placeid;
        }

        public void setPlaceid(String placeid) {
            this.placeid = placeid;
        }

        public String getNearby() {
            return nearby;
        }

        public void setNearby(String nearby) {
            this.nearby = nearby;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCat_id() {
            return cat_id;
        }

        public void setCat_id(String cat_id) {
            this.cat_id = cat_id;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getCash_discount() {
            return cash_discount;
        }

        public void setCash_discount(String cash_discount) {
            this.cash_discount = cash_discount;
        }

        public String getImageCount() {
            return imageCount;
        }

        public void setImageCount(String imageCount) {
            this.imageCount = imageCount;
        }

        public String getV() {
            return v;
        }

        public void setV(String v) {
            this.v = v;
        }

        String title, desc, dated, cost, cat, subcat, img_path;
        boolean setChk;

        String user_id, add_id, type;

        public String getImg_path() {
            return img_path;
        }

        public void setImg_path(String img_path) {
            this.img_path = img_path;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getDated() {
            return dated;
        }

        public void setDated(String dated) {
            this.dated = dated;
        }

        public String getCat() {
            return cat;
        }

        public void setCat(String cat) {
            this.cat = cat;
        }

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }

        public String getSubcat() {
            return subcat;
        }

        public void setSubcat(String subcat) {
            this.subcat = subcat;
        }

        public boolean getSetChk() {
            return setChk;
        }

        public void setSetChk(boolean setChk) {
            this.setChk = setChk;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getAdd_id() {
            return add_id;
        }

        public void setAdd_id(String add_id) {
            this.add_id = add_id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    private class MAdaptor extends RecyclerView.Adapter<MAdaptor.ViewHolder> implements View.OnTouchListener {


        Context ctx;
        List<MyAdd.MyAddPojo> name;
        ArrayList<Integer> chk_p;
        boolean[] itemChecked;
        int lastPosition = -1;


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_row_for_my_add, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            try {
                final MyAddPojo dataItem = name.get(position);
              /*  setAnimation(holder.itemView, position);*/
                //   final MyViewHolder myViewHolder = (MyViewHolder) viewHolder;
               /* myViewHolder.progressplay.setMax(dataItem.getMax());
                myViewHolder.progressplay.setProgress(dataItem.getPosition());*/

                holder.title.setText(dataItem.getTitle());
                holder.desc.setText(dataItem.getDesc());
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

                Date d = format.parse(dataItem.getDated());
                DateFormat df = new DateFormat();

                holder.dated.setText("Date: " + dataItem.getDated().substring(0, 10));
                holder.cost.setText("Rs " + dataItem.getCost() + "/-");
                holder.cat.setText("Cat: " + dataItem.getCat());
                holder.subcat.setText("Subcat: " + dataItem.getSubcat());

                holder.user_id.setText(dataItem.getUser_id());
                holder.add_id.setText(dataItem.getAdd_id());
                final int type = Integer.parseInt(dataItem.getType());

                holder.type.setText(dataItem.getType());


                holder.nearby.setText(dataItem.getNearby());

                holder.placeid.setText(dataItem.getPlaceid());
                holder.city.setText(dataItem.getCity());
                holder.img_cnt.setText(dataItem.getImageCount() + "");
                holder.v.setText("Views :" + dataItem.getV());

                holder.main_title.setText(dataItem.getTitle());
                holder.vvv.setText(dataItem.getV());

                holder.views.setText("Views : " + dataItem.getV());
                holder.adid.setText("Ad Id :" + dataItem.getAdd_id());
                holder.main_ad_id.setText("Ad Id :" + dataItem.getAdd_id());

                //    holder.main_t.setText(dataItem.getTitle());

                // holder.status.setText(dataItem.getFlag());
                //new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,getWindowManager().getDefaultDisplay().getHeight()-100));

                if (dataItem.getFlag().equals("1")) {

                    holder.status.setText("Active");
                    holder.main_status.setText("Active");

                    //holder.singleprod.setBackground(getResources().getDrawable(R.color.green_700));
                    // holder.status.setTextColor(getResources().getDrawable(R.color.green_700));
                    holder.status.setTextColor(Color.GREEN);
                    holder.main_status.setTextColor(Color.GREEN);


                } else {

                    holder.main_status.setText("Inactive");
                    holder.status.setText("Inactive");
                    //     holder.singleprod.setBackground(getResources().getDrawable(R.color.green_100));
                    //      holder.status.setTextColor(getResources().getDrawable(R.color.red_700));
                    holder.status.setTextColor(Color.RED);
                    holder.main_status.setTextColor(Color.RED);


                }

                //  holder.ll_color.setBackground(getResources().getDrawable(R.color.fblue));
                holder.main_View.setText("Views :" + dataItem.getV());

                holder.cash_discount.setText(dataItem.getCash_discount());
                holder.mdiscount.setText(dataItem.getDiscount());
                holder.img_path.setText(dataItem.getImg_path());

                if (type == 1) {


                    holder.main_title.setVisibility(View.VISIBLE);

                    holder.video_thumb.setVisibility(View.VISIBLE);
                    holder.video_thumb.setUp(dataItem.getImg_path().toString(), "");
                    holder.banner_video_vas.setVisibility(View.VISIBLE);
                    holder.banner_img.setVisibility(View.GONE);
                    holder.video_Container.setVisibility(View.VISIBLE);
                    holder.image_Container.setVisibility(View.GONE);
                  /*  Bitmap bitmap = createThumbnailFromPath(dataItem.getImg_path(), MediaStore.Video.Thumbnails.MINI_KIND);

                    BitmapDrawable ob = new BitmapDrawable(getResources(), bitmap);*/


                    Uri uri = Uri.parse(dataItem.getImg_path());
                    String mUri = getRealPathFromURI(ctx, uri, "VIDEO");

                    holder.bar_title.setVisibility(View.VISIBLE);
                    //Log.e("mUri", mUri);


                    holder.video_thumb.thumbImageView.setImageBitmap(ThumbnailUtils.createVideoThumbnail(
                            dataItem.getImg_path(), MediaStore.Video.Thumbnails.FULL_SCREEN_KIND));

                    // holder.video_thumb.thumbImageView.

                    //  BitmapDrawable ob = new BitmapDrawable(getResources(), thumb);

                    //   holder.banner_img.setBackgroundDrawable(ob);


                } else if (type == 2) {


                    holder.bar_title.setVisibility(View.VISIBLE);
                    holder.video_Container.setVisibility(View.VISIBLE);
                    holder.image_Container.setVisibility(View.GONE);
                    holder.banner_img.setVisibility(View.VISIBLE);
                    holder.video_thumb.setVisibility(View.GONE);

                    holder.main_title.setVisibility(View.VISIBLE);

                    holder.banner_video_vas.setVisibility(View.VISIBLE);
                    holder.banner_img.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    Picasso.with(Classified.getInstance()).load(dataItem.getImg_path()).placeholder(R.mipmap.logo).into(holder.banner_img);


                } else {

                  /*  holder.main_ll1.setVisibility(View.VISIBLE);
                    holder.main_linear_container.setVisibility(View.GONE);
                    holder.main_View.setVisibility(View.GONE);
                    holder.postimg.setVisibility(View.VISIBLE);
                    Picasso.with(Classified.getInstance()).load(dataItem.getImg_path() + "0.jpg").into(holder.postimg);

                    holder.video_thumb.setVisibility(View.GONE);*/


                    holder.bar_title.setVisibility(View.GONE);
                    holder.banner_video_vas.setVisibility(View.GONE);

                    holder.main_title.setVisibility(View.GONE);
                    holder.video_Container.setVisibility(View.GONE);
                    holder.image_Container.setVisibility(View.VISIBLE);
                    Picasso.with(Classified.getInstance()).load(dataItem.getImg_path() + "0.jpg").placeholder(R.mipmap.logo).into(holder.postimg);


                }

                holder.itemView.setTag(dataItem);

                final ViewHolder finalHc1 = holder;

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (type == 2) {


                            Dialog d = new Dialog(MyAdd.this);
                            d.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            d.setContentView(R.layout.banner_view);
                            ImageView i = (ImageView) d.findViewById(R.id.banner_view);

                            UniversalVideoView mVideoView = (UniversalVideoView) d.findViewById(R.id.videoView);
                            UniversalMediaController mMediaController = (UniversalMediaController) d.findViewById(R.id.media_controller);
                            mVideoView.setMediaController(mMediaController);
                            LinearLayout main_con = (LinearLayout) d.findViewById(R.id.ll_baner);
                            main_con.setVisibility(View.GONE);
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            Window window = d.getWindow();
                            lp.copyFrom(window.getAttributes());

                            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                            window.setAttributes(lp);

                            i.setScaleType(ImageView.ScaleType.FIT_CENTER);
                            Picasso.with(Classified.getInstance()).load(dataItem.getImg_path()).placeholder(R.mipmap.logo).into(i);
                            d.show();

                        } else {


                            Intent in = new Intent(ctx, AdapAddDetail.class);
                            in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            in.putExtra("v", finalHc1.vvv.getText().toString());
                            in.putExtra("nearby", finalHc1.nearby.getText().toString());
                            in.putExtra("placeid", finalHc1.placeid.getText().toString());
                            in.putExtra("city", finalHc1.city.getText().toString());
                            in.putExtra("discount", finalHc1.mdiscount.getText().toString());
                            in.putExtra("cash_discount", finalHc1.cash_discount.getText().toString());
                            in.putExtra("imageCount", finalHc1.img_cnt.getText().toString());
                            in.putExtra("type", finalHc1.type.getText().toString());
                            in.putExtra("add_id", finalHc1.add_id.getText().toString());
                            in.putExtra("user_id", finalHc1.user_id.getText().toString());
                            in.putExtra("category", finalHc1.cat.getText().toString());
                            in.putExtra("subcategory", finalHc1.subcat.getText().toString());
                            in.putExtra("imageLoc", finalHc1.img_path.getText().toString());

                            in.putExtra("price", finalHc1.cost.getText().toString());
                            in.putExtra("postTime", finalHc1.dated.getText().toString());
                            in.putExtra("title", finalHc1.title.getText().toString());
                            in.putExtra("description", finalHc1.desc.getText().toString());
                            ctx.startActivity(in);
                        }
                    }
                });


                holder.chk.setTag(dataItem);


                if (itemChecked[position])
                    holder.chk.setChecked(true);
                else
                    holder.chk.setChecked(false);


                final ViewHolder finalHc2 = holder;
                finalHc2.chk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        if (finalHc2.chk.isChecked() == true) {
                            itemChecked[position] = true;

                            MyAdd.user_id.add(finalHc2.user_id.getText().toString());
                            MyAdd.add_id.add(finalHc2.add_id.getText().toString());
                            MyAdd.type.add(finalHc2.type.getText().toString());

                        }

                        if (finalHc2.chk.isChecked() == false) {
                            itemChecked[position] = false;

                            if (MyAdd.add_id.contains(finalHc2.user_id.getText().toString())) {
                                MyAdd.user_id.remove(finalHc2.user_id.getText().toString());
                            }

                            if (MyAdd.add_id.contains(finalHc2.add_id.getText().toString())) {
                                MyAdd.add_id.remove(finalHc2.add_id.getText().toString());
                            }

                        }
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        public int getItemCount() {
            return name.size();
        }

        @Override
        public void onViewDetachedFromWindow(ViewHolder holder) {
            super.onViewDetachedFromWindow(holder);
           /* clearAnimation();*/
        }

        public void setData(Context ctx, List<MyAddPojo> name) {


            this.ctx = ctx;
            this.name = name;
            chk_p = new ArrayList<Integer>();
            itemChecked = new boolean[name.size()];
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            ImageView view = (ImageView) v;
            // make the image scalable as a matrix
            view.setScaleType(ImageView.ScaleType.MATRIX);
            float scale;

            // Handle touch events here...
            switch (event.getAction() & MotionEvent.ACTION_MASK) {

                case MotionEvent.ACTION_DOWN: //first finger down only
                    savedMatrix.set(matrix);
                    start.set(event.getX(), event.getY());
                    //Log.d(TAG, "mode=DRAG");
                    mode = DRAG;
                    break;
                case MotionEvent.ACTION_UP: //first finger lifted
                case MotionEvent.ACTION_POINTER_UP: //second finger lifted
                    mode = NONE;
                    //Log.d(TAG, "mode=NONE");
                    break;
                case MotionEvent.ACTION_POINTER_DOWN: //second finger down
                    oldDist = spacing(event); // calculates the distance between two points where user touched.
                    //Log.d(TAG, "oldDist=" + oldDist);
                    // minimal distance between both the fingers
                    if (oldDist > 5f) {
                        savedMatrix.set(matrix);
                        midPoint(mid, event); // sets the mid-point of the straight line between two points where user touched.
                        mode = ZOOM;
                        //Log.d(TAG, "mode=ZOOM");
                    }
                    break;

                case MotionEvent.ACTION_MOVE:
                    if (mode == DRAG) { //movement of first finger
                        matrix.set(savedMatrix);
                        if (view.getLeft() >= -392) {
                            matrix.postTranslate(event.getX() - start.x, event.getY() - start.y);
                        }
                    } else if (mode == ZOOM) { //pinch zooming
                        float newDist = spacing(event);
                        //Log.d(TAG, "newDist=" + newDist);
                        if (newDist > 5f) {
                            matrix.set(savedMatrix);
                            scale = newDist / oldDist; //thinking I need to play around with this value to limit it**
                            matrix.postScale(scale, scale, mid.x, mid.y);
                        }
                    }
                    break;
            }

            // Perform the transformation
            view.setImageMatrix(matrix);

            return true; // indicate event was handled
        }

        private float spacing(MotionEvent event) {
            float x = event.getX(0) - event.getX(1);
            float y = event.getY(0) - event.getY(1);

            float d = (float) Math.sqrt(x * x + y * y);
            return d;
        }

        private void midPoint(PointF point, MotionEvent event) {
            float x = event.getX(0) + event.getX(1);
            float y = event.getY(0) + event.getY(1);
            point.set(x / 2, y / 2);
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            MTextView title, desc, dated, cost, cat, subcat, views, main_View, main_title, status, adid, main_status, main_ad_id;
            ImageView postimg;
            CheckBox chk;

            LinearLayout image_Container, bar_title, banner_video_vas;
            RelativeLayout video_Container;
            JCVideoPlayerStandard video_thumb;

            CardView singleprod;

            LinearLayout.LayoutParams params;

            MTextView main_t;

            RelativeLayout color_status;
            ImageView banner_img;

            LinearLayout main_ll1, main_linear_container, imageContainer;
            /*  TextView user_id, add_id;*/
            TextView user_id, add_id, type, v, img_cnt, city, placeid, nearby, cash_discount, mdiscount, img_path,vvv;

            public ViewHolder(View itemView) {
                super(itemView);

                //  main_t = (MTextView) itemView.findViewById(R.id.main_t);
                vvv=(TextView)itemView.findViewById(R.id.vvv);
                banner_video_vas = (LinearLayout) itemView.findViewById(R.id.banner_video_vas);
                bar_title = (LinearLayout) itemView.findViewById(R.id.bar_title);
                banner_img = (ImageView) itemView.findViewById(R.id.banner_img);
                video_Container = (RelativeLayout) itemView.findViewById(R.id.video_container);
                image_Container = (LinearLayout) itemView.findViewById(R.id.image_Container);
                main_ad_id = (MTextView) itemView.findViewById(R.id.main_ad_id);
                video_thumb = (JCVideoPlayerStandard) itemView.findViewById(R.id.video_thumb);
                title = (MTextView) itemView.findViewById(R.id.ptitle);
                desc = (MTextView) itemView.findViewById(R.id.description);
                dated = (MTextView) itemView.findViewById(R.id.dated);
                cost = (MTextView) itemView.findViewById(R.id.cost);
                cat = (MTextView) itemView.findViewById(R.id.cat);
                subcat = (MTextView) itemView.findViewById(R.id.subcat);
                postimg = (ImageView) itemView.findViewById(R.id.prodimg);
                chk = (CheckBox) itemView.findViewById(R.id.check);


                main_status = (MTextView) itemView.findViewById(R.id.main_status);

                main_linear_container = (LinearLayout) itemView.findViewById(R.id.main_linear_container);

                color_status = (RelativeLayout) itemView.findViewById(R.id.color_status);
                views = (MTextView) itemView.findViewById(R.id.views);
                main_View = (MTextView) itemView.findViewById(R.id.main_View);
                main_title = (MTextView) itemView.findViewById(R.id.main_title);

                status = (MTextView) itemView.findViewById(R.id.status);

                adid = (MTextView) itemView.findViewById(R.id.adid);

                singleprod = (CardView) itemView.findViewById(R.id.singleprod);

                imageContainer = (LinearLayout) itemView.findViewById(R.id.imageContainer);


                user_id = (TextView) itemView.findViewById(R.id.user_id);
                add_id = (TextView) itemView.findViewById(R.id.add_id);

                user_id = (TextView) itemView.findViewById(R.id.user_id);
                add_id = (TextView) itemView.findViewById(R.id.add_id);
                type = (TextView) itemView.findViewById(R.id.type);
                v = (TextView) itemView.findViewById(R.id.v);
                img_cnt = (TextView) itemView.findViewById(R.id.img_cnt);
                city = (TextView) itemView.findViewById(R.id.city);
                placeid = (TextView) itemView.findViewById(R.id.placeid);
                nearby = (TextView) itemView.findViewById(R.id.nearby);
                cash_discount = (TextView) itemView.findViewById(R.id.cash_discount);
                mdiscount = (TextView) itemView.findViewById(R.id.mdiscount);

                img_path = (TextView) itemView.findViewById(R.id.img_path);

                main_ll1 = (LinearLayout) itemView.findViewById(R.id.main_ll1);
            }
        }

        public List<MyAddPojo> getStudentist() {
            return name;
        }

/*        private void setAnimation(View viewToAnimate, int position) {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition) {
                Animation animation = AnimationUtils.loadAnimation(ctx,
                        (position > lastPosition) ? R.anim.up_from_bottom
                                : R.anim.down_from_top);
                viewToAnimate.startAnimation(animation);
                lastPosition = position;

            }
        }*/

    }


    public static String getRealPathFromURI(Context context, Uri contentURI, String type) {

        String result = null;
        try {
            Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
            if (cursor == null) { // Source is Dropbox or other similar local file path
                result = contentURI.getPath();
                //Log.d("TAG", "result******************" + result);
            } else {
                cursor.moveToFirst();
                int idx = 0;
                if (type.equalsIgnoreCase("IMAGE")) {
                    idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                } else if (type.equalsIgnoreCase("VIDEO")) {
                    idx = cursor.getColumnIndex(MediaStore.Video.VideoColumns.DATA);
                } else if (type.equalsIgnoreCase("AUDIO")) {
                    idx = cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DATA);
                }
                result = cursor.getString(idx);
                //Log.d("TAG", "result*************else*****" + result);
                cursor.close();
            }
        } catch (Exception e) {
            //Log.e("TAG", "Exception ", e);
        }
        return result;
    }

   /* public Bitmap createThumbnailFromPath(String filePath, int type) {
        return ThumbnailUtils.createVideoThumbnail(filePath, type);
    }*/
}
