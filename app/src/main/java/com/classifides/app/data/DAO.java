package com.classifides.app.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.classifides.app.R;
import com.classifides.app.application.Classified;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by Rajeshwar on 15-02-2016.
 */
public class DAO {

    public static JSONObject getJsonFromUrl(String Targeturl, Map<String, String> paramdata) {
        URL url;
        HttpURLConnection connection = null;
        JSONObject error=null;
        try {
            error = new JSONObject("{\"success\":-1}");
            String parms="";
            Set mapset = paramdata.entrySet();
            Iterator mapitrator = mapset.iterator();
            while (mapitrator.hasNext()) {
                Map.Entry ent = (Map.Entry) mapitrator.next();
                parms += ent.getKey().toString() + "=";
                parms += URLEncoder.encode(ent.getValue().toString(), "UTF-8") + "&";
            }
            parms=parms.substring(0,parms.length()-1);
            url = new URL(Targeturl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length", "" + Integer.toString(parms.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            //Send request
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(parms);
            wr.flush();
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\n');
            }
            rd.close();
            is.close();
            return new JSONObject(response.toString());
            //return response.toString();


        } catch (Exception e) {
            e.printStackTrace();

        } finally {

            try {
                connection.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return error;
    }



    public static JSONArray getJsonArrayFromUrl(String Targeturl, Map<String, String> paramdata) {
        URL url;
        HttpURLConnection connection = null;
        JSONArray error=null;
        try {
            error = new JSONArray("[{\"success\":-1}]");
            String parms="";
            Set mapset = paramdata.entrySet();
            Iterator mapitrator = mapset.iterator();
            while (mapitrator.hasNext()) {
                Map.Entry ent = (Map.Entry) mapitrator.next();
                parms += ent.getKey().toString() + "=";
                parms += URLEncoder.encode(ent.getValue().toString(), "UTF-8") + "&";
            }
            parms=parms.substring(0,parms.length()-1);
            url = new URL(Targeturl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length", "" + Integer.toString(parms.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            //Send request
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(parms);
            wr.flush();
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\n');
            }
            rd.close();
            is.close();
            return new JSONArray(response.toString());
            //return response.toString();


        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            connection.disconnect();
        }

        return error;
    }




    public static JSONObject getUser(){
        JSONObject us=null;
        SharedPreferences sh = Classified.getInstance().getSharedPreferences("user", Context.MODE_PRIVATE);
        String user = sh.getString("users", "");
        try {
           us = new JSONObject(user);
          } catch (JSONException e) {
            e.printStackTrace();
        }
        return us;
    }


}
