package com.classifides.app.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.classifides.app.R;
import com.classifides.app.gcm.model.NewMessage;
import com.classifides.app.gcm.model.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Ashvini on 8/24/2016.
 */
public class NotiDbHelper extends SQLiteOpenHelper {


    //public static String  databaseName="users.db";


    static String DATABASE_NAME = "noti.db";


    public static final String TABLE_NAME_TWo = "noti";
    public static final String NEXT = "next_id";

    public static final String KEY_LNAME = "lname";
    public static final String KEY_ID = "id";
    public static final String TABLE_NAME = "users";
    public static final String CHAT_ROOM_ID = "chat_room_id";
    public static final String MESSAGE = "message";
    public static final String CREATED_AT = "created_at";


    public static final String name = "name";
    public static final String image = "image";
    public static final String mobile = "mobile";
    public static final String type = "type";
    public static final String id = "id";
    public static final String ustatus = "status";
    public static final String aid = "aid";
    public static final String block = "block";
    public static final String ticked = "ticked";


    public static final String messageTable = "message";
    public static final String mid = "id";
    public static final String sid = "sid";
    public static final String rid = "rid";
    public static final String msg = "message";
    public static final String stime = "stime";
    public static final String rtime = "rtime";
    public static final String mtypr = "type";
    public static final String uid = "uid";
    public static final String chk_image = "chk_image";
    public static final String rs = "rs";


    public static final String STATUS = "status";
    public static final String STATUS_NAME = "status_name";

    public NotiDbHelper(Context context) {
        super(context, DATABASE_NAME, null, 2);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" + id + " INTEGER, " + name + " TEXT," + image + " TEXT," + mobile + " TEXT," + type + " INTEGER , " + ustatus + " TEXT ," + aid + " INTEGER ," + block + " INTEGER DEFAULT 0, " + ticked + " Boolean )";

        String MESSAGE = "CREATE TABLE " + messageTable + " (" + mid + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " + sid + " INTEGER, " + rid + " INTEGER, " + msg + " TEXT ," + stime + " DATETIME DEFAULT CURRENT_TIMESTAMP ," + rtime + " DATETIME DEFAULT CURRENT_TIMESTAMP ," + mtypr + " TEXT," + uid + " INTEGER , " + chk_image + "  INTEGER DEFAULT 0, " + rs + " INTEGER  )";

        String CREATE_TABLE_TWO = "CREATE TABLE " + TABLE_NAME_TWo + " (" + NEXT + " TEXT PRIMARY KEY)";
        String CREATE_TABLE_STATUS = "CREATE TABLE " + STATUS + " (" + STATUS_NAME + " TEXT )";


        db.execSQL(MESSAGE);
        db.execSQL(CREATE_TABLE);
        db.execSQL(CREATE_TABLE_TWO);
        db.execSQL(CREATE_TABLE_STATUS);
        //Log.e("table1", "Table is created");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_TWo);
        db.execSQL("DROP TABLE IF EXISTS " + MESSAGE);
        db.execSQL("DROP TABLE IF EXISTS " + STATUS);


        onCreate(db);
    }


    public void deleteAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE  FROM " + TABLE_NAME);
        db.execSQL("DELETE  FROM " + TABLE_NAME_TWo);
        db.execSQL("DELETE  FROM " + MESSAGE);
        db.execSQL("DELETE  FROM " + STATUS);
    }

    public void saveUsers(ArrayList<User> users) {


        SQLiteDatabase db = this.getWritableDatabase();
        for (User user : users) {

            if (!usercheck(user)) {
                ContentValues values = new ContentValues();
                values.put(id, user.getId());
                values.put(name, user.getName());
                values.put(image, user.getImage());
                values.put(mobile, user.getMobile());
                values.put(type, user.getType());
                values.put(ustatus, user.getStatus());
                values.put(aid, user.getAid());
                db.insert(TABLE_NAME, null, values);
            }

        }
        db.close();


    }


    public void saveUnknownUsers(User users) {
        SQLiteDatabase db = this.getWritableDatabase();
        if (!usercheck2(users)) {
            ContentValues values = new ContentValues();
            values.put(id, users.getId());
            values.put(name, users.getName());
            values.put(image, users.getImage());
            values.put(mobile, users.getMobile());
            values.put(type, users.getType());
            values.put(ustatus, users.getStatus());
            values.put(aid, users.getAid());
            db.insert(TABLE_NAME, null, values);
        }
        db.close();


    }


    public void addValuesToStatus() {
        SQLiteDatabase db = this.getWritableDatabase();

        ArrayList<String> status = new ArrayList<String>();
        status.add("Meeting");
        status.add("Available");
        status.add("Busy");
        status.add("Work");
        status.add("Gym");
        status.add("Sleeping");


        ContentValues values = new ContentValues();

        for (int i = 0; i < status.size(); i++) {
            values.put(STATUS_NAME, status.get(i));
            db.insert(STATUS, null, values);
        }


        db.close();
    }

    public boolean getEntryInStatus() {
        boolean ch = false;
        String query = "select distinct(" + STATUS_NAME + ") from " + STATUS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {

            String s1 = cursor.getString(0);//, cursor.getInt(1), cursor.getInt(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getInt(6), cursor.getInt(7));
            if (s1.equals("")) {
                ch = false;
            } else {
                ch = true;
            }

        }


        return ch;
    }


    public ArrayList<String> getAllStatus() {
        ArrayList<String> st = new ArrayList<String>();
        String query = "select distinct(" + STATUS_NAME + ") from " + STATUS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                String s1 = cursor.getString(0);//, cursor.getInt(1), cursor.getInt(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getInt(6), cursor.getInt(7));
                st.add(s1);
                //Log.e("msg", s1.toString());

            } while (cursor.moveToNext());
        }


        return st;
    }


    public void addStatus(ArrayList<String> status) {
        SQLiteDatabase db = this.getWritableDatabase();
        for (int s = 0; s < status.size(); s++) {
            ContentValues values = new ContentValues();
            values.put(STATUS_NAME, status.get(0));
            db.insert(STATUS, null, values);

        }
        db.close();
    }


    public void saveSingleMessage(NewMessage msgs) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();

        values.put(sid, msgs.getSid());
        values.put(rid, msgs.getRid());
        values.put(msg, msgs.getMsg());
        values.put(stime, msgs.getStime());
        values.put(rtime, msgs.getRtime());
        values.put(mtypr, msgs.getMtypr());
        values.put(uid, msgs.getUid());
        values.put(chk_image, msgs.getCh_image());
        values.put(rs, msgs.getRs());
        db.insert(messageTable, null, values);
        db.close();
    }

    public boolean delgroup(int name) {
        SQLiteDatabase db = this.getReadableDatabase();
        return db.delete(TABLE_NAME, id + "=" + name, null) > 0;
    }

    public ArrayList<NewMessage> getMessageByRid_Sid(int msid, int mrid) {

        ArrayList<NewMessage> mmm = new ArrayList<NewMessage>();
        String query = "select * from " + messageTable + " where " + sid + "=" + msid + " AND " + rid + " = " + mrid;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                NewMessage message1 = new NewMessage(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getInt(6), cursor.getInt(7), cursor.getInt(8), cursor.getInt(9));
                mmm.add(message1);
                //Log.e("msg", message1.toString());

            } while (cursor.moveToNext());
        }
        return mmm;
    }

    public ArrayList<NewMessage> getMessageByGid(int rrrid) {

        ArrayList<NewMessage> mmm = new ArrayList<NewMessage>();
        String query = "select * from " + messageTable + " where " + type + " = 2  AND  " + sid + " = " + rrrid;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                NewMessage message1 = new NewMessage(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getInt(6), cursor.getInt(7), cursor.getInt(8), cursor.getInt(9));
                try {

                    //Log.e("AllMsgOfGroup", message1.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mmm.add(message1);

            } while (cursor.moveToNext());
        }
        return mmm;
    }


    public String getNameOfUser(int sid) {
        String sname = "";
        String query = "select * from " + TABLE_NAME + " WHERE " + id + "= " + sid;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            //String name, String image, int type, int id, String mobile, String status,int aid
            // id + " INTEGER, " + name + " TEXT," + image + " TEXT," + mobile + " TEXT," + type + " INTEGER ,
            // " + ustatus + " TEXT ,
            // " + aid + " INTEGER)";
            sname = cursor.getString(1);

        }
        return sname;
    }


    public User getUserById(int uid) {
        User user = null;
        String query = "select * from " + TABLE_NAME + " WHERE " + id + "= " + uid;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            //String name, String image, int type, int id, String mobile, String status,int aid
            // id + " INTEGER, " + name + " TEXT," + image + " TEXT," + mobile + " TEXT," + type + " INTEGER ,
            // " + ustatus + " TEXT ,
            // " + aid + " INTEGER)";


            user = new User(cursor.getString(1), cursor.getString(2), cursor.getInt(4), cursor.getInt(0), cursor.getString(3), cursor.getString(5), cursor.getInt(6), cursor.getInt(7), false);

        }
        return user;
    }

    public User getUserGroupById(int uid) {
        User user = null;
        String query = "select * from " + TABLE_NAME + " WHERE type =2 AND " + id + "= " + uid;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            user = new User(cursor.getString(1), cursor.getString(2), cursor.getInt(4), cursor.getInt(0), cursor.getString(3), cursor.getString(5), cursor.getInt(6), cursor.getInt(7), false);

        }
        return user;
    }


    public boolean deleteUsersChatById(int uid, int rrid) {
        SQLiteDatabase db = this.getReadableDatabase();
        return db.delete(messageTable, sid + "=" + uid + " AND " + rid + " = " + rrid, null) > 0;
    }


    public ArrayList<User> findfriend(String mobile_no) {
        User user = null;
        ArrayList<User> utr = new ArrayList<User>();
        String query = "select * from " + TABLE_NAME + " WHERE type= 1 AND " + mobile + " like '%" + mobile_no + "%' OR " + name + " like '%" + mobile_no+"%'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
            user = new User(cursor.getString(1), cursor.getString(2), cursor.getInt(4), cursor.getInt(0), cursor.getString(3), cursor.getString(5), cursor.getInt(6), cursor.getInt(7), false);
            if (user != null) {
                utr.add(user);
            }
            } while (cursor.moveToNext());
        }
        return utr;
    }


    public ArrayList<User> getUserByChat() {
        String query = "select DISTINCT(uid) from " + messageTable + " where type =1  ORDER BY   id DESC ";
        ArrayList<User> u1 = new ArrayList<User>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {

                User user = getUserById(cursor.getInt(0));

                if (user != null) {
                    u1.add(user);
                    //Log.e("usersnoti", user.toString());
                }


            } while (cursor.moveToNext());
        }
        return u1;

        //return null;
    }


    public ArrayList<User> getUsersGroupByMessage() {
        String query = "select DISTINCT(uid) from " + messageTable + " WHERE type =2  ORDER BY id DESC";
        String ids = "";
        ArrayList<User> u1 = new ArrayList<User>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                ids = ids + cursor.getInt(0) + ",";
                User user = getUserGroupById(cursor.getInt(0));

                if (user != null) {
                    u1.add(user);
                    //Log.e("usersnoti", user.toString());
                }


            } while (cursor.moveToNext());
        }

        try {
            User u12;
            String query1;
            if (ids.length() > 0) {
                ids = ids.substring(0, ids.length() - 1);
                query1 = "select * from " + TABLE_NAME + " WHERE  " + type + " = 2 AND id NOT IN (" + ids +
                        ")";
            } else {
                query1 = "select * from " + TABLE_NAME + " WHERE  " + type + " = 2 ";
            }

     /*   ArrayList<User> u1 = new ArrayList<User>();
        SQLiteDatabase db = this.getReadableDatabase();*/
            cursor = db.rawQuery(query1, null);
            if (cursor.moveToFirst()) {
                do {

                    u12 = new User(cursor.getString(1), cursor.getString(2), cursor.getInt(4), cursor.getInt(0), cursor.getString(3), cursor.getString(5), cursor.getInt(6), cursor.getInt(7), false);

                    if (u1 != null) {
                        u1.add(u12);
                  /*  String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

                    boolean s1 = getSingleGroupMessage(cursor.getInt(0));
                    if (s1 == true) {
                        Log.e("Added", "Already Added");
                    } else {
                        NewMessage newMessage = new NewMessage(0, u12.getId(), 0, "", timeStamp + "", timeStamp + "", 2, u12.getId(), 0, 0);
                        saveSingleMessage(newMessage);
                        Log.e("usersnoti", u12.toString());
                    }
*/

                    }


                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return u1;

        //return null;
    }


    public void unFriend(int id, int i) {


        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(block, i); //These Fields should be your String values of actual column names

        db.update(TABLE_NAME, cv, "id=" + id, null);
        db.close();


    }

    public ArrayList<User> getAllGroup() {
        User u12;
        String query = "select * from " + TABLE_NAME + " WHERE  " + type + " = 2 ";
        ArrayList<User> u1 = new ArrayList<User>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {

                u12 = new User(cursor.getString(1), cursor.getString(2), cursor.getInt(4), cursor.getInt(0), cursor.getString(3), cursor.getString(5), cursor.getInt(6), cursor.getInt(7), false);

                if (u1 != null) {
                    u1.add(u12);
                  /*  String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

                    boolean s1 = getSingleGroupMessage(cursor.getInt(0));
                    if (s1 == true) {
                        Log.e("Added", "Already Added");
                    } else {
                        NewMessage newMessage = new NewMessage(0, u12.getId(), 0, "", timeStamp + "", timeStamp + "", 2, u12.getId(), 0, 0);
                        saveSingleMessage(newMessage);
                        Log.e("usersnoti", u12.toString());
                    }
*/

                }


            } while (cursor.moveToNext());
        }
        return u1;
    }

    public boolean getSingleGroupMessage(int userid) {

        boolean ch_msg_added = false;
        String str = "";
        NewMessage message1 = null;
        String query = "select * from " + messageTable + " where " + sid + "  = " + userid + " AND type =2  ORDER BY   id  DESC";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            // message1 = new NewMessage(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getInt(6), cursor.getInt(7), cursor.getInt(8), cursor.getInt(9));
            do {
                if (cursor.getString(3).equals("")) {
                    ch_msg_added = true;
                    break;
                } else {
                    ch_msg_added = false;
                }

            } while (cursor.moveToNext());


        }
        return ch_msg_added;

    }

    public ArrayList<User> groupMessageDetails() {
        User u12;
        String query = "select * from " + TABLE_NAME + " WHERE  " + type + " = 2 ";
        ArrayList<User> u1 = new ArrayList<User>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {

                u12 = new User(cursor.getString(1), cursor.getString(2), cursor.getInt(4), cursor.getInt(0), cursor.getString(3), cursor.getString(5), cursor.getInt(6), cursor.getInt(7), false);

                if (u1 != null) {
                    u1.add(u12);
                    //Log.e("usersnoti", u12.toString());
                }


            } while (cursor.moveToNext());
        }
        return u1;
    }


    public boolean deleteGroup(int id) {
/*

        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(block, 1); //These Fields should be your String values of actual column names

        db.de(TABLE_NAME, cv, "id=" + id, null);
        db.close();
*/


        return true;
    }


    public NewMessage getLastMessage(int rrid) {
        String str = "";
        NewMessage message1 = null;
        String query = "select * from " + messageTable + " where " + rid + "  = " + rrid + " AND type =1  ORDER BY   id  DESC";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            message1 = new NewMessage(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getInt(6), cursor.getInt(7), cursor.getInt(8), cursor.getInt(9));

            //Log.e("LastMsg", message1.toString());

        }
        return message1;
    }


    public NewMessage getLastGroupMessage(int rrid) {
        String str = "";
        NewMessage message1 = null;
        String query = "select * from " + messageTable + " where " + sid + "  = " + rrid + " AND type =2  ORDER BY   id  DESC";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            if (cursor.moveToFirst()) {
                message1 = new NewMessage(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getInt(6), cursor.getInt(7), cursor.getInt(8), cursor.getInt(9));

                //Log.e("LastMsg", message1.toString());

            }
        }
        return message1;

    }


    public boolean check_blocked(int rrid) {
        boolean str = false;

        String query = "select * from " + TABLE_NAME + " where type= 1 and " + id + "  = " + rrid + " ";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
         /*   do {*/
              /*  User user = new User(cursor.getString(1), cursor.getString(2), cursor.getInt(4), cursor.getInt(0), cursor.getString(3), cursor.getString(5), cursor.getInt(6), cursor.getInt(7));
                users.add(user);*/
            int str1 = cursor.getInt(7);
/*




            } while (cursor.moveToNext());*/

            if (str1 == 1) {
                str = true;
            } else {
                str = false;
            }
            //Log.e("LastMsg", str1 + "");

        }

        return str;
    }


    public ArrayList<String> getStatus() {
        User u12;
        String query = "select * from " + STATUS + "";
        SQLiteDatabase db = null;
        ArrayList<String> u1 = new ArrayList<String>();
        try {
            db = this.getReadableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {

                String s = (cursor.getString(0));

                if (u1 != null) {
                    u1.add(s);
                    //Log.e("status", s.toString());
                }


            } while (cursor.moveToNext());
        }
        return u1;
    }


    private boolean usercheck(User user) {

        String query = "select * from " + TABLE_NAME + " where id=" + user.getId();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        ContentValues values = new ContentValues();
        if (cursor.moveToFirst()) {

            values.put(id, user.getId());
            values.put(name, user.getName());
            values.put(image, user.getImage());
            values.put(mobile, user.getMobile());
            values.put(type, user.getType());
            values.put(ustatus, user.getStatus());
            values.put(aid, user.getAid());
            db.update(TABLE_NAME, values, " id= " + user.getId(), null);
            return true;

        } else {
            return false;
        }

    }


    private boolean usercheck2(User user) {

        String query = "select * from " + TABLE_NAME + " where id=" + user.getId();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        ContentValues values = new ContentValues();
        if (cursor.moveToFirst()) {
            return true;
        } else {
            return false;
        }

    }

    public ArrayList<User> getAllUser() {
        ArrayList<User> users = new ArrayList<User>();

        String query = "select * from " + TABLE_NAME + " where type = 1 order by " + name;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                User user = new User(cursor.getString(1), cursor.getString(2), cursor.getInt(4), cursor.getInt(0), cursor.getString(3), cursor.getString(5), cursor.getInt(6), cursor.getInt(7), false);
                users.add(user);

            } while (cursor.moveToNext());
        }

        return users;
    }

}
