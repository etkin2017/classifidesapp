package com.classifides.app.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

/**
 * Created by Rajeshwar on 28/06/2016.
 */
public class DbHelper extends SQLiteOpenHelper {
    public static String databaseName = "classifieds.db";
    public static int id = 1;
    public static String create_query = "create table recentloc(id INTEGER PRIMARY KEY,loc TEXT)";
    public static String drop_query = "DROP TABLE IF EXISTS recentloc";


    public DbHelper(Context context) {
        super(context, databaseName, null, id);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(create_query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(drop_query);
        onCreate(db);
    }

    public long insertLoc(String loc)
    {

        SQLiteDatabase db=getWritableDatabase();

        Cursor cursor=db.query("recentloc", new String[]{}, "loc=?", new String[]{loc}, null, null, null);
        if(cursor.moveToFirst())
        {

            //Toast.makeText(this, cursor.getString(1)+" "+cursor.getInt(2),Toast.LENGTH_LONG).show();
        }else {
            //Toast.makeText(this,"No Record Found",Toast.LENGTH_LONG).show();
        }






        ContentValues values=new ContentValues();
        values.put("loc", loc);
        long re= db.insert("emp", null, values);
        db.close();
        return re;
    }

}
