package com.classifides.app.data;

/**
 * Created by Rajeshwar on 27-02-2016.
 */
public class Config {

    public static int noti_click = 0;

    public static String Server = "http://catchmeon.net/catchmeon/";

    public static String getRedeemRequest = "http://catchmeon.net/catchmeon/getRedeemRequest.php";

    public static String setImage = "http://catchmeon.net/catchmeon/photos.php";


    public static String getPassword = "http://catchmeon.net/catchmeon/getPassword.php";

    public static String adBySubCategory = "http://catchmeon.net/catchmeon/addBySubCategory.php";

    public static String getAdInfo = "http://catchmeon.net/catchmeon/getAdInfo.php";

    public static String myReply = "http://catchmeon.net/catchmeon/myReply.php";

    public static String userInfo = "http://catchmeon.net/catchmeon/getUserInfo.php";

    public static String delAdd = "http://catchmeon.net/catchmeon/deleteAdd.php";
    public static String selectOneAd = "http://catchmeon.net/catchmeon/selectOneAd.php";

    public static String deleteMyReply = "http://catchmeon.net/catchmeon/deleteMyMsg.php";
    public static String deleteOthersAdsReply = "http://catchmeon.net/catchmeon/deleteMyOthersReply.php";

    public static String addPlan = "http://catchmeon.net/catchmeon/storePlan.php";


    // public static String addPlanNew = "http://catchmeon.net/catchmeon/storeplannew.php";

    public static String addPlanNew = "http://catchmeon.net/catchmeon/addPU.php";


    public static String planHistory = "http://catchmeon.net/catchmeon/getPlans.php";

    // public static String CheckCode = "http://catchmeon.net/catchmeon/checkCode.php";

    public static String CheckCode = "http://catchmeon.net/catchmeon/checkCodeUpdate.php";

    //  public static String insertRedeemCode = "http://catchmeon.net/catchmeon/redeemCode.php";


    public static String insertRedeemCode = "http://catchmeon.net/catchmeon/redeemCodeUpdate.php";

    //   public static String addCashPlan = "http://catchmeon.net/catchmeon/addCashbackMain.php";


    // public static String addCashPlan = "http://catchmeon.net/catchmeon/addCashbackMainUpdate.php";


    public static String addCashPlan = "http://catchmeon.net/catchmeon/addCashPMUpdate.php";


    public static String cashBackHistory = "http://catchmeon.net/catchmeon/getCashbackPlan.php";

    public static String getCategory = "http://catchmeon.net/catchmeon/getCategory.php";

    public static String getSubCategory = "http://catchmeon.net/catchmeon/getSubCategory.php";

    public static String insertAdReply = "http://catchmeon.net/catchmeon/insertAdReplyMain.php";

    public static String getAdsMessage = "http://catchmeon.net/catchmeon/getAdsMessage.php";

    public static String updateAd = "http://catchmeon.net/catchmeon/updateAd.php";
    public static String updateCashCode = "http://catchmeon.net/catchmeon/updateCashCode.php";

    public static String getAllGroupById = "http://catchmeon.net/catchmeon/getAllGroupById.php";

    // public static String referlink = "http://catchmeon.net/catchmeon/referral.php";

    public static String referlink = "http://catchmeon.net/catchmeon/updateReferral.php";

    public static final String EMAIL = "classifieds013@gmail.com";
    public static final String PASSWORD = "ashvini13b";


    public static String contact = "http://catchmeon.net/catchmeon/contact.php";

    public static String showReferralearnedAmt = "http://catchmeon.net/catchmeon/showReferral.php";

    public static String rHistory = "http://catchmeon.net/catchmeon/rHistory.php";

    public static String addHistory = "http://catchmeon.net/catchmeon/userReferralHistory.php";

    //classifieds013


    public static String adAdvertise = "http://catchmeon.net/catchmeon/storeAddUpdateOne.php";


    public static boolean appendNotificationMessages = true;

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // type of push messages
    public static final int PUSH_TYPE_CHATROOM = 1;
    public static final int PUSH_TYPE_USER = 2;

    // id to handle the notification in the notification try
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;


    public static String updateView = "http://catchmeon.net/catchmeon/updateView.php";

    public static String ActivateAd = "http://catchmeon.net/catchmeon/activateAd.php";
    private static String apiKey = "AIzaSyDIcFkZPFmCG_uUAbW-6luFgaSJ0kBdRbk";
    public static String Address = "https://maps.googleapis.com/maps/api/place/autocomplete/json?types=(cities)&key=" + apiKey + "&input=";
    public static String NearBy = "https://maps.googleapis.com/maps/api/place/autocomplete/json?types=geocode&key=" + apiKey + "&input=";
    public static String Location = "https://maps.googleapis.com/maps/api/geocode/json?key=" + apiKey + "&location_type=GEOMETRIC_CENTER&latlng=";
    public static String Place = "https://maps.googleapis.com/maps/api/place/details/json?key=" + apiKey + "&placeid=";
}
