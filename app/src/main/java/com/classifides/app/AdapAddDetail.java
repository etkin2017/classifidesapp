package com.classifides.app;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;

import com.classifides.app.model.ViewDbHelper;
import com.classifides.app.view.MTextView;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;

public class AdapAddDetail extends AppCompatActivity implements OnMapReadyCallback, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    private GoogleMap mMap;
    JSONObject add;
    private String placeid;
    MTextView title, price, desc, cat, subcat, discount, cashback, address, views;
    JCVideoPlayerStandard video;
    private SliderLayout mDemoSlider;
    String url;
    int type;
    int imgcnt;
    ShareDialog shareDialog;
    CallbackManager callbackManager;

    LinearLayout card_id, mainCont, cashback_container, discount_container;
    ImageView fb, gp, tw, whatsapp, mail;

    public void getAdd() {

        placeid = getIntent().getStringExtra("placeid");
        title.setText(getIntent().getStringExtra("title"));
        price.setText(getIntent().getStringExtra("price"));
        desc.setText(getIntent().getStringExtra("description"));
        cat.setText(":  " + getIntent().getStringExtra("category"));
        subcat.setText(":  " + getIntent().getStringExtra("subcategory"));
        views.setText("Total Views :  " + getIntent().getStringExtra("v"));

        String disc = getIntent().getStringExtra("discount");

        if (disc.equals("0")) {
            discount.setVisibility(View.GONE);
            discount_container.setVisibility(View.GONE);
        } else {
            discount.setText(":  " + getIntent().getStringExtra("discount") + "%");

        }

        String cash = getIntent().getStringExtra("cash_discount");
        //  discount.setText(":  " + getIntent().getStringExtra("discount") + "%");

        if (cash.equals("0") || cash.equalsIgnoreCase("no")) {

            cashback.setVisibility(View.GONE);
            cashback_container.setVisibility(View.GONE);
        } else {
            cashback.setText("Yes");
        }
        try {
            url = getIntent().getStringExtra("imageLoc");
            //Log.e("imageLoc", url);
        } catch (Exception e) {

        }

        type = Integer.parseInt(getIntent().getStringExtra("type"));
        imgcnt = Integer.parseInt(getIntent().getStringExtra("imageCount"));


    }

    public void initView() {
        views = (MTextView) findViewById(R.id.views);
        title = (MTextView) findViewById(R.id.titles);
        price = (MTextView) findViewById(R.id.price);
        desc = (MTextView) findViewById(R.id.desc);
        cat = (MTextView) findViewById(R.id.cat);
        subcat = (MTextView) findViewById(R.id.subcat);
        discount = (MTextView) findViewById(R.id.discount);
        cashback = (MTextView) findViewById(R.id.cashback);
        address = (MTextView) findViewById(R.id.address);
        video = (JCVideoPlayerStandard) findViewById(R.id.video);
        mDemoSlider = (SliderLayout) findViewById(R.id.slider);
        card_id = (LinearLayout) findViewById(R.id.card_id);
        mainCont = (LinearLayout) findViewById(R.id.mainCont);
        cashback_container = (LinearLayout) findViewById(R.id.cashback_container);
        discount_container = (LinearLayout) findViewById(R.id.discount_container);
        fb = (ImageView) findViewById(R.id.fb);
        gp = (ImageView) findViewById(R.id.gp);
        tw = (ImageView) findViewById(R.id.tw);
        whatsapp = (ImageView) findViewById(R.id.whatsapp);
        mail = (ImageView) findViewById(R.id.gmail);
        card_id.setVisibility(View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            // only for gingerbread and newer versions
        } else {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");

        }

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        initView();
        getAdd();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        if (type == 1) {
            video.setVisibility(View.VISIBLE);
            mDemoSlider.setVisibility(View.GONE);
            video.setUp(url, "");
            mainCont.setVisibility(View.GONE);
        } else if (type == 2) {
            video.setVisibility(View.GONE);
            mDemoSlider.setVisibility(View.VISIBLE);
            setImageSlider();
        } else {
            video.setVisibility(View.GONE);
            mDemoSlider.setVisibility(View.VISIBLE);
            setImageSlider();
        }

        //Log.e("Imm", url + "0.jpg");


        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                    if (ShareDialog.canShow(ShareLinkContent.class)) {
                        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                .setContentTitle("Hey, Check out Catch Me On App . It has awesome ads and my ad is  " + title.getText().toString())
                                .setContentDescription(desc.getText().toString())
                                .setContentUrl(Uri.parse("http://catchmeon.net/catchmeon/ply_str.html"))
                                //  .setContentUrl(Uri.parse("http://catchmeon.net/catchmeon/rrl.php"))
                                //  .setContentUrl(Uri.parse("https://play.google.com/store/apps/"))
                                .setImageUrl(Uri.parse(url + "0.jpg"))
                                .build();
                        shareDialog.show(linkContent);
                    }
                } else {
                    Intent ii = new Intent(AdapAddDetail.this, FacebookShareActivity.class);
                    ii.putExtra("title", title.getText().toString());
                    ii.putExtra("image", url + "0.jpg");
                    ii.putExtra("desc", desc.getText().toString());
                    startActivity(ii);
                }

            }
        });

        gp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shareIntent = ShareCompat.IntentBuilder.from(AdapAddDetail.this)
                        .setType("text/plain")
                        .setText("Catch Me On App" + "\n" + "Hey, Check out Catch Me On App. It has awesome ads for you, visit to https://play.google.com/store/apps/details?id=com.classifides.app  and my ad is " + "\n" + title.getText().toString() + " \n " + desc.getText().toString())
                        .getIntent()
                        .setPackage("com.google.android.apps.plus");


                try {
                    startActivity(shareIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    ToastHelper.showToastLong(getApplicationContext(), "Google + have not been installed.");
                }


            }
        });

        tw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(url + "0.jpg");
                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                whatsappIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Catch Me On App");
                whatsappIntent.putExtra(Intent.EXTRA_TEXT, "Catch Me On App" + "\n" + "Hey, Check out Catch Me On App. It has awesome ads for you, visit to https://play.google.com/store/apps/details?id=com.classifides.app  and my ad is " + "\n" + title.getText().toString() + " \n " + desc.getText().toString());
                whatsappIntent.setType("text/plain");
                String c = url + "0.jpg";
                if (!c.equals("")) {
                    whatsappIntent.putExtra(Intent.EXTRA_STREAM, uri);
                    whatsappIntent.setType("image/jpeg");
                }
                whatsappIntent.setPackage("com.twitter.android");
                try {
                    startActivity(whatsappIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    ToastHelper.showToastLong(getApplicationContext(), "twitter have not been installed.");
                }

            }
        });

        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(url + "0.jpg");
                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                whatsappIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Catch Me On App");
                whatsappIntent.putExtra(Intent.EXTRA_TEXT, "Catch Me On App" + "\n" + "Hey, Check out Catch Me On App. It has awesome ads for you, visit to https://play.google.com/store/apps/details?id=com.classifides.app  and my ad is " + "\n" + title.getText().toString() + " \n " + desc.getText().toString());
                whatsappIntent.setType("text/plain");
                String c = url + "0.jpg";
              /*  if (!c.equals("")) {
                    whatsappIntent.putExtra(Intent.EXTRA_STREAM, uri);
                    whatsappIntent.setType("image/jpeg");
                }*/
                whatsappIntent.setPackage("com.whatsapp");

                try {
                    startActivity(whatsappIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    ToastHelper.showToastLong(getApplicationContext(), "Whatsapp have not been installed.");
                }

            }
        });

        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog d = new Dialog(AdapAddDetail.this);

                d.setContentView(R.layout.send_mail);

                final EditText editTextEmail = (EditText) d.findViewById(R.id.editTextEmail);

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = d.getWindow();
                lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                window.setAttributes(lp);
                MTextView buttonSend = (MTextView) d.findViewById(R.id.buttonSend);

                MTextView cancelSend = (MTextView) d.findViewById(R.id.cancelSend);

                buttonSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (TextUtils.isEmpty(editTextEmail.getText().toString())) {
                            editTextEmail.setError("plz enter email id");
                        } else {
                            //   AdSendMail sm = new AdSendMail(AdapAddDetail.this, "Catch Me On App" + "\n" + "Hey, Check out Catch Me On App. It has awesome ads for you, visit to https://play.google.com/store/apps/details?id=com.classifides.app  and my ad is " + "\n" + editTextEmail.getText().toString(), title.getText().toString(), desc.getText().toString());


                            SendMail sm = new SendMail(AdapAddDetail.this, editTextEmail.getText().toString(), "Catch Me On App", "Catch Me On App" + "\n" + "Hey, Check out Catch Me On App. It has awesome ads for you, visit to https://play.google.com/store/apps/details?id=com.classifides.app  and my ad is " + title.getText().toString() + "\n" + desc.getText().toString());


                            //Executing sendmail to send email
                            sm.execute();

                            d.dismiss();
                        }
                    }
                });

                cancelSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        d.dismiss();
                    }
                });
                d.show();

            }
        });


    }


    private void setImageSlider() {


        HashMap<String, String> url_maps = new HashMap<String, String>();
        for (int i = 0; i < imgcnt; i++) {
            url_maps.put(i + "", url + i + ".jpg");

        }

        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }

        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(5000);
        mDemoSlider.addOnPageChangeListener(this);
    }


    @Override
    protected void onPause() {
        super.onPause();
        JCVideoPlayer.releaseAllVideos();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        setLocation();

        // Add a marker in Sydney, Australia, and move the camera.

    }

    private void setLocation() {
        new AsyncTask() {
            JSONObject rest;

            @Override
            protected Object doInBackground(Object[] params) {

                Map<String, String> map = new HashMap<String, String>();
                map.put("abc", "abc");
                rest = DAO.getJsonFromUrl(Config.Place + placeid, map);


                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    if (rest.getString("status").equals("OK")) {
                        JSONObject latlang = rest.getJSONObject("result").getJSONObject("geometry").getJSONObject("location");
                        address.setText(Html.fromHtml(rest.getJSONObject("result").getString("adr_address")));
                        LatLng sydney = new LatLng(latlang.getDouble("lat"), latlang.getDouble("lng"));
                        mMap.addMarker(new MarkerOptions().position(sydney).title(getIntent().getStringExtra("title")));
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            super.onBackPressed();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }
}
