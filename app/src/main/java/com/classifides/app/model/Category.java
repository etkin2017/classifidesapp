package com.classifides.app.model;

/**
 * Created by Ashvini on 8/9/2016.
 */
public class Category {

    String c_name;

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }
}
