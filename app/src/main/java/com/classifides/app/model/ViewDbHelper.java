package com.classifides.app.model;

/**
 * Created by Ashvini on 14-07-2016.
 */


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ViewDbHelper extends SQLiteOpenHelper {


    //public static String  databaseName="users.db";


    static String DATABASE_NAME = "postview.db";
    public static final String TABLE_NAME = "user";
    public static final String AD_ID = "ad_id";

    public static final String TABLE_NAME_TWo = "noti";
    public static final String NEXT = "next_id";

    public static final String KEY_LNAME = "lname";
    public static final String KEY_ID = "id";

    public ViewDbHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" + AD_ID + " TEXT PRIMARY KEY)";

        String CREATE_TABLE_TWO = "CREATE TABLE " + TABLE_NAME_TWo + " (" + NEXT + " TEXT PRIMARY KEY)";

        db.execSQL(CREATE_TABLE);
        db.execSQL(CREATE_TABLE_TWO);

        //Log.e("table1","Table is created");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_TWo);


        onCreate(db);
    }
}
