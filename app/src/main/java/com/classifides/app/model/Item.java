package com.classifides.app.model;

/**
 * Created by bpncool on 2/23/2016.
 */
public class Item {

    private  String name;
    private  int id;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    private  boolean isChecked;

    public int getValue() {
        return value;
    }

    private  int value;

    public Item(String name, int id,int value,boolean isChecked) {
        this.name = name;
        this.id = id;
        this.value=value;
        this.isChecked=isChecked;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
