package com.classifides.app.model;

import java.util.ArrayList;

/**
 * Created by Rajeshwar on 29/06/2016.
 */
public class locations {
    public ArrayList<String> getLocs() {
        return locs;
    }

    public void setLocs(ArrayList<String> locs) {
        this.locs = locs;
    }

    private ArrayList<String> locs;
}
