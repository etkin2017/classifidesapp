package com.classifides.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.classifides.app.application.Classified;
import com.classifides.app.data.Blur;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.fragment.EditProfile;
import com.classifides.app.fragment.UserDetail;
import com.classifides.app.view.MTextView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserAccount extends AppCompatActivity {

    SharedPreferences sh;
    String img1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_account);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            // only for gingerbread and newer versions
        } else {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("User Account");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }




        sh = getSharedPreferences("user", Context.MODE_PRIVATE);
        String user = sh.getString("users", "");
        try {
            JSONObject us = new JSONObject(user);
            img1 = us.getString("image");

        } catch (JSONException e) {
            e.printStackTrace();
        }


      /*  getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container, new UserDetail(), "userdetail")
                .commit();*/

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container, new EditProfile(), "userdetail")
                .commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            super.onBackPressed();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getUserInfo(final String user_id) {

        final Map<String, String> data = new HashMap<String, String>();


        new AsyncTask() {
            JSONObject res = null;
            ProgressDialog pdd;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();


                data.put("id", user_id);

            /*    try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
            }

            @Override
            protected Object doInBackground(Object[] params) {
                res = DAO.getJsonFromUrl(Config.userInfo, data);

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                try {
                    if (res.getInt("success") > 0) {
                        //  Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_SHORT).show();
                        //  getActivity().finish();

                        JSONArray jsonArray = res.getJSONArray("user");
                        // Log.e("JsONArray", jsonArray.toString());
                        JSONObject jobj = jsonArray.getJSONObject(0);

                        sh = getSharedPreferences("user", Context.MODE_PRIVATE);
                        sh.edit().putString("users", res.getJSONArray("user").getJSONObject(0).toString()).commit();


                        //Log.e("image", jobj.getString("image"));

                        String iiii=jobj.getString("image");

                        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View v = inflater.inflate(R.layout.app_bar_home, null);
                        CircleImageView img = (CircleImageView) v.findViewById(R.id.uimg);
                        Picasso.with(Classified.getInstance()).load(iiii).into(img);


                        //  title.setText(jobj.getString("title"));


                    } else {
                        //  Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SharedPreferences sh = getSharedPreferences("user", Context.MODE_PRIVATE);
        String user = sh.getString("users", "");
        try {
            JSONObject us = new JSONObject(user);
            int id = us.getInt("id");
            getUserInfo(id + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
     /*   LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.app_bar_home, null);
        CircleImageView img = (CircleImageView) v.findViewById(R.id.uimg);
        Picasso.with(Classified.getInstance()).load(img1).into(img);*/
    //    startActivity(new Intent(UserAccount.this,BrowseByCategory.class));

    }


    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sh = getSharedPreferences("user", Context.MODE_PRIVATE);
        String user = sh.getString("users", "");
        try {
            JSONObject us = new JSONObject(user);
            int id = us.getInt("id");
            getUserInfo(id + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
       /* LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.app_bar_home, null);
        CircleImageView img = (CircleImageView) v.findViewById(R.id.uimg);
        Picasso.with(Classified.getInstance()).load(img1).into(img);*/
    }
}
