package com.classifides.app;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.classifides.app.application.Classified;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.model.ViewDbHelper;
import com.classifides.app.view.MEditText;
import com.classifides.app.view.MTextView;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;

public class AddDetail extends AppCompatActivity implements OnMapReadyCallback, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener, View.OnTouchListener {


    String TAG = "VideoView";

    boolean d;
    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();

    // We can be in one of these 3 states
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;

    // Remember some things for zooming
    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 1f;

    private ImageView iv;
    //  private Matrix matrix = new Matrix();
    private float scale = 1f;
    private ScaleGestureDetector SGD;


    private GoogleMap mMap;
    JSONObject add;
    private String placeid;
    MTextView title, price, desc, cat, subcat, discount, cashback, address, user_name, phone_no, email_id, website, views;
    JCVideoPlayerStandard video;
    private SliderLayout mDemoSlider;
    RelativeLayout slider_container;
    String url;
    int type;
    int p, n;
    int pres, npres;
    int imgcnt;
    LinearLayout send_msg_btn, web_container;
    EditText send_msg;

    ShareDialog shareDialog;
    CallbackManager callbackManager;
    public String un, um, up;
    HashMap<String, String> data;
    String add_id, uid, aid;

    String ad_post_user_id;
    public static boolean check = false;
    ImageView single_img;

    LinearLayout cashback_container;
    LinearLayout discount_container;

    private ViewDbHelper mHelper;
    private SQLiteDatabase dataBase;

    ImageView fb, gp, tw, whatsapp, mail;

    public void getAdd() {
        try {
            add = new JSONObject(getIntent().getStringExtra("add"));
            placeid = add.getString("placeid");
            title.setText(add.getString("title"));
            price.setText("Rs " + add.getString("price"));
            desc.setText(add.getString("description"));
            cat.setText(":  " + add.getString("category"));
            subcat.setText(":  " + add.getString("subcategory"));
            views.setText("Total Views :  " + add.getString("v"));
            //  cashback.setText(":  " + add.getString("cash_discount") + "%");


            String disc = add.getString("discount");

            //   String  d= getIntent().getStringExtra("discount");
            if (disc.equals("0") || disc.equals("null")) {
                discount.setVisibility(View.GONE);
                discount_container.setVisibility(View.GONE);
            } else {

                discount.setText(":  " + disc + "%");
            }

/*try {
    if (getIntent().getStringExtra("discount").equals("null")) {
        discount.setVisibility(View.GONE);
        discount_container.setVisibility(View.GONE);
    } else {

        discount.setText(":  " + getIntent().getStringExtra("discount") + "%");
    }
}catch (Exception e){
    discount.setVisibility(View.GONE);
    discount_container.setVisibility(View.GONE);
}*/

            String cash = add.getString("cash_discount");
            //  discount.setText(":  " + getIntent().getStringExtra("discount") + "%");

            if (cash.equals("No")) {

                cashback.setVisibility(View.GONE);
                cashback_container.setVisibility(View.GONE);
            } else {
                cashback.setText(": Yes");
            }

            String web = add.getString("website");
            if (web.equals("") || web.equals("null")) {

                web_container.setVisibility(View.GONE);
                website.setVisibility(View.GONE);
            } else {
                website.setText(web);
            }


            url = add.getString("imageLoc");
            //Log.e("url -m", url + "");
            type = add.getInt("type");
            imgcnt = add.getInt("imageCount");
            //Log.e("img_count -m", imgcnt + "");
/*

            if (imgcnt==1){
                single_img.setVisibility(View.VISIBLE);
                mDemoSlider.setVisibility(View.GONE);
                Picasso.with(getApplicationContext()).load(url).into(single_img);
            }
            else{
                single_img.setVisibility(View.GONE);
                mDemoSlider.setVisibility(View.VISIBLE);
                setImageSlider();
            }
*/

            aid = add.getString("add_id");

            add_id = add.getString("v");
            uid = add.getString("user_id");


            ad_post_user_id = add.getString("user_id");


            if (TextUtils.isEmpty(uid)) {

                //Log.e("NoUSer", uid.toString());
            } else {

                //Log.e("UidNoUSer", uid.toString());
                getUserInfo(ad_post_user_id);
            }


            if (displayData(add_id) == true) {

            } else {
                saveData(add_id);
            }


            send_msg_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (TextUtils.isEmpty(send_msg.getText().toString())) {
                        send_msg.setError("Type Message.....");
                    } else {

                        int id = 0;
                        JSONObject user = DAO.getUser();

                        try {
                            id = user.getInt("id");
                            um = user.getString("mobile");
                            up = user.getString("email");
                            un = user.getString("name");

                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                        //Log.e("user_id", id + "");


                        Map<String, String> map = new HashMap<String, String>();
                        map.put("aid", aid);
                        map.put("uname", un);
                        map.put("uemail", up);
                        map.put("umobile", um);
                        map.put("umessage", send_msg.getText().toString());
                        map.put("uid", uid);
                        map.put("cat", cat.getText().toString());
                        map.put("subcat", subcat.getText().toString());
                        insertAdRply(map);

                    }


                }
            });
        } catch (
                JSONException e
                )

        {
            e.printStackTrace();
        }

    }

    private void insertAdRply(final Map<String, String> map) {
        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //  ProgressDialog();
            }

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    res = DAO.getJsonFromUrl(Config.insertAdReply, map);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                //   DismissProgress();


                try {
                    if (res.getInt("success") > 0) {
                        Toast.makeText(AddDetail.this, res.getString("message"), Toast.LENGTH_LONG).show();

                        Intent ii = new Intent(AddDetail.this, HomeActivity.class);
                        startActivity(ii);
                    } else {
                       /* result.setText(res.getString("message"));
                        result.setVisibility(View.VISIBLE);*/

                        Toast.makeText(AddDetail.this, res.getString("message"), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                 /*   result.setText("Error in Registration");
                    result.setVisibility(View.VISIBLE);*/
                }
            }
        }.execute(null, null, null);
    }


    public void initView() {
        slider_container = (RelativeLayout) findViewById(R.id.slider_container);
        single_img = (ImageView) findViewById(R.id.single_img);
        title = (MTextView) findViewById(R.id.titles);
        price = (MTextView) findViewById(R.id.price);
        desc = (MTextView) findViewById(R.id.desc);
        cat = (MTextView) findViewById(R.id.cat);
        subcat = (MTextView) findViewById(R.id.subcat);
        discount = (MTextView) findViewById(R.id.discount);
        cashback = (MTextView) findViewById(R.id.cashback);
        address = (MTextView) findViewById(R.id.address);
        video = (JCVideoPlayerStandard) findViewById(R.id.video);
        mDemoSlider = (SliderLayout) findViewById(R.id.slider);
        data = new HashMap<String, String>();

        web_container = (LinearLayout) findViewById(R.id.web_container);

        user_name = (MTextView) findViewById(R.id.user_name);
        phone_no = (MTextView) findViewById(R.id.phone_name);
        email_id = (MTextView) findViewById(R.id.email_id_info);
        send_msg = (EditText) findViewById(R.id.replay_to_add);

        website = (MTextView) findViewById(R.id.website);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);


        fb = (ImageView) findViewById(R.id.fb);
        gp = (ImageView) findViewById(R.id.gp);
        tw = (ImageView) findViewById(R.id.tw);
        whatsapp = (ImageView) findViewById(R.id.whatsapp);
        mail = (ImageView) findViewById(R.id.gmail);
        cashback_container = (LinearLayout) findViewById(R.id.cashback_container);
        discount_container = (LinearLayout) findViewById(R.id.discount_container);
        send_msg_btn = (LinearLayout) findViewById(R.id.send_message);
        views = (MTextView) findViewById(R.id.views);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            // only for gingerbread and newer versions
        } else {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");

        }

        mHelper = new ViewDbHelper(getApplicationContext());

        initView();
        getAdd();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        p = imgcnt;
        n = imgcnt;

        if (type == 1) {
            video.setVisibility(View.VISIBLE);
            mDemoSlider.setVisibility(View.GONE);
            video.setUp(url, "");
        } else if (type == 2) {
            video.setVisibility(View.GONE);
            mDemoSlider.setVisibility(View.VISIBLE);
            setBannerImageSlider();
        } else {

            //Log.e("IMG_CNT", type + " n " + imgcnt + "");
            if (imgcnt == 1) {
                single_img.setVisibility(View.VISIBLE);
                video.setVisibility(View.GONE);
                mDemoSlider.setVisibility(View.GONE);
                Picasso.with(Classified.getInstance()).load(url + "0.jpg").placeholder(R.mipmap.logo).into(single_img);

                single_img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Dialog ddd = new Dialog(AddDetail.this, R.style.mydialogstyle);
                        ddd.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        ddd.setContentView(R.layout.zoom_on_img);


                        SGD = new ScaleGestureDetector(AddDetail.this, new ScaleListener());

                        ImageView b = (ImageView) ddd.findViewById(R.id.zoom_img);
                        Button pre = (Button) ddd.findViewById(R.id.previous);
                        Button next = (Button) ddd.findViewById(R.id.next);
                        Picasso.with(Classified.getInstance()).load(url + "0.jpg").placeholder(R.mipmap.logo).into(b);
                        b.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        b.setOnTouchListener(AddDetail.this);


                        pre.setVisibility(View.GONE);
                        next.setVisibility(View.GONE);
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = ddd.getWindow();
                        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                        window.setAttributes(lp);


                        //   Picasso.with(Classified.getInstance()).load(data.getJSONObject(position).getString("imageLoc")).into(i);
                        ddd.show();

                    }
                });

            } else {
                single_img.setVisibility(View.GONE);
                video.setVisibility(View.GONE);
                mDemoSlider.setVisibility(View.VISIBLE);
                setImageSlider();


              /*  slider_container.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Dialog ddd = new Dialog(AddDetail.this, R.style.mydialogstyle);
                        ddd.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        ddd.setContentView(R.layout.zoom_on_img);

                        final ImageView b = (ImageView) ddd.findViewById(R.id.zoom_img);
                        final Button pre = (Button) ddd.findViewById(R.id.previous);
                        final Button next = (Button) ddd.findViewById(R.id.next);
                        Picasso.with(Classified.getInstance()).load(url + "0.jpg").into(b);
                        pre.setVisibility(View.GONE);

                        next.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                pre.setVisibility(View.VISIBLE);
                                for (int i = 0; i < imgcnt; i++) {
                                    n = n - (1 + i);
                                    Picasso.with(Classified.getInstance()).load(url + i + ".jpg").into(b);

                                    if ((i - 1) == imgcnt) {
                                        next.setVisibility(View.GONE);
                                    }
                                }
                            }
                        });

                        pre.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                next.setVisibility(View.VISIBLE);
                                for (int i = 0; i < imgcnt; i++) {
                                    p = p - (1 + i);
                                    Picasso.with(Classified.getInstance()).load(url + i + ".jpg").into(b);

                                    if ((i - 1) == imgcnt) {
                                        pre.setVisibility(View.GONE);
                                    }
                                }
                            }
                        });

                    *//*    pre.setVisibility(View.GONE);
                        next.setVisibility(View.GONE);*//*
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = ddd.getWindow();
                        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                        window.setAttributes(lp);


                        //   Picasso.with(Classified.getInstance()).load(data.getJSONObject(position).getString("imageLoc")).into(i);
                        ddd.show();*/
                // }
                //    });
            }


            // mDemoSlider.setVisibility(View.VISIBLE);
            // setImageSlider();
        }

        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {

                    if (ShareDialog.canShow(ShareLinkContent.class)) {
                        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                .setContentTitle("Hey, Check out Catch Me On App . It has awesome ads and my ad is " + title.getText().toString())
                                .setContentDescription(desc.getText().toString())
                                .setContentUrl(Uri.parse("http://catchmeon.net/catchmeon/ply_str.html"))
                               // .setContentUrl(Uri.parse("http://catchmeon.net/catchmeon/rrl.php"))
                               // .setContentUrl(Uri.parse(""))
                              //  .setContentUrl(Uri.parse("https://play.google.com/store/apps/"))
                                .setImageUrl(Uri.parse(url + "0.jpg"))
                                .build();
                        shareDialog.show(linkContent);
                    }
                } else {

                    Intent ii = new Intent(AddDetail.this, FacebookShareActivity.class);
                    ii.putExtra("title", title.getText().toString());
                    ii.putExtra("image", url + "0.jpg");
                    ii.putExtra("desc", desc.getText().toString());
                    startActivity(ii);
                }


            }
        });

        gp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shareIntent = ShareCompat.IntentBuilder.from(AddDetail.this)
                        .setType("text/plain")
                        .setText("Catch Me On App" + "\n" + "Hey, Check out Catch Me On App. It has awesome ads for you, visit to https://play.google.com/store/apps/details?id=com.classifides.app  and my ad is " + "\n" + title.getText().toString() + " \n " + desc.getText().toString())
                        .getIntent()
                        .setPackage("com.google.android.apps.plus");


                try {
                    startActivity(shareIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    ToastHelper.showToastLong(getApplicationContext(), "Google + have not been installed.");
                }


            }
        });

        tw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(url + "0.jpg");
                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                whatsappIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Catch Me On App");
                whatsappIntent.putExtra(Intent.EXTRA_TEXT, "Catch Me On App" + "\n" + "Hey, Check out Catch Me On App. It has awesome ads for you, visit to https://play.google.com/store/apps/details?id=com.classifides.app  and my ad is " + "\n" + title.getText().toString() + " \n " + desc.getText().toString());
                whatsappIntent.setType("text/plain");
              /*  String c = url + "0.jpg";
                if (!c.equals("")) {
                    whatsappIntent.putExtra(Intent.EXTRA_STREAM, uri);
                    whatsappIntent.setType("image/jpeg");
                }*/
                whatsappIntent.setPackage("com.twitter.android");
                try {
                    startActivity(whatsappIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    ToastHelper.showToastLong(getApplicationContext(), "twitter have not been installed.");
                }

            }
        });

        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(url + "0.jpg");
                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                whatsappIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Catch Me On App");
                whatsappIntent.putExtra(Intent.EXTRA_TEXT, "Catch Me On App" + "\n" + "Hey, Check out Catch Me On App. It has awesome ads for you, visit to https://play.google.com/store/apps/details?id=com.classifides.app  and my ad is " + "\n" + title.getText().toString() + " \n " + desc.getText().toString());
                whatsappIntent.setType("text/plain");
               /* String c = url + "0.jpg";
                if (!c.equals("")) {
                    whatsappIntent.putExtra(Intent.EXTRA_STREAM, uri);
                    whatsappIntent.setType("image/jpeg");
                }*/
                whatsappIntent.setPackage("com.whatsapp");

                try {
                    startActivity(whatsappIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    ToastHelper.showToastLong(getApplicationContext(), "Whatsapp have not been installed.");
                }

            }
        });

        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog d = new Dialog(AddDetail.this);

                d.setContentView(R.layout.send_mail);

                final EditText editTextEmail = (EditText) d.findViewById(R.id.editTextEmail);

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = d.getWindow();
                lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                window.setAttributes(lp);
                MTextView buttonSend = (MTextView) d.findViewById(R.id.buttonSend);

                MTextView cancelSend = (MTextView) d.findViewById(R.id.cancelSend);

                buttonSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (TextUtils.isEmpty(editTextEmail.getText().toString())) {
                            editTextEmail.setError("plz enter email id");
                        } else {
                            SendMail sm = new SendMail(AddDetail.this, editTextEmail.getText().toString(), "Catch Me On App", "Catch Me On App" + "\n" + "Hey, Check out Catch Me On App. It has awesome ads for you, visit to https://play.google.com/store/apps/details?id=com.classifides.app  and my ad is " + title.getText().toString() + "\n" + desc.getText().toString());

                            //Executing sendmail to send email
                            sm.execute();

                            d.dismiss();
                        }
                    }
                });

                cancelSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        d.dismiss();
                    }
                });
                d.show();

            }
        });


    }

    private void setImageSlider() {


        HashMap<String, String> url_maps = new HashMap<String, String>();
        for (int i = 0; i < imgcnt; i++) {
            url_maps.put(i + "", url + i + ".jpg");

        }

        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }

        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(5000);
        mDemoSlider.addOnPageChangeListener(this);
    }

    private void setBannerImageSlider() {


        HashMap<String, String> url_maps = new HashMap<String, String>();
        for (int i = 0; i < imgcnt; i++) {
            url_maps.put(i + "", url);

        }

        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }

        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(5000);
        mDemoSlider.addOnPageChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        JCVideoPlayer.releaseAllVideos();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        setLocation();

        // Add a marker in Sydney, Australia, and move the camera.

    }

    private void setLocation() {
        new AsyncTask() {
            JSONObject rest;

            @Override
            protected Object doInBackground(Object[] params) {

                Map<String, String> map = new HashMap<String, String>();
                map.put("abc", "abc");
                rest = DAO.getJsonFromUrl(Config.Place + placeid, map);


                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    if (rest.getString("status").equals("OK")) {
                        JSONObject latlang = rest.getJSONObject("result").getJSONObject("geometry").getJSONObject("location");
                        address.setText(Html.fromHtml(rest.getJSONObject("result").getString("adr_address")));
                        LatLng sydney = new LatLng(latlang.getDouble("lat"), latlang.getDouble("lng"));
                        mMap.addMarker(new MarkerOptions().position(sydney).title(add.getString("title")));
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }


    private void saveData(String bv2) {
        dataBase = mHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ViewDbHelper.AD_ID, bv2);
        System.out.println("");
        dataBase.insert(ViewDbHelper.TABLE_NAME, null, values);
        //close database        
        dataBase.close();

        //   updateView(bv2);


    }

    public void updateView(final String add_id) {

        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                //   int id=0;
                data.put("add_id", add_id);
                //    data.put("uid",uid);

                try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getInt("id") + "");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected Object doInBackground(Object[] params) {
                res = DAO.getJsonFromUrl(Config.updateView, data);

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                //DismissProgress();
                try {
                    if (res.getInt("success") > 0) {
                        //  Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_SHORT).show();
                        //  getActivity().finish();
                    } else {
                        //  Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }


    public void getUserInfo(final String user_id) {

        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                data.put("id", user_id);

            }

            @Override
            protected Object doInBackground(Object[] params) {
                res = DAO.getJsonFromUrl(Config.userInfo, data);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                //DismissProgress();
                try {
                    if (res.getInt("success") > 0) {
                        //  Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_SHORT).show();
                        //  getActivity().finish();

                        JSONArray jsonArray = res.getJSONArray("user");
                        // Log.e("JsONArray", jsonArray.toString());
                        JSONObject jobj = jsonArray.getJSONObject(0);

                        user_name.setText(jobj.getString("name"));
                        phone_no.setText(jobj.getString("mobile"));
                        email_id.setText(jobj.getString("email"));


                        if (TextUtils.isEmpty(aid)) {

                            //Log.e("NoUSer", aid.toString());
                        } else {

                            //Log.e("NoUSer", aid.toString());

                            updateView(aid);
                        }

                    } else {


                        if (TextUtils.isEmpty(aid)) {

                            //Log.e("NoUSer", aid.toString());
                        } else {

                            //Log.e("NoUSer", aid.toString());

                            updateView(aid);
                        }
                        //  Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                    if (TextUtils.isEmpty(aid)) {

                        //Log.e("NoUSer", aid.toString());
                    } else {

                        //Log.e("NoUSer", aid.toString());

                        updateView(aid);
                    }
                    e.printStackTrace();
                }


            }
        }.execute(null, null, null);
    }

    public boolean displayData(String bv) {

        dataBase = mHelper.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM " + ViewDbHelper.TABLE_NAME, null);

        if (mCursor.moveToFirst()) {
            do {

                mCursor.getString(mCursor.getColumnIndex(ViewDbHelper.AD_ID));
                if (mCursor.getString(mCursor.getColumnIndex(ViewDbHelper.AD_ID)).equals(bv)) {
                    check = true;
                    break;
                }
            } while (mCursor.moveToNext());
        }

        mCursor.close();
        return check;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //startActivity(new Intent(AddDetail.this, HomeActivity.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            super.onBackPressed();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

        Dialog ddd = new Dialog(AddDetail.this);
        ddd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ddd.setContentView(R.layout.zoom_on_img);
        final int iii = imgcnt + imgcnt;
        final ImageView b = (ImageView) ddd.findViewById(R.id.zoom_img);
        final Button pre = (Button) ddd.findViewById(R.id.previous);
        final Button next = (Button) ddd.findViewById(R.id.next);
        Picasso.with(Classified.getInstance()).load(url + "0.jpg").placeholder(R.mipmap.logo).into(b);
        pre.setVisibility(View.VISIBLE);
        b.setScaleType(ImageView.ScaleType.FIT_CENTER);
        b.setOnTouchListener(this);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pres = pres + 1;
                if (pres == 1) {
                    b.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    Picasso.with(Classified.getInstance()).load(url + "1.jpg").placeholder(R.mipmap.logo).into(b);
                } else if (pres == 2) {
                    b.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    Picasso.with(Classified.getInstance()).load(url + "2.jpg").placeholder(R.mipmap.logo).into(b);
                } else if (pres == 3) {
                    b.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    Picasso.with(Classified.getInstance()).load(url + "3.jpg").placeholder(R.mipmap.logo).into(b);
                }

                if (pres == (imgcnt - 1)) {
                    next.setVisibility(View.VISIBLE);
                    pre.setVisibility(View.VISIBLE);
                    pres = 0;
                }

            }
        });


        pre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (npres == 1) {
                    b.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    Picasso.with(Classified.getInstance()).load(url + (imgcnt - 2) + ".jpg").placeholder(R.mipmap.logo).into(b);
                    next.setVisibility(View.VISIBLE);

                } else if (npres == 2) {
                    b.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    Picasso.with(Classified.getInstance()).load(url + (imgcnt - 3) + ".jpg").placeholder(R.mipmap.logo).into(b);
                    next.setVisibility(View.VISIBLE);

                } else if (npres == 3) {
                    b.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    Picasso.with(Classified.getInstance()).load(url + (imgcnt - 4) + ".jpg").placeholder(R.mipmap.logo).into(b);
                    next.setVisibility(View.VISIBLE);

                }


                if (npres == (imgcnt - 1)) {
                    pre.setVisibility(View.VISIBLE);
                    next.setVisibility(View.VISIBLE);
                    npres = 0;
                }
                npres = npres + 1;

            }
        });


            /*    pre.setVisibility(View.GONE);
     /*                   next.setVisibility(View.GONE);*//*
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = ddd.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);*/


        //   Picasso.with(Classified.getInstance()).load(data.getJSONObject(position).getString("imageLoc")).into(i);
        ddd.show();

    }

    public boolean onTouchEvent(MotionEvent ev) {
        SGD.onTouchEvent(ev);
        return true;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        ImageView view = (ImageView) v;
        // make the image scalable as a matrix
        view.setScaleType(ImageView.ScaleType.MATRIX);
        float scale;

        // Handle touch events here...
        switch (event.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN: //first finger down only
                savedMatrix.set(matrix);
                start.set(event.getX(), event.getY());
                Log.d(TAG, "mode=DRAG");
                mode = DRAG;
                break;
            case MotionEvent.ACTION_UP: //first finger lifted
            case MotionEvent.ACTION_POINTER_UP: //second finger lifted
                mode = NONE;
                Log.d(TAG, "mode=NONE");
                break;
            case MotionEvent.ACTION_POINTER_DOWN: //second finger down
                oldDist = spacing(event); // calculates the distance between two points where user touched.
                Log.d(TAG, "oldDist=" + oldDist);
                // minimal distance between both the fingers
                if (oldDist > 5f) {
                    savedMatrix.set(matrix);
                    midPoint(mid, event); // sets the mid-point of the straight line between two points where user touched.
                    mode = ZOOM;
                    Log.d(TAG, "mode=ZOOM");
                }
                break;

            case MotionEvent.ACTION_MOVE:
                if (mode == DRAG) { //movement of first finger
                    matrix.set(savedMatrix);
                    if (view.getLeft() >= -392) {
                        matrix.postTranslate(event.getX() - start.x, event.getY() - start.y);
                    }
                } else if (mode == ZOOM) { //pinch zooming
                    float newDist = spacing(event);
                    Log.d(TAG, "newDist=" + newDist);
                    if (newDist > 5f) {
                        matrix.set(savedMatrix);
                        scale = newDist / oldDist; //thinking I need to play around with this value to limit it**
                        matrix.postScale(scale, scale, mid.x, mid.y);
                    }
                }
                break;
        }

        // Perform the transformation
        view.setImageMatrix(matrix);

        return true; // indicate event was handled
    }

    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);

        float d = (float) Math.sqrt(x * x + y * y);
        return d;
    }

    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    private class ScaleListener extends ScaleGestureDetector.

            SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scale *= detector.getScaleFactor();
            scale = Math.max(0.1f, Math.min(scale, 5.0f));

            matrix.setScale(scale, scale);
            iv.setImageMatrix(matrix);
            return true;
        }
    }


}