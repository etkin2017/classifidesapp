package com.classifides.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.classifides.app.application.Classified;
import com.classifides.app.data.ComplexPreferences;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.model.locations;
import com.classifides.app.view.MTextView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ReplyToMyAdd extends AppCompatActivity {

    List<MyAdPojo> arr;
    MAdaptor adaptor;
    public ArrayList<String> delete;

    RecyclerView rec_reply_to_my_add;

    MTextView deletead;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reply_to_my_add);

        deletead = (MTextView) findViewById(R.id.deletead);
        rec_reply_to_my_add = (RecyclerView) findViewById(R.id.rec_reply_to_my_add);
        arr = new ArrayList<MyAdPojo>();
        adaptor = new MAdaptor();
        delete = new ArrayList<String>();
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rec_reply_to_my_add.setLayoutManager(llm);

        deletead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (int i = 0; i < delete.size(); i++) {

                    delAd(delete.get(i));
                }

            }
        });

        getProducts();
    }


    private void delAd(final String id) {

        new AsyncTask() {
            ProgressDialog p;
            JSONObject res;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
              /*  p = new ProgressDialog(MyAdd.this);
                pd.setMessage("We are updating Ad detail");
                pd.setTitle("Please wait....");
                pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                pd.setIndeterminate(true);
                pd.setCanceledOnTouchOutside(false);
                pd.show();*/
            }

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    Map<String, String> d = new HashMap<String, String>();
                    d.put("id", id);

                    res = DAO.getJsonFromUrl(Config.deleteOthersAdsReply, d);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
              /*  pd.dismiss();*/
                try {
                    if (res.getInt("success") > 0) {

                        MAdaptor adaptor1 = new MAdaptor();
                        Toast.makeText(getApplicationContext(), "Your has been deleted", Toast.LENGTH_LONG).show();
                        adaptor1.notifyDataSetChanged();
                       /* finish();
                        startActivity(new Intent(ReplyToMyAdd.this,ReplyToMyAdd.class));*/

                        arr.clear();
                        getProducts();

                    } else {
                        Toast.makeText(getApplicationContext(), "Ad not deleted", Toast.LENGTH_LONG).show();
                        //     setValues();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "error in deletion", Toast.LENGTH_LONG).show();
                }
            }
        }.execute(null, null, null);

    }

    private void getProducts() {

        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Object doInBackground(Object[] params) {
                int id = 0;
                try {
                    JSONObject user = DAO.getUser();

                    id = user.getInt("id");
                    //Log.e("user_id",id+"");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Map<String, String> map = new HashMap<String, String>();
                map.put("uid", id + "");
                rest = DAO.getJsonFromUrl(Config.getAdsMessage, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {

                    if (rest.getInt("success") > 0) {


                        for (int i = 0; i < rest.getJSONArray("advertis").length(); i++) {
                            JSONObject build = rest.getJSONArray("advertis").getJSONObject(i);

                            //Log.e("Advertise", build.toString());

                            MyAdPojo thumb = new MyAdPojo();

                            thumb.setId(build.getString("id"));
                            thumb.setAdid(build.getString("aid"));
                            ;

                            thumb.setName(build.getString("uname"));
                            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");


                            thumb.setEmail(build.getString("uemail"));
                            thumb.setContactNo(build.getString("umobile"));
                            thumb.setCat(build.getString("category"));
                            thumb.setSubcat(build.getString("subCategory"));

                            thumb.setMessage(build.getString("umessage"));
                            thumb.setTitle(build.getString("title"));
                            thumb.setDate(build.getString("postTime").substring(0, 10));


                            thumb.setPrice(build.getString("price"));


                            //  int type = build.getInt("type");


                            arr.add(thumb);
                        }
                        // customerAdapter.setData(arr_anp);

                        adaptor.setData(getApplicationContext(), arr);
                        rec_reply_to_my_add.setAdapter(adaptor);
                    } else {
                       // Toast.makeText(getApplicationContext(), "No Data is Present", Toast.LENGTH_SHORT).show();
                    }
                      /*  adaptor.setData(getApplicationContext(), rest.getJSONArray("advertis"));
                        prod.setAdapter(adaptor);
                    } else {
                        adaptor = new MyAddAdapter();
                        adaptor.setData(getApplicationContext(), new JSONArray());
                        prod.setAdapter(adaptor);
                        Toast.makeText(MyAdd.this, rest.getString("message"), Toast.LENGTH_SHORT).show();
                    }*/

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

    private class MAdaptor extends RecyclerView.Adapter<MAdaptor.ViewHolder> {


        Context ctx;
        List<MyAdPojo> name;
        ArrayList<Integer> chk_p;
        boolean[] itemChecked;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ad_details, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            try {

                //    MTextView title, date_ad, ad_rs, cost, ad_cat_sub, subcat,ad_reply_name,
                // ad_reply_email,ad_reply_cont_no,ad_reply_msg;


                holder.title.setText(name.get(position).getTitle());
                holder.date_ad.setText("Date: " + name.get(position).getDate().substring(0, 10));
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                holder.ad_rs.setText("Rs  " + name.get(position).getPrice() + "/-");
                holder.cost.setText("Ad Id: " + name.get(position).getAdid());
                holder.ad_cat_sub.setText("Cat " + name.get(position).getCat() + System.getProperty("line.separator") + "," + "Subcat " + name.get(position).getSubcat());


                holder.ad_reply_name.setText(name.get(position).getName());
                holder.ad_reply_email.setText(name.get(position).getEmail());
                // int type = Integer.parseInt(name.get(position).getType());

                holder.id.setText(name.get(position).getId());

                holder.ad_reply_cont_no.setText(name.get(position).getContactNo());


                holder.ad_reply_msg.setText(name.get(position).getMessage());


                holder.itemView.setTag(name.get(position));

                final ViewHolder finalHc1 = holder;


                holder.chk.setTag(name.get(position));


                if (itemChecked[position])
                    holder.chk.setChecked(true);
                else
                    holder.chk.setChecked(false);


                final ViewHolder finalHc2 = holder;
                finalHc2.chk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        if (finalHc2.chk.isChecked() == true) {
                            itemChecked[position] = true;
                            delete.add(finalHc2.id.getText().toString());

                        } else {
                            itemChecked[position] = false;

                            if (delete.contains(finalHc2.id.getText().toString())) {
                                delete.remove(finalHc2.id.getText().toString());
                            }


                        }
                    }
                });

           /*     holder.chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        try {
                            JSONObject obj = (JSONObject) buttonView.getTag();
                            obj.put("chk", isChecked);

                            if (holder.chk.isChecked()) {
                                holder.chk.setChecked(true);
                                MyAdd.user_id.add(holder.user_id.getText().toString());
                                MyAdd.add_id.add(holder.add_id.getText().toString());

                            } else {
                                holder.chk.setChecked(false);
                                if (MyAdd.add_id.contains(holder.user_id.getText().toString())) {
                                    MyAdd.user_id.remove(holder.user_id.getText().toString());
                                }

                                if (MyAdd.add_id.contains(holder.add_id.getText().toString())) {
                                    MyAdd.add_id.remove(holder.add_id.getText().toString());
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                holder.chk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        boolean cb = (boolean) view;
                        JSONObject object = (JSONObject) cb.getTag();
                        //   object.put("chk",holder)
                        cb.setChecked(true);


                        if (cb.isChecked()) {
                            holder.chk.setChecked(true);
                            MyAdd.user_id.add(holder.user_id.getText().toString());
                            MyAdd.add_id.add(holder.add_id.getText().toString());

                        } else {
                            holder.chk.setChecked(false);
                            if (MyAdd.add_id.contains(holder.user_id.getText().toString())) {
                                MyAdd.user_id.remove(holder.user_id.getText().toString());
                            }

                            if (MyAdd.add_id.contains(holder.add_id.getText().toString())) {
                                MyAdd.add_id.remove(holder.add_id.getText().toString());
                            }

                        }


                    }
                });*/


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        public int getItemCount() {
            return name.size();
        }

        public void setData(Context ctx, List<MyAdPojo> name) {


            this.ctx = ctx;
            this.name = name;
            chk_p = new ArrayList<Integer>();
            itemChecked = new boolean[name.size()];
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            MTextView title, date_ad, ad_rs, cost, ad_cat_sub, subcat, ad_reply_name, ad_reply_email, ad_reply_cont_no, ad_reply_msg;
            //   ImageView postimg;
            CheckBox chk;


            TextView id;
            //      TextView user_id, add_id, type, v, img_cnt, city, placeid, nearby, cash_discount, mdiscount, img_path;

            public ViewHolder(View itemView) {
                super(itemView);
                id = (TextView) itemView.findViewById(R.id.id);
                title = (MTextView) itemView.findViewById(R.id.ptitle);
                date_ad = (MTextView) itemView.findViewById(R.id.date_ad);
                ad_rs = (MTextView) itemView.findViewById(R.id.ad_rs);
                cost = (MTextView) itemView.findViewById(R.id.cost);
                ad_cat_sub = (MTextView) itemView.findViewById(R.id.ad_cat);
                subcat = (MTextView) itemView.findViewById(R.id.subcat);
                //  postimg = (ImageView) itemView.findViewById(R.id.prodimg);
                chk = (CheckBox) itemView.findViewById(R.id.check);

                ad_reply_name = (MTextView) itemView.findViewById(R.id.ad_reply_name);
                ad_reply_email = (MTextView) itemView.findViewById(R.id.ad_reply_email);
                ad_reply_cont_no = (MTextView) itemView.findViewById(R.id.ad_reply_cont_no);
                ad_reply_msg = (MTextView) itemView.findViewById(R.id.ad_reply_msg);

            }
        }

        public List<MyAdPojo> getStudentist() {
            return name;
        }

    }

}
