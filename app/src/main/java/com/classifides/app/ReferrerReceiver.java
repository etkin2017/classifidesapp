package com.classifides.app;

/**
 * Created by Ashvini on 02-08-2016.
 */

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

/**
 * To test a referrer broadcast from ADB:
 *
 * adb shell
 *
 * am broadcast -a com.android.vending.INSTALL_REFERRER -n com.example.android.custom.referrer.receiver/.ReferrerReceiver --es "referrer" "utm_source=YourAppName&utm_medium=YourMedium&utm_campaign=YourCampaign&utm_content=YourSampleContent"
 */
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * To test a referrer broadcast from ADB:
 * <p/>
 * adb shell
 * <p/>
 * am broadcast -a com.android.vending.INSTALL_REFERRER -n com.example.android.custom.referrer.receiver/.ReferrerReceiver --es "referrer" "utm_source=YourAppName&utm_medium=YourMedium&utm_campaign=YourCampaign&utm_content=YourSampleContent"
 */
public class ReferrerReceiver extends BroadcastReceiver {

    String unique_id="";
    Context ctx;

    @Override
    public void onReceive(Context context, Intent intent) {
        ctx = context;
        final String action = intent.getAction();
        if (action != null && TextUtils.equals(action, "com.android.vending.INSTALL_REFERRER")) {
            try {
                final String referrer = intent.getStringExtra("referrer");
// Parse parameters
                String[] params = referrer.split("&");
                for (String p : params) {
                    if (p.startsWith("utm_content=")) {
                        final String content = p.substring("utm_content=".length());


                        unique_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

                        //Log.e("UNIQUE ID", unique_id);

/**
 * USE HERE YOUR CONTENT (i.e. configure the app based on the link the user clicked)
 */
                        //Log.i("Receiver_utm_content", content);

                        addReferral(content,unique_id);


                    }

                    if (p.startsWith("utm_campaign=")) {
                        final String content = p.substring("utm_campaign=".length());
/**
 * USE HERE YOUR CONTENT (i.e. configure the app based on the link the user clicked)
 */
                        //Log.i("Receiver_utm_campaign", content);

                    }

                    if (p.startsWith("utm_source=")) {
                        final String content = p.substring("utm_source=".length());
/**
 * USE HERE YOUR CONTENT (i.e. configure the app based on the link the user clicked)
 */
                        //Log.i("Receiver_utm_source", content);

                    }


                    if (p.startsWith("utm_medium=")) {
                        final String content = p.substring("utm_medium=".length());
/**
 * USE HERE YOUR CONTENT (i.e. configure the app based on the link the user clicked)
 */
                        //Log.i("Receiver_utm_medium", content);

                    }
                    if (p.startsWith("utm_term=")) {
                        final String content = p.substring("utm_term=".length());
/**
 * USE HERE YOUR CONTENT (i.e. configure the app based on the link the user clicked)
 */
                        //Log.i("Receiver_utm_term", content);

                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
/**
 * OPTIONAL: Forward the intent to Google Analytics V2 receiver
 */
// new com.google.analytics.tracking.android.AnalyticsReceiver().onReceive(context, intent);
        }
    }

    public void addReferral(final String user_id,final  String deviceId) {
        new AsyncTask() {
            ProgressDialog p;
            JSONObject res;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
              /*  p = new ProgressDialog(MyAdd.this);
                pd.setMessage("We are updating Ad detail");
                pd.setTitle("Please wait....");
                pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                pd.setIndeterminate(true);
                pd.setCanceledOnTouchOutside(false);
                pd.show();*/
            }

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    Map<String, String> d = new HashMap<String, String>();
                  //  Log.e("user_id",user_id);
                    d.put("user_id", user_id);
                    d.put("device_id", deviceId);



                    res = DAO.getJsonFromUrl(Config.referlink, d);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
              /*  pd.dismiss();*/
                try {
                    if (res.getInt("success") > 0) {

                        //Toast.makeText(ctx, res.getString("message"), Toast.LENGTH_LONG).show();
                        //Log.e("ReferralAdd", "success");

                    } else {
                       // Toast.makeText(ctx, res.getString("message"), Toast.LENGTH_LONG).show();
                        //Log.e("ReferralError", "Failed");
                        //     setValues();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ctx, "error in updation", Toast.LENGTH_LONG).show();
                }
            }
        }.execute(null, null, null);
    }

}