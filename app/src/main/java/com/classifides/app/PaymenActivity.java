package com.classifides.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;

import java.io.ByteArrayOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;


import org.apache.http.util.EncodingUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PaymenActivity extends AppCompatActivity {
    private ArrayList<ByteArrayOutputStream> streams;
    //  String merchant_key = "4Ia9GJ";
    String merchant_key = "u0aVAPPu";

    // String salt = "ZxJl0K9v";

    String salt = "46LvbHVXQX";


    String action1 = "";
    String base_url = "https://secure.payu.in";
    int error = 0;
    String hashString = "";
    String txnid = "";
    String udf2 = "";
    String txn = "abcd";
    String hash = "";
    String hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
    String amount = "";
    String name = "";
    String email = "";
    String phone = "";
    String productinfo = "";
    String successurl = "";
    String failurl = "";
    String serviceprovider = "payu_paisa";
    WebView web;
    public static Context context;
    //  BuyPack buyPack;
    public ProgressDialog pd;
    String postData = "";
    // PaymentData data;
    String pack;

    String amount_two;
    Map<String, Object> mh;
    String user_id;
    String which;
    int e1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        web = (WebView) findViewById(R.id.payWeb);

        String intent_data = getIntent().getStringExtra("submit_ad");
        e1 = Integer.parseInt(getIntent().getStringExtra("which"));

        try {
            JSONObject array = new JSONObject(intent_data);
            mh = jsonToMap(array);
            //  amount_two = (String) mh.get("amt");

            amount_two = (String) mh.get("amount"); //"10";

        } catch (JSONException e) {
            e.printStackTrace();
        }

        WebSettings settings = web.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setLoadWithOverviewMode(true);
        settings.setDomStorageEnabled(true);
        web.clearHistory();
        web.clearCache(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        init(amount_two);
        web.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(PaymenActivity.this);
                builder.setMessage("SSL certificate is invalid");
                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.show();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {

                if (!pd.isShowing())
                    ShowPD();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                DismissPD();

                Log.e("url", url);
                if (url.contains(successurl)) {
                    try {

                        if (e1 == 1) {
                            successurl = Config.insertRedeemCode;
                            Map<String, String> newMap = new HashMap<String, String>();
                            for (Map.Entry<String, Object> entry : mh.entrySet()) {
                                if (entry.getValue() instanceof String) {
                                    newMap.put(entry.getKey(), (String) entry.getValue());
                                }
                            }

                     /*   Map<String, String> map = new HashMap<String, String>();
                        map.put("amount", total_amt.getText().toString());
                        map.put("r_date", cdate + " " + currentDateTimeString);
                        map.put("user_id", id + "");
                        map.put("cash_code", code_s);*/
                            String formdata = "amount=" + newMap.get("amount") + "&r_date=" + newMap.get("r_date") + "&user_id=" + user_id + "" + "&cash_code=" + newMap.get("code_s") + "&payment_status=1&txtid=" + txnid;
                            view.postUrl(successurl, EncodingUtils.getBytes(formdata, "BASE64"));
                            view.setWebViewClient(new WebViewClient() {
                                @Override
                                public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                                    super.onReceivedSslError(view, handler, error);
                                    handler.proceed();
                                }

                                @Override
                                public void onPageFinished(WebView view, String url) {
                                    super.onPageFinished(view, url);
                                    finish();

                                }
                            });

                        } else if (e1 == 2) {

                            successurl = Config.addCashPlan;
                            Map<String, String> newMap = new HashMap<String, String>();
                            for (Map.Entry<String, Object> entry : mh.entrySet()) {
                                if (entry.getValue() instanceof String) {
                                    newMap.put(entry.getKey(), (String) entry.getValue());
                                }
                            }

                     /*      //   Log.e("uID", uid);
        data.put("add_id", a_id.trim());
        data.put("user_id", uid);
        data.put("plan", finalPlanlist);
        data.put("views", "");
        data.put("view_date", cdate);
        data.put("cash_code", id + "");*/
                            String formdata = "add_id=" + newMap.get("add_id") + "&plan=" + newMap.get("plan") + "&user_id=" + user_id + "" + "&cash_code=" + newMap.get("cash_code") + "&payment_status=1&txtid=" + txnid + "&views=" + "" + "&view_date=" + newMap.get("view_date");
                            view.postUrl(successurl, EncodingUtils.getBytes(formdata, "BASE64"));
                            view.setWebViewClient(new WebViewClient() {
                                @Override
                                public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                                    super.onReceivedSslError(view, handler, error);
                                    handler.proceed();
                                }

                                @Override
                                public void onPageFinished(WebView view, String url) {
                                    super.onPageFinished(view, url);
                                    finish();

                                }
                            });


                        } else if (e1 == 3) {


                            successurl = Config.addPlanNew;
                            Map<String, String> newMap = new HashMap<String, String>();
                            for (Map.Entry<String, Object> entry : mh.entrySet()) {
                                if (entry.getValue() instanceof String) {
                                    newMap.put(entry.getKey(), (String) entry.getValue());
                                }
                            }

                     /*      //   Log.e("uID", uid);
         data.put("add_id", a_id.trim());
                data.put("user_id", u_id.trim());
                data.put("plan", finalPlanlist);
                data.put("views", "");
                data.put("view_date", cdate);*/
                            String formdata = "add_id=" + newMap.get("add_id") + "&plan=" + newMap.get("plan") + "&user_id=" + user_id + "" + "&cash_code=" + newMap.get("cash_code") + "&payment_status=1&txtid=" + txnid + "&views=" + "" + "&view_date=" + newMap.get("view_date");
                            view.postUrl(successurl, EncodingUtils.getBytes(formdata, "BASE64"));
                            view.setWebViewClient(new WebViewClient() {
                                @Override
                                public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                                    super.onReceivedSslError(view, handler, error);
                                    handler.proceed();
                                }

                                @Override
                                public void onPageFinished(WebView view, String url) {
                                    super.onPageFinished(view, url);
                                    finish();

                                }
                            });


                        }
                        //      submiPost(newMap);
                        //redeemCashCode(newMap);
                        Toast.makeText(PaymenActivity.this, "Payment Successfull... Your account will be activated Very soon..", Toast.LENGTH_SHORT).show();
                        //   getActivity().getSupportFragmentManager().popBackStack();
                        finish();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (url.contains(failurl)) {
                    try {
                      /*  String formdata = "pack=failed&Pd_Id=" + data.getProductId() + "&Uid=" + data.getUid() + "&email=" + data.getEmail() + "&Activate=deactivate&transid=" + txnid + "&pdur=null";
                        view.postUrl(failurl, EncodingUtils.getBytes(formdata, "BASE64"));*/
                        Toast.makeText(PaymenActivity.this, "Payment Not Successfull", Toast.LENGTH_SHORT).show();
                        //  getActivity().getSupportFragmentManager().popBackStack();

                        finish();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        web.postUrl(base_url.concat("/_payment"), EncodingUtils.getBytes(postData, "BASE64"));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void redeemCashCode(final Map<String, String> map) {

        Date d = new Date();
        //  SimpleDateFormat sdf=new SimpleDateFormat("hh:mm a");
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
        final String currentDateTimeString = sdf.format(d);

        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                /*pd.setCancelable(false);
                pd.setMessage("Please Wait");
                pd.show();*/

                ShowPD();

            }

            @Override
            protected Object doInBackground(Object[] params) {
                int id = 0;
                try {
                    JSONObject user = DAO.getUser();

                    id = user.getInt("id");
                    //Log.e("user_id", id + "");

                } catch (Exception e) {
                    e.printStackTrace();
                }

             /*   Map<String, String> map = new HashMap<String, String>();
                map.put("amount", amount);
                map.put("r_date", cdate + " " + currentDateTimeString);
                map.put("user_id", id + "");
                map.put("cash_code", c_code);*/
                rest = DAO.getJsonFromUrl(Config.insertRedeemCode, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                DismissPD();
                try {


                    if (rest.getInt("success") > 0) {

              /*          ed = shp.edit();
                        ed.putString("total_amt", 0 + "");
                        ed.commit();
                        total_amt.setText("0");
                        Toast.makeText(RedeemCashbackCode.this, "added", Toast.LENGTH_SHORT).show();
                        getRedeemRequest();
                        //Log.e("main_code_s", code_s);
                       *//* String[] splited = code_s.split("\\s+");

                        //Log.e("spilted", splited.toString());
                        for (int i = 0; i < splited.length; i++) {
                            updateCashcode(splited[i], total_amt.getText().toString());
                        }
*//*


                        pp = 0;*/

                        Toast.makeText(PaymenActivity.this, "added", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(PaymenActivity.this, "not added", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);


    }


    public void init(String amount1) {
        amount = amount1;
        try {
            JSONObject user = DAO.getUser();
            user_id = user.getString("id");
            name = user.getString("name");
            email = user.getString("email");
            phone = user.getString("mobile");
            productinfo = "Post Free Ad";
        } catch (JSONException e) {
            e.printStackTrace();
        }


        setTransId();
      /*  successurl = context.getString(R.string.ip) + "/payment";
        failurl = context.getString(R.string.ip) + "/fail";*/

        successurl = Config.insertRedeemCode;
        failurl = "fail";
        setHash();
        pd = new ProgressDialog(PaymenActivity.this);
        postData = "key=" + merchant_key + "&" + "hash=" + hash + "&" + "txnid=" + txnid + "&" + "udf2=" + txnid + "&" + "service_provider=" + serviceprovider + "&" + "amount=" + amount + "&" + "firstname=" + name + "&" + "email=" + email + "&" + "phone=" + phone + "&" + "productinfo=" + productinfo + "&" + "surl=" + successurl + "&" + "furl=" + failurl;


    }

    public void ShowPD() {
        pd.setMessage("Processing...");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setIndeterminate(true);
        pd.setCanceledOnTouchOutside(false);
        pd.setCancelable(false);
        pd.show();
    }

    public void DismissPD() {
        pd.dismiss();
    }

    public boolean empty(String s) {
        if (s == null || s.trim().equals(""))
            return true;
        else
            return false;
    }


    public String hashCal(String type, String str) {
        byte[] hashseq = str.getBytes();
        StringBuffer hexString = new StringBuffer();
        try {
            MessageDigest algorithm = MessageDigest.getInstance(type);
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();


            for (int i = 0; i < messageDigest.length; i++) {
                String hex = Integer.toHexString(0xFF & messageDigest[i]);
                if (hex.length() == 1) hexString.append("0");
                hexString.append(hex);
            }

        } catch (NoSuchAlgorithmException nsae) {
        }

        return hexString.toString();


    }


    public void setTransId() {
        if (empty(txnid)) {
            Random rand = new Random();
            String rndm = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);
            txnid = hashCal("SHA-256", rndm).substring(0, 20);
        }
        udf2 = txnid;
    }

    public void setHash() {
        if (empty(hash)) {
            if (empty(merchant_key)
                    || empty(txnid)
                    || empty(amount)
                    || empty(name)
                    || empty(email)
                    || empty(phone)
                    || empty(productinfo)
                    || empty(successurl)
                    || empty(failurl)
                    || empty(serviceprovider))
                error = 1;
            else {
                String[] hashVarSeq1 = new String[]{merchant_key, txnid, amount, productinfo, name, email, "", udf2, "", "", "", "", "", "", "", ""};
                for (String part : hashVarSeq1) {
                    hashString = (empty(part)) ? hashString.concat("") : hashString.concat(part);
                    hashString = hashString.concat("|");
                }
                hashString = hashString.concat(salt);


                hash = hashCal("SHA-512", hashString);
                action1 = base_url.concat("/_payment");
            }
        } else if (!empty(hash)) {
            hash = hash;
            action1 = base_url.concat("/_payment");
        }


    }


    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if (json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }
}
