package com.classifides.app;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.classifides.app.application.Classified;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.view.MTextView;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.facebook.CallbackManager;
import com.facebook.FacebookDialog;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ReferNEarnActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    private SliderLayout mDemoSlider;
    ImageView fb, gp, tw, whatsapp, mail;
    CallbackManager callbackManager;
    MTextView earned_amt;
    // private UiLife2cycleHelper uiHelper;

    LinearLayout layuot_warning, rtamt;

    ImageView imagerefernearn, classifieds_messanger;

    Calendar c = Calendar.getInstance();
    SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy");
    String cdate = df.format(c.getTime());
    ProgressDialog dialog;

    Map<String, String> data;


    ShareDialog shareDialog;

    RecyclerView redeem_cashback_code;
    MAdaptor adaptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer_nearn);


        MobileAds.initialize(Classified.getInstance().getApplicationContext(), "ca-app-pub-2758807869387921~3087322491");

        final AdView mAdView = (AdView) findViewById(R.id.adView_refer_n_earn);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);


        final Runnable adLoader = new Runnable() {
            @Override
            public void run() {
                mAdView.loadAd(new AdRequest.Builder().build());
            }
        };


        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                Log.e("onAdClosed", "onAdClosed");
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Log.e("onAdFailedToLoad", "onAdFailedToLoad");

                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        ReferNEarnActivity.this.runOnUiThread(adLoader);
                    }
                }, 3000);
            }


            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                Log.e("onAdLeftApplication", "onAdLeftApplication");
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                Log.e("onAdOpened", "onAdOpened");
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.e("onAdLoaded", "onAdLoaded");
            }
        });

        adLoader.run();


        mDemoSlider = (SliderLayout) findViewById(R.id.slider);
        FacebookSdk.sdkInitialize(getApplicationContext());
        shareDialog = new ShareDialog(this);
        callbackManager = CallbackManager.Factory.create();

        data = new HashMap<String, String>();

        imagerefernearn = (ImageView) findViewById(R.id.imagerefernearn);


        redeem_cashback_code = (RecyclerView) findViewById(R.id.redeem_cashback_code);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        redeem_cashback_code.setLayoutManager(llm);
        dialog = new ProgressDialog(ReferNEarnActivity.this);

        layuot_warning = (LinearLayout) findViewById(R.id.layuot_warning);
        rtamt = (LinearLayout) findViewById(R.id.rtamt);


        earned_amt = (MTextView) findViewById(R.id.earned_amt);
        fb = (ImageView) findViewById(R.id.fb);
        gp = (ImageView) findViewById(R.id.gp);
        tw = (ImageView) findViewById(R.id.tw);
        whatsapp = (ImageView) findViewById(R.id.whatsapp);
        mail = (ImageView) findViewById(R.id.gmail);

        classifieds_messanger = (ImageView) findViewById(R.id.classifieds_messanger);

        adaptor = new MAdaptor();

        rtamt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int tt = Integer.parseInt(earned_amt.getText().toString());
                if (tt < 200) {
                    layuot_warning.setVisibility(View.VISIBLE);
                } else {
                    addUserReferralredeem(tt + "", cdate);
                }
            }
        });


        // getHistory();
      /*
       */

        getEarnedAmt();

        int id = 0;
        try {
            JSONObject user = DAO.getUser();

            id = user.getInt("id");
            //Log.e("user_id", id + "");

        } catch (Exception e) {
            e.printStackTrace();
        }


        final String ap_link = "Hey, Check out Catch Me On App" + "\n" + "Refer and Earn Catch Me On App " + "\n" + "https://play.google.com/store/apps/details?id=com.classifides.app&referrer=utm_source%3Dgoogle%26utm_medium%3Demail%26utm_term%3Dclassifieds%26utm_content%3D" + id + "" + "%26utm_campaign%3Dcatchmeon";


      /*  ShareLinkContent content = new ShareLinkContent.Builder()
              *//*  .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.classifides.app"))*//*
                .setContentTitle("Hey, applink ")
                .setImageUrl(Uri.parse("android.resource://com.classifides.app/" + R.drawable.fb_o))
                .setContentDescription(ap_link)
                .build();

        ShareButton shareButton = (ShareButton) findViewById(R.id.fb_one);
        shareButton.setShareContent(content);*/


        classifieds_messanger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, ap_link);
                intent.setType("text/plain");
                intent.setPackage("com.classifides.app");


                try {
                    startActivity(intent);
                } catch (android.content.ActivityNotFoundException ex) {
                    ToastHelper.showToastLong(getApplicationContext(), "Catch Me On messenger have not been installed.");
                }

            }
        });


        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

             /*   if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                    if (ShareDialog.canShow(ShareLinkContent.class)) {
                        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                .setContentTitle("Refer and Earn")
                                .setContentDescription(ap_link)
                                .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.classifides.app"))
                                //  .setImageUrl(Uri.parse("http://bagpiper-andy.de/bilder/dudelsack%20app.png"))
                                .build();

                        shareDialog.show(linkContent);
                    }

                } else*/ //{
                Intent ii = new Intent(ReferNEarnActivity.this, FacebookShareActivity.class);
                ii.putExtra("apk_link", ap_link);
                startActivity(ii);

                //  }


            }
        });

        gp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shareIntent = ShareCompat.IntentBuilder.from(ReferNEarnActivity.this)
                        .setType("text/plain")
                        .setText(ap_link)
                        .getIntent()
                        .setPackage("com.google.android.apps.plus");


                try {
                    startActivity(shareIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    ToastHelper.showToastLong(getApplicationContext(), "Google + have not been installed.");
                }


            }
        });

        tw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, ap_link);
                intent.setType("text/plain");
                intent.setPackage("com.twitter.android");


                try {
                    startActivity(intent);
                } catch (android.content.ActivityNotFoundException ex) {
                    ToastHelper.showToastLong(getApplicationContext(), "twitter have not been installed.");
                }

            }
        });

        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                whatsappIntent.setType("text/plain");
                whatsappIntent.setPackage("com.whatsapp");
                whatsappIntent.putExtra(Intent.EXTRA_TEXT, ap_link);
                try {
                    startActivity(whatsappIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    ToastHelper.showToastLong(getApplicationContext(), "Whatsapp have not been installed.");
                }

            }
        });

        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Log.i("Send email", "");
                String[] TO = {""};
                String[] CC = {""};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);

                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                emailIntent.putExtra(Intent.EXTRA_CC, CC);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    finish();
                    Log.i("Finished sent email...", "");
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(ReferNEarnActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }*/


                final Dialog d = new Dialog(ReferNEarnActivity.this);

                d.setContentView(R.layout.send_mail);

                final EditText editTextEmail = (EditText) d.findViewById(R.id.editTextEmail);

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = d.getWindow();
                lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                window.setAttributes(lp);
                MTextView buttonSend = (MTextView) d.findViewById(R.id.buttonSend);

                MTextView cancelSend = (MTextView) d.findViewById(R.id.cancelSend);

                buttonSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (TextUtils.isEmpty(editTextEmail.getText().toString())) {
                            editTextEmail.setError("plz enter email id");
                        } else {
                            SendMail sm = new SendMail(ReferNEarnActivity.this, editTextEmail.getText().toString(), "Referral link", ap_link);

                            //Executing sendmail to send email
                            sm.execute();

                            d.dismiss();
                        }
                    }
                });

                cancelSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        d.dismiss();
                    }
                });
                d.show();

            }
        });

        setImageSlider();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void setImageSlider() {


        HashMap<String, String> url_maps = new HashMap<String, String>();

        url_maps.put(1 + "", "http://2.bp.blogspot.com/-pI8uxuf2zKs/VkyWWqV66-I/AAAAAAAAZM4/pNci7A2FEYU/s1600/Refer%2BTo%2BEarn%2BWorth%2BRs%2B1000%2BFreeCharge%2BCredit%2BFree%2BMaalFreeKaa.png");
        url_maps.put(2 + "", "http://3.bp.blogspot.com/-hcNsaQ5JS18/VQMOAazdKeI/AAAAAAAAHS8/yGD_uDM8V1M/s1600/snapdeal.PNG");
        url_maps.put(3 + "", "http://4.bp.blogspot.com/-0q9UYibMI8M/Vlh0T9ZhJXI/AAAAAAAABTk/jpXwjR1dMD4/s1600/refer-and-earn.png");
        url_maps.put(4 + "", "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRQ_BJynbRlBLFlx2TjM6P4dTm1xI-J6BmkJYccHK77kBFwrVbh");
        //   }

        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }

        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(5000);
        mDemoSlider.addOnPageChangeListener(this);
    }


    private void addUserReferralredeem(final String p, final String v) {
        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ProgressDialog();


                int id = 0;
                try {
                    JSONObject user = DAO.getUser();

                    id = user.getInt("id");
                    //Log.e("user_id", id + "");

                } catch (Exception e) {
                    e.printStackTrace();
                }


                data.put("u_id", id + "");
                data.put("amt", p);
                data.put("r_date", v);
                data.put("status", "1");

             /*   try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
            }

            @Override
            protected Object doInBackground(Object[] params) {

                res = DAO.getJsonFromUrl(Config.addHistory, data);

                  /*  if (res.getInt("success") > 0) {
                        Toast.makeText(UpdateAsValidity.this, "Plan Add Suceessfully", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(UpdateAsValidity.this, "Error ", Toast.LENGTH_LONG).show();

                    }
*/

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                DismissProgress();
                try {
                    if (res.getInt("success") > 0) {
                        Toast.makeText(getApplicationContext(), "Redeem Amount", Toast.LENGTH_SHORT).show();
                        getEarnedAmt();
                    } else {
                        //   Toast.makeText(getApplicationContext(), "not done", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

    public void getHistory() {

        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Object doInBackground(Object[] params) {
                int id = 0;
                try {
                    JSONObject user = DAO.getUser();

                    id = user.getInt("id");
                    //Log.e("user_id", id + "");

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Map<String, String> map = new HashMap<String, String>();
                map.put("id", id + "");
                rest = DAO.getJsonFromUrl(Config.rHistory, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {


                    if (rest.getInt("success") > 0) {
                        adaptor.setData(rest.getJSONArray("user"));
                        redeem_cashback_code.setAdapter(adaptor);
                    } else {


                        adaptor.setData(new JSONArray());
                        redeem_cashback_code.setAdapter(adaptor);
                        //      Toast.makeText(ReferNEarnActivity.this, rest.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

    public void ProgressDialog() {
        dialog.setMessage("While we posting your add.");
        dialog.setTitle("Please wait....");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void DismissProgress() {
        dialog.dismiss();
    }

    public void getEarnedAmt() {

        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Object doInBackground(Object[] params) {

                int id = 0;
                try {
                    JSONObject user = DAO.getUser();

                    id = user.getInt("id");
                    //Log.e("user_id", id + "");

                } catch (Exception e) {
                    e.printStackTrace();
                }

                Map<String, String> map = new HashMap<String, String>();
                map.put("id", id + "");
                rest = DAO.getJsonFromUrl(Config.showReferralearnedAmt, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {


                    if (rest.getInt("success") > 0) {

                        String tamt = rest.getJSONArray("user").getJSONObject(0).getString("referred_cnt");
                        // int t = Integer.parseInt(tamt) * 5;

                        int t = Integer.parseInt(tamt) * 20;

                        earned_amt.setText(t + "");

                    } else {
                        earned_amt.setText("0");
                    }


                    getHistory();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);


    }


    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private class MAdaptor extends RecyclerView.Adapter<MAdaptor.ViewHolder> {

        private JSONArray data;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.redeem_history, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            try {

                String d = data.getJSONObject(position).getString("r_status");
                if (d.equals("1")) {
                    holder.status_r.setText("Request sent");
                } else {
                    holder.status_r.setText("No");
                }

                holder.amt.setText("Rs " + data.getJSONObject(position).getString("amount"));
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");


                holder.r_date.setText(data.getJSONObject(position).getString("r_date"));
                // holder.viewers.setText(data.getJSONObject(position).getString("cashback_code"));

                //holder.viewers.setVisibility(View.GONE);


                holder.itemView.setTag(data.getJSONObject(position));


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        public int getItemCount() {
            return data.length();
        }

        public void setData(JSONArray data) {
            this.data = data;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView status_r, amt, r_date, viewers;


            TextView user_id, add_id;

            public ViewHolder(View itemView) {
                super(itemView);
                status_r = (TextView) itemView.findViewById(R.id.status_r);
                amt = (TextView) itemView.findViewById(R.id.amt);
                r_date = (TextView) itemView.findViewById(R.id.r_date);


            }
        }
    }
}
