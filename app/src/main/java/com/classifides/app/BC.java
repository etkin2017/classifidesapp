package com.classifides.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.classifides.app.Adaptor.CategoryAdaptor;
import com.classifides.app.application.Classified;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.fragment.Ads_Notification;
import com.classifides.app.fragment.BrowseCategory;
import com.classifides.app.fragment.Messanger;
import com.classifides.app.gcm.activity.HomeChatActivity;
import com.classifides.app.gcm.fragment.ChatFragment;
import com.classifides.app.gcm.fragment.ContactsFragment;
import com.classifides.app.gcm.gcm.GcmIntentService;
import com.classifides.app.model.BadgeView;
import com.classifides.app.services.LocationTressService;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class BC extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    public static boolean activate = true;
    CategoryAdaptor adaptor;
    GridView gridview;

    CircleImageView img;
    String img_nm;

    CircleImageView noti, home;
    TextView logout;
    public static int id;
    TextView icon_test;
    View target;
    BadgeView badge;
    SharedPreferences sh;
    public static int badge_data = 0;

    MyBroadcast broadcast;

    public static DrawerLayout drawer2;

    public static String eID, Name;
    CoordinatorLayout co_ordinator1;

    TextView messenger;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bc);
        //    co_ordinator1 = (CoordinatorLayout) findViewById(R.id.co_ordinator1);

      /*  co_ordinator1.setOnTouchListener(new BrowseByCategory() {
            public void onSwipeTop() {
                Toast.makeText(BC.this, "top", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeRight() {
                startActivity(new Intent(BC.this, HomeChatActivity.class));
            }

            public void onSwipeLeft() {
                Toast.makeText(BC.this, "left", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeBottom() {
                Toast.makeText(BC.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });
*/
        messenger = (TextView) findViewById(R.id.messenger);
        messenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //  startActivity(new Intent(BrowseByCategory.this, com.classifides.app.gcm.activity.MainActivity.class));
                startActivity(new Intent(BC.this, HomeChatActivity.class));
            }
        });


        Intent service = new Intent(this, GcmIntentService.class);
        startService(service);

        icon_test = (TextView) findViewById(R.id.icon_test);
        //   adaptor = new CategoryAdaptor(getApplicationContext());
        home = (CircleImageView) findViewById(R.id.home);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Fragment> fragments = getSupportFragmentManager().getFragments();
                if (fragments != null) {
                    for (Fragment fragment : fragments) {
                        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                        //   Log.e("fragmentmain1"+fragment.toString(),fragment.toString());
                    }
                }
                //    startActivity(new Intent(BrowseByCategory.this, BrowseByCategory.class));

                Intent i3 = new Intent(BC.this, BC.class);
                i3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i3);

            }
        });

        // gridview = (GridView) findViewById(R.id.gridview);
        // gridview.setAdapter(new CategoryAdaptor(this));

        //getCategory();


        logout = (TextView) findViewById(R.id.logout);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<Fragment> fragments = getSupportFragmentManager().getFragments();
                if (fragments != null) {
                    for (Fragment fragment : fragments) {
                        if (fragment.equals("browser")) {

                        } else {
                            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                        }
                        //  Log.e("fragmentmain2"+fragment.toString(),fragment.toString());
                    }
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(BC.this);
                builder.setTitle("Alert");
                builder.setMessage("Are you sure you want to logout?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        SharedPreferences sh = getSharedPreferences("log", MODE_PRIVATE);
                        sh.edit().putBoolean("login", false).commit();

                        List<Fragment> fragments = getSupportFragmentManager().getFragments();
                        if (fragments != null) {
                            for (Fragment fragment : fragments) {
                                if (fragment.equals("browser")) {

                                } else {
                                    getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                                }
                            }
                        }
                        Intent ii = new Intent(BC.this, LoginActivity.class);
                        ii.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(ii);
                        //   getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.container)).commit();
                        //    clearSharedData();
                        finish();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.setCancelable(false);
                builder.show();


                //    finish();
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            // only for gingerbread and newer versions
        } else {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Home");
        }
       // setSupportActionBar(toolbar);

        img = (CircleImageView) findViewById(R.id.uimg);
        //  getProducts();
        noti = (CircleImageView) findViewById(R.id.noti);
        try {
            JSONObject user = DAO.getUser();
            img_nm = user.getString("image");
            eID = user.getString("email");
            Name = user.getString("name");
            Picasso.with(this).load(img_nm).placeholder(R.drawable.user).into(img);


            id = user.getInt("id");

        } catch (Exception e) {
            e.printStackTrace();
        }
        broadcast = new MyBroadcast();
        IntentFilter filter = new IntentFilter(LocationTressService.name);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(broadcast, filter);


        target = findViewById(R.id.noti);
        badge = new BadgeView(getApplicationContext(), target);
        badge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Config.noti_click = 1;

                badge.hide();

                //  startActivity(new Intent(BrowseByCategory.this, HomeActivity.class));

                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, new Ads_Notification(), "not").commit();

                //trigger notification service
                Intent in = new Intent(BC.this, LocationTressService.class);
                startService(in);

            }
        });

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(BC.this, UserAccount.class));
            }
        });
/*

        Typeface fontAwesomeFont = Typeface.createFromAsset(getAssets(), "fonts/fontawesome-webfont.ttf");
        icon_test.setTypeface(fontAwesomeFont);
*/


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(BC.this);
        navigationView.setItemIconTintList(null);


        View v = navigationView.getHeaderView(0);//(R.layout.nav_header_home);
        ImageView img1 = (ImageView) v.findViewById(R.id.imageView);
        TextView name_textView = (TextView) v.findViewById(R.id.name_textView);
        TextView email_textView = (TextView) v.findViewById(R.id.textView);
        name_textView.setText(Name);
        email_textView.setText(eID);
        Picasso.with(Classified.getInstance()).load(img_nm).placeholder(R.drawable.user).into(img1);


      /*  DrawerLayout drawer3 = (DrawerLayout) findViewById(R.id.drawer_layout_two);
        ActionBarDrawerToggle toggle1 = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView1 = (NavigationView) findViewById(R.id.nav_view_two);
        navigationView1.setNavigationItemSelectedListener(BrowseByCategory.this);
        navigationView1.setItemIconTintList(null);

        navigationView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(BrowseByCategory.this, com.classifides.app.gcm.activity.LoginActivity.class);
                startActivity(i);
            }
        });
*/


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        mViewPager.setCurrentItem(0);


        mSectionsPagerAdapter.getItem(0);

        mViewPager.setOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        // When swiping between pages, select the
                        // corresponding tab.
                        if (position == 1) {
                            startActivity(new Intent(BC.this, HomeChatActivity.class));
                        }else{

                        }

//                        getActionBar().setSelectedNavigationItem(position);
                    }
                });


      /*  FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
*/
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.one) {
            startActivity(new Intent(BC.this, PostFreeAdd.class));
        } else if (id == R.id.two) {
            startActivity(new Intent(BC.this, PostVideoAdd.class));
        } else if (id == R.id.three) {
            startActivity(new Intent(BC.this, ManageNotification.class));
        } else if (id == R.id.fore) {
            startActivity(new Intent(BC.this, MyAdd.class));
        } else if (id == R.id.banner) {
            startActivity(new Intent(BC.this, PostBannerAdd.class));
        } else if (id == R.id.twelve) {

            startActivity(new Intent(BC.this, ReplyToMyAdd.class));
        } else if (id == R.id.five) {
            // MyReplyToAds.getProducts();
            startActivity(new Intent(BC.this, MyReplyToAds.class));

        } else if (id == R.id.six) {
            startActivity(new Intent(BC.this, RedeemCashbackCode.class));
        } else if (id == R.id.seven) {
            startActivity(new Intent(BC.this, ReferNEarnActivity.class));

        } else if (id == R.id.eight) {
            startActivity(new Intent(BC.this, ContactUs.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onDestroy() {
        try {
            if (broadcast != null)
                unregisterReceiver(broadcast);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }

        DrawerLayout drawer2 = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer2.isDrawerOpen(GravityCompat.END)) {
            drawer2.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
       /* if (Config.noti_click == 1) {

            badge.hide();
        }*/
    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }


    @Override
    protected void onPause() {
        super.onPause();
        activate = false;

    }

    public void clearSharedData() {
        SharedPreferences ppff = PreferenceManager.getDefaultSharedPreferences(BC.this);
        SharedPreferences.Editor editor;
        //Log.e("shredPref", ppff.getAll().toString());
        editor = ppff.edit();
        editor.clear().commit();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // unregisterReceiver(broadcast);
    }


    @Override
    protected void onResume() {
        super.onResume();

        activate = false;


    }


    /**
     * A placeholder fragment containing a simple view.
     */
  /*  public static class PlaceholderFragment extends Fragment {
        *//**
     * The fragment argument representing the section number for this
     * fragment.
     *//*
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        *//**
     * Returns a new instance of this fragment for the given section
     * number.
     *//*
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_bc, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }*/

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     *
     *
     *
     *
     */





    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return new BrowseCategory();
                case 1:
                    return new Messanger();

            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Home";
                case 1:
                    return "Messanger";

            }
            return null;
        }
    }
  /*  public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return new BrowseCategory();

                case 1:
                    return new Messanger();

            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Home";
                case 1:
                    return "Messanger";

            }
            return null;
        }
    }
*/
    public class MyBroadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String bd = intent.getExtras().getString("data");
            badge_data = Integer.parseInt(bd);
            //     Toast.makeText(context, bd, Toast.LENGTH_LONG).show();
            //Log.e("badge_data", bd);


            SharedPreferences sh = getSharedPreferences("count", MODE_PRIVATE);
            SharedPreferences.Editor editor = sh.edit();
            editor.putInt("cnt", Integer.parseInt(bd));
            editor.commit();


            try {
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds

                v.vibrate(1000);
                r.play();
/*
                JSONObject j = new JSONObject(DAO.getNotification());
                JSONArray ar = new JSONArray();
                ar.put(bd.getString("msg"));
                ar.put(bd.getString("title"));
                ar.put(bd.getString("link"));
                ar.put(true);
                j.put(bd.getString("key"), ar);
                DAO.storeNotification(j.toString());*/


                //Log.e("badge_data1", badge_data + "");

                //  if (Config.noti_click == 0) {

                if ((badge_data + "").equals("") && (badge_data + "").equals("null") && badge_data == 0) {
                    badge.hide();
                } else {
                    badge.setText(badge_data + "");
                    badge.show();
                }


                // }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

}
