package com.classifides.app;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.classifides.app.application.Classified;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;

import org.apache.http.util.EncodingUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentFragment extends Fragment {
  /*  String merchant_key = "4Ia9GJ";
    String salt = "ZxJl0K9v";
    String action1 = "";
    String base_url = "https://test.payu.in";*/

    String merchant_key = "u0aVAPPu";
    String salt = "46LvbHVXQX";
    String action1 = "";
    String base_url = "https://secure.payu.in";


    int error = 0;
    String hashString = "";
    String txnid = "";
    String udf2 = "";
    String txn = "abcd";
    private String videoPath;
    String hash = "";
    String hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
    String amount = "";
    String name = "";
    String email = "";
    String phone = "";
    String productinfo = "";
    String successurl = "";
    String failurl = "";
    String serviceprovider = "payu_paisa";
    WebView web;
    public static Context context;
    //  BuyPack buyPack;
    public ProgressDialog pd;
    String postData = "";
    // PaymentData data;
    String pack;
    private ProgressDialog dialog;
    String amount_two;
    Map<String, Object> mh;

    ArrayList<ByteArrayOutputStream> streams;
    String user_id;

    String fileName;

    ByteArrayOutputStream streams1;

    String which_ad;
    int e;

    public PaymentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_payment, container, false);


        web = (WebView) v.findViewById(R.id.payWeb);

        //   String intent_data = getActivity().getIntent().getStringExtra("submit_ad");

        String intent_data = getArguments().getString("submit_ad");
        which_ad = getArguments().getString("p");
        e = Integer.parseInt(getArguments().getString("e"));

        try {
            JSONObject array = new JSONObject(intent_data);
            mh = jsonToMap(array);
            amount_two = (String) mh.get("amt"); //"10";
            fileName = (String) mh.get("imagename");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        WebSettings settings = web.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setLoadWithOverviewMode(true);
        settings.setDomStorageEnabled(true);
        web.clearHistory();
        web.clearCache(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        init(amount_two);
        web.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {

                //handler.proceed();
                final AlertDialog.Builder builder = new AlertDialog.Builder(Classified.getInstance().getApplicationContext());
                builder.setMessage("SSL certificate is invalid");
                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.show();


            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {

                if (!pd.isShowing())
                    ShowPD();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                DismissPD();
                if (url.contains(successurl)) {
                    try {
                        Map<String, String> newMap = new HashMap<String, String>();
                        for (Map.Entry<String, Object> entry : mh.entrySet()) {
                            if (entry.getValue() instanceof String) {
                                newMap.put(entry.getKey(), (String) entry.getValue());
                            }
                        }

                        String formdata = "title=" + newMap.get("title") + "&add_id=" + newMap.get("add_id") + "&description=" + newMap.get("description") + "&category=" + newMap.get("category") + "&discount=" + newMap.get("discount") + "&cashdiscount=" + newMap.get("cashdiscount") + "&txtid=" + txnid + "&price=" + newMap.get("price") + "&payment_status=" + "1" + "&imagename=" + newMap.get("imagename")
                                + "&imgcount=" + newMap.get("imgcount") + "&city=" + newMap.get("city") + "&placeid=" + newMap.get("placeid") + "&nearby=" + newMap.get("nearby") + "&sex=" + newMap.get("sex") + "&type=" + newMap.get("type") + "&id=" + user_id + "&website=" + newMap.get("website") + "&default_view=" + newMap.get("default_view");
                        view.postUrl(successurl, EncodingUtils.getBytes(formdata, "BASE64"));
                        view.setWebViewClient(new WebViewClient() {
                            @Override
                            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                                super.onReceivedSslError(view, handler, error);
                                handler.proceed();
                            }


                        });


                        if (which_ad.equals("1")) {
                            uploadImage(newMap);
                        } else if (which_ad.equals("2")) {
                            uploadBannerImage(newMap);
                        } else if (which_ad.equals("3")) {
                            postNext(newMap);
                        }


                        //     redeemCashCode(newMap);
                        Toast.makeText(getActivity(), "Payment Successfull... Your account will be activated Very soon..", Toast.LENGTH_SHORT).show();
                        //  getActivity().getSupportFragmentManager().popBackStack();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (url.contains(failurl)) {
                    try {
                      /*  String formdata = "pack=failed&Pd_Id=" + data.getProductId() + "&Uid=" + data.getUid() + "&email=" + data.getEmail() + "&Activate=deactivate&transid=" + txnid + "&pdur=null";
                        view.postUrl(failurl, EncodingUtils.getBytes(formdata, "BASE64"));*/
                        Toast.makeText(getActivity(), "Payment Not Successfull", Toast.LENGTH_SHORT).show();
                        getActivity().getSupportFragmentManager().popBackStack();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        web.postUrl(base_url.concat("/_payment"), EncodingUtils.getBytes(postData, "BASE64"));

        return v;
    }

    public void setImages(ArrayList<ByteArrayOutputStream> streams) {
        this.streams = streams;
    }

    public void setBannerImages(ByteArrayOutputStream streams1) {
        this.streams1 = streams1;
    }


    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
       /* Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        String cdate = df.format(c.getTime());
        fileName = "classified" + cdate + getFileExt(new File(videoPath).getName());*/

    }

    private void postNext(final Map<String, String> d) {
        new AsyncTask<String, Integer, String>() {
            String rest = "";
            private String boundary;
            private static final String LINE_FEED = "\r\n";
            private HttpURLConnection httpConn;
            private String charset = "UTF-8";
            private OutputStream outputStream;
            private PrintWriter writer;


            @Override
            protected String doInBackground(String[] params) {

                try {
                    File uploadFile = new File(videoPath);

                    String requestURL = Config.Server + "upload.php";
                    // String requestURL = " http://192.168.1.108:8086/up/upload.php";


                    // creates a unique boundary based on time stamp
                    boundary = "===" + System.currentTimeMillis() + "===";
                    URL url = new URL(requestURL);
                    httpConn = (HttpURLConnection) url.openConnection();
                    httpConn.setRequestProperty("Connection", "keep-alive");
                    httpConn.setRequestMethod("POST");
                    httpConn.setRequestProperty("Accept-Encoding", "identity");
                    httpConn.setRequestProperty("Content-Type",
                            "multipart/form-data; boundary=" + boundary);
                    httpConn.setRequestProperty("User-Agent", "Etkin Agent");
                    httpConn.setRequestProperty("Test", "Etkin");


                    //httpConn.setUseCaches(false);
                    httpConn.setDoOutput(true);    // indicates POST method
                    //
                    httpConn.setDoInput(true);

                    String data = "--" + boundary + LINE_FEED + "Content-Disposition: form-data; name=\"" + "file" + "\"; filename=\"" + fileName + "\"" + LINE_FEED + "Content-Type: " + URLConnection.guessContentTypeFromName(fileName) + LINE_FEED + "Content-Transfer-Encoding: binary" + LINE_FEED + LINE_FEED;
                    String data2 = LINE_FEED + LINE_FEED + "--" + boundary + "--" + LINE_FEED;
                    int len = data.getBytes().length + (int) uploadFile.length() + data2.getBytes().length;

                    httpConn.setFixedLengthStreamingMode(len);
                    outputStream = httpConn.getOutputStream();
                    writer = new PrintWriter(new OutputStreamWriter(outputStream, charset), true);


                    writer.append("--" + boundary).append(LINE_FEED);
                    writer.append("Content-Disposition: form-data; name=\"" + "file" + "\"; filename=\"" + fileName + "\"")
                            .append(LINE_FEED);
                    writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(fileName))
                            .append(LINE_FEED);
                    writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
                    writer.append(LINE_FEED);
                    writer.flush();

                    publishProgress(0, (int) uploadFile.length() / 1024);
                    FileInputStream inputStream = new FileInputStream(uploadFile);
                    byte[] buffer = new byte[4096];
                    int bytesRead = -1;
                    int s = 0;
                    // BufferedWriter writer=new BufferedWriter(new PrintWriter(outputStream));
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                        s = s + (int) bytesRead / 1024;
                        publishProgress(s);
                                /*if (s > 8000)
                                    break;*/
                    }
                    outputStream.flush();
                    inputStream.close();

                    writer.append(LINE_FEED);
                    writer.flush();


                    writer.append(LINE_FEED).flush();
                    writer.append("--" + boundary + "--").append(LINE_FEED);
                    writer.close();

                    List<String> response = new ArrayList<String>();
                    // checks server's status code first
                    int status = httpConn.getResponseCode();
                    if (status == HttpURLConnection.HTTP_OK) {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(
                                httpConn.getInputStream()));
                        String line = null;
                        while ((line = reader.readLine()) != null) {
                            response.add(line);
                        }
                        reader.close();
                        httpConn.disconnect();
                    } else {
                        throw new IOException("Server returned non-OK status: " + status);
                    }





                           /* MultipartUtility mp=new MultipartUtility(requestURL,charset);
                            mp.addFilePart("file",img,this);
                            List<String> res=mp.finish();*/


                    for (String r : response) {
                        rest += "\n" + r;
                        Log.e("rest +", rest);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return rest;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                dialog.dismiss();
                getActivity().finish();
              /*  try {
                    //dialog.dismiss();

                    if (TextUtils.isEmpty(d.get("adid")))
                        submiPost(d);
                    else {

                        //Log.e("In Video Post","In Video Post update");
                        updatePost(d.get("adid"), d);
                    }
                } catch (Exception e) {

                }*/
                //   Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                if (values.length == 2) {
                    dialog = new ProgressDialog(getActivity());
                    dialog.setTitle("Uploading Video Add");
                    dialog.setCancelable(false);
                    dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    dialog.setMessage("Please wait while we uploading your add!");
                    dialog.setMax(values[1]);
                    dialog.setButton(DialogInterface.BUTTON_POSITIVE, "cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                        }
                    });
                    dialog.show();


                } else
                    dialog.setProgress(values[0]);

            }
        }.execute(null, null, null);
    }

    public void uploadImage(final Map<String, String> d) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ShowPD();
            }

            @Override
            protected Void doInBackground(Void... params) {
                int j = 0;


                for (ByteArrayOutputStream s : streams) {
                    byte[] image = s.toByteArray();

                    d.put("filename", d.get("imagename") + j + ".jpg");
                    Log.e("imagename", d.get("imagename") + j + ".jpg");
                    d.put("image", Base64.encodeToString(image, 0));
                    DAO.getJsonFromUrl(Config.Server + "uploadimage.php", d);
                    j++;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                DismissPD();
                getActivity().finish();
            }
        }.execute(null, null, null);


    }

    public void uploadBannerImage(final Map<String, String> d) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ShowPD();
            }

            @Override
            protected Void doInBackground(Void... params) {
                int j = 0;


                byte[] image = streams1.toByteArray();
                d.put("image_name", d.get("imagename") + "");
                d.put("encoded_string", Base64.encodeToString(image, 0));
                DAO.getJsonFromUrl(Config.setImage, d);
                j++;
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                DismissPD();
                getActivity().finish();
            }
        }.execute(null, null, null);


    }

    public void redeemCashCode(final Map<String, String> map) {

        Date d = new Date();
        //  SimpleDateFormat sdf=new SimpleDateFormat("hh:mm a");
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
        final String currentDateTimeString = sdf.format(d);

        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pd.setCancelable(false);
                pd.setMessage("Please Wait");
                pd.show();

            }

            @Override
            protected Object doInBackground(Object[] params) {
                int id = 0;
                try {
                    JSONObject user = DAO.getUser();

                    id = user.getInt("id");
                    //Log.e("user_id", id + "");

                } catch (Exception e) {
                    e.printStackTrace();
                }

             /*   Map<String, String> map = new HashMap<String, String>();
                map.put("amount", amount);
                map.put("r_date", cdate + " " + currentDateTimeString);
                map.put("user_id", id + "");
                map.put("cash_code", c_code);*/
                rest = DAO.getJsonFromUrl(Config.insertRedeemCode, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                pd.dismiss();
                try {


                    if (rest.getInt("success") > 0) {

              /*          ed = shp.edit();
                        ed.putString("total_amt", 0 + "");
                        ed.commit();
                        total_amt.setText("0");
                        Toast.makeText(RedeemCashbackCode.this, "added", Toast.LENGTH_SHORT).show();
                        getRedeemRequest();
                        //Log.e("main_code_s", code_s);
                       *//* String[] splited = code_s.split("\\s+");

                        //Log.e("spilted", splited.toString());
                        for (int i = 0; i < splited.length; i++) {
                            updateCashcode(splited[i], total_amt.getText().toString());
                        }
*//*


                        pp = 0;*/
                    } else {
                        Toast.makeText(getActivity(), "not added", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);


    }

    public String getFileExt(String fileName) {
        return fileName.substring(fileName.lastIndexOf("."), fileName.length());
    }


    public void init(String amount1) {
        amount = amount1;
        try {
            JSONObject user = DAO.getUser();
            user_id = user.getString("id");
            name = user.getString("name");
            email = user.getString("email");
            phone = user.getString("mobile");
            productinfo = "Post Free Ad";
        } catch (JSONException e) {
            e.printStackTrace();
        }


        setTransId();
      /*  successurl = context.getString(R.string.ip) + "/payment";
        failurl = context.getString(R.string.ip) + "/fail";*/

        //  successurl = Config.Server + "addadvertise.php";

       /* if (e == 0) {
      */


        //   successurl = "http://catchmeon.net/catchmeon/addadvertiseUpdate.php";
        successurl = "http://catchmeon.net/catchmeon/storeAddUpdateOne.php";

        /*  } else {
            successurl = "http://catchmeon.net/catchmeon/updateAdUpdate.php";
        }*/
        failurl = "fail";
        setHash();
        pd = new ProgressDialog(getActivity());
        postData = "key=" + merchant_key + "&" + "hash=" + hash + "&" + "txnid=" + txnid + "&" + "udf2=" + txnid + "&" + "service_provider=" + serviceprovider + "&" + "amount=" + amount + "&" + "firstname=" + name + "&" + "email=" + email + "&" + "phone=" + phone + "&" + "productinfo=" + productinfo + "&" + "surl=" + successurl + "&" + "furl=" + failurl;


    }

    public void ShowPD() {
        pd.setMessage("Processing...");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setIndeterminate(true);
        pd.setCanceledOnTouchOutside(false);
        pd.setCancelable(false);
        pd.show();
    }

    public void DismissPD() {
        pd.dismiss();
    }

    public boolean empty(String s) {
        if (s == null || s.trim().equals(""))
            return true;
        else
            return false;
    }


    public String hashCal(String type, String str) {
        byte[] hashseq = str.getBytes();
        StringBuffer hexString = new StringBuffer();
        try {
            MessageDigest algorithm = MessageDigest.getInstance(type);
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();


            for (int i = 0; i < messageDigest.length; i++) {
                String hex = Integer.toHexString(0xFF & messageDigest[i]);
                if (hex.length() == 1) hexString.append("0");
                hexString.append(hex);
            }

        } catch (NoSuchAlgorithmException nsae) {
        }

        return hexString.toString();


    }


    public void setTransId() {
        if (empty(txnid)) {
            Random rand = new Random();
            String rndm = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);
            txnid = hashCal("SHA-256", rndm).substring(0, 20);
        }
        udf2 = txnid;
    }

    public void setHash() {
        if (empty(hash)) {
            if (empty(merchant_key)
                    || empty(txnid)
                    || empty(amount)
                    || empty(name)
                    || empty(email)
                    || empty(phone)
                    || empty(productinfo)
                    || empty(successurl)
                    || empty(failurl)
                    || empty(serviceprovider))
                error = 1;
            else {
                String[] hashVarSeq1 = new String[]{merchant_key, txnid, amount, productinfo, name, email, "", udf2, "", "", "", "", "", "", "", ""};
                for (String part : hashVarSeq1) {
                    hashString = (empty(part)) ? hashString.concat("") : hashString.concat(part);
                    hashString = hashString.concat("|");
                }
                hashString = hashString.concat(salt);


                hash = hashCal("SHA-512", hashString);
                action1 = base_url.concat("/_payment");
            }
        } else if (!empty(hash)) {
            hash = hash;
            action1 = base_url.concat("/_payment");
        }


    }


    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if (json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }


    private void submiPost(final Map<String, String> data) {
        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ShowPD();

                try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected Object doInBackground(Object[] params) {
                res = DAO.getJsonFromUrl(Config.Server + "addadvertise.php", data);
                try {
                    if (res.getInt("success") > 0) {
                       /* int j = 0;
                        for (ByteArrayOutputStream s : streams) {
                            byte[] image = s.toByteArray();
                            Map<String, String> d = new HashMap<String, String>();
                            d.put("filename", data.get("imagename") + j + ".jpg");
                            d.put("image", Base64.encodeToString(image, 0));
                            DAO.getJsonFromUrl(Config.Server + "uploadimage.php", d);
                            j++;
                        }*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                DismissPD();
                try {
                    if (res.getInt("success") > 0) {
                        //     Toast.makeText(getActivity(), res.getString("message"), Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    } else {
                        //   Toast.makeText(getActivity(), res.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }


    private void updatePost(final String add_id, final Map<String, String> data) {
        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ShowPD();

                try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                    data.put("add_id", add_id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected Object doInBackground(Object[] params) {
                res = DAO.getJsonFromUrl(Config.updateAd, data);
                try {
                    if (res.getInt("success") > 0) {
                       /* int j = 0;
                        for (ByteArrayOutputStream s : streams) {
                            byte[] image = s.toByteArray();
                            Map<String, String> d = new HashMap<String, String>();
                            d.put("filename", data.get("imagename") + j + ".jpg");
                            d.put("image", Base64.encodeToString(image, 0));
                            DAO.getJsonFromUrl(Config.Server + "uploadimage.php", d);
                            j++;
                        }*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                DismissPD();
                try {
                    if (res.getInt("success") > 0) {
                        //   Toast.makeText(getActivity(), res.getString("message"), Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    } else {
                        //  Toast.makeText(getActivity(), res.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

}
