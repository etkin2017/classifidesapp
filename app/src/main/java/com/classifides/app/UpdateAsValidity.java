package com.classifides.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;


import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.squareup.picasso.Picasso;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class UpdateAsValidity extends AppCompatActivity {

    TextView ad_id, total_rs;
    RecyclerView rec_paymet;

    Button make_payment;
    CheckBox cb1, cb2, cb3;
    ProgressDialog dialog;
    Date today = new Date();
    Calendar c = Calendar.getInstance();
    public static String u_id, a_id, views, plan;

    public int pp = 0;
    MAdaptor adaptor;


    ArrayList<String> plans;

    public int p = 0;
    public int v = 0;
    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    String cdate = df.format(c.getTime());

    private Map<String, String> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_as_validity);
        //Log.e("cdate", cdate);
        u_id = getIntent().getStringExtra("user_id");
        a_id = getIntent().getStringExtra("add_id");


        plans = new ArrayList<String>();


        a_id = a_id.replaceAll("\\[", "").replaceAll("\\]", "");

        u_id = u_id.replaceAll("\\[", "").replaceAll("\\]", "");

        ad_id = (TextView) findViewById(R.id.id_no);
        ad_id.setText(a_id.toString().trim());


        total_rs = (TextView) findViewById(R.id.total_rs);
        make_payment = (Button) findViewById(R.id.make_payment);
        data = new HashMap<String, String>();

        dialog = new ProgressDialog(UpdateAsValidity.this);
        rec_paymet = (RecyclerView) findViewById(R.id.rec_payment);

        adaptor = new MAdaptor();

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rec_paymet.setLayoutManager(llm);

        cb1 = (CheckBox) findViewById(R.id.cb1);
        cb2 = (CheckBox) findViewById(R.id.cb2);
        cb3 = (CheckBox) findViewById(R.id.cb3);

        rec_paymet.setVisibility(View.VISIBLE);
        getHistory();


        cb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (cb1.isChecked() == true) {
                  /*  p = p + 1200;
                    v = v + 2500;*/

                    pp = pp + 1200;

                    total_rs.setText(pp + "");
                    plan = "1200";
                    views = "2500";
                } else {

                    pp = pp - 1200;

                    total_rs.setText(pp + "");
                /*    p = p - 1200;
                    v = v - 2500;*/

                }

            }
        });


        cb2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (cb2.isChecked() == true) {
/*
                    p = p + 2500;
                    v = v + 5000;*/
                    pp = pp + 2500;
                    total_rs.setText(pp + "");


                } else {

                    pp = pp - 2500;
                    total_rs.setText(pp + "");

                   /* p = p - 2500;
                    v = v - 5000;*/
                }

            }
        });


        cb3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (cb3.isChecked() == true) {
/*
                    p = p + 5000;
                    v = v + 10000;*/

                    pp = pp + 5000;
                    total_rs.setText(pp + "");

                    plan = "5000";
                    views = "10000";


                } else {

                    pp = pp - 5000;
                    total_rs.setText(pp + "");
                }

            }
        });

        make_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ((cb1.isChecked() == false) && (cb2.isChecked() == false) && (cb3.isChecked() == false)) {

                    Toast.makeText(getApplicationContext(), "Please Select Plan", Toast.LENGTH_LONG).show();

                } else {
                    if (cb1.isChecked() == true) {

                        p = 1200;
                        v = 2500;

                        plans.add(1200 + "");
                        rec_paymet.setVisibility(View.VISIBLE);

                    } else {
                        if (plans.contains(1200 + "")) {
                            plans.remove(1200 + "");
                        }
                    }
                    if (cb2.isChecked() == true) {
                        plans.add(2500 + "");

                        rec_paymet.setVisibility(View.VISIBLE);


                    } else {
                        if (plans.contains(2500 + "")) {
                            plans.remove(2500 + "");
                        }
                    }
                    if (cb3.isChecked() == true) {
                        plans.add(5000 + "");
                        rec_paymet.setVisibility(View.VISIBLE);
                    } else {
                        if (plans.contains(5000 + "")) {
                            plans.remove(5000 + "");
                        }
                    }


                    makePayment();
                }
            }
        });

    }


    private void makePayment() {

        String planlist = "";


        for (int i = 0; i < plans.size(); i++) {

            if (planlist.contains(plans.get(i))) {
            } else {
                planlist = planlist + plans.get(i) + ",";
            }


        }


        planlist = planlist.substring(0, planlist.length() - 1);


        final String finalPlanlist = planlist;

        data.put("amount", total_rs.getText().toString());
        data.put("add_id", a_id.trim());
        data.put("user_id", u_id.trim());
        data.put("plan", finalPlanlist);
        data.put("views", "");
        data.put("view_date", cdate);


        JSONObject jobj1 = new JSONObject(data);
        Intent i = new Intent(UpdateAsValidity.this, PaymenActivity.class);
        i.putExtra("submit_ad", jobj1.toString());
        i.putExtra("which", "3");
        startActivity(i);


        //Log.e("finalPlanlist", finalPlanlist);

      /*  new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ProgressDialog();
                data.put("add_id", a_id.trim());
                data.put("user_id", u_id.trim());
                data.put("plan", finalPlanlist);
                data.put("views", "");
                data.put("view_date", cdate);
             *//*   try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }*//*
            }

            @Override
            protected Object doInBackground(Object[] params) {

                res = DAO.getJsonFromUrl(Config.addPlanNew, data);

                  *//*  if (res.getInt("success") > 0) {
                        Toast.makeText(UpdateAsValidity.this, "Plan Add Suceessfully", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(UpdateAsValidity.this, "Error ", Toast.LENGTH_LONG).show();

                    }
*//*

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                DismissProgress();
                try {
                    if (res.getInt("success") > 0) {
                        Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_SHORT).show();
                      *//*  Intent ii= new Intent(UpdateAsValidity.this,MyAdd.class);
                        startActivity(ii);*//*

                        getHistory();

                        // adaptor.notifyDataSetChanged();

                    } else {
                        Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);*/
    }


    public void getHistory() {

        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Object doInBackground(Object[] params) {

                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", u_id.trim());
                rest = DAO.getJsonFromUrl(Config.planHistory, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {


                    if (rest.getInt("success") > 0) {
                        adaptor.setData(rest.getJSONArray("plan"));
                        rec_paymet.setAdapter(adaptor);
                    } else {


                        adaptor.setData(new JSONArray());
                        rec_paymet.setAdapter(adaptor);
                        Toast.makeText(UpdateAsValidity.this, rest.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }


    public void ProgressDialog() {
        dialog.setMessage("While we posting your add.");
        dialog.setTitle("Please wait....");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void DismissProgress() {
        dialog.dismiss();
    }


    @Override
    protected void onResume() {
        super.onResume();
        getHistory();
    }

    private class MAdaptor extends RecyclerView.Adapter<MAdaptor.ViewHolder> {

        private JSONArray data;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.plan_history, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            try {
                holder.ad_id.setText(data.getJSONObject(position).getString("add_id"));
                holder.sel_plan.setText("Rs " + data.getJSONObject(position).getString("plan"));
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");


                holder.plan_date_one.setText(data.getJSONObject(position).getString("plan_date"));
                holder.viewers.setText(data.getJSONObject(position).getString("views"));


                holder.itemView.setTag(data.getJSONObject(position));


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        public int getItemCount() {
            return data.length();
        }

        public void setData(JSONArray data) {
            this.data = data;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView sel_plan, ad_id, plan_date_one, viewers;


            TextView user_id, add_id;

            public ViewHolder(View itemView) {
                super(itemView);
                sel_plan = (TextView) itemView.findViewById(R.id.sel_plan);
                ad_id = (TextView) itemView.findViewById(R.id.ad_id);
                plan_date_one = (TextView) itemView.findViewById(R.id.plan_date_one);
                viewers = (TextView) itemView.findViewById(R.id.viewers);

            }
        }
    }
}
