package com.classifides.app.fragment;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.classifides.app.Adaptor.CategoryAdaptor;
import com.classifides.app.R;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.view.MTextView;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class BrowseCategory extends Fragment implements BaseSliderView.OnSliderClickListener {
    CategoryAdaptor adaptor;
    GridView gridview;
    View v;
    ProgressBar p;
    private SliderLayout mDemoSlider;

    public BrowseCategory() {
        // Required empty public constructor
    }


    public String[] cat_id = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
            "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
            "u", "v", "w", "x", "y", "z", "aa", "bb", "cc", "dd",
            "ee", "ff", "gg", "hh", "ii", "jj", "kk", "ll", "mm", "nn",
            "oo", "pp", "qq", "rr", "ss", "tt", "uu", "vv", "ww", "xx", "yy"};

    public String[] cat = {

            "Industrial Plant & Machine",
            "Electronics & Electrical",
            "Industrial Supplies",
            "Food & Beverages",
            "Building & Construction",
            "Apparel & Garments",
            "Chemicals, Dyes & Solvents",
            "Medical & Healthcare",
            "Packaging Machines & Goods",
            "Mechanical Parts & Spares",
            "Housewares & Supplies",
            "Lab Instruments & Supplies",
            "Handicrafts & Decoratives",
            "Automobile, Parts & Spares",
            "Furniture & Supplies",
            "Hand & Machine Tools",
            "Textiles, Yarn & Fabrics",
            "Cosmetics & Personal Care",
            "Metals, Alloys & Minerals",
            "Fashion Accessories & Gear",
            "Gems, Jewelry & Astrology",
            "Home Textile & Furnishing",
            "Agriculture & Farming",
            "Bags, Belts & Wallets",
            "Engineering Services",
            "Herbal & Ayurvedic Product",
            "Sports Goods, Toys & Games",
            "Computer & IT Solutions",
            "Kitchen Utensils & Appliances",
            "Paper & Paper Products",
            "Media, PR & Publishing",
            "Business & Audit Services",
            "Books & Stationery",
            "Telecom Equipment & Goods",
            "Transportation & Logistics",
            "IT & Telecom Services",
            "Education & Training",
            "Marble, Granite & Stones",
            "Call Center & BPO Services",
            "Travel, Tourism & Hotels",
            "Bicycle, Rickshaw & Spares",
            "Financial & Legal Service",
            "Leather Products",
            "Product Rental & Leasing",
            "HR Planning & Recruitment",
            "Architecture & Interiors",
            "Event Planner & Organizer",
            "R & D and Testing Labs",
            "Facility Management",
            "Contractors & Freelancers",
            "Rail, Shipping & Aviation"};
    public Integer[] mThumbIds = {
            R.drawable.industrial_plant_n_machine, R.drawable.electronics_n_electrical,
            R.drawable.industrial_supplies, R.drawable.food_n_beverages,
            R.drawable.building_construction, R.drawable.apparel_n_garments,
            R.drawable.chemicals_dyes_n_solvents, R.drawable.medical_n_healthcare,
            R.drawable.packaging_machines_n_goods, R.drawable.mechanical_parts_n_spares,
            R.drawable.housewares_n_supplies, R.drawable.lab_instruments_n_supplies,
            R.drawable.handicrafts_n_decoratives,


            R.drawable.automobile_parts_n_spares,

            R.drawable.furniture_n_supplies, R.drawable.hand_n_machine_tools,
            R.drawable.textiles_yarn_n_fabrics, R.drawable.cosmetics_n_personal_care,
            R.drawable.metals_alloys_n_minerals, R.drawable.fashion_accessories_n_gear,
            R.drawable.gems_jewelry_n_astrology, R.drawable.home_textile_n_furnishing,
            R.drawable.agriculture_n_farming, R.drawable.bags_belts_wallets,
            R.drawable.engineering_services, R.drawable.herbal_n_ayurvedic_product,
            R.drawable.sports_goods_toys_n_games,
            R.drawable.computer_n_it_solutions,
            R.drawable.kitchen_utensils_n_appliances,


            R.drawable.paper_n_paper_products,
            R.drawable.media_pr_n_publishing, R.drawable.business_n_audit_services,
            R.drawable.books_n_stationery, R.drawable.telecom_equipment_n_goods,
            R.drawable.transportation_n_logistics, R.drawable.it_n_telecom_services,
            R.drawable.education_n_training, R.drawable.marble_n_granite_n_stones,
            R.drawable.call_center_n_bpo_services, R.drawable.travel_tourism_n_hotels,
            R.drawable.bicycle_spare_parts,
            R.drawable.financial_n_legal_service,

            R.drawable.leather_products, R.drawable.product_rental_n_leasing,

            R.drawable.hr_planning_n_recruitment, R.drawable.architecture_n_interiors,
            R.drawable.event_planner_n_organizer, R.drawable.r_n_d_and_testing_labs,
            R.drawable.facility_management, R.drawable.contractors_n_freelancers,
            R.drawable.rail_shipping_aviation
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_browse_category, container, false);

        gridview = (GridView) v.findViewById(R.id.cat_grid);
      //  p = (ProgressBar) v.findViewById(R.id.prg_bar);
        mDemoSlider = (SliderLayout) v.findViewById(R.id.slider);
        adaptor = new CategoryAdaptor(getActivity(), cat, cat_id, mThumbIds);
     //   adaptor.setData(rest.getJSONArray("Category"));
        gridview.setAdapter(adaptor);
        //   getCategory();
        //  setImageSlider();

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get the GridView selected/clicked item text
                String selectedItem = parent.getItemAtPosition(position).toString();
                TextView cid = (TextView) view.findViewById(R.id.cat_id);
                MTextView c_name = (MTextView) view.findViewById(R.id.catname);
                //Log.e("CID", cid.getText().toString());
                //Log.e("CNAME", c_name.getText().toString());
                SubCategory ldf = new SubCategory();
                Bundle args = new Bundle();
                args.putString("cid", cid.getText().toString());
                args.putString("cname", c_name.getText().toString());
                ldf.setArguments(args);

//Inflate the fragment
                getFragmentManager().beginTransaction().replace(R.id.container, ldf, "suCat").addToBackStack("suCat").commit();


                // Display the selected/clicked item text and position on TextView
              /*  tv.setText("GridView item clicked : " +selectedItem
                        + "\nAt index position : " + position);*/
            }
        });

        return v;
    }

    private void setImageSlider() {


        HashMap<String, String> url_maps = new HashMap<String, String>();

        url_maps.put(" ", "http://classifides.etkininfotech.com/slider/c_onw.png");
        url_maps.put("  ", "http://classifides.etkininfotech.com/slider/c_two.png");

        url_maps.put("    ", "http://classifides.etkininfotech.com/slider/images.png");

        url_maps.put("      ", "http://classifides.etkininfotech.com/slider/post-free-ads.jpg");


        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView.image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }

        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(5000);
        mDemoSlider.addOnPageChangeListener(null);
    }

    public void getCategory() {
        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                p.setVisibility(View.VISIBLE);

            }

            @Override
            protected Object doInBackground(Object[] params) {

                Map<String, String> map = new HashMap<String, String>();
                map.put("abc", "abc");
                rest = DAO.getJsonFromUrl(Config.getCategory, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    p.setVisibility(View.GONE);
                    gridview.setVisibility(View.VISIBLE);


                  /*  if (rest.getInt("success") > 0) {
                        adaptor.setData(rest.getJSONArray("Category"));
                        gridview.setAdapter(adaptor);
                    } else {


                        adaptor.setData(new JSONArray());
                        gridview.setAdapter(adaptor);
                        Toast.makeText(getActivity(), "Data not found", Toast.LENGTH_SHORT).show();
                    }
*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }
}
