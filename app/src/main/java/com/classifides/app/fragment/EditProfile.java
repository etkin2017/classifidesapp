package com.classifides.app.fragment;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.CursorLoader;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.classifides.app.R;
import com.classifides.app.application.*;
import com.classifides.app.application.Classified;
import com.classifides.app.data.Blur;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.view.MTextView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfile extends Fragment {
    ImageView backimage, userimage;
    AutoCompleteTextView name, email, mob, et_pswd;
    RadioButton male, female;
    LinearLayout editProfile, update, date_picker;
    private ProgressDialog pd;
    private String imagepath = null;
    private RadioGroup sex_grp;
    MTextView dp;

    EditText bank_nm, branch_nm, acc_holder_nm, acc_no, ifsc_code;
    Calendar myCalendar = Calendar.getInstance();
    Long tsLong;
    String ts;
    SharedPreferences sh;

    public EditProfile() {
        // Required empty public constructor
    }

    private void initView(View v) {
        pd = new ProgressDialog(getActivity());
        backimage = (ImageView) v.findViewById(R.id.imageback);

        userimage = (ImageView) v.findViewById(R.id.userimage);
        name = (AutoCompleteTextView) v.findViewById(R.id.et_name);
        et_pswd = (AutoCompleteTextView) v.findViewById(R.id.et_pswd);
        email = (AutoCompleteTextView) v.findViewById(R.id.et_email);
        mob = (AutoCompleteTextView) v.findViewById(R.id.et_mob);
        male = (RadioButton) v.findViewById(R.id.sex_male);
        female = (RadioButton) v.findViewById(R.id.sex_female);
        sex_grp = (RadioGroup) v.findViewById(R.id.sex_group);
        editProfile = (LinearLayout) v.findViewById(R.id.editProfile);
        dp = (MTextView) v.findViewById(R.id.dp);
        date_picker = (LinearLayout) v.findViewById(R.id.date_picker);

        bank_nm = (EditText) v.findViewById(R.id.bank_nm);
        branch_nm = (EditText) v.findViewById(R.id.branch_nm);
        acc_holder_nm = (EditText) v.findViewById(R.id.acc_holder_nm);
        acc_no = (EditText) v.findViewById(R.id.acc_no);
        ifsc_code = (EditText) v.findViewById(R.id.ifsc_code);


        update = (LinearLayout) v.findViewById(R.id.submit);
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Light.ttf");
        name.setTypeface(face);
        email.setTypeface(face);
        mob.setTypeface(face);
        male.setTypeface(face);
        female.setTypeface(face);
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View v = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        tsLong = System.currentTimeMillis() / 1000;
        ts = tsLong.toString();
        initView(v);
        addListener();

        return v;
    }


    public void setIcon(final String u) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.app_bar_home, null);
        CircleImageView img = (CircleImageView) v.findViewById(R.id.uimg);
        Picasso.with(Classified.getInstance()).load(u).into(img);


    }

    private void updateLabel() {

        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dp.setText(sdf.format(myCalendar.getTime()));
    }

    private static final int RESULT_LOAD_IMG = 0;

    private void addListener() {
        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
            }
        });

        date_picker.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nm = name.getText().toString();
                String em = email.getText().toString();
                String mo = mob.getText().toString();
                if (TextUtils.isEmpty(nm)) {
                    Toast.makeText(getActivity(), "plese enter name", Toast.LENGTH_SHORT).show();
                    return;
                } else if (TextUtils.isEmpty(mo)) {
                    Toast.makeText(getActivity(), "plese enter mobile no", Toast.LENGTH_SHORT).show();
                    return;
                } else if (TextUtils.isEmpty(dp.getText().toString())) {
                    dp.setError("Please select birthdate");
                    return;
                } else if (TextUtils.isEmpty(branch_nm.getText().toString())) {
                    branch_nm.setError("enter branch name");
                } else if (TextUtils.isEmpty(bank_nm.getText().toString())) {
                    bank_nm.setError("enter bank name");
                } else if (TextUtils.isEmpty(acc_holder_nm.getText().toString())) {
                  /*  Toast.makeText(getActivity(), "plese enter mobile no", Toast.LENGTH_SHORT).show();
                    return;*/

                    acc_holder_nm.setError("enter acc holder name");
                } else if (TextUtils.isEmpty(acc_no.getText().toString())) {
                    acc_no.setError("enter account no");
                } else if (TextUtils.isEmpty(ifsc_code.getText().toString())) {
                  /*  Toast.makeText(getActivity(), "plese enter mobile no", Toast.LENGTH_SHORT).show();
                    return;*/

                    ifsc_code.setError("enter ifsc code");
                } else if (TextUtils.isEmpty(et_pswd.getText().toString())) {
                  /*  Toast.makeText(getActivity(), "plese enter mobile no", Toast.LENGTH_SHORT).show();
                    return;*/

                    et_pswd.setError("enter password");
                } else {
                    int selectedId = sex_grp.getCheckedRadioButtonId();
                    String sex = ((RadioButton) getActivity().findViewById(selectedId)).getText().toString();
                    updateDetail(nm, sex, em, mo, dp.getText().toString(), bank_nm.getText().toString(), branch_nm.getText().toString(),
                            acc_no.getText().toString(), acc_holder_nm.getText().toString(), ifsc_code.getText().toString(), et_pswd.getText().toString());
                }
            }
        });
    }

    private void updateDetail(final String nm, final String sex, final String em, final String mo, final String dob, final String bank_nm, final String branch_nm,
                              final String acc_no, final String ac_holder_nm, final String ifsc_code, final String pwd) {
        new AsyncTask() {
            ProgressDialog p;
            JSONObject res;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                p = new ProgressDialog(getActivity());
                pd.setMessage("We are updating profile detail");
                pd.setTitle("Please wait....");
                pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                pd.setIndeterminate(true);
                pd.setCanceledOnTouchOutside(false);
                pd.show();

            }

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    Map<String, String> d = new HashMap<String, String>();
                    d.put("email", em);
                    d.put("name", nm);
                    d.put("sex", sex);
                    d.put("mobile", mo);

                    d.put("pwd", pwd);
                    d.put("bn", bank_nm);
                    d.put("branch_nm", branch_nm);
                    d.put("acc_hd_nm", ac_holder_nm);
                    d.put("acc_no", acc_no);
                    d.put("ifsc_code", ifsc_code);
                    d.put("dob", dob);


                    res = DAO.getJsonFromUrl(Config.Server + "updatedetail.php", d);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                pd.dismiss();
                try {
                    if (res.getInt("success") > 0) {
                        SharedPreferences sh = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
                        sh.edit().putString("users", res.getJSONArray("user").getJSONObject(0).toString()).commit();


                    }
                    Toast.makeText(getActivity(), "update success", Toast.LENGTH_LONG).show();
                    SharedPreferences sh = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
                    String user = sh.getString("users", "");
                    try {
                        JSONObject us = new JSONObject(user);
                        int id = us.getInt("id");
                        getUserInfo(id + "");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "error in updation", Toast.LENGTH_LONG).show();
                }
            }
        }.execute(null, null, null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       /* if (resultCode == getActivity().RESULT_OK && requestCode == 1) {
            try {

                Uri selectedImage = data.getData();

                updateProfile(selectedImage);


            } catch (Exception e) {
                e.printStackTrace();
            }

        }*/

        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == getActivity().RESULT_OK
                    && null != data) {
                // Get the Image from data

                Bundle b = new Bundle();
                b.putString("ts", ts);
                String selectedImage = data.getDataString();
                ImageCrop obj = new ImageCrop();
                obj.setArguments(b);
                obj.setPath(selectedImage);
                obj.setEmail(email.getText().toString());
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, obj, "imagecrop")
                        .commit();
             /*   Uri selectedImage1 = data.getData();

                updateProfile(selectedImage1);*/


            } else {
                Toast.makeText(getActivity(), "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }

    }

    private void updateProfile(final Uri selectedImage) {
        final String name = email.getText().toString();
        new AsyncTask() {
            String selectedImagePath;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ProgressDialog();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(getActivity(), selectedImage, projection, null, null,
                        null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                selectedImagePath = cursor.getString(column_index);
            }

            @Override
            protected Object doInBackground(Object[] params) {
                try {

                    InputStream imageStream = getActivity().getContentResolver().openInputStream(selectedImage);

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;
                    Bitmap bitmap = BitmapFactory.decodeFile(selectedImagePath, options);

                    if (byteSizeOf(bitmap) > 921600) {

                        options = new BitmapFactory.Options();
                        options.inSampleSize = 4;
                        bitmap = BitmapFactory.decodeFile(selectedImagePath, options);
                    }
                    if (byteSizeOf(bitmap) > 2097152) {

                        options = new BitmapFactory.Options();
                        options.inSampleSize = 8;
                        bitmap = BitmapFactory.decodeFile(selectedImagePath, options);
                    }

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] image = stream.toByteArray();
                    Map<String, String> d = new HashMap<String, String>();
                    d.put("filename", name.substring(0, name.indexOf("@")) + ".jpg");
                    d.put("image", Base64.encodeToString(image, 0));
                    DAO.getJsonFromUrl(Config.Server + "updateprofile.php", d);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }


                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                DismissProgress();
                Picasso.with(com.classifides.app.application.Classified.getInstance()).invalidate("http://classifides.etkininfotech.com/profile/" + name.substring(0, name.indexOf("@")) + ".jpg");
                Picasso.with(com.classifides.app.application.Classified.getInstance()).load("http://classifides.etkininfotech.com/profile/" + name.substring(0, name.indexOf("@")) + ".jpg").placeholder(R.drawable.user).error(R.drawable.user).transform(new Blur(getActivity(), 80)).into(backimage);
                Picasso.with(com.classifides.app.application.Classified.getInstance()).load("http://classifides.etkininfotech.com/profile/" + name.substring(0, name.indexOf("@")) + ".jpg").placeholder(R.drawable.user).error(R.drawable.user).into(userimage);

                setValues();

            }
        }.execute(null, null, null);
    }

    public void updateProfData(String imgpath) {
        //Log.e("IIII", imgpath);
        Picasso.with(com.classifides.app.application.Classified.getInstance()).invalidate(imgpath);
        /*  Picasso.with(com.classifides.app.application.Classified.getInstance()).load(imgpath).placeholder(R.drawable.user).error(R.drawable.user).transform(new Blur(getActivity(), 80)).into(backimage);
        Picasso.with(com.classifides.app.application.Classified.getInstance()).load(imgpath).placeholder(R.drawable.user).error(R.drawable.user).into(userimage);

   */

        setValues();

    }


    public int byteSizeOf(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return bitmap.getAllocationByteCount();
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
            return bitmap.getByteCount();
        } else {
            return bitmap.getRowBytes() * bitmap.getHeight();
        }
    }


    public void getUserInfo(final String user_id) {

        final Map<String, String> data = new HashMap<String, String>();


        new AsyncTask() {
            JSONObject res = null;
            ProgressDialog pdd;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();


                data.put("id", user_id);

            /*    try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
            }

            @Override
            protected Object doInBackground(Object[] params) {
                res = DAO.getJsonFromUrl(Config.userInfo, data);

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                try {
                    if (res.getInt("success") > 0) {
                        //  Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_SHORT).show();
                        //  getActivity().finish();

                        JSONArray jsonArray = res.getJSONArray("user");
                        // Log.e("JsONArray", jsonArray.toString());
                        JSONObject jobj = jsonArray.getJSONObject(0);

                        sh = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
                        sh.edit().putString("users", res.getJSONArray("user").getJSONObject(0).toString()).commit();

                        //Log.e("Jobj", jobj.toString());
                        //Log.e("name", jobj.getString("name"));

                        //Log.e("mobile", jobj.getString("mobile"));


                        //  title.setText(jobj.getString("title"));

                        name.setText(jobj.getString("name"));
                        dp.setText(jobj.getString("dob"));
                        bank_nm.setText(jobj.getString("bank_name"));
                        branch_nm.setText(jobj.getString("branch"));
                        acc_holder_nm.setText(jobj.getString("acc_holder_name"));
                        acc_no.setText(jobj.getString("acc_no"));
                        ifsc_code.setText(jobj.getString("ifsc_code"));


                        try {
                            et_pswd.setText(jobj.getString("p"));
                        } catch (Exception e) {

                        }

                        if (et_pswd.getText().toString().equals("null")) {
                            et_pswd.setText("-");
                        }

                        if (bank_nm.getText().toString().equals("null")) {
                            bank_nm.setText("-");
                        } else {

                        }
                        if (branch_nm.getText().toString().equals("null")) {
                            branch_nm.setText("-");
                        } else {

                        }

                        if (acc_holder_nm.getText().toString().equals("null")) {
                            acc_holder_nm.setText("-");
                        } else {

                        }
                        if (acc_no.getText().toString().equals("null")) {
                            acc_no.setText("-");
                        } else {

                        }
                        if (ifsc_code.getText().toString().equals("null")) {
                            ifsc_code.setText("-");
                        } else {

                        }
                        if (dp.getText().toString().equals("null")) {
                            dp.setText("-");
                        } else {

                        }


                        email.setText(jobj.getString("email"));
                        mob.setText(jobj.getString("mobile"));
                        String sex = jobj.getString("sex");
                        if (male.getText().equals(sex)) {
                            male.setChecked(true);
                        } else {
                            female.setChecked(true);
                        }
/*

                        Picasso.with(getActivity()).load(jobj.getString("image")).placeholder(R.drawable.user).error(R.drawable.user).transform(new Blur(getActivity(), 80)).into(backimage);
                        Picasso.with(getActivity()).load(jobj.getString("image")).placeholder(R.drawable.user).error(R.drawable.user).into(userimage);
                        imagepath = jobj.getString("image");
*/


                        String url = jobj.getString("image");


                        Picasso.with(Classified.getInstance()).load(url).into(userimage);
                        Picasso.with(Classified.getInstance()).load(url).transform(new Blur(getActivity(), 80)).into(backimage);
                        setIcon(url);


                    } else {
                        //  Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

    @Override
    public void onResume() {
        super.onResume();
        //  setValues();
        SharedPreferences sh = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        String user = sh.getString("users", "");
        try {
            JSONObject us = new JSONObject(user);
            int id = us.getInt("id");
            getUserInfo(id + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setValues() {
        SharedPreferences sh = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        String user = sh.getString("users", "");
        try {
            JSONObject us = new JSONObject(user);
            name.setText(us.getString("name"));
            dp.setText(us.getString("dob"));
            bank_nm.setText(us.getString("bank_name"));
            branch_nm.setText(us.getString("branch"));
            acc_holder_nm.setText(us.getString("acc_holder_name"));
            acc_no.setText(us.getString("acc_no"));
            ifsc_code.setText(us.getString("ifsc_code"));


            try {
                et_pswd.setText(us.getString("p"));
            } catch (Exception e) {

            }

            if (et_pswd.getText().toString().equals("null")) {
                et_pswd.setText("-");
            }

            if (bank_nm.getText().toString().equals("null")) {
                bank_nm.setText("-");
            } else {

            }
            if (branch_nm.getText().toString().equals("null")) {
                branch_nm.setText("-");
            } else {

            }

            if (acc_holder_nm.getText().toString().equals("null")) {
                acc_holder_nm.setText("-");
            } else {

            }
            if (acc_no.getText().toString().equals("null")) {
                acc_no.setText("-");
            } else {

            }
            if (ifsc_code.getText().toString().equals("null")) {
                ifsc_code.setText("-");
            } else {

            }
            if (dp.getText().toString().equals("null")) {
                dp.setText("-");
            } else {

            }


            email.setText(us.getString("email"));
            mob.setText(us.getString("mobile"));
            String sex = us.getString("sex");
            if (male.getText().equals(sex)) {
                male.setChecked(true);
            } else {
                female.setChecked(true);
            }

            Picasso.with(getActivity()).load(us.getString("image")).placeholder(R.drawable.user).error(R.drawable.user).transform(new Blur(getActivity(), 80)).into(backimage);
            Picasso.with(getActivity()).load(us.getString("image")).placeholder(R.drawable.user).error(R.drawable.user).into(userimage);
            imagepath = us.getString("image");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void ProgressDialog() {
        pd.setMessage("We are updating profile ");
        pd.setTitle("Please wait....");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setIndeterminate(true);
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    public void DismissProgress() {
        pd.dismiss();
    }
}
