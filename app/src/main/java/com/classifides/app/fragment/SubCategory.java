package com.classifides.app.fragment;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.classifides.app.AddDetail;
import com.classifides.app.R;
import com.classifides.app.application.*;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.view.MTextView;
import com.squareup.picasso.Picasso;
import com.universalvideoview.UniversalMediaController;
import com.universalvideoview.UniversalVideoView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubCategory extends Fragment {

    View v;
    RecyclerView sub_cat_list;
    //   ProgressBar progress_bar;
    MAdaptor adaptor;

    List<String> aarr;

    int abc;

    public SubCategory() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_sub_category, container, false);

        String cname = getArguments().getString("cname");
        String value = getArguments().getString("cid");
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(cname);
        //  ((AppCompatActivity)getActivity()).getSupportActionBar().

        aarr = new ArrayList<String>();

        abc = getResources().getIdentifier(value, "array", getActivity().getPackageName());

        //Log.e("AA", abc + "");

        String[] itemNames = getResources().getStringArray(abc);

        //Log.e("itemNames", itemNames.toString());
        for (int i = 0; i < itemNames.length; i++) {
            //Log.e("VVVVVVVVVVVV" + i, itemNames[i]);
            aarr.add(itemNames[i]);

        }


        adaptor = new MAdaptor(getActivity(), aarr);
        //      progress_bar = (ProgressBar) v.findViewById(R.id.progress_bar);
        sub_cat_list = (RecyclerView) v.findViewById(R.id.sub_cat_list);
        sub_cat_list.setLayoutManager(new LinearLayoutManager(getActivity()));
        //   JSONArray jsArray = new JSONArray(aarr);
        //   adaptor.setData(jsArray);
        sub_cat_list.setAdapter(adaptor);

        //  getSubCategory(value);
        return v;
    }


    public void getSubCategory(final String id) {
        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //    progress_bar.setVisibility(View.VISIBLE);

            }

            @Override
            protected Object doInBackground(Object[] params) {

                Map<String, String> map = new HashMap<String, String>();
                map.put("id", id);
                rest = DAO.getJsonFromUrl(Config.getSubCategory, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    //     progress_bar.setVisibility(View.GONE);
                    sub_cat_list.setVisibility(View.VISIBLE);


                  /*  if (rest.getInt("success") > 0) {
                        adaptor.setData(rest.getJSONArray("subcategory"));
                        sub_cat_list.setAdapter(adaptor);
                    } else {


                        adaptor.setData(new JSONArray());
                        sub_cat_list.setAdapter(adaptor);
                        Toast.makeText(getActivity(), "Data not found", Toast.LENGTH_SHORT).show();
                    }*/

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }


    private class MAdaptor extends RecyclerView.Adapter<MAdaptor.ViewHolder> {

        private JSONArray data;
        private int lastPosition = -1;

        List<String> mStr;
        Context context;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sub_category, null);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
          /*  try {*/
            //    holder. video_thumb.(setCallback(EasyVideoCallback));

            //  setAnimation(holder.itemView, position);
            holder.title.setText(mStr.get(position));
              /*  holder.scid.setText(data.getJSONObject(position).getString("subcat_id"));
                holder.cid.setText(data.getJSONObject(position).getString("cat_id"));*/

            //  holder.video_thumb.setCallback(EasyVideoProgressCallback);


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String sc = holder.title.getText().toString();
                    //Log.e("SCCC", sc);
                    AdsBySubCategory ldf = new AdsBySubCategory();
                    Bundle args = new Bundle();
                    args.putString("sub_cat", sc);
                    ldf.setArguments(args);


//Inflate the fragment
                    getFragmentManager().beginTransaction().add(R.id.container, ldf, "suCat").addToBackStack("suCat").commit();
                }
            });
           /* } catch (JSONException e) {
                e.printStackTrace();
            }*/


        }


        public void onBack() {
            notifyDataSetChanged();
        }


        @Override
        public int getItemCount() {
            return mStr.size();
        }

      /*  public void setData(JSONArray data) {
            this.data = data;
            this.notifyDataSetChanged();
        }
*/

        @Override
        public void onViewDetachedFromWindow(ViewHolder holder) {
            super.onViewDetachedFromWindow(holder);
            //clearAnimation();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {
            MTextView title;
            TextView scid, cid;

            public ViewHolder(View itemView) {
                super(itemView);
                //video_click = (CardView) itemView.findViewById(R.id.video_click);

                title = (MTextView) itemView.findViewById(R.id.sub_cat_name);
                scid = (TextView) itemView.findViewById(R.id.scid);
                cid = (TextView) itemView.findViewById(R.id.cid);


            }

        }

        public MAdaptor(Context context, List<String> mStr) {
            this.context = context;
            this.mStr = mStr;
        }


        private void setAnimation(View viewToAnimate, int position) {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition) {
                Animation animation = AnimationUtils.loadAnimation(getActivity(),
                        (position > lastPosition) ? R.anim.up_from_bottom
                                : R.anim.down_from_top);
                viewToAnimate.startAnimation(animation);
                lastPosition = position;

            }
        }


    }
}
