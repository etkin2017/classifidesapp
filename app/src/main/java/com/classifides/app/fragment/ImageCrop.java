package com.classifides.app.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.CursorLoader;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.classifides.app.BrowseByCategory;
import com.classifides.app.R;
import com.classifides.app.application.*;
import com.classifides.app.data.Blur;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.util.ImageLoadingUtils;
import com.classifides.app.view.CropImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImageCrop extends Fragment {

    LinearLayout send, cancel;
    private ImageLoadingUtils utils;
    Bitmap bitmap = null;
    CropImageView cropImageView;
    FileOutputStream out = null;
    Bitmap scaledBitmap = null;
    String path = "";
    String filename;
    private String email;
    ProgressDialog pd;

    SharedPreferences sh;

    public static int id;

    String ts;

    Calendar c;

    public void setPath(String path) {
        this.path = path;
    }


    public ImageCrop() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_image_crop, container, false);

        ts = getArguments().getString("ts");

        send = (LinearLayout) v.findViewById(R.id.send);
        cancel = (LinearLayout) v.findViewById(R.id.can);
        pd = new ProgressDialog(getActivity());

        //   c=Calendar.getInstance();


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    out = new FileOutputStream(filename);
                    cropImageView.getCroppedBitmap().compress(Bitmap.CompressFormat.JPEG, 100, out);
                    out.close();
                    updateProfile(filename);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              //  getActivity().getSupportFragmentManager().popBackStack();

                EditProfile ep = new EditProfile();

                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, ep, "imagecropba")
                        .commit();
              //  getActivity().onBackPressed();
            }
        });

        cropImageView = (CropImageView) v.findViewById(R.id.cropImageView);
        cropImageView.setGuideColor(0xff86c166);
        cropImageView.setHandleColor(0xff86c166);
        cropImageView.setFrameColor(0xff86c166);
        // cropImageView.setHandleSizeInDp(5);
        cropImageView.setHandleShowMode(CropImageView.ShowMode.SHOW_ON_TOUCH);
        cropImageView.setGuideShowMode(CropImageView.ShowMode.SHOW_ON_TOUCH);
        utils = new ImageLoadingUtils(com.classifides.app.application.Classified.getInstance());
        new ImageCompressionAsyncTask(true).execute(path);
        return v;
    }


    private void updateProfile(final String selectedImagePath) {

        new AsyncTask() {

            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ProgressDialog();
            }

            @Override
            protected Object doInBackground(Object[] params) {
                try {

                    SharedPreferences sh = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
                    String user = sh.getString("users", "");
                    try {
                        JSONObject us = new JSONObject(user);
                        id = us.getInt("id");
                        //  obj.getUserInfo(id + "");
                      /*  getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.container, obj, "editProf")
                                .commit();*/

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;
                    Bitmap bitmap = BitmapFactory.decodeFile(selectedImagePath, options);

                    if (byteSizeOf(bitmap) > 921600) {

                        options = new BitmapFactory.Options();
                        options.inSampleSize = 4;
                        bitmap = BitmapFactory.decodeFile(selectedImagePath, options);
                    }
                    if (byteSizeOf(bitmap) > 2097152) {

                        options = new BitmapFactory.Options();
                        options.inSampleSize = 8;
                        bitmap = BitmapFactory.decodeFile(selectedImagePath, options);
                    }

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] image = stream.toByteArray();
                    Map<String, String> d = new HashMap<String, String>();
                    d.put("id", id + "");
                    d.put("filename", ts + ".jpg");
                    d.put("image", Base64.encodeToString(image, 0));
                    res = DAO.getJsonFromUrl(Config.Server + "updateprofile.php", d);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                DismissProgress();

                try {
                    if (res.getInt("success") > 0) {


                        sh = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
                        sh.edit().putString("users", res.getJSONArray("user").getJSONObject(0).toString()).commit();


                    }
                } catch (JSONException e) {
                    e.printStackTrace();


                }

                Picasso.with(com.classifides.app.application.Classified.getInstance()).invalidate("http://classifides.etkininfotech.com/profile/" + email.substring(0, email.indexOf("@")) + ".jpg");

                //Log.e("IMGPATH1", "http://classifides.etkininfotech.com/profile/" + email.substring(0, email.indexOf("@")) + ".jpg");

               /* Picasso.with(getActivity()).load(imagepath).placeholder(R.drawable.user).error(R.drawable.user).transform(new Blur(getActivity(), 80)).into(backimage);
                Picasso.with(getActivity()).load(imagepath).placeholder(R.drawable.user).error(R.drawable.user).into(userimage);

                setValues();


*/

                String ipath = "http://classifides.etkininfotech.com/profile/" + email.substring(0, email.indexOf("@")) + ".jpg";
                EditProfile obj = new EditProfile();
                SharedPreferences sh = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
                String user = sh.getString("users", "");
                try {
                    JSONObject us = new JSONObject(user);
                    int id = us.getInt("id");
                    obj.getUserInfo(id + "");
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.container, obj, "editProf")
                            .commit();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //   getActivity().getSupportFragmentManager().popBackStack();

               /* getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container, obj, "editProf")
                        .commit();*/
           /*     getActivity().finish();*/

            }
        }.execute(null, null, null);


    }

    public void ProgressDialog() {
        pd.setMessage("We are updating profile pic");
        pd.setTitle("Please wait....");
        pd.setProgressStyle(android.app.ProgressDialog.STYLE_SPINNER);
        pd.setIndeterminate(true);
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    public void DismissProgress() {
        pd.dismiss();
    }


    public int byteSizeOf(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return bitmap.getAllocationByteCount();
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
            return bitmap.getByteCount();
        } else {
            return bitmap.getRowBytes() * bitmap.getHeight();
        }
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }


    class ImageCompressionAsyncTask extends AsyncTask<String, Void, String> {
        private boolean fromGallery;

        public ImageCompressionAsyncTask(boolean fromGallery) {
            this.fromGallery = fromGallery;
        }

        @Override
        protected String doInBackground(String... params) {
            String filePath = compressImage(params[0]);
            return filePath;
        }

        public String compressImage(String imageUri) {

            String filePath = getRealPathFromURI(imageUri);


            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;
            float maxHeight = 816.0f;
            float maxWidth = 612.0f;
            float imgRatio = actualWidth / actualHeight;
            float maxRatio = maxWidth / maxHeight;

            if (actualHeight > maxHeight || actualWidth > maxWidth) {
                if (imgRatio < maxRatio) {
                    imgRatio = maxHeight / actualHeight;
                    actualWidth = (int) (imgRatio * actualWidth);
                    actualHeight = (int) maxHeight;
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth;
                    actualHeight = (int) (imgRatio * actualHeight);
                    actualWidth = (int) maxWidth;
                } else {
                    actualHeight = (int) maxHeight;
                    actualWidth = (int) maxWidth;

                }
            }

            options.inSampleSize = utils.calculateInSampleSize(options, actualWidth, actualHeight);
            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inTempStorage = new byte[16 * 1024];

            try {
                bmp = BitmapFactory.decodeFile(filePath, options);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();

            }
            try {
                scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();
            }

            float ratioX = actualWidth / (float) options.outWidth;
            float ratioY = actualHeight / (float) options.outHeight;
            float middleX = actualWidth / 2.0f;
            float middleY = actualHeight / 2.0f;

            Matrix scaleMatrix = new Matrix();
            scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

            Canvas canvas = new Canvas(scaledBitmap);
            canvas.setMatrix(scaleMatrix);
            canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));


            ExifInterface exif;
            try {
                exif = new ExifInterface(filePath);

                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
                Log.d("EXIF", "Exif: " + orientation);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90);
                    Log.d("EXIF", "Exif: " + orientation);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                    Log.d("EXIF", "Exif: " + orientation);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                    Log.d("EXIF", "Exif: " + orientation);
                }
                scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            FileOutputStream out = null;
            filename = getFilename();


            return filename;

        }

        private String getRealPathFromURI(String contentURI) {
            Uri contentUri = Uri.parse(contentURI);
            Cursor cursor = getActivity().getContentResolver().query(contentUri, null, null, null, null);
            if (cursor == null) {
                return contentUri.getPath();
            } else {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                return cursor.getString(idx);
            }
        }

        public String getFilename() {
            File file = new File(Environment.getExternalStorageDirectory().getPath(), "Android/data/com.classifides/.user");
            if (!file.exists()) {
                file.mkdirs();
            }
            String uriSting = (file.getAbsolutePath() + "/user.jpg");
            return uriSting;

        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {

                //  bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),Uri.parse("/storage/emulated/0/MyFolder/Images/IMG_20151019_200801010.jpg"));
                cropImageView.setImageBitmap(scaledBitmap);

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

}
