package com.classifides.app.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.classifides.app.BrowseByCategory;
import com.classifides.app.LocationMap;
import com.classifides.app.PaymentFragment;
import com.classifides.app.R;
import com.classifides.app.application.*;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.view.MTextView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostVideoAddTwo extends Fragment {


    private Map<String, String> data;
    EditText price;
    LinearLayout submitAdd;
    private ProgressDialog dialog;

    Context ctx;


    MTextView result, paymentres, submittext, address, addressone, addresstwo;

    RelativeLayout otherLocOne, otherLocTwo, defaultloc;
    LinearLayout otherLocbtnOne, otherLocbtnTwo, adddefault;
    MTextView otherLocRemoveOne, otherLocRemoveTwo;
    private String videoPath;
    private String fileName;

    private String cityname;
    private String placeid;
    private String nearbyname;
    private CheckBox primiumone, primiumtwo;
    private int amt = 1200;
    private int views = 0;
    int e;

    public static String adid;

    public PostVideoAddTwo() {
        // Required empty public constructor
    }


    private void initView(View v) {
        result = (MTextView) v.findViewById(R.id.result);
        address = (MTextView) v.findViewById(R.id.addressloc);
        addressone = (MTextView) v.findViewById(R.id.addresslocone);
        addresstwo = (MTextView) v.findViewById(R.id.addressloctwo);
        price = (EditText) v.findViewById(R.id.et_price);
        submitAdd = (LinearLayout) v.findViewById(R.id.submitadd);
        otherLocOne = (RelativeLayout) v.findViewById(R.id.otherlocone);
        otherLocTwo = (RelativeLayout) v.findViewById(R.id.otherloctwo);
        defaultloc = (RelativeLayout) v.findViewById(R.id.defaultloc);
        adddefault = (LinearLayout) v.findViewById(R.id.add_loc_one);
        otherLocbtnOne = (LinearLayout) v.findViewById(R.id.add_loc);
        otherLocbtnTwo = (LinearLayout) v.findViewById(R.id.add_locone);
        otherLocRemoveOne = (MTextView) v.findViewById(R.id.removeone);
        otherLocRemoveTwo = (MTextView) v.findViewById(R.id.removetwo);
        otherLocOne.setVisibility(View.GONE);
        otherLocTwo.setVisibility(View.GONE);
        defaultloc.setVisibility(View.GONE);
        paymentres = (MTextView) v.findViewById(R.id.paymentres);
        paymentres.setVisibility(View.GONE);
        submittext = (MTextView) v.findViewById(R.id.submitaddtext);
        submittext.setText("Pay Amount & Submit Ad");
        primiumone = (CheckBox) v.findViewById(R.id.primiumone);
        primiumtwo = (CheckBox) v.findViewById(R.id.primiumtwo);
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Light.ttf");
        primiumone.setTypeface(face);
        primiumtwo.setTypeface(face);
        // pd = new ProgressDialog(getActivity());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_post_video_add_two, container, false);


        MobileAds.initialize(com.classifides.app.application.Classified.getInstance().getApplicationContext(), "ca-app-pub-2758807869387921~3087322491");

        try {
            final AdView mAdView = (AdView) v.findViewById(R.id.adView_post_video);
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            mAdView.loadAd(adRequest);


            final Runnable adLoader = new Runnable() {
                @Override
                public void run() {
                    mAdView.loadAd(new AdRequest.Builder().build());
                }
            };


            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    Log.e("onAdClosed", "onAdClosed");
                }

                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    Log.e("onAdFailedToLoad", "onAdFailedToLoad");

                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            try {
                                getActivity().runOnUiThread(adLoader);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 3000);
                }


                @Override
                public void onAdLeftApplication() {
                    super.onAdLeftApplication();
                    Log.e("onAdLeftApplication", "onAdLeftApplication");
                }

                @Override
                public void onAdOpened() {
                    super.onAdOpened();
                    Log.e("onAdOpened", "onAdOpened");
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    Log.e("onAdLoaded", "onAdLoaded");
                }
            });

            adLoader.run();
        } catch (Exception e) {
            e.printStackTrace();
        }



        ctx = getActivity();


        try {
            adid = getArguments().getString("AD_ID");
            getAdInfo(adid);


        } catch (Exception e) {

        }


        try {
            //dialog.dismiss();

            if (adid == null)// TextUtils.isEmpty(adid)
                e = 0;
            else {

                e = 1;
            }
        } catch (Exception e) {

        }
        initView(v);
        addListeners();

        return v;
    }


    private void getAdInfo(final String uid) {

        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Object doInBackground(Object[] params) {


                Map<String, String> map = new HashMap<String, String>();
                map.put("aid", uid);
                rest = DAO.getJsonFromUrl(Config.getAdInfo, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {

                    if (rest.getInt("success") > 0) {

                        JSONArray jsonArray = rest.getJSONArray("advertis");
                        //Log.e("JsONArray", jsonArray.toString());
                        JSONObject jobj = jsonArray.getJSONObject(0);

                        //    Log.e("json Data",jsonArray.getString("title"))

                        defaultloc.setVisibility(View.VISIBLE);
                        address.setText(jobj.getString("nearby"));
                        nearbyname = jobj.getString("nearby");
                        placeid = jobj.getString("placeid");
                        cityname = jobj.getString("city");

                        adddefault.setVisibility(View.GONE);


                        //     title.setText();


                    } else {
                       /* adaptor = new MAdaptor();
                        adaptor.setData(new JSONArray());
                        prod.setAdapter(adaptor);*/
                        Toast.makeText(getActivity(), rest.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);

    }

    private void addListeners() {

        primiumone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    amt = amt + 2500;

                    views = views + 5000;
                } else {
                    amt = amt - 2500;
                    views = views - 5000;
                }
                totalAmout();
            }
        });
        primiumtwo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    amt = amt + 5000;
                    views = views + 10000;
                } else {
                    amt = amt - 5000;
                    views = views - 10000;
                }
                totalAmout();
            }
        });


        adddefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), LocationMap.class), 101);
            }
        });
        otherLocbtnOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), LocationMap.class), 102);
            }
        });

        otherLocbtnTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), LocationMap.class), 103);
            }
        });

        otherLocRemoveOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otherLocOne.setVisibility(View.GONE);
                otherLocbtnOne.setVisibility(View.VISIBLE);
                amt = amt - 200;
                totalAmout();
               /* if (otherLocbtnTwo.getVisibility() == View.VISIBLE) {
                    paymentres.setVisibility(View.GONE);
                    submittext.setText("Submit Ad");
                } else {
                    submittext.setText("Pay Amount & Submit Ad");
                    paymentres.setText("Total Amount: Rs 200/- for adding 1 extra location for your advertisement.");
                }*/
            }
        });
        otherLocRemoveTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otherLocTwo.setVisibility(View.GONE);
                otherLocbtnTwo.setVisibility(View.VISIBLE);
                amt = amt - 200;
                totalAmout();
               /* if (otherLocbtnOne.getVisibility() == View.VISIBLE) {
                    paymentres.setVisibility(View.GONE);
                    submittext.setText("Submit Ad");
                } else {
                    submittext.setText("Pay Amount & Submit Ad");
                    paymentres.setText("Total Amount: Rs 200/- for adding 1 extra location for your advertisement.");
                }*/
            }
        });


        submitAdd.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {

                                             data.put("price", "0");
                                             data.put("imagename", fileName);
                                             data.put("imgcount", 1 + "");
                                             result.setVisibility(View.GONE);

                                             if (TextUtils.isEmpty(address.getText().toString())) {

                                             } else if (TextUtils.isEmpty(cityname)) {
                                                 result.setText("please select proper location");
                                                 result.setVisibility(View.VISIBLE);
                                             } else if (TextUtils.isEmpty(placeid)) {
                                                 result.setText("please select proper location");
                                                 result.setVisibility(View.VISIBLE);
                                             } else if (TextUtils.isEmpty(nearbyname)) {
                                                 result.setText("please select proper location");
                                                 result.setVisibility(View.VISIBLE);
                                             } /*else if (primiumone.isChecked() == false && primiumtwo.isChecked() == false) {
                                                 result.setText("please select primium plan");
                                                 result.setVisibility(View.VISIBLE);
                                             }*/ else if (adid == null) {//TextUtils.isEmpty(adid)
                                             /*    if (primiumone.isChecked() == false && primiumtwo.isChecked() == false) {
                                                     result.setText("please select primium plan");
                                                     result.setVisibility(View.VISIBLE);
                                                 } else {*/
                                                     data.put("default_view", views + "");
                                                     data.put("city", cityname);
                                                     data.put("placeid", placeid);
                                                     data.put("nearby", nearbyname);
                                                     data.put("add_id", adid);
                                                     data.put("type", 1 + "");
                                                     data.put("amt", amt + "");

                                                     JSONObject jobj1 = new JSONObject(data);
                                                     Bundle b = new Bundle();
                                                     PaymentFragment obj = new PaymentFragment();
                                                     b.putString("submit_ad", jobj1.toString());
                                                     b.putString("p", "3");
                                                     b.putString("e", e + "");
                                                     obj.setArguments(b);
                                                     obj.setVideoPath(videoPath);
                                                     getActivity().getSupportFragmentManager().beginTransaction()
                                                             .add(R.id.container, obj, "pf1").addToBackStack("pf1").commit();
                                                     //    postNext();
                                                // }
                                             } else {
                                                 postNext();
                                             }

                                         }
                                     }

        );


    }

    public void totalAmout() {

        paymentres.setVisibility(View.VISIBLE);
        paymentres.setText("Total Amount: Rs " + amt + "/-.");
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {

            switch (requestCode) {
                case 101:
                    defaultloc.setVisibility(View.VISIBLE);
                    address.setText(data.getStringExtra("address"));
                    cityname = data.getStringExtra("locality");
                    placeid = data.getStringExtra("placeid");
                    nearbyname = data.getStringExtra("short");
                    amt = amt + 200;
                    totalAmout();
                    break;
                case 102:
                    otherLocOne.setVisibility(View.VISIBLE);
                    otherLocbtnOne.setVisibility(View.GONE);
                    paymentres.setVisibility(View.VISIBLE);
                   /* if (otherLocbtnTwo.getVisibility() == View.GONE) {
                        paymentres.setText("Total Amount: Rs 400/- for adding 2 extra location for your advertisement.");

                    } else {
                        paymentres.setText("Total Amount: Rs 200/- for adding 1 extra location for your advertisement.");
                    }*/
                    amt = amt + 200;
                    totalAmout();
                    addressone.setText(data.getStringExtra("address"));
                    break;
                case 103:
                    otherLocTwo.setVisibility(View.VISIBLE);
                    otherLocbtnTwo.setVisibility(View.GONE);
                    paymentres.setVisibility(View.VISIBLE);
                  /*  if (otherLocbtnOne.getVisibility() == View.GONE) {
                        paymentres.setText("Total Amount: Rs 400/- for adding 2 extra location for your advertisement.");
                    } else {
                        paymentres.setText("Total Amount: Rs 200/- for adding 1 extra location for your advertisement.");
                    }*/
                    amt = amt + 200;
                    totalAmout();
                    addresstwo.setText(data.getStringExtra("address"));
                    break;
            }


        }

    }


    private void postNext() {
        new AsyncTask<String, Integer, String>() {
            String rest = "";
            private String boundary;
            private static final String LINE_FEED = "\r\n";
            private HttpURLConnection httpConn;
            private String charset = "UTF-8";
            private OutputStream outputStream;
            private PrintWriter writer;


            @Override
            protected String doInBackground(String[] params) {

                try {
                    File uploadFile = new File(videoPath);

                    String requestURL = Config.Server + "upload.php";
                    // String requestURL = " http://192.168.1.108:8086/up/upload.php";


                    // creates a unique boundary based on time stamp
                    boundary = "===" + System.currentTimeMillis() + "===";
                    URL url = new URL(requestURL);
                    httpConn = (HttpURLConnection) url.openConnection();
                    httpConn.setRequestProperty("Connection", "keep-alive");
                    httpConn.setRequestMethod("POST");
                    httpConn.setRequestProperty("Accept-Encoding", "identity");
                    httpConn.setRequestProperty("Content-Type",
                            "multipart/form-data; boundary=" + boundary);
                    httpConn.setRequestProperty("User-Agent", "Etkin Agent");
                    httpConn.setRequestProperty("Test", "Etkin");


                    //httpConn.setUseCaches(false);
                    httpConn.setDoOutput(true);    // indicates POST method
                    //
                    httpConn.setDoInput(true);

                    String data = "--" + boundary + LINE_FEED + "Content-Disposition: form-data; name=\"" + "file" + "\"; filename=\"" + fileName + "\"" + LINE_FEED + "Content-Type: " + URLConnection.guessContentTypeFromName(fileName) + LINE_FEED + "Content-Transfer-Encoding: binary" + LINE_FEED + LINE_FEED;
                    String data2 = LINE_FEED + LINE_FEED + "--" + boundary + "--" + LINE_FEED;
                    int len = data.getBytes().length + (int) uploadFile.length() + data2.getBytes().length;

                    httpConn.setFixedLengthStreamingMode(len);
                    outputStream = httpConn.getOutputStream();
                    writer = new PrintWriter(new OutputStreamWriter(outputStream, charset), true);


                    writer.append("--" + boundary).append(LINE_FEED);
                    writer.append("Content-Disposition: form-data; name=\"" + "file" + "\"; filename=\"" + fileName + "\"")
                            .append(LINE_FEED);
                    writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(fileName))
                            .append(LINE_FEED);
                    writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
                    writer.append(LINE_FEED);
                    writer.flush();

                    publishProgress(0, (int) uploadFile.length() / 1024);
                    FileInputStream inputStream = new FileInputStream(uploadFile);
                    byte[] buffer = new byte[4096];
                    int bytesRead = -1;
                    int s = 0;
                    // BufferedWriter writer=new BufferedWriter(new PrintWriter(outputStream));
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                        s = s + (int) bytesRead / 1024;
                        publishProgress(s);
                                /*if (s > 8000)
                                    break;*/
                    }
                    outputStream.flush();
                    inputStream.close();

                    writer.append(LINE_FEED);
                    writer.flush();


                    writer.append(LINE_FEED).flush();
                    writer.append("--" + boundary + "--").append(LINE_FEED);
                    writer.close();

                    List<String> response = new ArrayList<String>();
                    // checks server's status code first
                    int status = httpConn.getResponseCode();
                    if (status == HttpURLConnection.HTTP_OK) {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(
                                httpConn.getInputStream()));
                        String line = null;
                        while ((line = reader.readLine()) != null) {
                            response.add(line);
                        }
                        reader.close();
                        httpConn.disconnect();
                    } else {
                        throw new IOException("Server returned non-OK status: " + status);
                    }





                           /* MultipartUtility mp=new MultipartUtility(requestURL,charset);
                            mp.addFilePart("file",img,this);
                            List<String> res=mp.finish();*/

                    for (String r : response) {
                        rest += "\n" + r;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return rest;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    //dialog.dismiss();


                    //Log.e("In Video Post","In Video Post update");
                    updatePost(adid);
                    // }
                } catch (Exception e) {

                }
                //   Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                if (values.length == 2) {
                    dialog = new ProgressDialog(getActivity());
                    dialog.setTitle("Uploading Video Add");
                    dialog.setCancelable(false);
                    dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    dialog.setMessage("Please wait while we uploading your add!");
                    dialog.setMax(values[1]);
                    dialog.setButton(DialogInterface.BUTTON_POSITIVE, "cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                        }
                    });
                    dialog.show();


                } else
                    dialog.setProgress(values[0]);

            }
        }.execute(null, null, null);
    }


    private void submiPost() {
        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ProgressDialog();
                data.put("city", cityname);
                data.put("placeid", placeid);
                data.put("nearby", nearbyname);
                data.put("website", "");
                data.put("type", 1 + "");
                try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected Object doInBackground(Object[] params) {
                res = DAO.getJsonFromUrl(Config.Server + "addadvertise.php", data);
                try {
                    if (res.getInt("success") > 0) {
                       /* int j = 0;
                        for (ByteArrayOutputStream s : streams) {
                            byte[] image = s.toByteArray();
                            Map<String, String> d = new HashMap<String, String>();
                            d.put("filename", data.get("imagename") + j + ".jpg");
                            d.put("image", Base64.encodeToString(image, 0));
                            DAO.getJsonFromUrl(Config.Server + "uploadimage.php", d);
                            j++;
                        }*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                DismissProgress();
                try {
                    if (res.getInt("success") > 0) {
                        //     Toast.makeText(getActivity(), res.getString("message"), Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    } else {
                        //   Toast.makeText(getActivity(), res.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }


    private void updatePost(final String add_id) {
        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ProgressDialog();
                data.put("city", cityname);
                data.put("placeid", placeid);
                data.put("nearby", nearbyname);
                data.put("add_id", add_id);
                data.put("type", 1 + "");
                try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected Object doInBackground(Object[] params) {
                res = DAO.getJsonFromUrl(Config.updateAd, data);
                try {
                    if (res.getInt("success") > 0) {
                       /* int j = 0;
                        for (ByteArrayOutputStream s : streams) {
                            byte[] image = s.toByteArray();
                            Map<String, String> d = new HashMap<String, String>();
                            d.put("filename", data.get("imagename") + j + ".jpg");
                            d.put("image", Base64.encodeToString(image, 0));
                            DAO.getJsonFromUrl(Config.Server + "uploadimage.php", d);
                            j++;
                        }*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                DismissProgress();
                try {
                    if (res.getInt("success") > 0) {
                        //   Toast.makeText(getActivity(), res.getString("message"), Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    } else {
                        //  Toast.makeText(getActivity(), res.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }


    public void setData(Map<String, String> data) {
        this.data = data;
    }

    public void ProgressDialog() {
        dialog.setMessage("While we posting your add.");
        dialog.setTitle("Please wait....");
     /*   dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);*/
        //pd.show();
    }

    public void DismissProgress() {
        dialog.dismiss();
    }


    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        String cdate = df.format(c.getTime());
        fileName = "classified" + cdate + getFileExt(new File(videoPath).getName());

    }

    public String getFileExt(String fileName) {
        return fileName.substring(fileName.lastIndexOf("."), fileName.length());
    }


}
