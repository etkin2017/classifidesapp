package com.classifides.app.fragment;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.classifides.app.Adaptor.SortBasedOnView;
import com.classifides.app.AddDetail;
import com.classifides.app.R;
import com.classifides.app.application.*;
import com.classifides.app.application.Classified;
import com.classifides.app.data.ComplexPreferences;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.model.locations;
import com.classifides.app.view.MTextView;
import com.squareup.picasso.Picasso;
import com.universalvideoview.UniversalMediaController;
import com.universalvideoview.UniversalVideoView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdsBySubCategory extends Fragment {
    RecyclerView prod;
    View v;
    String value;
    String TAG = "VideoView";

    boolean d;
    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();

    // We can be in one of these 3 states
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;

    // Remember some things for zooming
    PointF start = new PointF();
    PointF mid = new PointF();

    JSONArray jarr, j2;

    MAdaptor adaptor;
    float oldDist = 1f;


    public AdsBySubCategory() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_ads_by_sub_category, container, false);
        jarr = new JSONArray();
        j2 = new JSONArray();
        value = getArguments().getString("sub_cat").trim();
        //Log.e("valuesssssss", value);

//        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(value);
        adaptor = new MAdaptor();
        prod = (RecyclerView) v.findViewById(R.id.postrecyc);
        prod.setLayoutManager(new LinearLayoutManager(getActivity()));
        SharedPreferences sh = getActivity().getSharedPreferences("saved_ad", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sh.edit();
        //    editor.putString("sads", rest.getJSONArray("advertis").toString());

        String saved_ads = sh.getString("sads", "");
        //   if(strJson != null) JSONObject jsonData = new JSONObject(saved_ads);
        try {
            JSONArray array = new JSONArray(saved_ads);
            int len = array.length();

            for (int a = 0; a < len; a++) {

                String strrr = array.getJSONObject(a).getString("subcategory").trim();
                //Log.e("strrr", strrr);

                if ((strrr).equals(value)) {
                    //Log.e("subcategory4444", array.getJSONObject(a).getString("subcategory").trim());

                    //Log.e("arrayget", array.get(a).toString());
                    jarr.put(array.get(a));
                } else {

                    //Log.e("subcategory11", array.getJSONObject(a).getString("subcategory").trim());

                }
            }


            j2 = getSortedList(jarr);
            adaptor.setData(j2);
            prod.setAdapter(adaptor);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return v;
    }


    public static JSONArray getSortedList(JSONArray array) throws JSONException {
        List<JSONObject> list = new ArrayList<JSONObject>();
        for (int i = 0; i < array.length(); i++) {
            list.add(array.getJSONObject(i));
        }
        Collections.sort(list, new SortBasedOnView());
        JSONArray resultArray = new JSONArray(list);

        return resultArray;
    }

    @Override
    public void onResume() {
        super.onResume();
        //  getProducts(value);
    }

    private void getProducts(final String subcat) {

        new AsyncTask() {
            JSONObject rest = null;
            String city = "";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected Object doInBackground(Object[] params) {
                String filter = "";

                Map<String, String> map = new HashMap<String, String>();
                map.put("sub_cat", subcat);

                rest = DAO.getJsonFromUrl(Config.adBySubCategory, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    MAdaptor adaptor = new MAdaptor();
                    if (rest.getInt("success") > 0) {

                        adaptor.setData(rest.getJSONArray("advertis"));
                        prod.setAdapter(adaptor);

                    } else {
                        adaptor = new MAdaptor();
                        adaptor.setData(new JSONArray());
                        prod.setAdapter(adaptor);

                        Toast.makeText(getActivity(), rest.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

 /*   public void clearAnimation() {
        prod.clearAnimation();
    }*/

    private class MAdaptor extends RecyclerView.Adapter<MAdaptor.ViewHolder> implements View.OnTouchListener {

        private JSONArray data;
        private int lastPosition = -1;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_prod, null);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            try {
                //    holder. video_thumb.(setCallback(EasyVideoCallback));

             /*   setAnimation(holder.itemView, position);
            */


                if ((data.getJSONObject(position).getString("subcategory").trim()).equals(value)) {

                    //    Collections.sort();

                    holder.main_title.setText(data.getJSONObject(position).getString("title"));
                    holder.title.setText(data.getJSONObject(position).getString("title"));
                    holder.desc.setText(data.getJSONObject(position).getString("description"));
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

                    Date d = format.parse(data.getJSONObject(position).getString("postTime"));
                    android.text.format.DateFormat df = new android.text.format.DateFormat();

                    holder.dated.setText("Date: " + df.format("dd-MM-yyyy", d));
                    holder.cost.setText("Rs " + data.getJSONObject(position).getString("price") + "/-");
                    holder.cat.setText("Cat: " + data.getJSONObject(position).getString("category"));
                    holder.subcat.setText("Subcat: " + data.getJSONObject(position).getString("subcategory"));
                    holder.post_view.setText("Views: " + data.getJSONObject(position).getString("v"));
                    holder.main_v.setText("Views: " + data.getJSONObject(position).getString("v"));

                    holder.aID.setText(data.getJSONObject(position).getString("add_id"));

                    holder.ttyy.setText(data.getJSONObject(position).getString("imageLoc"));
                    final int type = data.getJSONObject(position).getInt("type");
                    //  holder.video_thumb.setCallback(EasyVideoProgressCallback);


                    if (type == 1) {
                        // setImageFromVideo(holder.video_thumb, data.getJSONObject(position).getString("imageLoc"));
                        //  Picasso.with(Classified.getInstance()).load(R.drawable.play).into(holder.postimg);


                 /*   holder.video_thumb.setSource(Uri.parse(data.getJSONObject(position).getString("imageLoc")));
                    holder.video_thumb.setClickable(true);
                    holder.video_thumb.pause();*/
               /*         //  holder.video_thumb.seekTo(1000);
                    holder.video_thumb.setAutoFullscreen(true);
*/

                        holder.mVideoView.setVideoPath(data.getJSONObject(position).getString("imageLoc"));
                        holder.mVideoView.seekTo(15);
                        holder.mVideoView.stopPlayback();
                        holder.mVideoView.pause();


                        holder.video_thumb.setVisibility(View.VISIBLE);
                        holder.bannerimg.setVisibility(View.GONE);
                        holder.bar_desc.setVisibility(View.GONE);
                        holder.main_title.setVisibility(View.VISIBLE);
                        holder.main_bar.setVisibility(View.VISIBLE);
                        holder.bar_v.setVisibility(View.VISIBLE);

                        holder.postimg.setVisibility(View.GONE);
                    } else if (type == 2) {
                        holder.bannerimg.setVisibility(View.VISIBLE);
                        holder.postimg.setVisibility(View.GONE);
                        holder.video_thumb.setVisibility(View.GONE);
                        holder.bar_desc.setVisibility(View.GONE);
                        holder.main_title.setVisibility(View.VISIBLE);
                        holder.main_bar.setVisibility(View.VISIBLE);
                        holder.bar_v.setVisibility(View.VISIBLE);
                        //Log.e("Type 2", data.getJSONObject(position).getString("imageLoc"));
                        Picasso.with(com.classifides.app.application.Classified.getInstance()).load(data.getJSONObject(position).getString("imageLoc")).placeholder(R.drawable.classifieds).into(holder.bannerimg);


                    } else {
                        holder.bannerimg.setVisibility(View.GONE);
                        holder.video_thumb.setVisibility(View.GONE);
                        holder.bar_desc.setVisibility(View.VISIBLE);
                        holder.main_title.setVisibility(View.GONE);
                        holder.main_bar.setVisibility(View.GONE);
                        holder.bar_v.setVisibility(View.GONE);
                        holder.postimg.setVisibility(View.VISIBLE);
                        Picasso.with(com.classifides.app.application.Classified.getInstance()).load(data.getJSONObject(position).getString("imageLoc") + "0.jpg").placeholder(R.drawable.classifieds).into(holder.postimg);
                    }


                    holder.mVideoView.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View view, MotionEvent motionEvent) {

                            //Log.e("TOuchClick", "TOuchClick");
                            return false;
                        }
                    });

                    holder.video_thumb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //Log.e("mMediaCont_listener", "mMediaCont_listener ERROR");

                            String aid = holder.aID.getText().toString();
                           /* Intent intent = new Intent(getActivity(), AdVideoViewActivity.class);
                            intent.putExtra("aid", aid);
                            intent.putExtra("video_url", holder.ttyy.getText().toString());
                            getActivity().startActivity(intent);*/

                            Dialog ddd = new Dialog(getActivity());
                            ddd.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            ddd.setContentView(R.layout.banner_view);
                            //   mVideoView = (UniversalVideoView) ddd.findViewById(R.id.videoView);


                            UniversalVideoView mVideoView = (UniversalVideoView) ddd.findViewById(R.id.videoView);
                            UniversalMediaController mMediaController = (UniversalMediaController) ddd.findViewById(R.id.media_controller);
                            mMediaController.setOnLoadingView(R.layout.view_for_loading);
                            mVideoView.setMediaController(mMediaController);
                            FrameLayout main_con = (FrameLayout) ddd.findViewById(R.id.video_thumb);
                            LinearLayout ll_baner = (LinearLayout) ddd.findViewById(R.id.ll_baner);
                            ll_baner.setVisibility(View.VISIBLE);
                            main_con.setVisibility(View.VISIBLE);
                            ImageView b = (ImageView) ddd.findViewById(R.id.banner_view);

                            b.setVisibility(View.GONE);
                     /*   WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = ddd.getWindow();
                        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                        window.setAttributes(lp);*/
                            mVideoView.setVideoPath(holder.ttyy.getText().toString());


                            updateView(aid);
                            notifyDataSetChanged();

                            //   Picasso.with(Classified.getInstance()).load(data.getJSONObject(position).getString("imageLoc")).into(i);
                            ddd.show();

                        }
                    });


                    holder.mMediaController.setOnErrorViewClick(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            TextView v = (TextView) view.findViewById(R.id.tx);

                            //Log.e("ONERRORLISTENER", "LISTENER ERROR");

                            String aid = holder.aID.getText().toString();
                           /* Intent intent = new Intent(getActivity(), AdVideoViewActivity.class);
                            intent.putExtra("aid", aid);
                            intent.putExtra("video_url", holder.ttyy.getText().toString());
                            getActivity().startActivity(intent);*/

                            Dialog ddd = new Dialog(getActivity());
                            ddd.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            ddd.setContentView(R.layout.banner_view);
                            //   mVideoView = (UniversalVideoView) ddd.findViewById(R.id.videoView);


                            UniversalVideoView mVideoView = (UniversalVideoView) ddd.findViewById(R.id.videoView);
                            UniversalMediaController mMediaController = (UniversalMediaController) ddd.findViewById(R.id.media_controller);
                            mMediaController.setOnLoadingView(R.layout.view_for_loading);
                            // mMediaController.setOnLoadingView(R.layout.view_for_loading);

                            mMediaController.setOnErrorView(R.layout.view_for_error);
                            mVideoView.setMediaController(mMediaController);
                            FrameLayout main_con = (FrameLayout) ddd.findViewById(R.id.video_thumb);
                            LinearLayout ll_baner = (LinearLayout) ddd.findViewById(R.id.ll_baner);
                            ll_baner.setVisibility(View.VISIBLE);
                            main_con.setVisibility(View.VISIBLE);
                            ImageView b = (ImageView) ddd.findViewById(R.id.banner_view);

                            b.setVisibility(View.GONE);
                      /*  WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = ddd.getWindow();
                        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                        window.setAttributes(lp);*/
                            mVideoView.setVideoPath(holder.ttyy.getText().toString());
                            mVideoView.seekTo(30);
                            mVideoView.start();
                            mVideoView.setVideoViewCallback(new UniversalVideoView.VideoViewCallback() {
                                @Override
                                public void onScaleChange(boolean isFullscreen) {
                                }

                                @Override
                                public void onPause(MediaPlayer mediaPlayer) { // Video pause
                                    //Log.e("onPause", "onPause UniversalVideoView callback");
                                }

                                @Override
                                public void onStart(MediaPlayer mediaPlayer) { // Video start/resume to play
                                    //Log.e("onStart", "onStart UniversalVideoView callback");
                       /* String aid = holder.aID.getText().toString();
                        updateView(aid);*/


                                }

                                @Override
                                public void onBufferingStart(MediaPlayer mediaPlayer) {// steam start loading
                                    //Log.e("onBufferingStart", "onBufferingStart UniversalVideoView callback");

                                }

                                @Override
                                public void onBufferingEnd(MediaPlayer mediaPlayer) {// steam end loading
                                    //Log.e("onBufferingEnd", "onBufferingEnd UniversalVideoView callback");

                                }

                            });


                            updateView(aid);
                            notifyDataSetChanged();

                            //   Picasso.with(Classified.getInstance()).load(data.getJSONObject(position).getString("imageLoc")).into(i);
                            ddd.show();

                        }
                    });

                    holder.itemView.setTag(data.getJSONObject(position));


                    holder.mVideoView.setVideoViewCallback(new UniversalVideoView.VideoViewCallback() {
                        @Override
                        public void onScaleChange(boolean isFullscreen) {
                        }

                        @Override
                        public void onPause(MediaPlayer mediaPlayer) { // Video pause
                            //Log.e("onPause", "onPause UniversalVideoView callback");
                        }

                        @Override
                        public void onStart(MediaPlayer mediaPlayer) { // Video start/resume to play
                            //Log.e("onStart", "onStart UniversalVideoView callback");
                       /* String aid = holder.aID.getText().toString();
                        updateView(aid);*/

                            String aid = holder.aID.getText().toString();
                           /* Intent intent = new Intent(getActivity(), AdVideoViewActivity.class);
                            intent.putExtra("aid", aid);
                            intent.putExtra("video_url", holder.ttyy.getText().toString());
                            getActivity().startActivity(intent);*/

                            Dialog ddd = new Dialog(getActivity());
                            ddd.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            ddd.getWindow().setBackgroundDrawable(getResources().getDrawable(R.color.black));
                            ddd.setContentView(R.layout.banner_view);
                            //   mVideoView = (UniversalVideoView) ddd.findViewById(R.id.videoView);


                            UniversalVideoView mVideoView = (UniversalVideoView) ddd.findViewById(R.id.videoView);
                            UniversalMediaController mMediaController = (UniversalMediaController) ddd.findViewById(R.id.media_controller);
                            mMediaController.setOnLoadingView(R.layout.view_for_loading);
                            mMediaController.setOnLoadingView(R.layout.view_for_loading);
                            mVideoView.setMediaController(mMediaController);
                            FrameLayout main_con = (FrameLayout) ddd.findViewById(R.id.video_thumb);
                            LinearLayout ll_baner = (LinearLayout) ddd.findViewById(R.id.ll_baner);
                            ll_baner.setVisibility(View.VISIBLE);

                            main_con.setVisibility(View.VISIBLE);
                            ImageView b = (ImageView) ddd.findViewById(R.id.banner_view);

                            b.setVisibility(View.GONE);
//                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                        Window window = ddd.getWindow();
//                        lp.copyFrom(window.getAttributes());
////This makes the dialog take up the full width
//                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//                        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
//                        window.setAttributes(lp);
                            mVideoView.setVideoPath(holder.ttyy.getText().toString());
                            mVideoView.start();
                            mVideoView.seekTo(15);
                            mVideoView.setVideoViewCallback(new UniversalVideoView.VideoViewCallback() {
                                @Override
                                public void onScaleChange(boolean isFullscreen) {
                                }

                                @Override
                                public void onPause(MediaPlayer mediaPlayer) { // Video pause
                                    //Log.e("onPause", "onPause UniversalVideoView callback");
                                }

                                @Override
                                public void onStart(MediaPlayer mediaPlayer) { // Video start/resume to play
                                    //Log.e("onStart", "onStart UniversalVideoView callback");
                       /* String aid = holder.aID.getText().toString();
                        updateView(aid);*/


                                }

                                @Override
                                public void onBufferingStart(MediaPlayer mediaPlayer) {// steam start loading
                                    //Log.e("onBufferingStart", "onBufferingStart UniversalVideoView callback");

                                }

                                @Override
                                public void onBufferingEnd(MediaPlayer mediaPlayer) {// steam end loading
                                    //Log.e("onBufferingEnd", "onBufferingEnd UniversalVideoView callback");

                                }

                            });


                            updateView(aid);
                            notifyDataSetChanged();

                            //   Picasso.with(Classified.getInstance()).load(data.getJSONObject(position).getString("imageLoc")).into(i);
                            ddd.show();


                        }

                        @Override
                        public void onBufferingStart(MediaPlayer mediaPlayer) {// steam start loading
                            //Log.e("onBufferingStart", "onBufferingStart UniversalVideoView callback");

                        }

                        @Override
                        public void onBufferingEnd(MediaPlayer mediaPlayer) {// steam end loading
                            //Log.e("onBufferingEnd", "onBufferingEnd UniversalVideoView callback");

                        }

                    });


                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            if (type == 1) {
                                String aid = holder.aID.getText().toString();
                           /* Intent intent = new Intent(getActivity(), AdVideoViewActivity.class);
                            intent.putExtra("aid", aid);
                            intent.putExtra("video_url", holder.ttyy.getText().toString());
                            getActivity().startActivity(intent);*/

                                Dialog ddd = new Dialog(getActivity());
                                ddd.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                ddd.getWindow().setBackgroundDrawable(getResources().getDrawable(R.color.black));
                                ddd.setContentView(R.layout.banner_view);
                                //   mVideoView = (UniversalVideoView) ddd.findViewById(R.id.videoView);


                                UniversalVideoView mVideoView = (UniversalVideoView) ddd.findViewById(R.id.videoView);
                                UniversalMediaController mMediaController = (UniversalMediaController) ddd.findViewById(R.id.media_controller);
                                mMediaController.setOnLoadingView(R.layout.view_for_loading);
                                mVideoView.setMediaController(mMediaController);
                                FrameLayout main_con = (FrameLayout) ddd.findViewById(R.id.video_thumb);
                                LinearLayout ll_baner = (LinearLayout) ddd.findViewById(R.id.ll_baner);
                                ll_baner.setVisibility(View.VISIBLE);

                                main_con.setVisibility(View.VISIBLE);
                                ImageView b = (ImageView) ddd.findViewById(R.id.banner_view);

                                b.setVisibility(View.GONE);
                         /*   WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            Window window = ddd.getWindow();
                            lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
                            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                            window.setAttributes(lp);*/
                                mVideoView.setVideoPath(holder.ttyy.getText().toString());
                                mVideoView.start();

                                mVideoView.setVideoViewCallback(new UniversalVideoView.VideoViewCallback() {
                                    @Override
                                    public void onScaleChange(boolean isFullscreen) {
                                    }

                                    @Override
                                    public void onPause(MediaPlayer mediaPlayer) { // Video pause
                                        //Log.e("onPause", "onPause UniversalVideoView callback");
                                    }

                                    @Override
                                    public void onStart(MediaPlayer mediaPlayer) { // Video start/resume to play
                                        //Log.e("onStart", "onStart UniversalVideoView callback");
                       /* String aid = holder.aID.getText().toString();
                        updateView(aid);*/


                                    }

                                    @Override
                                    public void onBufferingStart(MediaPlayer mediaPlayer) {// steam start loading
                                        //Log.e("onBufferingStart", "onBufferingStart UniversalVideoView callback");

                                    }

                                    @Override
                                    public void onBufferingEnd(MediaPlayer mediaPlayer) {// steam end loading
                                        //Log.e("onBufferingEnd", "onBufferingEnd UniversalVideoView callback");

                                    }

                                });


                                updateView(aid);
                                notifyDataSetChanged();

                                //   Picasso.with(Classified.getInstance()).load(data.getJSONObject(position).getString("imageLoc")).into(i);
                                ddd.show();
                            } else if (type == 2) {

                                String aid = holder.aID.getText().toString();
                                Dialog ddd = new Dialog(getActivity(), R.style.mydialogstyle);
                                ddd.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                ddd.setContentView(R.layout.banner_view);
                                ImageView i = (ImageView) ddd.findViewById(R.id.banner_view);

                                UniversalVideoView mVideoView = (UniversalVideoView) ddd.findViewById(R.id.videoView);
                                UniversalMediaController mMediaController = (UniversalMediaController) ddd.findViewById(R.id.media_controller);
                                mVideoView.setMediaController(mMediaController);
                                FrameLayout main_con = (FrameLayout) ddd.findViewById(R.id.video_thumb);
                                main_con.setVisibility(View.GONE);
                                //   iii.setVisibility(View.GONE);
                                LinearLayout ll_baner = (LinearLayout) ddd.findViewById(R.id.ll_baner);
                                ll_baner.setVisibility(View.GONE);

                          /*  WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            Window window = ddd.getWindow();
                            lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
                            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                            window.setAttributes(lp);*/
                                updateView(aid);


                                notifyDataSetChanged();

                                Picasso.with(Classified.getInstance()).load(holder.ttyy.getText().toString()).placeholder(R.mipmap.logo).into(i);
                                i.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                i.setOnTouchListener(MAdaptor.this);
                                ddd.show();
                            } else {
                                JSONObject obj = (JSONObject) v.getTag();
                                Intent in = new Intent(getActivity(), AddDetail.class);
                                in.putExtra("add", obj.toString());
                                startActivity(in);
                            }
                            notifyDataSetChanged();
                        }
                    });
                } else {
                    Toast.makeText(getActivity(), "No Ads", Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }


        public void updateView(final String aid) {

            final Map<String, String> d = new HashMap<String, String>();

            new AsyncTask() {
                JSONObject res = null;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();

                    int id = 0;
                    d.put("add_id", aid);
                    //    data.put("uid",uid);

                    try {
                        JSONObject user = DAO.getUser();
                        d.put("id", user.getInt("id") + "");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                protected Object doInBackground(Object[] params) {
                    res = DAO.getJsonFromUrl(Config.updateView, d);

                    return null;
                }

                @Override
                protected void onPostExecute(Object o) {
                    super.onPostExecute(o);
                    //DismissProgress();
                    try {
                        if (res.getInt("success") > 0) {
                            //  Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_SHORT).show();
                            //  getActivity().finish();
                        } else {
                            //  Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }.execute(null, null, null);
        }

        public void onBack() {
            notifyDataSetChanged();
        }

        public void forwardSong(UniversalVideoView mPlayer) {
            if (mPlayer != null) {
                int currentPosition = mPlayer.getCurrentPosition();
                if (currentPosition + 10 <= mPlayer.getDuration()) {
                    mPlayer.seekTo(currentPosition + 10);
                } else {
                    mPlayer.seekTo(mPlayer.getDuration());
                }
            }
        }

        private void setImageFromVideo(final JCVideoPlayerStandard postimg, final String imageLoc) {
            new AsyncTask<Bitmap, Integer, Bitmap>() {
                @Override
                protected Bitmap doInBackground(Bitmap... params) {


                   /* FFmpegMediaMetadataRetriever mmr = new FFmpegMediaMetadataRetriever();
                    mmr.setDataSource(imageLoc);
                    mmr.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_ALBUM);
                    mmr.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_ARTIST);
                    Bitmap b = mmr.getFrameAtTime(1000000, FFmpegMediaMetadataRetriever.OPTION_CLOSEST); // frame at 2 seconds
                    // byte[] artwork = mmr.getEmbeddedPicture();

                    mmr.release();
                    return b;*/
                    return null;
                }

                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    super.onPostExecute(bitmap);
                    postimg.thumbImageView.setImageBitmap(bitmap);
                    //   setImageBitmap(bitmap);
                }
            }.execute(null, null, null);
        }

        @Override
        public int getItemCount() {
            return data.length();
        }

        public void setData(JSONArray data) {
            this.data = data;
            this.notifyDataSetChanged();
        }


        @Override
        public void onViewDetachedFromWindow(ViewHolder holder) {
            super.onViewDetachedFromWindow(holder);
          /*  clearAnimation();*/
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            ImageView view = (ImageView) v;
            // make the image scalable as a matrix
            view.setScaleType(ImageView.ScaleType.MATRIX);
            float scale;

            // Handle touch events here...
            switch (event.getAction() & MotionEvent.ACTION_MASK) {

                case MotionEvent.ACTION_DOWN: //first finger down only
                    savedMatrix.set(matrix);
                    start.set(event.getX(), event.getY());
                    //Log.d(TAG, "mode=DRAG");
                    mode = DRAG;
                    break;
                case MotionEvent.ACTION_UP: //first finger lifted
                case MotionEvent.ACTION_POINTER_UP: //second finger lifted
                    mode = NONE;
                    //Log.d(TAG, "mode=NONE");
                    break;
                case MotionEvent.ACTION_POINTER_DOWN: //second finger down
                    oldDist = spacing(event); // calculates the distance between two points where user touched.
                    //Log.d(TAG, "oldDist=" + oldDist);
                    // minimal distance between both the fingers
                    if (oldDist > 5f) {
                        savedMatrix.set(matrix);
                        midPoint(mid, event); // sets the mid-point of the straight line between two points where user touched.
                        mode = ZOOM;
                        //Log.d(TAG, "mode=ZOOM");
                    }
                    break;

                case MotionEvent.ACTION_MOVE:
                    if (mode == DRAG) { //movement of first finger
                        matrix.set(savedMatrix);
                        if (view.getLeft() >= -392) {
                            matrix.postTranslate(event.getX() - start.x, event.getY() - start.y);
                        }
                    } else if (mode == ZOOM) { //pinch zooming
                        float newDist = spacing(event);
                        //Log.d(TAG, "newDist=" + newDist);
                        if (newDist > 5f) {
                            matrix.set(savedMatrix);
                            scale = newDist / oldDist; //thinking I need to play around with this value to limit it**
                            matrix.postScale(scale, scale, mid.x, mid.y);
                        }
                    }
                    break;
            }

            // Perform the transformation
            view.setImageMatrix(matrix);

            return true; // indicate event was handled
        }

        private float spacing(MotionEvent event) {
            float x = event.getX(0) - event.getX(1);
            float y = event.getY(0) - event.getY(1);

            float d = (float) Math.sqrt(x * x + y * y);
            return d;
        }

        private void midPoint(PointF point, MotionEvent event) {
            float x = event.getX(0) + event.getX(1);
            float y = event.getY(0) + event.getY(1);
            point.set(x / 2, y / 2);
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            MTextView title, desc, dated, cost, cat, subcat, main_title, main_v;
            ImageView postimg;

            TextView ttyy, aID;

            LinearLayout bar_desc, bar_v, main_bar;

            FrameLayout video_thumb;
            ImageView bannerimg;
            TextView post_view;
            CardView video_click;

            FrameLayout frame_video_container;
            UniversalVideoView mVideoView;
            UniversalMediaController mMediaController;

            public ViewHolder(View itemView) {
                super(itemView);
                //video_click = (CardView) itemView.findViewById(R.id.video_click);
                ttyy = (TextView) itemView.findViewById(R.id.ttyy);
                video_thumb = (FrameLayout) itemView.findViewById(R.id.video_thumb);
                //      frame_video_container = (FrameLayout) itemView.findViewById(R.id.frame_video_container);
                //  assert video_thumb != null;
                aID = (TextView) itemView.findViewById(R.id.aID);

                mVideoView = (UniversalVideoView) itemView.findViewById(R.id.videoView);
                mMediaController = (UniversalMediaController) itemView.findViewById(R.id.media_controller);
                mMediaController.setOnLoadingView(R.layout.view_for_loading);
                mMediaController.setOnErrorView(R.layout.view_for_error);
                //   mMediaController.showError();
                mVideoView.setMediaController(mMediaController);


                // video_thumb = (VideoView) itemView.findViewById(R.id.video_thumb);
                bannerimg = (ImageView) itemView.findViewById(R.id.bannerimg);
                main_title = (MTextView) itemView.findViewById(R.id.main_title);
                main_v = (MTextView) itemView.findViewById(R.id.main_v);
                bar_desc = (LinearLayout) itemView.findViewById(R.id.desc_bar);
                bar_v = (LinearLayout) itemView.findViewById(R.id.bar_v);
                main_bar = (LinearLayout) itemView.findViewById(R.id.bar_title);
                title = (MTextView) itemView.findViewById(R.id.ptitle);
                desc = (MTextView) itemView.findViewById(R.id.description);
                dated = (MTextView) itemView.findViewById(R.id.dated);
                cost = (MTextView) itemView.findViewById(R.id.cost);
                cat = (MTextView) itemView.findViewById(R.id.cat);
                subcat = (MTextView) itemView.findViewById(R.id.subcat);
                postimg = (ImageView) itemView.findViewById(R.id.prodimg);
                post_view = (TextView) itemView.findViewById(R.id.post_view);
            }
        }

     /*   private void setAnimation(View viewToAnimate, int position) {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition) {
                Animation animation = AnimationUtils.loadAnimation(getActivity(),
                        (position > lastPosition) ? R.anim.up_from_bottom
                                : R.anim.down_from_top);
                viewToAnimate.startAnimation(animation);
                lastPosition = position;

            }
        }*/


    }

}
