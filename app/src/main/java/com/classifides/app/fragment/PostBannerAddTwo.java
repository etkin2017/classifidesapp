package com.classifides.app.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.classifides.app.BrowseByCategory;
import com.classifides.app.LocationMap;
import com.classifides.app.PaymentFragment;
import com.classifides.app.R;
import com.classifides.app.application.*;
import com.classifides.app.application.Classified;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.gcm.model.NewMessage;
import com.classifides.app.view.MTextView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostBannerAddTwo extends Fragment {

    private Map<String, String> data;
    EditText price;
    LinearLayout submitAdd;
    private ProgressDialog dialog;
    public static String encoded_string, imagename;

    MTextView result, paymentres, submittext, address, addressone, addresstwo;

    RelativeLayout otherLocOne, otherLocTwo, defaultloc;
    LinearLayout otherLocbtnOne, otherLocbtnTwo, adddefault;
    MTextView otherLocRemoveOne, otherLocRemoveTwo;
    private String videoPath;
    private String fileName;

    public static String adid;

    private String cityname;
    private String placeid;
    private String nearbyname;
    private ByteArrayOutputStream streams;

    //  private ArrayList<ByteArrayOutputStream> streams;
    private CheckBox primiumone, primiumtwo;
    private int amt = 1200;
    private int views = 0;

    int e;

    public PostBannerAddTwo() {
        // Required empty public constructor
    }

    private void initView(View v) {
        result = (MTextView) v.findViewById(R.id.result);
        address = (MTextView) v.findViewById(R.id.addressloc);
        addressone = (MTextView) v.findViewById(R.id.addresslocone);
        addresstwo = (MTextView) v.findViewById(R.id.addressloctwo);
        price = (EditText) v.findViewById(R.id.et_price);
        submitAdd = (LinearLayout) v.findViewById(R.id.submitadd);
        otherLocOne = (RelativeLayout) v.findViewById(R.id.otherlocone);
        otherLocTwo = (RelativeLayout) v.findViewById(R.id.otherloctwo);
        defaultloc = (RelativeLayout) v.findViewById(R.id.defaultloc);
        adddefault = (LinearLayout) v.findViewById(R.id.add_loc_one);
        otherLocbtnOne = (LinearLayout) v.findViewById(R.id.add_loc);
        otherLocbtnTwo = (LinearLayout) v.findViewById(R.id.add_locone);
        otherLocRemoveOne = (MTextView) v.findViewById(R.id.removeone);
        otherLocRemoveTwo = (MTextView) v.findViewById(R.id.removetwo);
        otherLocOne.setVisibility(View.GONE);
        otherLocTwo.setVisibility(View.GONE);
        defaultloc.setVisibility(View.GONE);
        paymentres = (MTextView) v.findViewById(R.id.paymentres);
        paymentres.setVisibility(View.GONE);
        submittext = (MTextView) v.findViewById(R.id.submitaddtext);
        submittext.setText("Pay Amount & Submit Ad");
        primiumone = (CheckBox) v.findViewById(R.id.primiumone);
        primiumtwo = (CheckBox) v.findViewById(R.id.primiumtwo);
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Light.ttf");
        primiumone.setTypeface(face);
        primiumtwo.setTypeface(face);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_post_banner_add_two, container, false);
        try {
            MobileAds.initialize(Classified.getInstance().getApplicationContext(), "ca-app-pub-2758807869387921~3087322491");

            final AdView mAdView = (AdView) v.findViewById(R.id.adView_post_banner);
            AdRequest adRequest = new AdRequest.Builder()
                    //   .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    //.addTestDevice("CA7416822EE39EB5991049AECDE61839")
                    //.addTestDevice(device_id)
                    .build();
            mAdView.loadAd(adRequest);

            final Runnable adLoader = new Runnable() {
                @Override
                public void run() {
                    mAdView.loadAd(new AdRequest.Builder().build());
                }
            };


            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    Log.e("onAdClosed", "onAdClosed");
                }

                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    Log.e("onAdFailedToLoad", "onAdFailedToLoad");

                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            try {
                                getActivity().runOnUiThread(adLoader);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 3000);
                }


                @Override
                public void onAdLeftApplication() {
                    super.onAdLeftApplication();
                    Log.e("onAdLeftApplication", "onAdLeftApplication");
                }

                @Override
                public void onAdOpened() {
                    super.onAdOpened();
                    Log.e("onAdOpened", "onAdOpened");
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    Log.e("onAdLoaded", "onAdLoaded");
                }
            });

            adLoader.run();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            adid = getArguments().getString("AD_ID");
            getAdInfo(adid);

        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            //dialog.dismiss();

            if ((adid == null))//TextUtils.isEmpty(adid) | adid.equals("") |
                e = 0;
            else {

                e = 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        initView(v);
        addListeners();
        dialog = new ProgressDialog(getActivity());

        return v;
    }


    private void getAdInfo(final String uid) {

        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Object doInBackground(Object[] params) {


                Map<String, String> map = new HashMap<String, String>();
                map.put("aid", uid);
                rest = DAO.getJsonFromUrl(Config.getAdInfo, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {

                    if (rest.getInt("success") > 0) {

                        JSONArray jsonArray = rest.getJSONArray("advertis");
                        //Log.e("JsONArray", jsonArray.toString());
                        JSONObject jobj = jsonArray.getJSONObject(0);

                        //    Log.e("json Data",jsonArray.getString("title"))

                        defaultloc.setVisibility(View.VISIBLE);
                        address.setText(jobj.getString("nearby"));
                        nearbyname = jobj.getString("nearby");
                        placeid = jobj.getString("placeid");
                        cityname = jobj.getString("city");

                        adddefault.setVisibility(View.GONE);


                        //     title.setText();


                    } else {
                       /* adaptor = new MAdaptor();
                        adaptor.setData(new JSONArray());
                        prod.setAdapter(adaptor);*/
                        Toast.makeText(getActivity(), rest.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);

    }

    private void addListeners() {

        primiumone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    amt = amt + 2500;
                    views = views + 5000;
                } else {
                    amt = amt - 2500;
                    views = views - 5000;
                }
                totalAmout();
            }
        });
        primiumtwo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    amt = amt + 5000;

                    views = views + 10000;
                } else {
                    amt = amt - 5000;
                    views = views - 10000;
                }
                totalAmout();
            }
        });


        adddefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), LocationMap.class), 101);
            }
        });
        otherLocbtnOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), LocationMap.class), 102);
            }
        });

        otherLocbtnTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), LocationMap.class), 103);
            }
        });

        otherLocRemoveOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otherLocOne.setVisibility(View.GONE);
                otherLocbtnOne.setVisibility(View.VISIBLE);
                amt = amt - 200;
                totalAmout();
               /* if (otherLocbtnTwo.getVisibility() == View.VISIBLE) {

                    submittext.setText("Submit Ad");
                } else {
                    submittext.setText("Pay Amount & Submit Ad");
                    paymentres.setText("Total Amount: Rs 400/- for adding 1 extra location for your advertisement.");
                }*/


            }
        });
        otherLocRemoveTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otherLocTwo.setVisibility(View.GONE);
                otherLocbtnTwo.setVisibility(View.VISIBLE);
                amt = amt - 200;
                totalAmout();
               /* if (otherLocbtnOne.getVisibility() == View.VISIBLE) {
                    paymentres.setText("Total Amount: Rs 1400/-, Rs 200/- for adding 1 location and Rs 1200/- for 2500 ad view plan by default.");
                    submittext.setText("Pay Amount & Submit Ad");
                } else {
                    submittext.setText("Pay Amount & Submit Ad");
                    paymentres.setText("Total Amount: Rs 1600/-, Rs 400/- for adding 2 location and Rs 1200/- for 2500 ad view plan by default.");
                }*/
            }
        });


        submitAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                data.put("price", "0");
               /* Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
                String cdate = df.format(c.getTime());
                final String imagename = "classified" + cdate;
                data.put("imagename", imagename);*/


                //   data.put("imgcount", streams.size() + "");
                data.put("imgcount", 1 + "");
                result.setVisibility(View.GONE);

                if (TextUtils.isEmpty(address.getText().toString())) {
                    result.setText("please add location");
                    result.setVisibility(View.VISIBLE);
                } else if (TextUtils.isEmpty(cityname)) {
                    result.setText("please select proper location");
                    result.setVisibility(View.VISIBLE);
                } else if (TextUtils.isEmpty(placeid)) {
                    result.setText("please select proper location");
                    result.setVisibility(View.VISIBLE);
                } else if (TextUtils.isEmpty(nearbyname)) {
                    result.setText("please select proper location");
                    result.setVisibility(View.VISIBLE);
                } /* if (primiumone.isChecked()==false && primiumtwo.isChecked()==false) {
                    result.setText("please select primium plan");
                    result.setVisibility(View.VISIBLE);
                }*/ else if ((adid == null)) {//(!TextUtils.isEmpty(adid)) |

/*
                    if (primiumone.isChecked() == false && primiumtwo.isChecked() == false) {
                        result.setText("please select primium plan");
                        result.setVisibility(View.VISIBLE);
                    } else {*/

                    data.put("default_view", views + "");
                    data.put("city", cityname);
                    data.put("placeid", placeid);
                    data.put("nearby", nearbyname);
                    data.put("type", 2 + "");
                    data.put("website", "");
                    data.put("amt", amt + "");
                    data.put("add_id", adid);
                    JSONObject jobj1 = new JSONObject(data);
                    Bundle b = new Bundle();
                    PaymentFragment obj = new PaymentFragment();
                    b.putString("submit_ad", jobj1.toString());
                    b.putString("p", "2");
                    b.putString("e", e + "");
                    obj.setArguments(b);
                    obj.setBannerImages(streams);//Images(streams);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .add(R.id.container, obj, "pf1").addToBackStack("pf1").commit();
                    //   }

                } else {
                    updatePost(adid);


                }
            }
        });


    }

    public void totalAmout() {

        paymentres.setVisibility(View.VISIBLE);
        paymentres.setText("Total Amount: Rs " + amt + "/-.");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {

            switch (requestCode) {
                case 101:
                    defaultloc.setVisibility(View.VISIBLE);
                    address.setText(data.getStringExtra("address"));
                    cityname = data.getStringExtra("locality");
                    placeid = data.getStringExtra("placeid");
                    nearbyname = data.getStringExtra("short");
                    amt = amt + 200;
                    totalAmout();
                    break;
                case 102:
                    otherLocOne.setVisibility(View.VISIBLE);
                    otherLocbtnOne.setVisibility(View.GONE);

                   /* if (otherLocbtnTwo.getVisibility() == View.GONE) {
                        paymentres.setText("Total Amount: Rs 1800/-, Rs 600/- for adding 3 location and Rs 1200/- for 2500 ad view plan by default.");

                    } else {
                        paymentres.setText("Total Amount: Rs 1600/-, Rs 400/- for adding 2 location and Rs 1200/- for 2500 ad view plan by default.");
                    }*/
                    amt = amt + 200;
                    totalAmout();

                    addressone.setText(data.getStringExtra("address"));
                    break;
                case 103:
                    otherLocTwo.setVisibility(View.VISIBLE);
                    otherLocbtnTwo.setVisibility(View.GONE);

                 /*   if (otherLocbtnOne.getVisibility() == View.GONE) {
                        paymentres.setText("Total Amount: Rs 1800/-, Rs 600/- for adding 3 location and Rs 1200/- for 2500 ad view plan by default.");
                    } else {
                        paymentres.setText("Total Amount: Rs 1600/-, Rs 400/- for adding 2 location and Rs 1200/- for 2500 ad view plan by default.");
                    }*/
                    amt = amt + 200;
                    totalAmout();

                    addresstwo.setText(data.getStringExtra("address"));
                    break;
            }


        }

    }


    private class Encode_image extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            // bitmap = BitmapFactory.decodeFile(file_uri.getPath());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            PostBannerAddOne.bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            //   Picasso.with(getApplicationContext()).load(fileUri.getPath()).resize(100,stream);
            byte[] array = stream.toByteArray();
            encoded_string = Base64.encodeToString(array, 0);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            makeRequest();
        }


        private void makeRequest() {
            // RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            //  pd.show();
            StringRequest request = new StringRequest(Request.Method.POST, Config.setImage, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {

                        JSONObject obj = new JSONObject(response);
                        int result = obj.getInt("success");
                        if (result == 1) {

                            Toast.makeText(getActivity(), "Image Uploaded", Toast.LENGTH_LONG).show();


                        } else {


                            Toast.makeText(getActivity(), "Image Not Uploaded", Toast.LENGTH_LONG).show();


                        }

                    } catch (Exception e) {
                        // pd.dismiss();

                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Failed t0 upload Image", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {

                public void onErrorResponse(VolleyError error) {
                    //   pd.dismiss();

                    error.printStackTrace();
                    error.toString();
                    Toast.makeText(getActivity(), "Check Your Internet Connection!", Toast.LENGTH_LONG).show();
                }

            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {


                    HashMap<String, String> map = new HashMap<>();
                    map.put("image_name", imagename);
                    map.put("encoded_string", encoded_string);
                    return map;
                }


            };

            com.classifides.app.application.Classified.getInstance().addToReqQueue(request);
        }

    }

    private void submiPost() {
        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ProgressDialog();
                data.put("city", cityname);
                data.put("placeid", placeid);
                data.put("nearby", nearbyname);
                data.put("type", 2 + "");
                data.put("website", "");
                try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

              /*  ByteArrayOutputStream stream = new ByteArrayOutputStream();
                PostBannerAddOne.bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);

                new Encode_image().execute();*/
            }

            @Override
            protected Object doInBackground(Object[] params) {

                res = DAO.getJsonFromUrl(Config.Server + "addadvertise.php", data);
                try {
                    if (res.getInt("success") > 0) {
                        int j = 0;
                        byte[] image = streams.toByteArray();
                        Map<String, String> d = new HashMap<String, String>();
                        d.put("image_name", data.get("imagename") + "");
                        d.put("encoded_string", Base64.encodeToString(image, 0));
                        DAO.getJsonFromUrl(Config.setImage, d);
                        j++;


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                DismissProgress();
                try {
                    if (res.getInt("success") > 0) {
                        Toast.makeText(getActivity(), res.getString("message"), Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    } else {
                        Toast.makeText(getActivity(), res.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }


    private void updatePost(final String add_id) {
        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ProgressDialog();
                data.put("city", cityname);
                data.put("placeid", placeid);
                data.put("nearby", nearbyname);
                data.put("add_id", add_id);
                data.put("type", 2 + "");
                try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected Object doInBackground(Object[] params) {

                try {
                    res = DAO.getJsonFromUrl(Config.updateAd, data);
                    if (res.getInt("success") > 0) {
                        int j = 0;
                        byte[] image = streams.toByteArray();
                        Map<String, String> d = new HashMap<String, String>();
                        d.put("image_name", data.get("imagename") + "");
                        d.put("encoded_string", Base64.encodeToString(image, 0));
                        DAO.getJsonFromUrl(Config.setImage, d);
                        j++;

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                DismissProgress();
                try {
                    if (res.getInt("success") > 0) {
                        Toast.makeText(getActivity(), res.getString("message"), Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    } else {
                        Toast.makeText(getActivity(), res.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }


    public void setData(Map<String, String> data) {
        this.data = data;
    }

    public void ProgressDialog() {
        dialog.setMessage("While we posting your add.");
        dialog.setTitle("Please wait....");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void DismissProgress() {
        dialog.dismiss();
    }


    public void setImages(ByteArrayOutputStream streams) {
        this.streams = streams;
    }

    public String getFileExt(String fileName) {
        return fileName.substring(fileName.lastIndexOf("."), fileName.length());
    }


}
