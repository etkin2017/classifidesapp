package com.classifides.app.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.classifides.app.ContactUs;
import com.classifides.app.ManageNotification;
import com.classifides.app.MyAdd;
import com.classifides.app.MyReplyToAds;
import com.classifides.app.PostBannerAdd;
import com.classifides.app.PostFreeAdd;
import com.classifides.app.PostVideoAdd;
import com.classifides.app.R;
import com.classifides.app.RedeemCashbackCode;
import com.classifides.app.ReferNEarnActivity;
import com.classifides.app.ReplyToMyAdd;
import com.classifides.app.view.MTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Classified extends Fragment {

    RecyclerView classified;

    public Classified() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_classified, container, false);
        classified = (RecyclerView) v.findViewById(R.id.classified);
        classified.setLayoutManager(new LinearLayoutManager(getActivity()));
        ClassifiedAdaptor mAdaptor = new ClassifiedAdaptor();
        classified.setAdapter(mAdaptor);
        return v;
    }

    private class ClassifiedAdaptor extends RecyclerView.Adapter<ClassifiedAdaptor.ViewHolder> {

        ArrayList<String> menuItems;
        ArrayList<Integer> menuicons;

        ClassifiedAdaptor() {
            menuItems = new ArrayList<String>();
            menuicons = new ArrayList<Integer>();
            getmenuItems();
        }

        private void getmenuItems() {
            //0
            menuItems.add("Post free ad");
            menuicons.add(R.drawable.freeadd);
            //1
            menuItems.add("Post video ad");
            menuicons.add(R.drawable.videoadd);
            //2
            menuItems.add("Post Banner Ad");
            menuicons.add(R.drawable.freeadd);

            //3
            menuItems.add("Manage notification");
            menuicons.add(R.drawable.managenoti);
            //4
            menuItems.add("My advertisement");
            menuicons.add(R.drawable.myadd);
            //5
            menuItems.add("My Reply's To Ad");
            menuicons.add(R.drawable.reply);
            //6
            menuItems.add("Reply's To My Ad");
            menuicons.add(R.drawable.replyother);


            //7
            menuItems.add("Redeem cashback code");
            menuicons.add(R.drawable.redeem);

            //8
            menuItems.add("Refer and Earn");
            menuicons.add(R.drawable.share);

            //9
            menuItems.add("Contact Us");
            menuicons.add(R.drawable.contact);


        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_classified_menu_item, null);
            ViewHolder viewHolder = new ViewHolder(view);

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            holder.menutitle.setText(menuItems.get(position));
            holder.itemView.setClickable(true);
            Picasso.with(com.classifides.app.application.Classified.getInstance()).load(menuicons.get(position)).resize(100, 100).into(holder.menuIcon);
            //holder.menuIcon.setImageDrawable(getActivity().getDrawable(menuicons.get(0)));
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (position == 0) {
                        startActivity(new Intent(getActivity(), PostFreeAdd.class));
                    }
                    if (position == 1) {
                        startActivity(new Intent(getActivity(), PostVideoAdd.class));
                    }
                    if (position == 2) {
                        startActivity(new Intent(getActivity(), PostBannerAdd.class));
                    }
                    if (position == 3) {
                        startActivity(new Intent(getActivity(), ManageNotification.class));
                    }
                    if (position == 4) {
                        startActivity(new Intent(getActivity(), MyAdd.class));
                    }
                    if (position==5) {

                        startActivity(new Intent(getActivity(), MyReplyToAds.class));
                    }
                    if (position==6) {
                        // MyReplyToAds.getProducts();
                        startActivity(new Intent(getActivity(), ReplyToMyAdd.class));

                    }
                    if (position ==7) {
                        startActivity(new Intent(getActivity(), RedeemCashbackCode.class));
                    }
                    if (position==8) {
                        startActivity(new Intent(getActivity(), ReferNEarnActivity.class));

                    }
                    if (position ==9) {
                        startActivity(new Intent(getActivity(), ContactUs.class));
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return menuItems.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            MTextView menutitle;
            ImageView menuIcon;

            public ViewHolder(View itemView) {
                super(itemView);
                menutitle = (MTextView) itemView.findViewById(R.id.menutitle);
                menuIcon = (ImageView) itemView.findViewById(R.id.menuicon);
            }
        }
    }
}
