package com.classifides.app.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.classifides.app.LocationMap;
import com.classifides.app.PaymentFragment;
import com.classifides.app.R;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.view.MTextView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostFreeAddTwo extends Fragment {

    public static String s_sex;

    Context ctx;

    private Map<String, String> data;
    EditText price;
    LinearLayout submitAdd;
    private ProgressDialog pd;
    private ArrayList<ByteArrayOutputStream> streams;
    MTextView result, paymentres, submittext, address, addressone, addresstwo;

    RelativeLayout otherLocOne, otherLocTwo, defaultloc;
    LinearLayout otherLocbtnOne, otherLocbtnTwo, adddefault;
    MTextView otherLocRemoveOne, otherLocRemoveTwo;

    private String cityname;
    private String placeid;
    private String nearbyname;
    private CheckBox primiumone, primiumtwo, primiumthree;

    CheckBox s_male, s_female;

    private int amt = 0;
    private int views = 10000;
    String adid;

    int e;


    public PostFreeAddTwo() {
        // Required empty public constructor
    }

    private void initView(View v) {

        s_male = (CheckBox) v.findViewById(R.id.s_male);
        s_female = (CheckBox) v.findViewById(R.id.s_female);


        result = (MTextView) v.findViewById(R.id.result);
        address = (MTextView) v.findViewById(R.id.addressloc);
        addressone = (MTextView) v.findViewById(R.id.addresslocone);
        addresstwo = (MTextView) v.findViewById(R.id.addressloctwo);
        price = (EditText) v.findViewById(R.id.et_price);
        submitAdd = (LinearLayout) v.findViewById(R.id.submitadd);
        otherLocOne = (RelativeLayout) v.findViewById(R.id.otherlocone);
        otherLocTwo = (RelativeLayout) v.findViewById(R.id.otherloctwo);
        defaultloc = (RelativeLayout) v.findViewById(R.id.defaultloc);
        adddefault = (LinearLayout) v.findViewById(R.id.add_loc_one);
        otherLocbtnOne = (LinearLayout) v.findViewById(R.id.add_loc);
        otherLocbtnTwo = (LinearLayout) v.findViewById(R.id.add_locone);
        otherLocRemoveOne = (MTextView) v.findViewById(R.id.removeone);
        otherLocRemoveTwo = (MTextView) v.findViewById(R.id.removetwo);
        otherLocOne.setVisibility(View.GONE);
        otherLocTwo.setVisibility(View.GONE);
        defaultloc.setVisibility(View.GONE);
        paymentres = (MTextView) v.findViewById(R.id.paymentres);
        paymentres.setVisibility(View.GONE);
        submittext = (MTextView) v.findViewById(R.id.submitaddtext);

        primiumone = (CheckBox) v.findViewById(R.id.primiumone);
        primiumtwo = (CheckBox) v.findViewById(R.id.primiumtwo);
        primiumthree = (CheckBox) v.findViewById(R.id.primiumthree);
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Light.ttf");
        primiumone.setTypeface(face);
        primiumtwo.setTypeface(face);
        primiumthree.setTypeface(face);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_post_free_add_two_old, container, false);
        ctx = getActivity();
        pd = new ProgressDialog(ctx);

        MobileAds.initialize(com.classifides.app.application.Classified.getInstance().getApplicationContext(), "ca-app-pub-2758807869387921~3087322491");

        try {
            final AdView mAdView = (AdView) v.findViewById(R.id.adView_post_free);
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            mAdView.loadAd(adRequest);


            final Runnable adLoader = new Runnable() {
                @Override
                public void run() {
                    mAdView.loadAd(new AdRequest.Builder().build());
                }
            };


            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    Log.e("onAdClosed", "onAdClosed");
                }

                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    Log.e("onAdFailedToLoad", "onAdFailedToLoad");

                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            try {
                                getActivity().runOnUiThread(adLoader);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 3000);
                }


                @Override
                public void onAdLeftApplication() {
                    super.onAdLeftApplication();
                    Log.e("onAdLeftApplication", "onAdLeftApplication");
                }

                @Override
                public void onAdOpened() {
                    super.onAdOpened();
                    Log.e("onAdOpened", "onAdOpened");
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    Log.e("onAdLoaded", "onAdLoaded");
                }
            });

            adLoader.run();
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            adid = getArguments().getString("AD_ID");
            getAdInfo(adid);

        } catch (Exception e) {

        }


        try {
            //dialog.dismiss();

            if (adid == null)//TextUtils.isEmpty(adid)
                e = 0;
            else {

                e = 1;
            }
        } catch (Exception e) {

        }
        initView(v);
        addListeners();


        return v;
    }

    private void getAdInfo(final String uid) {

        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Object doInBackground(Object[] params) {


                Map<String, String> map = new HashMap<String, String>();
                map.put("aid", uid);
                rest = DAO.getJsonFromUrl(Config.getAdInfo, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {

                    if (rest.getInt("success") > 0) {

                        JSONArray jsonArray = rest.getJSONArray("advertis");
                        //Log.e("JsONArray", jsonArray.toString());
                        JSONObject jobj = jsonArray.getJSONObject(0);

                        //    Log.e("json Data",jsonArray.getString("title"))

                        defaultloc.setVisibility(View.VISIBLE);
                        address.setText(jobj.getString("nearby"));
                        nearbyname = jobj.getString("nearby");
                        placeid = jobj.getString("placeid");
                        cityname = jobj.getString("city");

                        adddefault.setVisibility(View.GONE);


                        //     title.setText();


                    } else {
                       /* adaptor = new MAdaptor();
                        adaptor.setData(new JSONArray());
                        prod.setAdapter(adaptor);*/
                        Toast.makeText(getActivity(), rest.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);

    }

    private void addListeners() {
        primiumone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    amt = amt + 1200;
                    views = views + 2500;

                } else {
                    amt = amt - 1200;
                    views = views - 2500;
                }
                totalAmout();
            }
        });
        primiumtwo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    amt = amt + 2500;
                    views = views + 5000;
                } else {
                    amt = amt - 2500;
                    views = views - 5000;
                }
                totalAmout();
            }
        });
        primiumthree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    amt = amt + 5000;
                    views = views + 10000;
                } else {
                    amt = amt - 5000;
                    views = views - 10000;
                }
                totalAmout();
            }
        });

        adddefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), LocationMap.class), 101);
            }
        });
        otherLocbtnOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), LocationMap.class), 102);
            }
        });

        otherLocbtnTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), LocationMap.class), 103);
            }
        });


        otherLocRemoveOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otherLocOne.setVisibility(View.GONE);
                otherLocbtnOne.setVisibility(View.VISIBLE);
                amt = amt - 200;
                totalAmout();
                /*if (otherLocbtnTwo.getVisibility() == View.VISIBLE) {
                    paymentres.setVisibility(View.GONE);
                    submittext.setText("Submit Ad");
                } else {
                    submittext.setText("Pay Amount & Submit Ad");
                    paymentres.setText("Total Amount: Rs 200/- for adding 1 extra location for your advertisement.");
                }*/
            }
        });
        otherLocRemoveTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otherLocTwo.setVisibility(View.GONE);
                otherLocbtnTwo.setVisibility(View.VISIBLE);
                amt = amt - 200;
                totalAmout();
               /* if (otherLocbtnOne.getVisibility() == View.VISIBLE) {
                    paymentres.setVisibility(View.GONE);
                    submittext.setText("Submit Ad");
                } else {
                    submittext.setText("Pay Amount & Submit Ad");
                    paymentres.setText("Total Amount: Rs 200/- for adding 1 extra location for your advertisement.");
                }*/
            }
        });


        submitAdd.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {


                                             String sprice = price.getText().toString().trim();
                                             data.put("price", sprice);
                                             Calendar c = Calendar.getInstance();
                                             SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
                                             String cdate = df.format(c.getTime());
                                             final String imagename = "classified" + cdate;
                                             data.put("imagename", imagename);
                                             data.put("imgcount", streams.size() + "");
                                             result.setVisibility(View.GONE);

                                             if (s_male.isChecked() && !s_female.isChecked()) {
                                                 s_sex = "MALE";
                                                 // Toast.makeText(getActivity(),s_sex,Toast.LENGTH_LONG).show();
                                             } else if (s_female.isChecked() && !s_male.isChecked()) {
                                                 s_sex = "FEMALE";
                                                 //  Toast.makeText(getActivity(),s_sex,Toast.LENGTH_LONG).show();

                                             }


                                             if (s_male.isChecked() && s_female.isChecked()) {
                                                 s_sex = "BOTH";
                                                 //  Toast.makeText(getActivity(),s_sex,Toast.LENGTH_LONG).show();
                                             }

                                             if (TextUtils.isEmpty(sprice)) {
                                                 result.setText("please add prise");
                                                 result.setVisibility(View.VISIBLE);
                                             } else if (TextUtils.isEmpty(cityname)) {
                                                 result.setText("please select proper location");
                                                 result.setVisibility(View.VISIBLE);
                                             } else if (TextUtils.isEmpty(placeid)) {
                                                 result.setText("please select proper location");
                                                 result.setVisibility(View.VISIBLE);
                                             } else if (TextUtils.isEmpty(nearbyname)) {
                                                 result.setText("please select proper location");
                                                 result.setVisibility(View.VISIBLE);
                                             } else if (TextUtils.isEmpty(address.getText().toString())) {
                                                 result.setText("please add location");
                                                 result.setVisibility(View.VISIBLE);
                                             } /*else if (primiumone.isChecked() == false && primiumtwo.isChecked() == false && primiumthree.isChecked() == false) {
                                                 result.setText("please select primium plan");
                                                 result.setVisibility(View.VISIBLE);
                                             }*/ else if (adid == null && amt != 0) {//TextUtils.isEmpty(adid)
                                                 if (amt == 200 || amt == 400) {
                                                     data.put("default_view", "10000");
                                                 } else {
                                                     data.put("default_view", views + "");
                                                 }


                                                 data.put("city", cityname);
                                                 data.put("placeid", placeid);
                                                 data.put("nearby", nearbyname);

                                                 data.put("sex", s_sex);
                                                 data.put("type", 0 + "");
                                                 data.put("amt", amt + "");
                                                 data.put("add_id", adid);

                                                 data.put("streams", streams.toString());

                                                 JSONObject jobj1 = new JSONObject(data);
                                                 Bundle b = new Bundle();
                                                 PaymentFragment obj = new PaymentFragment();
                                                 b.putString("submit_ad", jobj1.toString());
                                                 b.putString("p", "1");
                                                 b.putString("e", e + "");
                                                 obj.setArguments(b);
                                                 obj.setImages(streams);
                                                 getActivity().getSupportFragmentManager().beginTransaction()
                                                         .add(R.id.container, obj, "pf1").addToBackStack("pf1").commit();


                                                 // submiPost();

                                             } else if (amt == 0 && adid == null) {

                                                 submiPost();

                                             } else {

                                                 //Log.e("UpdateAD", PostFreeAddOne.add_id);
                                                 updateAd(adid);
                                             }

                                         }
                                     }

        );


    }

    public void totalAmout() {

        paymentres.setVisibility(View.VISIBLE);
        paymentres.setText("Total Amount: Rs " + amt + "/-.");
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {

            switch (requestCode) {
                case 101:
                    defaultloc.setVisibility(View.VISIBLE);
                    address.setText(data.getStringExtra("address"));
                    cityname = data.getStringExtra("locality");
                    placeid = data.getStringExtra("placeid");
                    nearbyname = data.getStringExtra("short");
                    break;
                case 102:
                    otherLocOne.setVisibility(View.VISIBLE);
                    otherLocbtnOne.setVisibility(View.GONE);
                    paymentres.setVisibility(View.VISIBLE);
                    /*if (otherLocbtnTwo.getVisibility() == View.GONE) {
                        paymentres.setText("Total Amount: Rs 400/- for adding 2 extra location for your advertisement.");

                    } else {
                        paymentres.setText("Total Amount: Rs 200/- for adding 1 extra location for your advertisement.");
                    }*/
                    amt = amt + 200;
                    totalAmout();
                    submittext.setText("Pay Amount & Submit Ad");
                    addressone.setText(data.getStringExtra("address"));
                    break;
                case 103:
                    otherLocTwo.setVisibility(View.VISIBLE);
                    otherLocbtnTwo.setVisibility(View.GONE);
                    paymentres.setVisibility(View.VISIBLE);
                   /* if (otherLocbtnOne.getVisibility() == View.GONE) {
                        paymentres.setText("Total Amount: Rs 400/- for adding 2 extra location for your advertisement.");
                    } else {
                        paymentres.setText("Total Amount: Rs 200/- for adding 1 extra location for your advertisement.");
                    }*/
                    amt = amt + 200;
                    totalAmout();
                    submittext.setText("Pay Amount & Submit Ad");
                    addresstwo.setText(data.getStringExtra("address"));
                    break;
            }


        }

    }


    private void updateAd(final String add_id) {


        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ProgressDialog();
                data.put("city", cityname);
                data.put("placeid", placeid);
                data.put("nearby", nearbyname);

               /* RadioGroup sex_grp = (RadioGroup) getActivity().findViewById(R.id.sex_group);
                int selectedId = sex_grp.getCheckedRadioButtonId();
                String sex = ((RadioButton) getActivity().findViewById(selectedId)).getText().toString();
                */

                data.put("sex", s_sex);
                data.put("type", 0 + "");
                data.put("add_id", add_id);
                try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected Object doInBackground(Object[] params) {
                res = DAO.getJsonFromUrl(Config.updateAd, data);
                try {
                    if (res.getInt("success") > 0) {
                        int j = 0;
                        for (ByteArrayOutputStream s : streams) {
                            byte[] image = s.toByteArray();
                            Map<String, String> d = new HashMap<String, String>();
                            d.put("filename", data.get("imagename") + j + ".jpg");
                            d.put("image", Base64.encodeToString(image, 0));
                            DAO.getJsonFromUrl(Config.Server + "uploadimage.php", d);
                            j++;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                DismissProgress();
                try {
                    if (res.getInt("success") > 0) {
                        Toast.makeText(getActivity(), res.getString("message"), Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    } else {
                        Toast.makeText(getActivity(), res.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);


    }

    private void submiPost() {
        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                ProgressDialog();
                data.put("city", cityname);
                data.put("placeid", placeid);
                data.put("nearby", nearbyname);
                data.put("default_view", "10000");

                data.put("txtid", "");
                data.put("payment_status", "0");
               /* RadioGroup sex_grp = (RadioGroup) getActivity().findViewById(R.id.sex_group);
                int selectedId = sex_grp.getCheckedRadioButtonId();
                String sex = ((RadioButton) getActivity().findViewById(selectedId)).getText().toString();
                */

                data.put("sex", s_sex);
                data.put("type", 0 + "");
                try {
                    JSONObject user = DAO.getUser();
                    data.put("id", user.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected Object doInBackground(Object[] params) {
                //     res = DAO.getJsonFromUrl(Config.Server + "addadvertise.php", data);
                res = DAO.getJsonFromUrl(Config.Server + "storeAddUpdateOne.php", data);
                try {
                    if (res.getInt("success") > 0) {
                        int j = 0;
                        for (ByteArrayOutputStream s : streams) {
                            byte[] image = s.toByteArray();
                            Map<String, String> d = new HashMap<String, String>();
                            d.put("filename", data.get("imagename") + j + ".jpg");
                            d.put("image", Base64.encodeToString(image, 0));
                            DAO.getJsonFromUrl(Config.Server + "uploadimage.php", d);
                            j++;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                DismissProgress();
                try {
                    if (res.getInt("success") > 0) {
                        Toast.makeText(getActivity(), res.getString("message"), Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    } else {
                        Toast.makeText(getActivity(), res.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }


    public void setData(Map<String, String> data) {
        this.data = data;
    }

    public void ProgressDialog() {
        pd.setMessage("While we posting your add.");
        pd.setTitle("Please wait....");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setIndeterminate(true);
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    public void DismissProgress() {
        pd.dismiss();
    }

    public void setImages(ArrayList<ByteArrayOutputStream> streams) {
        this.streams = streams;
    }


}
