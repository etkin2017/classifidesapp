package com.classifides.app.fragment;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.afollestad.easyvideoplayer.EasyVideoProgressCallback;
import com.classifides.app.AdVideoViewActivity;
import com.classifides.app.AddDetail;
import com.classifides.app.BrowseByCategory;
import com.classifides.app.R;
import com.classifides.app.VideoView;
import com.classifides.app.application.Classified;
import com.classifides.app.data.ComplexPreferences;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.model.locations;
import com.classifides.app.services.LocationTressService;
import com.classifides.app.view.MTextView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.squareup.picasso.Picasso;
import com.universalvideoview.UniversalMediaController;
import com.universalvideoview.UniversalVideoView;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;

import static android.media.MediaPlayer.*;


/**
 * A simple {@link Fragment} subclass.
 */
public class Ads_Notification extends Fragment {
    RecyclerView prod;
    String sub_cat;
    MyBroadcast broadcast;
    MAdaptor adaptor;
    locations lt;
    SharedPreferences sh;
    SharedPreferences.Editor editor;

    public Ads_Notification() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_ads__notification, container, false);

        MobileAds.initialize(Classified.getInstance().getApplicationContext(), "ca-app-pub-2758807869387921~3087322491");
        try {
            final AdView mAdView = (AdView) v.findViewById(R.id.adView_noti);
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            mAdView.loadAd(adRequest);

            final Runnable adLoader = new Runnable() {
                @Override
                public void run() {
                    mAdView.loadAd(new AdRequest.Builder().build());
                }
            };


            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    Log.e("onAdClosed", "onAdClosed");
                }

                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    Log.e("onAdFailedToLoad", "onAdFailedToLoad");

                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            try {
                                getActivity().runOnUiThread(adLoader);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 3000);
                }


                @Override
                public void onAdLeftApplication() {
                    super.onAdLeftApplication();
                    Log.e("onAdLeftApplication", "onAdLeftApplication");
                }

                @Override
                public void onAdOpened() {
                    super.onAdOpened();
                    Log.e("onAdOpened", "onAdOpened");
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    Log.e("onAdLoaded", "onAdLoaded");
                }
            });

            adLoader.run();
        } catch (Exception e) {
            e.printStackTrace();
        }


        adaptor = new MAdaptor();
      /*  broadcast = new MyBroadcast();
        IntentFilter filter = new IntentFilter(LocationTressService.name);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        getActivity().registerReceiver(broadcast, filter);


        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcast,
                new IntentFilter(LocationTressService.name));*/
        sh = getActivity().getSharedPreferences("saved_ad", Context.MODE_PRIVATE);
        editor = sh.edit();

        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(getActivity(), "lc", 0);
        lt = complexPreferences.getObject("locs", locations.class);

        prod = (RecyclerView) v.findViewById(R.id.postrecyc);
        prod.setLayoutManager(new LinearLayoutManager(getActivity()));

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        getProducts();
    }

    private void getProducts() {

        new AsyncTask() {
            JSONObject rest = null;
            String city = "";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();


             /*   try {
                    for (int i = 0; i < lt.getLocs().size(); i++) {
                        Log.e("lt" + i, lt.getLocs().get(i).toString());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
*/

                ArrayList<String> locs = new ArrayList<String>();
                if (lt != null) {
                    locs = lt.getLocs();
                    if (locs != null) {
                        if (locs.size() > 0) {
                            for (int i = 0; i < locs.size(); i++)
                                city = city + locs.get(i) + "|";
                        } else {
                            city = "";
                        }
                    }

                } else
                    city = "";

                if (city.length() > 0) {
                    city = city.substring(0, city.length() - 1);
                }


            }

            @Override
            protected Object doInBackground(Object[] params) {
                String filter = "";
                try {
                    SharedPreferences sh = com.classifides.app.application.Classified.getInstance().getSharedPreferences("categories", Context.MODE_PRIVATE);
                    String cats = sh.getString("cat", "");

                    JSONObject obj = new JSONObject(cats);
                    Iterator<String> key = obj.keys();
                    while (key.hasNext()) {
                        String c = key.next();
                        filter = filter + c + ",";
                        //Log.e("filters" + key, filter);
                    }
                    if (!filter.equals("")) {
                        filter = " and a.category in (" + filter.substring(0, filter.length() - 1) + ")";
                        //Log.e("filter",filter);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                try {
                    if (!city.equals("")) {
                        Map<String, String> map = new HashMap<String, String>();
                        map.put("filter", filter);
                        map.put("city", city);
                        rest = DAO.getJsonFromUrl(Config.Server + "advertise.php", map);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
              /*  Map<String, String> map = new HashMap<String, String>();
                map.put("filter", filter);
                map.put("city", city);
                rest = DAO.getJsonFromUrl(Config.Server + "advertise.php", map);*/
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {

                    if (!city.equals("")) {
                        MAdaptor adaptor = new MAdaptor();
                        if ((rest.getInt("success") > 0) && (lt != null)) {// && (lt != null)
                            adaptor.setData(rest.getJSONArray("advertis"));
                            editor.putString("sads", rest.getJSONArray("advertis").toString());
                            editor.commit();
                            prod.setAdapter(adaptor);

                        } else {
                            adaptor = new MAdaptor();
                            adaptor.setData(new JSONArray());
                            prod.setAdapter(adaptor);
                            Toast.makeText(Classified.getInstance(), "Advertise Not Found", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(Classified.getInstance(), "advertise not found", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

  /*  public void clearAnimation() {
        prod.clearAnimation();
    }*/

    private class MAdaptor extends RecyclerView.Adapter<MAdaptor.ViewHolder> {

        private JSONArray data;
        private int lastPosition = -1;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_prod, null);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            try {
                //    holder. video_thumb.(setCallback(EasyVideoCallback));

                //setAnimation(holder.itemView, position);
                holder.main_title.setText(data.getJSONObject(position).getString("title"));
                holder.title.setText(data.getJSONObject(position).getString("title"));
                holder.desc.setText(data.getJSONObject(position).getString("description"));
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");


                String da = data.getJSONObject(position).getString("postTime").substring(0, 10);

                Date d = format.parse(data.getJSONObject(position).getString("postTime"));
                android.text.format.DateFormat df = new android.text.format.DateFormat();

                //  holder.dated.setText("Date: " + df.format("dd-MM-yyyy", d));

                holder.dated.setText("Date: " + da + "");
                holder.cost.setText("Rs " + data.getJSONObject(position).getString("price") + "/-");
                holder.cat.setText("Cat: " + data.getJSONObject(position).getString("category"));
                holder.subcat.setText("Subcat: " + data.getJSONObject(position).getString("subcategory"));
                holder.post_view.setText("Views: " + data.getJSONObject(position).getString("v"));
                holder.main_v.setText("Views: " + data.getJSONObject(position).getString("v"));

                holder.aID.setText(data.getJSONObject(position).getString("add_id"));

                holder.ttyy.setText(data.getJSONObject(position).getString("imageLoc"));
                final int type = data.getJSONObject(position).getInt("type");
                //  holder.video_thumb.setCallback(EasyVideoProgressCallback);


                if (type == 1) {
                    // setImageFromVideo(holder.video_thumb, data.getJSONObject(position).getString("imageLoc"));
                    //  Picasso.with(Classified.getInstance()).load(R.drawable.play).into(holder.postimg);


                 /*   holder.video_thumb.setSource(Uri.parse(data.getJSONObject(position).getString("imageLoc")));
                    holder.video_thumb.setClickable(true);
                    holder.video_thumb.pause();*/
               /*         //  holder.video_thumb.seekTo(1000);
                    holder.video_thumb.setAutoFullscreen(true);
*/

                    holder.mVideoView.setVideoPath(data.getJSONObject(position).getString("imageLoc"));
                    holder.mVideoView.seekTo(15);
                    holder.mVideoView.stopPlayback();
                    holder.mVideoView.pause();


                    holder.video_thumb.setVisibility(View.VISIBLE);
                    holder.bannerimg.setVisibility(View.GONE);
                    holder.bar_desc.setVisibility(View.GONE);
                    holder.main_title.setVisibility(View.VISIBLE);
                    holder.main_bar.setVisibility(View.VISIBLE);
                    holder.bar_v.setVisibility(View.VISIBLE);

                    holder.postimg.setVisibility(View.GONE);
                } else if (type == 2) {
                    holder.bannerimg.setVisibility(View.VISIBLE);
                    holder.postimg.setVisibility(View.GONE);
                    holder.video_thumb.setVisibility(View.GONE);
                    holder.bar_desc.setVisibility(View.GONE);
                    holder.main_title.setVisibility(View.VISIBLE);
                    holder.main_bar.setVisibility(View.VISIBLE);
                    holder.bar_v.setVisibility(View.VISIBLE);
                    //Log.e("Type 2", data.getJSONObject(position).getString("imageLoc"));
                    Picasso.with(Classified.getInstance()).load(data.getJSONObject(position).getString("imageLoc")).into(holder.bannerimg);


                } else {
                    holder.bannerimg.setVisibility(View.GONE);
                    holder.video_thumb.setVisibility(View.GONE);
                    holder.bar_desc.setVisibility(View.VISIBLE);
                    holder.main_title.setVisibility(View.GONE);
                    holder.main_bar.setVisibility(View.GONE);
                    holder.bar_v.setVisibility(View.GONE);
                    holder.postimg.setVisibility(View.VISIBLE);
                    Picasso.with(Classified.getInstance()).load(data.getJSONObject(position).getString("imageLoc") + "0.jpg").into(holder.postimg);
                }


                holder.mVideoView.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {

                        //Log.e("TOuchClick", "TOuchClick");
                        return false;
                    }
                });

                holder.video_thumb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Log.e("mMediaCont_listener", "mMediaCont_listener ERROR");

                        String aid = holder.aID.getText().toString();
                           /* Intent intent = new Intent(getActivity(), AdVideoViewActivity.class);
                            intent.putExtra("aid", aid);
                            intent.putExtra("video_url", holder.ttyy.getText().toString());
                            getActivity().startActivity(intent);*/

                        Dialog ddd = new Dialog(getActivity());
                        ddd.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        ddd.setContentView(R.layout.banner_view);
                        //   mVideoView = (UniversalVideoView) ddd.findViewById(R.id.videoView);


                        UniversalVideoView mVideoView = (UniversalVideoView) ddd.findViewById(R.id.videoView);
                        UniversalMediaController mMediaController = (UniversalMediaController) ddd.findViewById(R.id.media_controller);
                        mVideoView.setMediaController(mMediaController);
                        FrameLayout main_con = (FrameLayout) ddd.findViewById(R.id.video_thumb);
                        LinearLayout ll_baner = (LinearLayout) ddd.findViewById(R.id.ll_baner);
                        ll_baner.setVisibility(View.VISIBLE);
                        main_con.setVisibility(View.VISIBLE);
                        ImageView b = (ImageView) ddd.findViewById(R.id.banner_view);

                        b.setVisibility(View.GONE);
                     /*   WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = ddd.getWindow();
                        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                        window.setAttributes(lp);*/
                        mVideoView.setVideoPath(holder.ttyy.getText().toString());


                        updateView(aid);

                        //   Picasso.with(Classified.getInstance()).load(data.getJSONObject(position).getString("imageLoc")).into(i);
                        ddd.show();

                        holder.aID.setText((Integer.parseInt(holder.aID.getText().toString()) + 1) + "");


                    }
                });


                holder.mMediaController.setOnErrorViewClick(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TextView v = (TextView) view.findViewById(R.id.tx);

                        //Log.e("ONERRORLISTENER", "LISTENER ERROR");

                        String aid = holder.aID.getText().toString();


                        Intent in = new Intent(getActivity(), VideoView.class);
                        in.putExtra("add", aid + "");
                        in.putExtra("banner", false);
                        in.putExtra("path", holder.ttyy.getText().toString());
                        startActivity(in);
                           /* Intent intent = new Intent(getActivity(), AdVideoViewActivity.class);
                            intent.putExtra("aid", aid);
                            intent.putExtra("video_url", holder.ttyy.getText().toString());
                            getActivity().startActivity(intent);*/

                    /*    Dialog ddd = new Dialog(getActivity());
                        ddd.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        ddd.setContentView(R.layout.banner_view);
                        //   mVideoView = (UniversalVideoView) ddd.findViewById(R.id.videoView);


                        UniversalVideoView mVideoView = (UniversalVideoView) ddd.findViewById(R.id.videoView);
                        UniversalMediaController mMediaController = (UniversalMediaController) ddd.findViewById(R.id.media_controller);
                        mMediaController.setOnLoadingView(R.layout.view_for_loading);

                        mVideoView.setMediaController(mMediaController);
                        FrameLayout main_con = (FrameLayout) ddd.findViewById(R.id.video_thumb);
                        LinearLayout ll_baner = (LinearLayout) ddd.findViewById(R.id.ll_baner);
                        ll_baner.setVisibility(View.VISIBLE);
                        main_con.setVisibility(View.VISIBLE);
                        ImageView b = (ImageView) ddd.findViewById(R.id.banner_view);

                        b.setVisibility(View.GONE);
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = ddd.getWindow();
                        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                        window.setAttributes(lp);
                        mVideoView.setVideoPath(holder.ttyy.getText().toString());
                        mVideoView.seekTo(5);
                        mVideoView.start();
                        mVideoView.setVideoViewCallback(new UniversalVideoView.VideoViewCallback() {
                            @Override
                            public void onScaleChange(boolean isFullscreen) {
                            }

                            @Override
                            public void onPause(MediaPlayer mediaPlayer) { // Video pause
                                Log.e("onPause", "onPause UniversalVideoView callback");
                            }

                            @Override
                            public void onStart(MediaPlayer mediaPlayer) { // Video start/resume to play
                                Log.e("onStart", "onStart UniversalVideoView callback");
                       *//* String aid = holder.aID.getText().toString();
                        updateView(aid);*//*


                            }

                            @Override
                            public void onBufferingStart(MediaPlayer mediaPlayer) {// steam start loading
                                Log.e("onBufferingStart", "onBufferingStart UniversalVideoView callback");

                            }

                            @Override
                            public void onBufferingEnd(MediaPlayer mediaPlayer) {// steam end loading
                                Log.e("onBufferingEnd", "onBufferingEnd UniversalVideoView callback");

                            }

                        });


                        updateView(aid);
                        holder.aID.setText((Integer.parseInt(holder.aID.getText().toString()) + 1) + "");

                        //   Picasso.with(Classified.getInstance()).load(data.getJSONObject(position).getString("imageLoc")).into(i);
                        ddd.show();
*/
                    }
                });

                holder.itemView.setTag(data.getJSONObject(position));


                holder.mVideoView.setVideoViewCallback(new UniversalVideoView.VideoViewCallback() {
                    @Override
                    public void onScaleChange(boolean isFullscreen) {
                    }

                    @Override
                    public void onPause(MediaPlayer mediaPlayer) { // Video pause
                        //Log.e("onPause", "onPause UniversalVideoView callback");
                    }

                    @Override
                    public void onStart(MediaPlayer mediaPlayer) { // Video start/resume to play
                        //Log.e("onStart", "onStart UniversalVideoView callback");
                       /* String aid = holder.aID.getText().toString();
                        updateView(aid);*/
/*

                        String aid = holder.aID.getText().toString();
                           */
/* Intent intent = new Intent(getActivity(), AdVideoViewActivity.class);
                            intent.putExtra("aid", aid);
                            intent.putExtra("video_url", holder.ttyy.getText().toString());
                            getActivity().startActivity(intent);*//*


                        Dialog ddd = new Dialog(getActivity());
                        ddd.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        ddd.getWindow().setBackgroundDrawable(getResources().getDrawable(R.color.black));
                        ddd.setContentView(R.layout.banner_view);
                        //   mVideoView = (UniversalVideoView) ddd.findViewById(R.id.videoView);


                        UniversalVideoView mVideoView = (UniversalVideoView) ddd.findViewById(R.id.videoView);
                        UniversalMediaController mMediaController = (UniversalMediaController) ddd.findViewById(R.id.media_controller);
                        mMediaController.setOnLoadingView(R.layout.view_for_loading);
                        mVideoView.setMediaController(mMediaController);
                        FrameLayout main_con = (FrameLayout) ddd.findViewById(R.id.video_thumb);
                        LinearLayout ll_baner = (LinearLayout) ddd.findViewById(R.id.ll_baner);
                        ll_baner.setVisibility(View.VISIBLE);

                        main_con.setVisibility(View.VISIBLE);
                        ImageView b = (ImageView) ddd.findViewById(R.id.banner_view);

                        b.setVisibility(View.GONE);
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = ddd.getWindow();
                        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                        window.setAttributes(lp);
                        mVideoView.setVideoPath(holder.ttyy.getText().toString());
                        mVideoView.start();
                        mVideoView.seekTo(15);
                        mVideoView.setVideoViewCallback(new UniversalVideoView.VideoViewCallback() {
                            @Override
                            public void onScaleChange(boolean isFullscreen) {
                            }

                            @Override
                            public void onPause(MediaPlayer mediaPlayer) { // Video pause
                                Log.e("onPause", "onPause UniversalVideoView callback");
                            }

                            @Override
                            public void onStart(MediaPlayer mediaPlayer) { // Video start/resume to play
                                Log.e("onStart", "onStart UniversalVideoView callback");
                       */
/* String aid = holder.aID.getText().toString();
                        updateView(aid);*//*



                            }

                            @Override
                            public void onBufferingStart(MediaPlayer mediaPlayer) {// steam start loading
                                Log.e("onBufferingStart", "onBufferingStart UniversalVideoView callback");

                            }

                            @Override
                            public void onBufferingEnd(MediaPlayer mediaPlayer) {// steam end loading
                                Log.e("onBufferingEnd", "onBufferingEnd UniversalVideoView callback");

                            }

                        });
*/


                      /*  updateView(aid);
                        holder.aID.setText((Integer.parseInt(holder.aID.getText().toString()) + 1) + "");

                        //   Picasso.with(Classified.getInstance()).load(data.getJSONObject(position).getString("imageLoc")).into(i);
                        ddd.show();*/

                        String aid = holder.aID.getText().toString();


                        Intent in = new Intent(getActivity(), VideoView.class);
                        in.putExtra("add", aid + "");
                        in.putExtra("banner", false);
                        in.putExtra("path", holder.ttyy.getText().toString());
                        startActivity(in);


                    }

                    @Override
                    public void onBufferingStart(MediaPlayer mediaPlayer) {// steam start loading
                        //Log.e("onBufferingStart", "onBufferingStart UniversalVideoView callback");

                    }

                    @Override
                    public void onBufferingEnd(MediaPlayer mediaPlayer) {// steam end loading
                        //Log.e("onBufferingEnd", "onBufferingEnd UniversalVideoView callback");

                    }

                });


                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (type == 1) {
                         /*   String aid = holder.aID.getText().toString();
                           *//* Intent intent = new Intent(getActivity(), AdVideoViewActivity.class);
                            intent.putExtra("aid", aid);
                            intent.putExtra("video_url", holder.ttyy.getText().toString());
                            getActivity().startActivity(intent);*//*

                            Dialog ddd = new Dialog(getActivity());
                            ddd.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            ddd.getWindow().setBackgroundDrawable(getResources().getDrawable(R.color.black));
                            ddd.setContentView(R.layout.banner_view);
                            //   mVideoView = (UniversalVideoView) ddd.findViewById(R.id.videoView);


                            UniversalVideoView mVideoView = (UniversalVideoView) ddd.findViewById(R.id.videoView);
                            UniversalMediaController mMediaController = (UniversalMediaController) ddd.findViewById(R.id.media_controller);
                            mMediaController.setOnLoadingView(R.layout.view_for_loading);
                            mVideoView.setMediaController(mMediaController);
                            FrameLayout main_con = (FrameLayout) ddd.findViewById(R.id.video_thumb);
                            LinearLayout ll_baner = (LinearLayout) ddd.findViewById(R.id.ll_baner);
                            ll_baner.setVisibility(View.VISIBLE);

                            main_con.setVisibility(View.VISIBLE);
                            ImageView b = (ImageView) ddd.findViewById(R.id.banner_view);

                            b.setVisibility(View.GONE);
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            Window window = ddd.getWindow();
                            lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
                            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                            window.setAttributes(lp);
                            mVideoView.setVideoPath(holder.ttyy.getText().toString());
                            mVideoView.start();

                            mVideoView.setVideoViewCallback(new UniversalVideoView.VideoViewCallback() {
                                @Override
                                public void onScaleChange(boolean isFullscreen) {
                                }

                                @Override
                                public void onPause(MediaPlayer mediaPlayer) { // Video pause
                                    Log.e("onPause", "onPause UniversalVideoView callback");
                                }

                                @Override
                                public void onStart(MediaPlayer mediaPlayer) { // Video start/resume to play
                                    Log.e("onStart", "onStart UniversalVideoView callback");
                       *//* String aid = holder.aID.getText().toString();
                        updateView(aid);*//*


                                }

                                @Override
                                public void onBufferingStart(MediaPlayer mediaPlayer) {// steam start loading
                                    Log.e("onBufferingStart", "onBufferingStart UniversalVideoView callback");

                                }

                                @Override
                                public void onBufferingEnd(MediaPlayer mediaPlayer) {// steam end loading
                                    Log.e("onBufferingEnd", "onBufferingEnd UniversalVideoView callback");

                                }

                            });


                            updateView(aid);
                            holder.aID.setText((Integer.parseInt(holder.aID.getText().toString()) + 1) + "");

                            notifyDataSetChanged();

                            //   Picasso.with(Classified.getInstance()).load(data.getJSONObject(position).getString("imageLoc")).into(i);
                            ddd.show();*/


                            String aid = holder.aID.getText().toString();


                            Intent in = new Intent(getActivity(), VideoView.class);
                            in.putExtra("add", aid + "");
                            in.putExtra("banner", false);
                            in.putExtra("path", holder.ttyy.getText().toString());
                            startActivity(in);
                        } else if (type == 2) {

                            String aid = holder.aID.getText().toString();
                            /*  Dialog ddd = new Dialog(getActivity(), R.style.mydialogstyle);
                            ddd.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            ddd.setContentView(R.layout.banner_view);
                            ImageView i = (ImageView) ddd.findViewById(R.id.banner_view);

                            UniversalVideoView mVideoView = (UniversalVideoView) ddd.findViewById(R.id.videoView);
                            UniversalMediaController mMediaController = (UniversalMediaController) ddd.findViewById(R.id.media_controller);
                            mVideoView.setMediaController(mMediaController);
                            FrameLayout main_con = (FrameLayout) ddd.findViewById(R.id.video_thumb);
                            main_con.setVisibility(View.GONE);
                            //   iii.setVisibility(View.GONE);
                            LinearLayout ll_baner = (LinearLayout) ddd.findViewById(R.id.ll_baner);
                            ll_baner.setVisibility(View.GONE);

                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            Window window = ddd.getWindow();
                            lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
                            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                            window.setAttributes(lp);
                            updateView(aid);

                            holder.aID.setText((Integer.parseInt(holder.aID.getText().toString()) + 1) + "");

                            Picasso.with(Classified.getInstance()).load(holder.ttyy.getText().toString()).into(i);
                            ddd.show();*/


                            Intent in = new Intent(getActivity(), VideoView.class);
                            in.putExtra("add", aid + "");
                            in.putExtra("banner", true);
                            in.putExtra("path", holder.ttyy.getText().toString());
                            startActivity(in);

                        } else {
                            JSONObject obj = (JSONObject) v.getTag();
                            Intent in = new Intent(getActivity(), AddDetail.class);

                            Log.e("add", obj.toString());

                            in.putExtra("add", obj.toString());
                            startActivity(in);
                        }
                        notifyDataSetChanged();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }


        }


        public void refreshitem() {


        }


        public void onBack() {
            notifyDataSetChanged();
        }

        public void forwardSong(UniversalVideoView mPlayer) {
            if (mPlayer != null) {
                int currentPosition = mPlayer.getCurrentPosition();
                if (currentPosition + 10 <= mPlayer.getDuration()) {
                    mPlayer.seekTo(currentPosition + 10);
                } else {
                    mPlayer.seekTo(mPlayer.getDuration());
                }
            }
        }


        private void setImageFromVideo(final JCVideoPlayerStandard postimg, final String imageLoc) {
            new AsyncTask<Bitmap, Integer, Bitmap>() {
                @Override
                protected Bitmap doInBackground(Bitmap... params) {


                   /* FFmpegMediaMetadataRetriever mmr = new FFmpegMediaMetadataRetriever();
                    mmr.setDataSource(imageLoc);
                    mmr.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_ALBUM);
                    mmr.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_ARTIST);
                    Bitmap b = mmr.getFrameAtTime(1000000, FFmpegMediaMetadataRetriever.OPTION_CLOSEST); // frame at 2 seconds
                    // byte[] artwork = mmr.getEmbeddedPicture();

                    mmr.release();
                    return b;*/
                    return null;
                }

                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    super.onPostExecute(bitmap);
                    postimg.thumbImageView.setImageBitmap(bitmap);
                    //   setImageBitmap(bitmap);
                }
            }.execute(null, null, null);
        }

        @Override
        public int getItemCount() {
            return data.length();
        }

        public void setData(JSONArray data) {
            this.data = data;
            this.notifyDataSetChanged();
        }


        @Override
        public void onViewDetachedFromWindow(ViewHolder holder) {
            super.onViewDetachedFromWindow(holder);
            //clearAnimation();
        }

        public void updateView(final String aid) {

            final Map<String, String> d = new HashMap<String, String>();

            new AsyncTask() {
                JSONObject res = null;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();

                    int id = 0;
                    d.put("add_id", aid);
                    //    data.put("uid",uid);

                    try {
                        JSONObject user = DAO.getUser();
                        d.put("id", user.getInt("id") + "");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                protected Object doInBackground(Object[] params) {
                    res = DAO.getJsonFromUrl(Config.updateView, d);

                    return null;
                }

                @Override
                protected void onPostExecute(Object o) {
                    super.onPostExecute(o);
                    //DismissProgress();
                    try {
                        if (res.getInt("success") > 0) {


                            //  Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_SHORT).show();
                            //  getActivity().finish();


                        } else {
                            //  Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }.execute(null, null, null);
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            MTextView title, desc, dated, cost, cat, subcat, main_title, main_v;
            ImageView postimg;

            TextView ttyy, aID;

            LinearLayout bar_desc, bar_v, main_bar;

            FrameLayout video_thumb;
            ImageView bannerimg;
            TextView post_view;
            CardView video_click;

            FrameLayout frame_video_container;
            UniversalVideoView mVideoView;
            UniversalMediaController mMediaController;

            public ViewHolder(View itemView) {
                super(itemView);
                //video_click = (CardView) itemView.findViewById(R.id.video_click);
                ttyy = (TextView) itemView.findViewById(R.id.ttyy);
                video_thumb = (FrameLayout) itemView.findViewById(R.id.video_thumb);
                //      frame_video_container = (FrameLayout) itemView.findViewById(R.id.frame_video_container);
                //  assert video_thumb != null;
                aID = (TextView) itemView.findViewById(R.id.aID);

                mVideoView = (UniversalVideoView) itemView.findViewById(R.id.videoView);
                mMediaController = (UniversalMediaController) itemView.findViewById(R.id.media_controller);
                mMediaController.setOnLoadingView(R.layout.view_for_loading);
                mMediaController.setOnErrorView(R.layout.view_for_error);
                //   mMediaController.showError();
                mVideoView.setMediaController(mMediaController);


                // video_thumb = (VideoView) itemView.findViewById(R.id.video_thumb);
                bannerimg = (ImageView) itemView.findViewById(R.id.bannerimg);
                main_title = (MTextView) itemView.findViewById(R.id.main_title);
                main_v = (MTextView) itemView.findViewById(R.id.main_v);
                bar_desc = (LinearLayout) itemView.findViewById(R.id.desc_bar);
                bar_v = (LinearLayout) itemView.findViewById(R.id.bar_v);
                main_bar = (LinearLayout) itemView.findViewById(R.id.bar_title);
                title = (MTextView) itemView.findViewById(R.id.ptitle);
                desc = (MTextView) itemView.findViewById(R.id.description);
                dated = (MTextView) itemView.findViewById(R.id.dated);
                cost = (MTextView) itemView.findViewById(R.id.cost);
                cat = (MTextView) itemView.findViewById(R.id.cat);
                subcat = (MTextView) itemView.findViewById(R.id.subcat);
                postimg = (ImageView) itemView.findViewById(R.id.prodimg);
                post_view = (TextView) itemView.findViewById(R.id.post_view);
            }
        }

       /* private void setAnimation(View viewToAnimate, int position) {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition) {
                Animation animation = AnimationUtils.loadAnimation(getActivity(),
                        (position > lastPosition) ? R.anim.up_from_bottom
                                : R.anim.down_from_top);
                viewToAnimate.startAnimation(animation);
                lastPosition = position;

            }
        }
*/

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        //  LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcast);

    }

    public class MyBroadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            String jsonArray = intent.getStringExtra("array");
            try {
                if (jsonArray.equals("null")) {
                    JSONArray array = new JSONArray(jsonArray);
                    adaptor.setData(array);
                    prod.setAdapter(adaptor);
                } else {
                    adaptor.setData(new JSONArray());
                    prod.setAdapter(adaptor);
                }

                try {
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(getActivity(), notification);
                    Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                    // Vibrate for 500 milliseconds

                    v.vibrate(1000);
                    r.play();
/*
                JSONObject j = new JSONObject(DAO.getNotification());
                JSONArray ar = new JSONArray();
                ar.put(bd.getString("msg"));
                ar.put(bd.getString("title"));
                ar.put(bd.getString("link"));
                ar.put(true);
                j.put(bd.getString("key"), ar);
                DAO.storeNotification(j.toString());*/



/*


                Log.e("badge_data1", badge_data + "");

                //  if (Config.noti_click == 0) {

                if ((badge_data + "").equals("") && (badge_data + "").equals("null") && badge_data == 0) {
                    badge.hide();
                } else {
                    badge.setText(badge_data + "");
                    badge.show();
                }

*/

                    // }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {

            }

        }
    }


}
