package com.classifides.app.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.classifides.app.R;
import com.classifides.app.gcm.activity.HomeChatActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class Messanger extends Fragment {


    public Messanger() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_messanger, container, false);

      //  startActivity(new Intent(getActivity(), HomeChatActivity.class));

        return v;
    }

}
