package com.classifides.app.fragment;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.classifides.app.R;
import com.classifides.app.application.*;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.data.ImageLoadingUtils;
import com.classifides.app.view.MTextView;
import com.google.android.gms.vision.text.Text;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostFreeAddOne extends Fragment {

    JSONObject cats = null;
    ArrayList<String> categories = new ArrayList<String>();
    AppCompatSpinner catSpin, subCatSpin, discount, cashback, discountysno;
    EditText title, desc, website;
    LinearLayout imgs;
    HorizontalScrollView img_container;
    ImageView gallary, camera;
    LinearLayout next;
    ArrayList<ByteArrayOutputStream> streams;
    MTextView result;

    ImageView imageone;
    String add_id;

    Intent i;

    public PostFreeAddOne() {
        // Required empty public constructors
    }


    private void intiView(View v) {
        catSpin = (AppCompatSpinner) v.findViewById(R.id.catspin);
        subCatSpin = (AppCompatSpinner) v.findViewById(R.id.subcatspin);
        discount = (AppCompatSpinner) v.findViewById(R.id.discount);
        discount.setVisibility(View.GONE);
        discountysno = (AppCompatSpinner) v.findViewById(R.id.discountyesno);
        cashback = (AppCompatSpinner) v.findViewById(R.id.cashback);
        title = (EditText) v.findViewById(R.id.et_title);
        desc = (EditText) v.findViewById(R.id.et_desc);
        website = (EditText) v.findViewById(R.id.et_website);

        img_container = (HorizontalScrollView) v.findViewById(R.id.img_container);
        gallary = (ImageView) v.findViewById(R.id.gallry);
        camera = (ImageView) v.findViewById(R.id.camera);
        imgs = (LinearLayout) v.findViewById(R.id.imgs);
        next = (LinearLayout) v.findViewById(R.id.postnext);
        result = (MTextView) v.findViewById(R.id.result);
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Light.ttf");
        title.setTypeface(face);
        desc.setTypeface(face);
        website.setTypeface(face);
        getCategories();

        imageone = (ImageView) v.findViewById(R.id.imageone);

        streams = new ArrayList<ByteArrayOutputStream>();


        ArrayList<String> ass = new ArrayList<>();
        ass.add("No");
        ass.add("Yes");

        MySpinnerAdapter ddis = new MySpinnerAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, ass);
        cashback.setAdapter(ddis);
        discountysno.setAdapter(ddis);
        cashback.setSelection(0);
        discountysno.setSelection(0);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_post_free_add_one, container, false);

        try {
            add_id = getArguments().getString("add_id");
            //Log.e("add_id_postfree", add_id);
        } catch (Exception e) {

        }

        ///   String add_id = getActivity().getIntent().getStringExtra("add_id");

        intiView(v);
        if ( add_id == null) {//TextUtils.isEmpty(add_id)
            addListener();
            imageone.setVisibility(View.GONE);
        } else {
            img_container.setVisibility(View.VISIBLE);
            //     imgs.setVisibility(View.VISIBLE);
            imageone.setVisibility(View.VISIBLE);

            //Log.e("add_id_empty", add_id);
            editAd(add_id);
            addListener();


        }


        return v;
    }


    private void editAd(final String add_id) {

        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Object doInBackground(Object[] params) {
                int id = 0;
                try {
                    JSONObject user = DAO.getUser();
                    id = user.getInt("id");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Map<String, String> map = new HashMap<String, String>();
                map.put("add_id", add_id);
                rest = DAO.getJsonFromUrl(Config.selectOneAd, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {

                    if (rest.getInt("success") > 0) {

                        JSONArray jsonArray = rest.getJSONArray("user");
                        //Log.e("JsONArray", jsonArray.toString());
                        JSONObject jobj = jsonArray.getJSONObject(0);

                        //Log.e("Jobj", jobj.toString());
                        //Log.e("tittle", jobj.getString("title"));

                        //    Log.e("json Data",jsonArray.getString("title"))


                        title.setText(jobj.getString("title"));
                        try {
                            desc.setText(jobj.getString("description"));
                        } catch (Exception ex) {

                        }
                        img_container.setVisibility(View.VISIBLE);
                        imgs.setVisibility(View.VISIBLE);
                        imageone.setVisibility(View.VISIBLE);
                        Picasso.with(getActivity()).load(jobj.getString("imageLoc") + "0.jpg").into(imageone);

                        String catg = jobj.getString("category");
                        String subCatg = jobj.getString("subcategory");

                        catSpin.setSelection(retrieveAllItems(catSpin, catg));
                        subCatSpin.setSelection(retrieveAllItems(subCatSpin, subCatg));


                        //     title.setText();


                    } else {
                       /* adaptor = new MAdaptor();
                        adaptor.setData(new JSONArray());
                        prod.setAdapter(adaptor);*/
                        Toast.makeText(getActivity(), rest.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);


    }


    private void addListener() {

        discountysno.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    ArrayList<String> as = new ArrayList<>();
                    as.add("No");
                    for (int i = 0; i < 101; i++) {
                        as.add(i + "%");
                    }
                    MySpinnerAdapter dis = new MySpinnerAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, as);
                    discount.setAdapter(dis);
                    discount.setSelection(1);
                    discount.setVisibility(View.VISIBLE);
                    discountysno.setVisibility(View.GONE);


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        discount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    discount.setVisibility(View.GONE);
                    discountysno.setVisibility(View.VISIBLE);
                    discountysno.setSelection(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        catSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {

                    JSONObject subcat = cats.getJSONObject(categories.get(position));
                    ArrayList<String> scat = new ArrayList<String>();
                    Iterator<String> sss = subcat.keys();
                    while (sss.hasNext()) {
                        scat.add(sss.next());

                    }
                    MySpinnerAdapter subccc = new MySpinnerAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, scat);
                    subCatSpin.setAdapter(subccc);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        gallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImageFromGalary();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gatherInformation();

            }


        });
    }

    private void gatherInformation() {
        String stitle = title.getText().toString().trim();
        String sdesc = desc.getText().toString().trim();
        String cat = String.valueOf(catSpin.getSelectedItem());
        String subcat = String.valueOf(subCatSpin.getSelectedItem());
        String category = "0";
        String webs = website.getText().toString().trim();
        try {
            category = cats.getJSONObject(cat).getString(subcat);
        } catch (JSONException e) {
            e.printStackTrace();
            category = "0";
        }
        String sdiscount = String.valueOf(discount.getSelectedItem());
        String cashDiscount = String.valueOf(cashback.getSelectedItem());
        Map<String, String> map = new HashMap<>();
        map.put("title", stitle);
        map.put("description", sdesc);
        map.put("category", category);
        map.put("discount", sdiscount);

        //Log.e("discount",sdiscount+"");

        map.put("cashdiscount", cashDiscount);

        //Log.e("cashdiscount",cashDiscount+"");

        map.put("website", webs);
        result.setVisibility(View.GONE);
        if (TextUtils.isEmpty(stitle)) {
            result.setText("Please add the title");
            result.setVisibility(View.VISIBLE);
            return;
        }
        if (TextUtils.isEmpty(sdesc)) {
            result.setText("Please add the description");
            result.setVisibility(View.VISIBLE);
            return;
        }


        for (int i = 0; i < imgs.getChildCount(); i++) {
            View v = imgs.getChildAt(i);
            if (v instanceof ImageView) {
                ImageView imageView = (ImageView) v;
                ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
                try {
                    ((BitmapDrawable) imageView.getDrawable()).getBitmap().compress(Bitmap.CompressFormat.PNG, 100, stream1);
                } catch (Exception e) {
                    //   Toast.makeText(getActivity(),"Please Select Image",Toast.LENGTH_LONG).show();
                }
                streams.add(stream1);

            }
        }

        if (streams.size() == 0) {
            result.setText("Please add atleast one image");
            result.setVisibility(View.VISIBLE);
            return;
        }


        if (add_id == null) {//TextUtils.isEmpty(add_id)

            PostFreeAddTwo obj = new PostFreeAddTwo();
            obj.setData(map);
            obj.setImages(streams);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, obj, "ps2").addToBackStack("ps2").commit();
        } else {


            PostFreeAddTwo obj = new PostFreeAddTwo();
            obj.setData(map);
            obj.setImages(streams);

            Bundle b = new Bundle();
            b.putString("AD_ID", add_id);
            obj.setArguments(b);

            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, obj, "ps2").addToBackStack("ps2").commit();

        }


    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getCategories();
    }

    private void getCategories() {
        new AsyncTask() {


            @Override
            protected Object doInBackground(Object[] params) {
                Map<String, String> map = new HashMap<String, String>();
                map.put("abc", "abc");
                cats = DAO.getJsonFromUrl(Config.Server + "categories.php", map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    if (cats.getInt("success") > 0) {
                        cats = cats.getJSONObject("categories");
                        Iterator<String> category = cats.keys();
                        while (category.hasNext()) {
                            categories.add(category.next());
                        }

                        MySpinnerAdapter catAdp = new MySpinnerAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, categories);
                        catSpin.setAdapter(catAdp);
                        ;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }


    public void pickImageFromGalary() {

        if (imgs.getChildCount() < 4) {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, 1);
        } else {
            Toast.makeText(getActivity(), "You can only add 4 images", Toast.LENGTH_SHORT).show();
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK && requestCode == 1) {
            try {
                img_container.setVisibility(View.VISIBLE);
                Uri selectedImage = data.getData();
                InputStream imageStream = null;
                imageStream = getActivity().getContentResolver().openInputStream(selectedImage);
                Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                ImageView imageView = new ImageView(getActivity());

                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imgs.addView(imageView);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(300, 300);
                lp.setMargins(5, 5, 5, 5);
                imageView.setBackground(getActivity().getResources().getDrawable(R.drawable.edittext_back));
                imageView.setLayoutParams(lp);
                // Picasso.with(getActivity()).load(data.getDataString()).resize(100, 100).into(imageView);
                imageView.setImageBitmap(yourSelectedImage);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //imgs.removeViewAt(0);

                    }
                });

                // new ImageCompressionAsyncTask(true).execute(data.getDataString());

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
    }


    private class MySpinnerAdapter extends ArrayAdapter<String> {
        // Initialise custom font, for example:
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Light.ttf");

        // (In reality I used a manager which caches the Typeface objects)
        // Typeface font = FontManager.getInstance().getFont(getContext(), BLAMBOT);

        private MySpinnerAdapter(Context context, int resource, List<String> items) {
            super(context, resource, items);
        }


        // Affects default (closed) state of the spinner
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getView(position, convertView, parent);
            view.setTypeface(font);
            view.setTextSize(16);
            return view;
        }

        // Affects opened state of the spinner
        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getDropDownView(position, convertView, parent);
            view.setTypeface(font);
            return view;
        }
    }


    private class ImageCompressionAsyncTask extends AsyncTask<String, Void, String> {
        private boolean fromGallery;
        private ImageLoadingUtils utils;
        Bitmap scaledBitmap = null;
        private String filename;

        public ImageCompressionAsyncTask(boolean fromGallery) {
            this.fromGallery = fromGallery;
        }

        @Override
        protected String doInBackground(String... params) {
            utils = new ImageLoadingUtils(com.classifides.app.application.Classified.getInstance());
            String filePath = compressImage(params[0]);
            return filePath;
        }

        public String compressImage(String imageUri) {

            String filePath = getRealPathFromURI(imageUri);


            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;
            float maxHeight = 816.0f;
            float maxWidth = 612.0f;
            float imgRatio = actualWidth / actualHeight;
            float maxRatio = maxWidth / maxHeight;

            if (actualHeight > maxHeight || actualWidth > maxWidth) {
                if (imgRatio < maxRatio) {
                    imgRatio = maxHeight / actualHeight;
                    actualWidth = (int) (imgRatio * actualWidth);
                    actualHeight = (int) maxHeight;
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth;
                    actualHeight = (int) (imgRatio * actualHeight);
                    actualWidth = (int) maxWidth;
                } else {
                    actualHeight = (int) maxHeight;
                    actualWidth = (int) maxWidth;

                }
            }

            options.inSampleSize = utils.calculateInSampleSize(options, actualWidth, actualHeight);
            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inTempStorage = new byte[16 * 1024];

            try {
                bmp = BitmapFactory.decodeFile(filePath, options);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();

            }
            try {
                scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();
            }

            float ratioX = actualWidth / (float) options.outWidth;
            float ratioY = actualHeight / (float) options.outHeight;
            float middleX = actualWidth / 2.0f;
            float middleY = actualHeight / 2.0f;

            Matrix scaleMatrix = new Matrix();
            scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

            Canvas canvas = new Canvas(scaledBitmap);
            canvas.setMatrix(scaleMatrix);
            canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));


            ExifInterface exif;
            try {
                exif = new ExifInterface(filePath);

                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
                //Log.d("EXIF", "Exif: " + orientation);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90);
                    //Log.d("EXIF", "Exif: " + orientation);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                    //Log.d("EXIF", "Exif: " + orientation);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                    //Log.d("EXIF", "Exif: " + orientation);
                }
                scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            FileOutputStream out = null;
            filename = getFilename();


            return filename;

        }

        private String getRealPathFromURI(String contentURI) {
            Uri contentUri = Uri.parse(contentURI);
            Cursor cursor = com.classifides.app.application.Classified.getInstance().getContentResolver().query(contentUri, null, null, null, null);
            if (cursor == null) {
                return contentUri.getPath();
            } else {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                return cursor.getString(idx);
            }
        }

        public String getFilename() {
            File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
            if (!file.exists()) {
                file.mkdirs();
            }
            String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
            return uriSting;

        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {

                //  bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),Uri.parse("/storage/emulated/0/MyFolder/Images/IMG_20151019_200801010.jpg"));
                // cropImageView.setImageBitmap(scaledBitmap);

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    public int retrieveAllItems(AppCompatSpinner theSpinner, String s) {
        int index = 0;

        try {
            for (int i = 0; i < 50; i++) {

                if (theSpinner.getItemAtPosition(i).equals(s)) {
                    index = i;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return index;
    }

    public class CategoryOne {

        private Integer id;

        private String name;

        /**
         * Getters and Setters
         **/

        @Override
        public String toString() {
            return name;
        }
    }


}
