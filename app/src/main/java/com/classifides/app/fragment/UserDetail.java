package com.classifides.app.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.classifides.app.R;
import com.classifides.app.application.*;
import com.classifides.app.application.Classified;
import com.classifides.app.data.Blur;
import com.classifides.app.view.MTextView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserDetail extends Fragment {
    ImageView backimage, userimage;
    MTextView topname, name, email, mob;
    LinearLayout editProfile;

    public UserDetail() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_user_detail, container, false);
        initView(v);
        addListener();
        return v;
    }


    private void initView(View v) {
        backimage = (ImageView) v.findViewById(R.id.imageback);

        userimage = (ImageView) v.findViewById(R.id.userimage);

        topname = (MTextView) v.findViewById(R.id.topname);
        name = (MTextView) v.findViewById(R.id.name);
        email = (MTextView) v.findViewById(R.id.email);
        mob = (MTextView) v.findViewById(R.id.mob);
        editProfile = (LinearLayout) v.findViewById(R.id.editProfile);

    }

    private void addListener() {
        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity()
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new EditProfile(), "edit")
                        .addToBackStack("edit")
                        .commit();
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences sh = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        String user = sh.getString("users", "");
        try {
            JSONObject us = new JSONObject(user);
            topname.setText(us.getString("name"));
            name.setText(us.getString("name"));
            email.setText(us.getString("email"));
            mob.setText(us.getString("mobile"));
            Picasso.with(com.classifides.app.application.Classified.getInstance()).load(us.getString("image")).placeholder(R.drawable.user).error(R.drawable.user).transform(new Blur(getActivity(), 80)).into(backimage);
            Picasso.with(Classified.getInstance()).load(us.getString("image")).placeholder(R.drawable.user).error(R.drawable.user).into(userimage);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
