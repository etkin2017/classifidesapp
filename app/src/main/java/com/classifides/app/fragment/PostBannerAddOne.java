package com.classifides.app.fragment;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.CursorLoader;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.classifides.app.PostBannerAdd;
import com.classifides.app.R;
import com.classifides.app.application.*;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.view.MTextView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostBannerAddOne extends Fragment {

    JSONObject cats = null;
    ArrayList<String> categories = new ArrayList<String>();
    AppCompatSpinner catSpin, subCatSpin;

    public static String encoded_string, imagename;

    Map<String, String> map;

    public static Bitmap bm;

    //  ArrayList<ByteArrayOutputStream> streams;

    EditText title;
    LinearLayout imgs;
    RelativeLayout video_container;
    ImageView banner;
    ImageView gallary, camera;
    LinearLayout next;
    // ArrayList<ByteArrayOutputStream> streams;
    MTextView result;
    private String path;

    public static String s_sex;

    String add_id;


    CheckBox s_male, s_female;

    ByteArrayOutputStream stream1;

    private void intiView(View v) {


        title = (EditText) v.findViewById(R.id.et_title);

        video_container = (RelativeLayout) v.findViewById(R.id.img_container);


        //streams = new ArrayList<ByteArrayOutputStream>();

        gallary = (ImageView) v.findViewById(R.id.gallry);
        camera = (ImageView) v.findViewById(R.id.camera);
        imgs = (LinearLayout) v.findViewById(R.id.imgs);
        next = (LinearLayout) v.findViewById(R.id.postnext);
        result = (MTextView) v.findViewById(R.id.result);
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Light.ttf");
        title.setTypeface(face);

        s_male = (CheckBox) v.findViewById(R.id.s_male);
        s_female = (CheckBox) v.findViewById(R.id.s_female);

        map = new HashMap<>();

        getCategories();

        //streams = new ArrayList<ByteArrayOutputStream>();


    }

    public PostBannerAddOne() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_post_banner_add_one, container, false);
        catSpin = (AppCompatSpinner) v.findViewById(R.id.catspin);
        subCatSpin = (AppCompatSpinner) v.findViewById(R.id.subcatspin);
        banner = (ImageView) v.findViewById(R.id.banner);
        intiView(v);
        try {
            add_id = getArguments().getString("add_id");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if ((add_id == null)) {//TextUtils.isEmpty(add_id) | add_id.equals("")|
            addListener();
        } else {
            editAd(add_id);
            addListener();
        }


        return v;
    }


    public int retrieveAllItems(AppCompatSpinner theSpinner, String s) {
        int index = 0;

        try {
            for (int i = 0; i < 50; i++) {

                if (theSpinner.getItemAtPosition(i).equals(s)) {
                    index = i;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return index;
    }


    private void editAd(final String add_id) {

        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Object doInBackground(Object[] params) {
                int id = 0;
                try {
                    JSONObject user = DAO.getUser();
                    id = user.getInt("id");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Map<String, String> map = new HashMap<String, String>();
                map.put("add_id", add_id);
                rest = DAO.getJsonFromUrl(Config.selectOneAd, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {

                    if (rest.getInt("success") > 0) {

                        JSONArray jsonArray = rest.getJSONArray("user");
                        //Log.e("JsONArray", jsonArray.toString());
                        JSONObject jobj = jsonArray.getJSONObject(0);

                        //Log.e("Jobj", jobj.toString());
                        //Log.e("tittle", jobj.getString("title"));

                        //    Log.e("json Data",jsonArray.getString("title"))


                        title.setText(jobj.getString("title"));
                       /* try {
                            desc.setText(jobj.getString("description"));
                        } catch (Exception ex) {

                        }*/
                        video_container.setVisibility(View.VISIBLE);
                        //     imgs.setVisibility(View.VISIBLE);
                        banner.setVisibility(View.VISIBLE);
                        Picasso.with(getActivity()).load(jobj.getString("imageLoc")).into(banner);


                        //     title.setText();


                        String catg = jobj.getString("category");
                        String subCatg = jobj.getString("subcategory");

                        catSpin.setSelection(retrieveAllItems(catSpin, catg));
                        subCatSpin.setSelection(retrieveAllItems(subCatSpin, subCatg));


                    } else {
                       /* adaptor = new MAdaptor();
                        adaptor.setData(new JSONArray());
                        prod.setAdapter(adaptor);*/
                        Toast.makeText(getActivity(), rest.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);


    }

    private void addListener() {
        catSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {

                    JSONObject subcat = cats.getJSONObject(categories.get(position));
                    ArrayList<String> scat = new ArrayList<String>();
                    Iterator<String> sss = subcat.keys();
                    while (sss.hasNext()) {
                        scat.add(sss.next());

                    }
                    MySpinnerAdapter subccc = new MySpinnerAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, scat);
                    subCatSpin.setAdapter(subccc);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        gallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImageFromGalary();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (s_male.isChecked() && !s_female.isChecked()) {

                    s_sex = "MALE";
                    //   Toast.makeText(getActivity(),s_sex,Toast.LENGTH_LONG).show();

                } else if (s_female.isChecked() && !s_male.isChecked()) {

                    s_sex = "FEMALE";
                    //  Toast.makeText(getActivity(),s_sex,Toast.LENGTH_LONG).show();

                }

                if (s_male.isChecked() && s_female.isChecked()) {
                    s_sex = "BOTH";
                    //  Toast.makeText(getActivity(),s_sex,Toast.LENGTH_LONG).show();
                }

                gatherInformation();
                //Toast.makeText(getActivity(), "This page is in under construction", Toast.LENGTH_SHORT).show();

            }


        });
    }

    private void gatherInformation() {
        String stitle = title.getText().toString().trim();
        String sdesc = "";
        String cat = String.valueOf(catSpin.getSelectedItem());
        String subcat = String.valueOf(subCatSpin.getSelectedItem());
        String category = "0";
      /*  RadioGroup sex_grp = (RadioGroup) getActivity().findViewById(R.id.sex_group);
        int selectedId = sex_grp.getCheckedRadioButtonId();
        String sex = ((RadioButton) getActivity().findViewById(selectedId)).getText().toString();*/

        try {
            category = cats.getJSONObject(cat).getString(subcat);
        } catch (JSONException e) {
            e.printStackTrace();
            category = "0";
        }

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        String cdate = df.format(c.getTime());
        imagename = "classified" + cdate + "0.jpg";

        //Log.e("ImageName", imagename);


        map.put("title", stitle);
        map.put("description", "");
        map.put("category", category);
        map.put("discount", "0");
        map.put("cashdiscount", "0");
        map.put("sex", s_sex);
        map.put("imagename", imagename);


        result.setVisibility(View.GONE);
        if (TextUtils.isEmpty(stitle)) {
            result.setText("Please add the title");
            result.setVisibility(View.VISIBLE);
            return;
        }


/*
        for (int i = 0; i < imgs.getChildCount(); i++) {
            View v = imgs.getChildAt(i);
            if (v instanceof ImageView) {
                ImageView imageView = (ImageView) v;
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                ((BitmapDrawable) imageView.getDrawable()).getBitmap().compress(Bitmap.CompressFormat.PNG, 100, stream);
                streams.add(stream);

            }
        }*/

      /*  if (TextUtils.isEmpty(path)) {
            result.setText("Please add the video");
            result.setVisibility(View.VISIBLE);
            return;
       /* }*/
       /* ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
        ((BitmapDrawable) banner.getDrawable()).getBitmap().compress(Bitmap.CompressFormat.PNG, 100, stream1);*/

        try {
            stream1 = new ByteArrayOutputStream();
            ((BitmapDrawable) banner.getDrawable()).getBitmap().compress(Bitmap.CompressFormat.PNG, 100, stream1);
            nextPage();
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Please Select Image", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }        //   ByteArrayOutputStream stream = new ByteArrayOutputStream();
        //  bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);


        //   new Encode_image().execute();

     /*   if (TextUtils.isEmpty(add_id)) {


            PostBannerAddTwo obj = new PostBannerAddTwo();
            obj.setData(map);
            obj.setImages(stream);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, obj, "psv2").addToBackStack("psv2").commit();
        } else {
            PostBannerAddTwo obj = new PostBannerAddTwo();
            obj.setData(map);
            obj.setImages(stream);
            Bundle b = new Bundle();
            b.putString("AD_ID", add_id);
            obj.setArguments(b);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, obj, "psv2").addToBackStack("psv2").commit();
        }*/


    }


    public void nextPage() {

        if ( (add_id == null)) {//TextUtils.isEmpty(add_id)|


            PostBannerAddTwo obj = new PostBannerAddTwo();
            obj.setData(map);
            obj.setImages(stream1);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, obj, "psv2").addToBackStack("psv2").commit();
        } else {
            PostBannerAddTwo obj = new PostBannerAddTwo();
            obj.setData(map);
            obj.setImages(stream1);
            Bundle b = new Bundle();
            b.putString("AD_ID", add_id);
            obj.setArguments(b);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, obj, "psv2").addToBackStack("psv2").commit();
        }


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getCategories();
    }

    private void getCategories() {
        new AsyncTask() {


            @Override
            protected Object doInBackground(Object[] params) {
                Map<String, String> map = new HashMap<String, String>();
                map.put("abc", "abc");
                cats = DAO.getJsonFromUrl(Config.Server + "categories.php", map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    if (cats.getInt("success") > 0) {
                        cats = cats.getJSONObject("categories");
                        Iterator<String> category = cats.keys();
                        while (category.hasNext()) {
                            categories.add(category.next());
                        }

                        MySpinnerAdapter catAdp = new MySpinnerAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, categories);
                        catSpin.setAdapter(catAdp);
                        ;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }


    public void pickImageFromGalary() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 1);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK && requestCode == 1) {
            try {

              /*  Uri selectedImage = data.getData();
                InputStream imageStream = getActivity().getContentResolver().openInputStream(selectedImage);
                Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                *//*String[] projection = {MediaStore.MediaColumns.DATA};

                CursorLoader cursorLoader = new CursorLoader(getActivity(), selectedImage, projection, null, null,
                        null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                path= cursor.getString(column_index);*//*
                video_container.setVisibility(View.VISIBLE);
                banner.setImageBitmap(yourSelectedImage);*/


                video_container.setVisibility(View.VISIBLE);
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                Cursor cursor = getActivity().managedQuery(selectedImageUri, projection, null, null,
                        null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();

                String selectedImagePath = cursor.getString(column_index);


                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(selectedImagePath, options);
                banner.setImageBitmap(bm);


            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    private class MySpinnerAdapter extends ArrayAdapter<String> {
        // Initialise custom font, for example:
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Light.ttf");

        // (In reality I used a manager which caches the Typeface objects)
        // Typeface font = FontManager.getInstance().getFont(getContext(), BLAMBOT);

        private MySpinnerAdapter(Context context, int resource, List<String> items) {
            super(context, resource, items);
        }


        // Affects default (closed) state of the spinner
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getView(position, convertView, parent);
            view.setTypeface(font);
            view.setTextSize(16);
            return view;
        }

        // Affects opened state of the spinner
        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getDropDownView(position, convertView, parent);
            view.setTypeface(font);
            return view;
        }
    }

}
