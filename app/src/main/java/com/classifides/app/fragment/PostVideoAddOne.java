package com.classifides.app.fragment;


import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.CursorLoader;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.classifides.app.R;
import com.classifides.app.data.Config;
import com.classifides.app.data.DAO;
import com.classifides.app.view.MTextView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostVideoAddOne extends Fragment {
    JSONObject cats = null;
    ArrayList<String> categories = new ArrayList<String>();
    AppCompatSpinner catSpin, subCatSpin;
    EditText title;
    LinearLayout imgs;
    RelativeLayout video_container;
    JCVideoPlayer video;
    ImageView gallary, camera;

    String s_sex, add_id;

    LinearLayout next;
    ArrayList<ByteArrayOutputStream> streams;
    MTextView result;
    private String path;

    CheckBox s_male, s_female;

    private void intiView(View v) {
        catSpin = (AppCompatSpinner) v.findViewById(R.id.catspin);
        subCatSpin = (AppCompatSpinner) v.findViewById(R.id.subcatspin);

        s_male = (CheckBox) v.findViewById(R.id.s_male);
        s_female = (CheckBox) v.findViewById(R.id.s_female);

        title = (EditText) v.findViewById(R.id.et_title);

        video_container = (RelativeLayout) v.findViewById(R.id.img_container);
        video = (JCVideoPlayer) v.findViewById(R.id.video);
        gallary = (ImageView) v.findViewById(R.id.gallry);
        camera = (ImageView) v.findViewById(R.id.camera);
        imgs = (LinearLayout) v.findViewById(R.id.imgs);
        next = (LinearLayout) v.findViewById(R.id.postnext);
        result = (MTextView) v.findViewById(R.id.result);
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Light.ttf");
        title.setTypeface(face);

        getCategories();
        streams = new ArrayList<ByteArrayOutputStream>();


    }

    public PostVideoAddOne() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_post_video_add_one, container, false);


        try {
            add_id = getArguments().getString("add_id");
        } catch (Exception e) {

        }
        intiView(v);
        addListener();

        if (add_id == null) {  //TextUtils.isEmpty(add_id)

        } else {

            //Log.e("Uid",add_id);
            editAd(add_id);
        }


        return v;
    }

    private void editAd(final String add_id) {

        new AsyncTask() {
            JSONObject rest = null;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                getCategories();


            }

            @Override
            protected Object doInBackground(Object[] params) {
                int id = 0;
                try {
                    JSONObject user = DAO.getUser();
                    id = user.getInt("id");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Map<String, String> map = new HashMap<String, String>();
                map.put("add_id", add_id);
                rest = DAO.getJsonFromUrl(Config.selectOneAd, map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {

                    if (rest.getInt("success") > 0) {

                        JSONArray jsonArray = rest.getJSONArray("user");
                        //Log.e("JsONArray", jsonArray.toString());
                        JSONObject jobj = jsonArray.getJSONObject(0);

                        //Log.e("Jobj", jobj.toString());
                        //Log.e("tittle", jobj.getString("title"));

                        //    Log.e("json Data",jsonArray.getString("title"))


                        title.setText(jobj.getString("title"));

                        video_container.setVisibility(View.VISIBLE);


                        video.setUp(jobj.getString("imageLoc"), "");


                        String catg = jobj.getString("category");
                        String subCatg = jobj.getString("subcategory");

                        catSpin.setSelection(retrieveAllItems(catSpin, catg));
                        subCatSpin.setSelection(retrieveAllItems(subCatSpin, subCatg));


                        //     title.setText();


                    } else {
                       /* adaptor = new MAdaptor();
                        adaptor.setData(new JSONArray());
                        prod.setAdapter(adaptor);*/
                        Toast.makeText(getActivity(), rest.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);


    }


    public int retrieveAllItems(AppCompatSpinner theSpinner, String s) {
        int index = 0;

        try {
            for (int i = 0; i < 50; i++) {

                if (theSpinner.getItemAtPosition(i).equals(s)) {
                    index = i;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return index;
    }

    private void addListener() {
        catSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {

                    JSONObject subcat = cats.getJSONObject(categories.get(position));
                    ArrayList<String> scat = new ArrayList<String>();
                    Iterator<String> sss = subcat.keys();
                    while (sss.hasNext()) {
                        scat.add(sss.next());

                    }
                    MySpinnerAdapter subccc = new MySpinnerAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, scat);
                    subCatSpin.setAdapter(subccc);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        gallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImageFromGalary();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (s_male.isChecked() && !s_female.isChecked()) {
                    s_sex = "MALE";
                    //   Toast.makeText(getActivity(),s_sex,Toast.LENGTH_LONG).show();
                } else if (s_female.isChecked() && !s_male.isChecked()) {

                    s_sex = "FEMALE";
                    //  Toast.makeText(getActivity(),s_sex,Toast.LENGTH_LONG).show();
                }

                if (s_male.isChecked() && s_female.isChecked()) {

                    s_sex = "BOTH";

                    //  Toast.makeText(getActivity(),s_sex,Toast.LENGTH_LONG).show();
                }
                gatherInformation();
                //Toast.makeText(getActivity(), "This page is in under construction", Toast.LENGTH_SHORT).show();

            }


        });
    }

    private void gatherInformation() {
        String stitle = title.getText().toString().trim();
        String sdesc = "";
        String cat = String.valueOf(catSpin.getSelectedItem());
        String subcat = String.valueOf(subCatSpin.getSelectedItem());
        String category = "0";
     /*   RadioGroup sex_grp = (RadioGroup) getActivity().findViewById(R.id.sex_group);
        int selectedId = sex_grp.getCheckedRadioButtonId();
        String sex = ((RadioButton) getActivity().findViewById(selectedId)).getText().toString();*/

        try {
            category = cats.getJSONObject(cat).getString(subcat);
        } catch (JSONException e) {
            e.printStackTrace();
            category = "0";
        }

        Map<String, String> map = new HashMap<>();
        map.put("title", stitle);
        map.put("description", "");
        map.put("category", category);
        map.put("discount", "0");
        map.put("cashdiscount", "0");
        map.put("sex", s_sex);
        result.setVisibility(View.GONE);
        if (TextUtils.isEmpty(stitle)) {
            result.setText("Please add the title");
            result.setVisibility(View.VISIBLE);
            return;
        }



        /*for (int i = 0; i < imgs.getChildCount(); i++) {
            View v = imgs.getChildAt(i);
            if (v instanceof ImageView) {
                ImageView imageView = (ImageView) v;
                ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
                ((BitmapDrawable) imageView.getDrawable()).getBitmap().compress(Bitmap.CompressFormat.PNG, 100, stream1);
                streams.add(stream1);

            }
        }*/

        if (TextUtils.isEmpty(path)) {
            result.setText("Please add the video");
            result.setVisibility(View.VISIBLE);
            return;
        }

        PostVideoAddTwo obj = new PostVideoAddTwo();

        if (add_id == null) {//TextUtils.isEmpty(add_id)

            obj.setData(map);
            obj.setVideoPath(path);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, obj, "psv2").addToBackStack("psv2").commit();

        } else {

            obj.setData(map);
            obj.setVideoPath(path);

            Bundle b = new Bundle();
            b.putString("AD_ID", add_id);
            obj.setArguments(b);

            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, obj, "psv2").addToBackStack("psv2").commit();
        }


    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getCategories();
    }

    private void getCategories() {
        new AsyncTask() {


            @Override
            protected Object doInBackground(Object[] params) {
                Map<String, String> map = new HashMap<String, String>();
                map.put("abc", "abc");
                cats = DAO.getJsonFromUrl(Config.Server + "categories.php", map);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    if (cats.getInt("success") > 0) {
                        cats = cats.getJSONObject("categories");
                        Iterator<String> category = cats.keys();
                        while (category.hasNext()) {
                            categories.add(category.next());
                        }

                        MySpinnerAdapter catAdp = new MySpinnerAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, categories);
                        catSpin.setAdapter(catAdp);
                        ;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }


    public void pickImageFromGalary() {


        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("video/*");
        startActivityForResult(photoPickerIntent, 1);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK && requestCode == 1) {
            try {

                Uri selectedImage = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};

                CursorLoader cursorLoader = new CursorLoader(getActivity(), selectedImage, projection, null, null,
                        null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                //Log.e("column_index",column_index+"");
                cursor.moveToFirst();
                path = cursor.getString(column_index);
                video_container.setVisibility(View.VISIBLE);
                video.setUp(path, "");


            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    private class MySpinnerAdapter extends ArrayAdapter<String> {
        // Initialise custom font, for example:
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Light.ttf");

        // (In reality I used a manager which caches the Typeface objects)
        // Typeface font = FontManager.getInstance().getFont(getContext(), BLAMBOT);

        private MySpinnerAdapter(Context context, int resource, List<String> items) {
            super(context, resource, items);
        }


        // Affects default (closed) state of the spinner
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getView(position, convertView, parent);
            view.setTypeface(font);
            view.setTextSize(16);
            return view;
        }

        // Affects opened state of the spinner
        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView view = (TextView) super.getDropDownView(position, convertView, parent);
            view.setTypeface(font);
            return view;
        }
    }

}
