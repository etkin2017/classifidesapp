package com.classifides.app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;

import com.classifides.app.fragment.PostBannerAddOne;
import com.classifides.app.fragment.PostVideoAddOne;
import com.classifides.app.view.MTextView;

public class PostBannerAdd extends AppCompatActivity {
    MTextView close;
    String adid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_banner_add);

        try {
             adid = getIntent().getStringExtra("add_id");
            Bundle bundle = new Bundle();
            bundle.putString("add_id", adid);
        }catch (Exception ex){

        }


        initView();
        addListener();
    }
    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Bundle bundle = new Bundle();
        bundle.putString("add_id", adid);

        PostBannerAddOne freeAddOne=new PostBannerAddOne();


        if(TextUtils.isEmpty(adid)){
            getSupportFragmentManager().beginTransaction().add(R.id.container,freeAddOne , "pstbbone").commit();
            close = (MTextView) findViewById(R.id.close);
        }else{

            freeAddOne.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().add(R.id.container,freeAddOne , "pstbbone").commit();
            close = (MTextView) findViewById(R.id.close);
        }


    }

    private void addListener() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
