package com.classifides.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.classifides.app.fragment.BrowseCategory;
import com.classifides.app.services.LocationTressService;
import com.classifides.app.view.MTextView;

public class SplashScreen extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 4500;
    Boolean log;
    Thread splashTread;

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        SharedPreferences sh = getSharedPreferences("log", MODE_PRIVATE);
        log = sh.getBoolean("login", false);
        StartAnimations();

       /* Intent in = new Intent(SplashScreen.this, LocationTressService.class);
        startService(in);*/


       /* new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                //   int result = preferences.getInt("a", 0);
                if (log) {
                    Intent l1 = new Intent(SplashScreen.this, HomeActivity.class);
                    startActivity(l1);
                    finish();
                } else {
                    Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }

        }, SPLASH_TIME_OUT);*/
    }


   /* private void checkPreviousLogin() {
        SharedPreferences sh = getSharedPreferences("log", MODE_PRIVATE);
        Boolean log = sh.getBoolean("login", false);
        if (log) {

            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            //  startActivity(new Intent(LoginActivity.this, BrowseByCategory.class));
            finish();
        }
    }*/


    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        LinearLayout l = (LinearLayout) findViewById(R.id.lin_lay);
        l.clearAnimation();
        l.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        MTextView iv = (MTextView) findViewById(R.id.splash);
        iv.clearAnimation();
        iv.startAnimation(anim);

        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    // Splash screen pause time
                    while (waited < 4500) {
                        sleep(100);
                        waited += 100;
                    }
                    if (log) {
                        Intent l1 = new Intent(SplashScreen.this, BrowseByCategory.class);
                        startActivity(l1);
                        finish();
                    } else {
                        Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    // do nothing
                } finally {
                    SplashScreen.this.finish();
                }

            }
        };
        splashTread.start();

    }

}

